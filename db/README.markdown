Request first for a copy of the production database dump. Then run the
following steps to recreate your local db:

    rake db:drop
    rake db:create
    pg_restore -cOd acc-online_development production.dump
    rake db:migrate
    rake db:test:prepare
    rake db:seed
