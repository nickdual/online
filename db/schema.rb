# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130816051224) do

  create_table "admins", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "role"
    t.string   "cached_slug"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
  end

  add_index "admins", ["cached_slug"], :name => "index_admins_on_cached_slug"
  add_index "admins", ["created_by_id"], :name => "index_admins_on_created_by_id"
  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true
  add_index "admins", ["updated_by_id"], :name => "index_admins_on_updated_by_id"

  create_table "coordinators", :force => true do |t|
    t.integer  "user_id",    :null => false
    t.integer  "group_id",   :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "coordinators", ["group_id", "user_id"], :name => "index_coordinators_on_group_id_and_user_id"
  add_index "coordinators", ["group_id"], :name => "index_coordinators_on_group_id"
  add_index "coordinators", ["user_id", "group_id"], :name => "index_coordinators_on_user_id_and_group_id"
  add_index "coordinators", ["user_id"], :name => "index_coordinators_on_user_id"

  create_table "dragonfly_thumbs", :force => true do |t|
    t.string   "job"
    t.string   "uid"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "dragonfly_thumbs", ["job"], :name => "index_dragonfly_thumbs_on_job"

  create_table "employment_learning_plans", :force => true do |t|
    t.integer  "employment_id"
    t.integer  "learning_plan_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.datetime "due_at"
    t.datetime "completed_at"
    t.string   "status"
    t.datetime "active_at"
  end

  add_index "employment_learning_plans", ["employment_id", "learning_plan_id"], :name => "index_employment_employment_learning_plans"
  add_index "employment_learning_plans", ["employment_id"], :name => "index_employment_learning_plans_on_employment_id"
  add_index "employment_learning_plans", ["learning_plan_id"], :name => "index_employment_learning_plans"

  create_table "employments", :force => true do |t|
    t.integer  "user_id"
    t.integer  "service_id",                           :null => false
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.string   "role",            :default => "staff"
    t.date     "started_at"
    t.date     "ended_at"
    t.string   "employee_number"
    t.integer  "job_title_id"
  end

  add_index "employments", ["service_id", "user_id"], :name => "index_employments_on_service_id_and_user_id"
  add_index "employments", ["user_id", "service_id"], :name => "index_employments_on_user_id_and_service_id"

  create_table "employments_group_sessions", :id => false, :force => true do |t|
    t.integer "employment_id"
    t.integer "group_session_id"
  end

  add_index "employments_group_sessions", ["group_session_id", "employment_id"], :name => "index_employments_group_sessions"

  create_table "group_job_titles", :force => true do |t|
    t.string   "name"
    t.integer  "group_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "group_job_titles", ["group_id"], :name => "index_group_job_categories_on_group_id"

  create_table "group_program_lists", :force => true do |t|
    t.integer  "group_program_id", :null => false
    t.integer  "program_id",       :null => false
    t.integer  "position"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "group_id",         :null => false
  end

  add_index "group_program_lists", ["group_id", "group_program_id"], :name => "index_group_program_lists_on_group_id_and_group_program_id"
  add_index "group_program_lists", ["program_id"], :name => "index_group_program_lists_on_program_id"

  create_table "group_sessions", :force => true do |t|
    t.datetime "viewed_at"
    t.string   "medium"
    t.integer  "service_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "created_by_id"
    t.integer  "program_id"
    t.boolean  "essentials"
    t.boolean  "evidence"
    t.boolean  "extension"
  end

  add_index "group_sessions", ["service_id"], :name => "index_group_sessions_on_service_id"

  create_table "groups", :force => true do |t|
    t.string   "name"
    t.string   "country"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "cached_slug"
    t.integer  "disabled_by_id"
    t.datetime "disabled_at"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
  end

  add_index "groups", ["cached_slug"], :name => "index_groups_on_cached_slug"
  add_index "groups", ["created_by_id"], :name => "index_groups_on_created_by_id"
  add_index "groups", ["disabled_by_id"], :name => "index_groups_on_disabled_by_id"
  add_index "groups", ["updated_by_id"], :name => "index_groups_on_updated_by_id"

  create_table "groups_packages", :id => false, :force => true do |t|
    t.integer "group_id"
    t.integer "package_id"
  end

  add_index "groups_packages", ["group_id", "package_id"], :name => "index_groups_packages_on_group_id_and_package_id"
  add_index "groups_packages", ["package_id", "group_id"], :name => "index_groups_packages_on_package_id_and_group_id"

  create_table "learning_plan_programs", :force => true do |t|
    t.integer "program_id"
    t.integer "learning_plan_id"
    t.boolean "essentials"
    t.boolean "extension"
    t.boolean "evidence"
  end

  add_index "learning_plan_programs", ["program_id", "learning_plan_id"], :name => "index_learning_plans_programs"

  create_table "learning_plans", :force => true do |t|
    t.string   "name"
    t.integer  "service_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "cached_slug"
    t.text     "description"
    t.integer  "days_till_due"
    t.datetime "active_at"
  end

  add_index "learning_plans", ["cached_slug"], :name => "index_training_plans_on_cached_slug"
  add_index "learning_plans", ["service_id"], :name => "index_learning_plans_on_service_id"

  create_table "learning_records", :force => true do |t|
    t.datetime "viewed_at"
    t.integer  "program_id",                             :null => false
    t.integer  "employment_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "medium",           :default => "Online"
    t.integer  "group_session_id"
  end

  add_index "learning_records", ["employment_id"], :name => "index_learning_records_on_employment_id"
  add_index "learning_records", ["program_id"], :name => "index_learning_records_on_program_id"

  create_table "learning_resource_downloads", :force => true do |t|
    t.integer  "employment_id"
    t.integer  "learning_resource_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  add_index "learning_resource_downloads", ["employment_id"], :name => "index_learning_resource_downloads_on_employment_id"
  add_index "learning_resource_downloads", ["learning_resource_id"], :name => "index_learning_resource_downloads_on_learning_resource_id"

  create_table "learning_resources", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.integer  "program_id",                           :null => false
    t.string   "cached_slug"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.string   "resource_uid"
    t.string   "resource_name"
    t.boolean  "coordinators_only", :default => false
  end

  add_index "learning_resources", ["cached_slug"], :name => "index_learning_resources_on_cached_slug"
  add_index "learning_resources", ["created_by_id"], :name => "index_learning_resources_on_created_by_id"
  add_index "learning_resources", ["program_id"], :name => "index_learning_resources_on_program_id"
  add_index "learning_resources", ["updated_by_id"], :name => "index_learning_resources_on_updated_by_id"

  create_table "online_program_assessments", :force => true do |t|
    t.string   "name"
    t.integer  "program_id"
    t.integer  "time_allowed"
    t.string   "assessment_type", :default => "Essentials"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.integer  "pass_mark",       :default => 50,           :null => false
    t.datetime "activated_at"
  end

  create_table "package_program_lists", :force => true do |t|
    t.integer  "program_id",         :null => false
    t.integer  "package_program_id", :null => false
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "position"
  end

  add_index "package_program_lists", ["package_program_id"], :name => "index_package_program_lists_on_package_program_id"
  add_index "package_program_lists", ["program_id"], :name => "index_package_program_lists_on_program_id"

  create_table "package_programs", :force => true do |t|
    t.integer  "program_id", :null => false
    t.integer  "package_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "position"
  end

  add_index "package_programs", ["package_id", "program_id"], :name => "index_package_programs_on_package_id_and_program_id"
  add_index "package_programs", ["package_id"], :name => "index_package_programs_on_package_id"
  add_index "package_programs", ["program_id"], :name => "index_package_programs_on_program_id"

  create_table "packages", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.string   "cached_slug"
  end

  add_index "packages", ["cached_slug"], :name => "index_packages_on_cached_slug"
  add_index "packages", ["created_by_id"], :name => "index_packages_on_created_by_id"
  add_index "packages", ["updated_by_id"], :name => "index_packages_on_updated_by_id"

  create_table "packages_services", :id => false, :force => true do |t|
    t.integer "service_id"
    t.integer "package_id"
  end

  add_index "packages_services", ["service_id", "package_id"], :name => "index_packages_services_on_service_id_and_package_id"

  create_table "program_assessment_fill_gap_answers", :force => true do |t|
    t.integer  "fill_gap_question_id"
    t.string   "answer"
    t.boolean  "correct"
    t.integer  "position"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "program_assessment_fill_gap_questions", :force => true do |t|
    t.integer  "learning_outcome_id"
    t.integer  "section_id"
    t.string   "question"
    t.string   "reference_image_uid"
    t.string   "reference_image_name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "program_assessment_learning_outcomes", :force => true do |t|
    t.integer  "assessment_id"
    t.text     "description"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.integer  "allocation_percentage", :default => 0
  end

  create_table "program_assessment_matching_task_items", :force => true do |t|
    t.integer  "matching_task_question_id"
    t.string   "text"
    t.string   "answer"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "program_assessment_matching_task_questions", :force => true do |t|
    t.integer  "section_id"
    t.integer  "learning_outcome_id"
    t.text     "directions"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  create_table "program_assessment_multiple_choice_answers", :force => true do |t|
    t.integer  "multiple_choice_question_id"
    t.string   "answer"
    t.boolean  "correct"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  create_table "program_assessment_multiple_choice_questions", :force => true do |t|
    t.integer  "section_id"
    t.text     "question"
    t.string   "reference_image_uid"
    t.string   "reference_image_name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "image_caption"
    t.integer  "learning_outcome_id"
  end

  create_table "program_assessment_sections", :force => true do |t|
    t.string   "name"
    t.string   "question_type"
    t.integer  "assessment_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.integer  "minimum_number_of_questions"
  end

  create_table "program_assessment_sequential_answer_items", :force => true do |t|
    t.integer  "sequential_question_id"
    t.string   "text"
    t.string   "reference_image_uid"
    t.string   "reference_image_name"
    t.integer  "correct_position"
    t.integer  "position"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  create_table "program_assessment_sequential_questions", :force => true do |t|
    t.integer  "section_id"
    t.text     "question"
    t.string   "question_type"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "learning_outcome_id"
  end

  create_table "program_assessment_true_or_false_questions", :force => true do |t|
    t.text     "question"
    t.boolean  "answer",               :default => true
    t.string   "reference_image_uid"
    t.string   "reference_image_name"
    t.integer  "section_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "image_caption"
    t.integer  "learning_outcome_id"
  end

  create_table "program_assessments", :force => true do |t|
    t.integer  "employment_id"
    t.datetime "created_at",                                 :null => false
    t.datetime "updated_at",                                 :null => false
    t.integer  "program_id"
    t.string   "assessment_type",  :default => "Essentials"
    t.integer  "coordinator_id"
    t.integer  "group_session_id"
    t.datetime "assessed_at"
  end

  add_index "program_assessments", ["employment_id"], :name => "index_program_assessments_on_employment_id"

  create_table "program_categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "cached_slug"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.string   "ancestry"
    t.string   "parent_category_name"
  end

  add_index "program_categories", ["ancestry"], :name => "index_program_categories_on_ancestry"
  add_index "program_categories", ["cached_slug"], :name => "index_program_categories_on_cached_slug"
  add_index "program_categories", ["created_by_id"], :name => "index_program_categories_on_created_by_id"
  add_index "program_categories", ["updated_by_id"], :name => "index_program_categories_on_updated_by_id"

  create_table "program_categories_programs", :id => false, :force => true do |t|
    t.integer "program_id"
    t.integer "program_category_id"
  end

  create_table "program_thumbs", :force => true do |t|
    t.string   "image_uid"
    t.string   "image_name"
    t.integer  "program_id", :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "program_thumbs", ["program_id"], :name => "index_program_thumbs_on_program_id"

  create_table "program_types", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "programs", :force => true do |t|
    t.string   "title"
    t.string   "code"
    t.text     "description"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "cached_slug"
    t.integer  "zencoder_id"
    t.boolean  "processing",      :default => true
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.string   "classification",  :default => "program"
    t.integer  "category_id"
    t.integer  "program_type_id"
    t.string   "accreditation"
    t.string   "duration"
    t.integer  "video_width"
    t.integer  "video_height"
  end

  add_index "programs", ["cached_slug"], :name => "index_programs_on_cached_slug"
  add_index "programs", ["category_id"], :name => "index_programs_on_category_id"
  add_index "programs", ["created_by_id"], :name => "index_programs_on_created_by_id"
  add_index "programs", ["program_type_id"], :name => "index_programs_on_program_type_id"
  add_index "programs", ["updated_by_id"], :name => "index_programs_on_updated_by_id"

  create_table "service_job_titles", :force => true do |t|
    t.string   "name"
    t.integer  "service_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "service_job_titles", ["service_id"], :name => "index_service_job_categories_on_service_id"

  create_table "service_program_lists", :force => true do |t|
    t.integer  "service_program_id", :null => false
    t.integer  "program_id",         :null => false
    t.integer  "position"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "service_id",         :null => false
  end

  add_index "service_program_lists", ["program_id"], :name => "index_service_program_lists_on_program_id"
  add_index "service_program_lists", ["service_id", "service_program_id"], :name => "index_service_program_list"

  create_table "services", :force => true do |t|
    t.string   "name"
    t.string   "country"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.integer  "group_id"
    t.string   "cached_slug"
    t.integer  "disabled_by_id"
    t.datetime "disabled_at"
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.string   "activation_token"
    t.datetime "token_generated_at"
    t.integer  "zendesk_id"
  end

  add_index "services", ["cached_slug"], :name => "index_facilities_on_cached_slug"
  add_index "services", ["created_by_id"], :name => "index_services_on_created_by_id"
  add_index "services", ["disabled_by_id"], :name => "index_services_on_disabled_by_id"
  add_index "services", ["group_id"], :name => "index_services_on_group_id"
  add_index "services", ["updated_by_id"], :name => "index_services_on_updated_by_id"

  create_table "slugs", :force => true do |t|
    t.string   "scope"
    t.string   "slug"
    t.integer  "record_id"
    t.datetime "created_at"
  end

  add_index "slugs", ["scope", "record_id", "created_at"], :name => "index_slugs_on_scope_and_record_id_and_created_at"
  add_index "slugs", ["scope", "record_id"], :name => "index_slugs_on_scope_and_record_id"
  add_index "slugs", ["scope", "slug", "created_at"], :name => "index_slugs_on_scope_and_slug_and_created_at"
  add_index "slugs", ["scope", "slug"], :name => "index_slugs_on_scope_and_slug"

  create_table "user_assessment_result_questions", :force => true do |t|
    t.integer  "assessment_result_id",                    :null => false
    t.integer  "question_id",                             :null => false
    t.boolean  "correct",              :default => false
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.string   "question_type"
  end

  create_table "user_assessment_results", :force => true do |t|
    t.integer  "assessment_id",                    :null => false
    t.integer  "user_id",                          :null => false
    t.boolean  "passed",        :default => false
    t.integer  "time_taken"
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                         :default => ""
    t.string   "encrypted_password",            :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                 :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.integer  "created_by_id"
    t.integer  "updated_by_id"
    t.date     "born_at"
    t.string   "gender"
    t.string   "cached_slug"
    t.datetime "activation_code_set_at"
    t.string   "time_zone"
    t.datetime "email_verified_at"
    t.string   "last_email_status"
    t.string   "last_email_bounce_description"
    t.datetime "welcome_email_resent_at"
  end

  add_index "users", ["cached_slug"], :name => "index_users_on_cached_slug"
  add_index "users", ["created_by_id"], :name => "index_users_on_created_by_id"
  add_index "users", ["email"], :name => "user_email_unique", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["updated_by_id"], :name => "index_users_on_updated_by_id"

  create_table "videos", :force => true do |t|
    t.string   "description"
    t.string   "file_uid"
    t.string   "file_name"
    t.integer  "program_id",  :null => false
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "zencoder_id"
    t.string   "codec"
    t.integer  "height"
    t.integer  "width"
    t.integer  "order_id"
  end

  add_index "videos", ["description"], :name => "index_videos_on_description"
  add_index "videos", ["order_id"], :name => "index_videos_on_order_id"
  add_index "videos", ["program_id"], :name => "index_videos_on_program_id"

  add_foreign_key "coordinators", "groups", :name => "managers_group_id_fk"
  add_foreign_key "coordinators", "users", :name => "managers_user_id_fk"

  add_foreign_key "employment_learning_plans", "employments", :name => "employment_training_plans_employment_id_fk"

  add_foreign_key "employments", "services", :name => "employments_facility_fk"
  add_foreign_key "employments", "users", :name => "employments_user_fk"

  add_foreign_key "group_job_titles", "groups", :name => "group_job_categories_group_id_fk"

  add_foreign_key "group_program_lists", "programs", :name => "group_program_lists_program_id_fk"

  add_foreign_key "learning_records", "programs", :name => "training_record_program_fk"

  add_foreign_key "learning_resources", "programs", :name => "learning_resources_program_fk"

  add_foreign_key "package_program_lists", "package_programs", :name => "package_program_lists_package_program_id_fk"
  add_foreign_key "package_program_lists", "programs", :name => "package_program_lists_program_id_fk"

  add_foreign_key "package_programs", "programs", :name => "package_programs_programs_fk"

  add_foreign_key "program_assessments", "employments", :name => "program_assessments_employment_fk"

  add_foreign_key "program_thumbs", "programs", :name => "program_thumb_program_fk"

  add_foreign_key "programs", "program_types", :name => "programs_program_type_id_fk"

  add_foreign_key "service_job_titles", "services", :name => "service_job_categories_service_id_fk"

  add_foreign_key "service_program_lists", "programs", :name => "facility_program_lists_program_id_fk"

  add_foreign_key "videos", "programs", :name => "videos_program_fk"

  create_view "group_programs", "SELECT groups_packages.group_id, groups_packages.package_id, package_programs.program_id FROM (groups_packages LEFT JOIN package_programs ON ((groups_packages.package_id = package_programs.package_id)));", :force => true do |v|
    v.column :group_id
    v.column :package_id
    v.column :program_id
  end

  create_view "service_programs", "SELECT package_programs.program_id, packages_services.service_id, packages_services.package_id, NULL::integer AS group_id FROM ((packages_services LEFT JOIN package_programs ON ((package_programs.package_id = packages_services.package_id))) LEFT JOIN programs ON ((programs.id = package_programs.program_id))) WHERE ((programs.processing <> true) AND (packages_services.service_id IS NOT NULL)) UNION SELECT package_programs.program_id, services.id AS service_id, groups_packages.package_id, groups_packages.group_id FROM (((groups_packages LEFT JOIN services ON ((services.group_id = groups_packages.group_id))) LEFT JOIN package_programs ON ((package_programs.package_id = groups_packages.package_id))) LEFT JOIN programs ON ((programs.id = package_programs.program_id))) WHERE ((programs.processing <> true) AND (services.id IS NOT NULL));", :force => true do |v|
    v.column :program_id
    v.column :service_id
    v.column :package_id
    v.column :group_id
  end

  create_view "service_subscriptions", "SELECT packages_services.service_id, packages_services.package_id, NULL::integer AS group_id FROM packages_services UNION SELECT services.id AS service_id, groups_packages.package_id, groups_packages.group_id FROM (groups_packages LEFT JOIN services ON ((services.group_id = groups_packages.group_id)));", :force => true do |v|
    v.column :service_id
    v.column :package_id
    v.column :group_id
  end

  create_view "user_programs", "SELECT package_programs.program_id, packages_services.package_id, NULL::integer AS group_id, packages_services.service_id, employments.user_id FROM (((((packages_services LEFT JOIN employments ON ((packages_services.service_id = employments.service_id))) LEFT JOIN package_programs ON ((package_programs.package_id = packages_services.package_id))) LEFT JOIN programs ON ((programs.id = package_programs.program_id))) LEFT JOIN services ON ((services.id = employments.service_id))) LEFT JOIN groups ON ((groups.id = services.group_id))) WHERE (((((((packages_services.package_id IS NOT NULL) AND (employments.user_id IS NOT NULL)) AND (package_programs.program_id IS NOT NULL)) AND (employments.ended_at IS NULL)) AND (programs.processing <> true)) AND (groups.disabled_at IS NULL)) AND (services.disabled_at IS NULL)) UNION SELECT package_programs.program_id, package_programs.package_id, groups_packages.group_id, services.id AS service_id, employments.user_id FROM (((((services LEFT JOIN employments ON ((services.id = employments.service_id))) LEFT JOIN groups_packages ON ((groups_packages.group_id = services.group_id))) LEFT JOIN groups ON ((groups.id = services.group_id))) LEFT JOIN package_programs ON ((package_programs.package_id = groups_packages.package_id))) LEFT JOIN programs ON ((programs.id = package_programs.program_id))) WHERE ((((((groups_packages.package_id IS NOT NULL) AND (employments.user_id IS NOT NULL)) AND (package_programs.program_id IS NOT NULL)) AND (employments.ended_at IS NULL)) AND (programs.processing <> true)) AND (groups.disabled_at IS NULL));", :force => true do |v|
    v.column :program_id
    v.column :package_id
    v.column :group_id
    v.column :service_id
    v.column :user_id
  end

end
