class AddPackagesAndProgramsJoin < ActiveRecord::Migration
  def self.up
    create_table :packages_programs, :id => false do |t|
      t.integer :package_id
      t.integer :program_id
    end
  end

  def self.down
    drop_table :packages_programs
  end
end
