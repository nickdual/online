class AddProcessingToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :processing, :boolean, :default => true
  end
end
