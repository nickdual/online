class RemoveGroupSessionsProgramsTable < ActiveRecord::Migration
  def up
    drop_table :group_sessions_programs
  end

  def down
    create_table :group_sessions_programs, :id => false do |t|
      t.integer :group_session_id
      t.integer :program_id
    end
  end
end
