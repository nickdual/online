class AddDragonflyToLearningResources < ActiveRecord::Migration
  def change
    add_column :learning_resources, :resource_uid, :string
    add_column :learning_resources, :resource_name, :string
  end
end
