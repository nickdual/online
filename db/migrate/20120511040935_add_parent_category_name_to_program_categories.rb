class AddParentCategoryNameToProgramCategories < ActiveRecord::Migration
  def change
    add_column :program_categories, :parent_category_name, :string

    ProgramCategory.where("ancestry IS NOT NULL").each do |program|
      parent_category_name = program.parent.name.gsub("'", "\\'")
      ActiveRecord::Base.connection.execute("UPDATE program_categories SET parent_category_name = '#{parent_category_name}' WHERE id = #{program.id}")
    end
  end
end
