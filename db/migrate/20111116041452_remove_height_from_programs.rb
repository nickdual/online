class RemoveHeightFromPrograms < ActiveRecord::Migration
  def up
    remove_column(:programs, :height)
  end

  def down
    add_column(:programs, :height, :integer)
  end
end
