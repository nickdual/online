class AddCreatedByIdAndUpdatedByIdToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :created_by_id, :integer
    add_column :groups, :updated_by_id, :integer
  end
end
