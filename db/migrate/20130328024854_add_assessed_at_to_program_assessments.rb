class AddAssessedAtToProgramAssessments < ActiveRecord::Migration
  def change
    add_column :program_assessments, :assessed_at, :datetime

    ProgramAssessment.all.each do |assessment|
      ActiveRecord::Base.connection.execute("UPDATE program_assessments SET assessed_at = '#{assessment.created_at}' WHERE program_assessments.id = #{assessment.id}")
    end
  end
end
