class CreateProgramAssessmentSections < ActiveRecord::Migration
  def change
    create_table "program_assessment_sections" do |t|
      t.string :name
      t.string :question_type
      t.belongs_to :assessment
      t.timestamps
    end
  end
end
