class AddActiveAtToEmploymentLearningPlans < ActiveRecord::Migration
  def change
    add_column :employment_learning_plans, :active_at, :datetime

    EmploymentLearningPlan.all.each do |e|
      active_at = e.learning_plan.active_at
      ActiveRecord::Base.connection.execute("UPDATE employment_learning_plans SET active_at = '#{active_at}' WHERE id = #{e.id};")
    end
  end
end
