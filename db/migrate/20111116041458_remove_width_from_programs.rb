class RemoveWidthFromPrograms < ActiveRecord::Migration
  def up
    remove_column(:programs, :width)
  end

  def down
    add_column(:programs, :width, :integer)
  end
end
