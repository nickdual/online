class AddForeignKeyToPackagePrograms < ActiveRecord::Migration
  def change
    execute("DROP VIEW user_programs")
    execute("DROP VIEW facility_programs")
    execute("DROP VIEW group_programs")

    execute("ALTER TABLE package_programs ADD CONSTRAINT package_programs_programs_fk FOREIGN KEY (program_id) REFERENCES programs(id)")
    change_column(:package_programs, :program_id, :integer, :null => false)

    execute("CREATE VIEW group_programs AS
              SELECT groups_packages.group_id, groups_packages.package_id, package_programs.program_id FROM groups_packages LEFT JOIN package_programs ON groups_packages.package_id = package_programs.package_id;")

    execute("CREATE VIEW facility_programs AS
              SELECT package_programs.program_id, facilities_packages.facility_id, facilities_packages.package_id, null as group_id FROM facilities_packages LEFT JOIN package_programs ON package_programs.package_id = facilities_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE programs.processing != true UNION
              SELECT package_programs.program_id, facilities.id as facility_id, groups_packages.package_id, groups_packages.group_id FROM groups_packages LEFT JOIN facilities ON facilities.group_id = groups_packages.group_id LEFT JOIN package_programs ON package_programs.package_id = groups_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE programs.processing != true")
    
    execute("CREATE VIEW user_programs AS
              SELECT package_programs.program_id, facilities_packages.package_id, null as group_id, facilities_packages.facility_id, employments.user_id FROM facilities_packages LEFT JOIN employments ON facilities_packages.facility_id = employments.facility_id LEFT JOIN package_programs ON package_programs.package_id = facilities_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id LEFT JOIN facilities ON facilities.id = employments.facility_id LEFT JOIN groups ON groups.id = facilities.group_id WHERE facilities_packages.package_id IS NOT NULL AND employments.user_id IS NOT NULL AND package_programs.program_id IS NOT NULL AND employments.ended_at IS NULL AND programs.processing != true AND groups.disabled_at IS NULL AND facilities.disabled_at IS NULL UNION
              SELECT package_programs.program_id, package_programs.package_id, groups_packages.group_id, facilities.id as facility_id, employments.user_id FROM facilities LEFT JOIN employments ON facilities.id = employments.facility_id LEFT JOIN groups_packages ON groups_packages.group_id = facilities.group_id LEFT JOIN groups ON groups.id = facilities.group_id LEFT JOIN package_programs ON package_programs.package_id = groups_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE groups_packages.package_id IS NOT NULL AND employments.user_id IS NOT NULL AND package_programs.program_id IS NOT NULL AND employments.ended_at IS NULL AND programs.processing != true AND groups.disabled_at IS NULL;")
  end
end
