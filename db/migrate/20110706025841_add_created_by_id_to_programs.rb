class AddCreatedByIdToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :created_by_id, :integer
  end
end
