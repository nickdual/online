class CreateOnlineProgramAssessments < ActiveRecord::Migration
  def change
    create_table "online_program_assessments" do |t|
      t.string :name
      t.belongs_to :program
      t.integer :time_allowed
      t.string :assessment_type,  default: "Essentials"
      t.timestamps
    end
  end
end
