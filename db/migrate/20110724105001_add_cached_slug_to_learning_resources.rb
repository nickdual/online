class AddCachedSlugToLearningResources < ActiveRecord::Migration
  
  def self.up
    add_column :learning_resources, :cached_slug, :string
    add_index  :learning_resources, :cached_slug
  end

  def self.down
    remove_column :learning_resources, :cached_slug
  end
  
end
