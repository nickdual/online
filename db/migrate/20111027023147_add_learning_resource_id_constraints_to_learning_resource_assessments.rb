class AddLearningResourceIdConstraintsToLearningResourceAssessments < ActiveRecord::Migration
  def change
    execute("ALTER TABLE learning_resource_assessments ADD CONSTRAINT learning_resource_assessments_learning_resource_id_fk FOREIGN KEY (learning_resource_id) REFERENCES learning_resources(id)")
  end
end
