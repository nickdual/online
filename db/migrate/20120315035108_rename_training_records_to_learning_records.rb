class RenameTrainingRecordsToLearningRecords < ActiveRecord::Migration
  def up
    rename_table(:training_records, :learning_records)
  end

  def down
    rename_table(:learning_records, :training_records)
  end
end
