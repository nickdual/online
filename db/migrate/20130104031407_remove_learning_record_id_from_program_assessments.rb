class RemoveLearningRecordIdFromProgramAssessments < ActiveRecord::Migration
  def up
    remove_column :program_assessments, :learning_record_id
  end

  def down
    add_column :program_assessments, :learning_record_id, :integer
  end
end
