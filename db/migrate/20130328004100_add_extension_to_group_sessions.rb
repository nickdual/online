class AddExtensionToGroupSessions < ActiveRecord::Migration
  def change
    add_column :group_sessions, :extension, :boolean
  end
end
