class AddParentIdToServiceJobCategories < ActiveRecord::Migration
  def change
    add_column :service_job_categories, :parent_id, :integer
    add_foreign_key :service_job_categories, :job_categories, column: :parent_id
  end
end
