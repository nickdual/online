class RenameFacilityProgramListsToServices < ActiveRecord::Migration
  def up
    rename_table :facility_program_lists, :service_program_lists
    rename_column :service_program_lists, :facility_program_id, :service_program_id
    rename_column :service_program_lists, :facility_id, :service_id
  end


  def down
    rename_table :facility_program_lists, :service_program_lists
    rename_column :facility_program_lists, :service_program_id, :facility_program_id
    rename_column :facility_program_lists, :service_id, :facility_id
  end
end
