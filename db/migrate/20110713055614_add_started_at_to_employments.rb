class AddStartedAtToEmployments < ActiveRecord::Migration
  def change
    add_column :employments, :started_at, :datetime
  end
end
