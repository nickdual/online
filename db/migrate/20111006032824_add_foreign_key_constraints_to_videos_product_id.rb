class AddForeignKeyConstraintsToVideosProductId < ActiveRecord::Migration
  def change
    execute("ALTER TABLE videos ADD CONSTRAINT videos_program_fk FOREIGN KEY (program_id) REFERENCES programs(id)")
    change_column(:videos, :program_id, :integer, :null => false)
  end
end
