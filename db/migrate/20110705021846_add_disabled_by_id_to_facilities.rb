class AddDisabledByIdToFacilities < ActiveRecord::Migration
  def change
    add_column :facilities, :disabled_by_id, :integer
  end
end
