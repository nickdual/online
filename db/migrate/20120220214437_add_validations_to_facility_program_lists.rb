class AddValidationsToFacilityProgramLists < ActiveRecord::Migration
  def change
    change_column(:facility_program_lists, :program_id, :integer, :null => false)
    change_column(:facility_program_lists, :facility_program_id, :integer, :null => false)
    change_column(:facility_program_lists, :facility_id, :integer, :null => false)

    add_foreign_key(:facility_program_lists, :programs)
  end
end