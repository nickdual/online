class CreateFacilityRegionJoinTable < ActiveRecord::Migration
  def change
    create_table :facilities_regions, :id => false do |t|
      t.integer :facility_id
      t.integer :region_id
    end
  end
end
