class AddEmployeeNumberAndJobTypeToEmployments < ActiveRecord::Migration
  def change
    add_column :employments, :employee_number, :string
    add_column :employments, :job_type, :string
  end
end
