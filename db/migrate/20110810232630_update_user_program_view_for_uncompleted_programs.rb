class UpdateUserProgramViewForUncompletedPrograms < ActiveRecord::Migration
  def self.up
    execute("DROP VIEW user_programs")
    execute("CREATE VIEW user_programs AS
              SELECT packages_programs.program_id, facilities_packages.package_id, null as group_id, facilities_packages.facility_id, employments.user_id FROM facilities_packages LEFT JOIN employments ON facilities_packages.facility_id = employments.facility_id LEFT JOIN packages_programs ON packages_programs.package_id = facilities_packages.package_id LEFT JOIN programs ON programs.id = packages_programs.program_id WHERE facilities_packages.package_id IS NOT NULL AND employments.user_id IS NOT NULL AND packages_programs.program_id IS NOT NULL AND employments.ended_at IS NULL AND programs.processing != true UNION
              SELECT packages_programs.program_id, packages_programs.package_id, groups_packages.group_id, facilities.id as facility_id, employments.user_id FROM facilities LEFT JOIN employments ON facilities.id = employments.facility_id LEFT JOIN groups_packages ON groups_packages.group_id = facilities.group_id LEFT JOIN packages_programs ON packages_programs.package_id = groups_packages.package_id LEFT JOIN programs ON programs.id = packages_programs.program_id WHERE groups_packages.package_id IS NOT NULL AND employments.user_id IS NOT NULL AND packages_programs.program_id IS NOT NULL AND employments.ended_at IS NULL AND programs.processing != true;")
  end

  def self.down
    execute("DROP VIEW user_programs")
    execute("CREATE VIEW user_programs AS
              SELECT packages_programs.program_id, facilities_packages.package_id, null as group_id, facilities_packages.facility_id, employments.user_id FROM facilities_packages LEFT JOIN employments ON facilities_packages.facility_id = employments.facility_id LEFT JOIN packages_programs ON packages_programs.package_id = facilities_packages.package_id WHERE facilities_packages.package_id IS NOT NULL AND employments.user_id IS NOT NULL AND packages_programs.program_id IS NOT NULL AND employments.ended_at IS NULL UNION
              SELECT packages_programs.program_id, packages_programs.package_id, groups_packages.group_id, facilities.id as facility_id, employments.user_id FROM facilities LEFT JOIN employments ON facilities.id = employments.facility_id LEFT JOIN groups_packages ON groups_packages.group_id = facilities.group_id LEFT JOIN packages_programs ON packages_programs.package_id = groups_packages.package_id WHERE groups_packages.package_id IS NOT NULL AND employments.user_id IS NOT NULL AND packages_programs.program_id IS NOT NULL AND employments.ended_at IS NULL;")    
  end
end
