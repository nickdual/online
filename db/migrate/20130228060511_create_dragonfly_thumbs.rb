class CreateDragonflyThumbs < ActiveRecord::Migration
  def change
    create_table :dragonfly_thumbs do |t|
      t.string :job
      t.string :uid

      t.timestamps
    end
  end
end
