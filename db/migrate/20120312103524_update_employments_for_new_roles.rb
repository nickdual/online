class UpdateEmploymentsForNewRoles < ActiveRecord::Migration
  def up
    execute("UPDATE employments SET role = 'coordinator' WHERE role = 'manager'")
  end

  def down
    execute("UPDATE employments SET role = 'coordinator' WHERE role = 'manager'")
  end
end
