class CreateProgramAssessmentMultipleChoiceQuestions < ActiveRecord::Migration
  def change
    create_table :program_assessment_multiple_choice_questions do |t|
      t.belongs_to :section
      t.string :question
      t.string :reference_image_uid
      t.string :reference_image_name
      t.timestamps
    end
  end
end
