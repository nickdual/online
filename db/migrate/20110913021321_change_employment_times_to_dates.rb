class ChangeEmploymentTimesToDates < ActiveRecord::Migration
  def up
    execute("DROP VIEW user_programs")
    change_column :employments, :started_at, :date
    change_column :employments, :ended_at, :date
    
    execute("CREATE VIEW user_programs AS
              SELECT package_programs.program_id, facilities_packages.package_id, null as group_id, facilities_packages.facility_id, employments.user_id FROM facilities_packages LEFT JOIN employments ON facilities_packages.facility_id = employments.facility_id LEFT JOIN package_programs ON package_programs.package_id = facilities_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE facilities_packages.package_id IS NOT NULL AND employments.user_id IS NOT NULL AND package_programs.program_id IS NOT NULL AND employments.ended_at IS NULL AND programs.processing != true UNION
              SELECT package_programs.program_id, package_programs.package_id, groups_packages.group_id, facilities.id as facility_id, employments.user_id FROM facilities LEFT JOIN employments ON facilities.id = employments.facility_id LEFT JOIN groups_packages ON groups_packages.group_id = facilities.group_id LEFT JOIN package_programs ON package_programs.package_id = groups_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE groups_packages.package_id IS NOT NULL AND employments.user_id IS NOT NULL AND package_programs.program_id IS NOT NULL AND employments.ended_at IS NULL AND programs.processing != true;")
  end

  def down
    execute("DROP VIEW user_programs")    
    change_column :employments, :started_at, :datetime
    change_column :employments, :ended_at, :datetime
    
    execute("CREATE VIEW user_programs AS
              SELECT package_programs.program_id, facilities_packages.package_id, null as group_id, facilities_packages.facility_id, employments.user_id FROM facilities_packages LEFT JOIN employments ON facilities_packages.facility_id = employments.facility_id LEFT JOIN package_programs ON package_programs.package_id = facilities_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE facilities_packages.package_id IS NOT NULL AND employments.user_id IS NOT NULL AND package_programs.program_id IS NOT NULL AND employments.ended_at IS NULL AND programs.processing != true UNION
              SELECT package_programs.program_id, package_programs.package_id, groups_packages.group_id, facilities.id as facility_id, employments.user_id FROM facilities LEFT JOIN employments ON facilities.id = employments.facility_id LEFT JOIN groups_packages ON groups_packages.group_id = facilities.group_id LEFT JOIN package_programs ON package_programs.package_id = groups_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE groups_packages.package_id IS NOT NULL AND employments.user_id IS NOT NULL AND package_programs.program_id IS NOT NULL AND employments.ended_at IS NULL AND programs.processing != true;")    
  end
end
