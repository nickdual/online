class RemoveMobileNumberFromUsers < ActiveRecord::Migration
  def up
    remove_column :users, :mobile_number
  end

  def down
    add_column :users, :mobile_number
  end
end
