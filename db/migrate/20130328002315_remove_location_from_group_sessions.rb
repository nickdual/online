class RemoveLocationFromGroupSessions < ActiveRecord::Migration
  def up
    remove_column :group_sessions, :location
  end

  def down
    add_column :group_sessions, :location, :string
  end
end
