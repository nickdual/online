class AddActivationCodeSetAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :activation_code_set_at, :datetime
  end
end
