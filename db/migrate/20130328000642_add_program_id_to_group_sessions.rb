class AddProgramIdToGroupSessions < ActiveRecord::Migration
  def change
    add_column :group_sessions, :program_id, :integer
  end
end
