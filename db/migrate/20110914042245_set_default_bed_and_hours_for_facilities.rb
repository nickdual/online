class SetDefaultBedAndHoursForFacilities < ActiveRecord::Migration
  def up
    change_column :facilities, :beds, :integer, :default => 0
    change_column :facilities, :hours, :integer, :default => 0
  end

  def down
    change_column :facilities, :beds, :integer, :default => nil
    change_column :facilities, :hours, :integer, :default => nil
  end
end
