class ChangeGenderToStringForUsers < ActiveRecord::Migration
  def up
    change_column :users, :gender, :string
    User.where(:gender => "true").each { |u| u.update_attribute(:gender, "male") }
    User.where(:gender => "false").each { |u| u.update_attribute(:gender, "female") }
    
  end

  def down
    change_column :users, :gender, :string
  end
end
