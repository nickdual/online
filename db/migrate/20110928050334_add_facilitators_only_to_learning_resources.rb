class AddFacilitatorsOnlyToLearningResources < ActiveRecord::Migration
  def change
    add_column :learning_resources, :facilitators_only, :boolean, :default => false
  end
end
