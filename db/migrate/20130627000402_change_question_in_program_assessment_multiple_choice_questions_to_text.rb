class ChangeQuestionInProgramAssessmentMultipleChoiceQuestionsToText < ActiveRecord::Migration
  def up
    change_column :program_assessment_multiple_choice_questions, :question, :text
  end

  def down
    change_column :program_assessment_multiple_choice_questions, :question, :string
  end
end
