class AddVideoWidthToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :video_width, :integer

    Program.all.each do |program|
      video_width = program.videos.sd.where(description: "browser_webm_sd").first.try(:width)
      ActiveRecord::Base.connection.execute("UPDATE programs SET video_width = #{video_width} WHERE id = #{program.id}")
    end
  end
end
 ActiveRecord::Base.connection.execute("select * from programs;")