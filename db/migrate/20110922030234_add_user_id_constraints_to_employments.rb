class AddUserIdConstraintsToEmployments < ActiveRecord::Migration
  def change
    execute("ALTER TABLE employments ADD CONSTRAINT employments_user_fk FOREIGN KEY (user_id) REFERENCES users(id)")
  end
end
