class CreateLearningPlanPrograms < ActiveRecord::Migration
  def change
    rename_table :learning_plans_programs, :learning_plan_programs
    add_column :learning_plan_programs, :id, :primary_key
  end
end
