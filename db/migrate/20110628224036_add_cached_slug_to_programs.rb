class AddCachedSlugToPrograms < ActiveRecord::Migration
  
  def self.up
    add_column :programs, :cached_slug, :string
    add_index  :programs, :cached_slug
  end

  def self.down
    remove_column :programs, :cached_slug
  end
  
end
