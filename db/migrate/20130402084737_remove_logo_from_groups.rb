class RemoveLogoFromGroups < ActiveRecord::Migration
  def up
    remove_column :groups, :logo_uid
    remove_column :groups, :logo_name
  end

  def down
    add_column :groups, :logo_uid,  :string
    add_column :groups, :logo_name, :string
  end
end
