class AddGroupsPackagesJoin < ActiveRecord::Migration
  def self.up
    create_table :groups_packages, :id => false do |t|
      t.integer :group_id
      t.integer :package_id
    end
  end

  def self.down
    remove_table :groups_packages
  end
end
