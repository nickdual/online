class AddCreatedByIdAndUpdatedByIdToJobCategories < ActiveRecord::Migration
  def change
    add_column :job_categories, :created_by_id, :integer
    add_column :job_categories, :updated_by_id, :integer
  end
end
