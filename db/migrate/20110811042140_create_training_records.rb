class CreateTrainingRecords < ActiveRecord::Migration
  def change
    create_table :training_records do |t|
      t.datetime :viewed_at
      t.integer :program_id
      t.integer :user_id

      t.timestamps
    end
  end
end
