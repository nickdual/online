class RemoveProgramsWatchedFromUserTrainingPlans < ActiveRecord::Migration
  def up
    remove_column :user_training_plans, :programs_watched
  end

  def down
    add_column :user_training_plans, :programs_watched, :integer
  end
end
