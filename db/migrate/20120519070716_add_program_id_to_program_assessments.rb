class AddProgramIdToProgramAssessments < ActiveRecord::Migration
  def change
    add_column :program_assessments, :program_id, :integer

    ProgramAssessment.includes(:learning_record).all.each do |assessment|
      program_id = assessment.learning_record.program_id
      assessment.update_attribute(:program_id, program_id)
    end
  end
end