class AddHeightToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :height, :integer, :default => 0
  end
end
