class CreateProgramAssessmentSequentialQuestions < ActiveRecord::Migration
  def change
    create_table :program_assessment_sequential_questions do |t|
      t.belongs_to :section
      t.string :text
      t.string :question_type
      t.timestamps
    end
  end
end
