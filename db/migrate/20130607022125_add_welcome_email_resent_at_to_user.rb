class AddWelcomeEmailResentAtToUser < ActiveRecord::Migration
  def change
    add_column :users, :welcome_email_resent_at, :datetime
  end
end
