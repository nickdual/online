class AddFacilitiesPackagesJoin < ActiveRecord::Migration
  def self.up
    create_table :facilities_packages, :id => false do |t|
      t.integer :facility_id
      t.integer :package_id
    end
  end

  def self.down
    remove_table :facilities_packages
  end
end
