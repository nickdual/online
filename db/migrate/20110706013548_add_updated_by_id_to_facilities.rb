class AddUpdatedByIdToFacilities < ActiveRecord::Migration
  def change
    add_column :facilities, :updated_by_id, :integer
  end
end
