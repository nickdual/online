class AddVideoHeightToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :video_height, :integer

    Program.all.each do |program|
      video_height = program.videos.sd.where(description: "browser_webm_sd").first.try(:height)
      ActiveRecord::Base.connection.execute("UPDATE programs SET video_height = #{video_height} WHERE id = #{program.id}")
    end
  end
end
