class RenameFacilityToServiceInGroupSessions < ActiveRecord::Migration
  def up
    rename_column(:group_sessions, :facility_id, :service_id)
  end

  def down
    rename_column(:group_sessions, :service_id, :facility_id)
  end
end
