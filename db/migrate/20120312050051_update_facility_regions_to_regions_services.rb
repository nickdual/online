class UpdateFacilityRegionsToRegionsServices < ActiveRecord::Migration
  def up
    rename_table(:facilities_regions, :regions_services)
    rename_column(:regions_services, :facility_id, :service_id)
  end

  def down
    rename_table(:regions_services, :facilities_regions)
    rename_column(:facilities_regions, :service_id, :facility_id)
  end
end
