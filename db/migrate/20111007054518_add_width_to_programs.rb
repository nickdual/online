class AddWidthToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :width, :integer, :default => 0
  end
end
