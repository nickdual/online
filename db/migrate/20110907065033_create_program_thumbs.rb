class CreateProgramThumbs < ActiveRecord::Migration
  def change
    create_table :program_thumbs do |t|
      t.integer :image_uid
      t.string :image_name
      t.integer :program_id

      t.timestamps
    end
  end
end
