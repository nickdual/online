class DeleteQuestionResultsWithoutQuestionTypes < ActiveRecord::Migration
  def up
    User::AssessmentResultQuestion.where(question_type: nil).delete_all
    User::AssessmentResult.select{|r| r.questions.empty? }.each{|r| r.destroy }
  end

  def down
    # do nothing
  end
end
