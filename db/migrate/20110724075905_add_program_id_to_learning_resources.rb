class AddProgramIdToLearningResources < ActiveRecord::Migration
  def change
    add_column :learning_resources, :program_id, :integer
  end
end
