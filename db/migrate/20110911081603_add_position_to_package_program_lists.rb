class AddPositionToPackageProgramLists < ActiveRecord::Migration
  def change
    add_column :package_program_lists, :position, :integer
  end
end
