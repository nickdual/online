class AddForeignKeyConstraintsToTrainingRecordsProductId < ActiveRecord::Migration
  def change
    execute("ALTER TABLE training_records ADD CONSTRAINT training_record_program_fk FOREIGN KEY (program_id) REFERENCES programs(id)")
    change_column(:training_records, :program_id, :integer, :null => false)
  end
end
