class RenameUserTrainingPlansToEmploymentTrainingPlans < ActiveRecord::Migration
  def up
    rename_table(:user_training_plans, :employment_training_plans)
  end

  def down
    rename_table(:employment_training_plans, :user_training_plans)
  end
end
