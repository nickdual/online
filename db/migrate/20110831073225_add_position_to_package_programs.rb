class AddPositionToPackagePrograms < ActiveRecord::Migration
  def change
    add_column :package_programs, :position, :integer
  end
end
