class AddValidationsToPackageProgramLists < ActiveRecord::Migration
  def change
    change_column(:package_program_lists, :program_id, :integer, :null => false)
    change_column(:package_program_lists, :package_program_id, :integer, :null => false)

    add_foreign_key(:package_program_lists, :programs)
    add_foreign_key(:package_program_lists, :package_programs)
  end
end
