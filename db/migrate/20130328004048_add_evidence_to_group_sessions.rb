class AddEvidenceToGroupSessions < ActiveRecord::Migration
  def change
    add_column :group_sessions, :evidence, :boolean
  end
end
