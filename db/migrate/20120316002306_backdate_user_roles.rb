class BackdateUserRoles < ActiveRecord::Migration
  def up
    Employment.where(role: "coordinator").each do |e|
      e.update_attribute(:role, "group coordinator")
    end
    
    Employment.where(role: "facilitator").each do |e|
      e.update_attribute(:role, "coordinator")
    end
  end

  def down
  end
end
