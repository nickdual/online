class CreateProgramAssessmentLearningOutcomes < ActiveRecord::Migration
  def change
    create_table :program_assessment_learning_outcomes do |t|
      t.belongs_to :assessment
      t.text :description
      t.timestamps
    end
  end
end
