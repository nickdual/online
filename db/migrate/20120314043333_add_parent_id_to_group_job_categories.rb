class AddParentIdToGroupJobCategories < ActiveRecord::Migration
  def change
    add_column :group_job_categories, :parent_id, :integer
    add_foreign_key :group_job_categories, :job_categories, column: :parent_id
  end
end
