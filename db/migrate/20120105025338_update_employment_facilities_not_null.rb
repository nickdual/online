class UpdateEmploymentFacilitiesNotNull < ActiveRecord::Migration
  def up
    change_column_null(:employments, :facility_id, false)
  end

  def down
    change_column_null(:employments, :facility_id, true)
  end
end
