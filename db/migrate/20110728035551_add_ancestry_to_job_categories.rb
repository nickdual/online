class AddAncestryToJobCategories < ActiveRecord::Migration
  def change
    add_column :job_categories, :ancestry, :string
    add_index :job_categories, :ancestry
  end
end
