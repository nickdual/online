class RenameProgramIdInProgramAssessmentsToTrainingRecordId < ActiveRecord::Migration
  def up
    rename_column(:program_assessments, :program_id, :training_record_id)
  end

  def down
    rename_column(:program_assessments, :training_record_id, :program_id)
  end
end
