class AddFillGapSectionToAllAssessments < ActiveRecord::Migration
  def up
    Program::Assessment.find_each do |assessment|
      assessment.add_default_sections
      assessment.sections.each do |section|
        section.minimum_number_of_questions ||= 0
      end
      assessment.save!
    end
  end

  def down
    # do nothing
  end
end
