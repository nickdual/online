class AddMediumToTrainingRecords < ActiveRecord::Migration
  def change
    add_column :training_records, :medium, :string, :default => "Online"
  end
end
