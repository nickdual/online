class UpdateProgramIdFkForProgramAssessments < ActiveRecord::Migration
  def up
    execute("ALTER TABLE program_assessments DROP CONSTRAINT learning_resource_assessments_learning_resource_id_fk")
    execute("ALTER TABLE program_assessments ADD CONSTRAINT program_assessments_program_fk FOREIGN KEY (program_id) REFERENCES programs(id)")
  end

  def down
    execute("ALTER TABLE program_assessments DROP CONSTRAINT program_assessments_program_fk")
    execute("ALTER TABLE program_assessments ADD CONSTRAINT learning_resource_assessments_learning_resource_id_fk FOREIGN KEY (program_id) REFERENCES programs(id)")
  end
end
