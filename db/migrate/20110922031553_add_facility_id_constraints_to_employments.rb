class AddFacilityIdConstraintsToEmployments < ActiveRecord::Migration
  def change
    execute("ALTER TABLE employments ADD CONSTRAINT employments_facility_fk FOREIGN KEY (facility_id) REFERENCES facilities(id)")    
  end
end
