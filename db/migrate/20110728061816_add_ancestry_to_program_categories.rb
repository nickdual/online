class AddAncestryToProgramCategories < ActiveRecord::Migration
  def change
    add_column :program_categories, :ancestry, :string
    add_index :program_categories, :ancestry
  end
end
