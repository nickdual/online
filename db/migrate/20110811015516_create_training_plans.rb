class CreateTrainingPlans < ActiveRecord::Migration
  def change
    create_table :training_plans do |t|
      t.string :name
      t.date :due_at
      t.integer :facility_id
      t.timestamps
    end
  end
end
