class UpdateTrainingRecordsUserIdToEmploymentId < ActiveRecord::Migration
  def up
    rename_column(:training_records, :user_id, :employment_id)
  end

  def down
    rename_column(:training_records, :user_id, :employment_id)    
  end
end
