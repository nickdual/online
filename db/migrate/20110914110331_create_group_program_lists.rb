class CreateGroupProgramLists < ActiveRecord::Migration
  def change
    create_table :group_program_lists do |t|
      t.integer :group_program_id
      t.integer :program_id
      t.integer :position

      t.timestamps
    end
  end
end
