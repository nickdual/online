class AddEndedAtToEmployments < ActiveRecord::Migration
  def change
    add_column :employments, :ended_at, :datetime
  end
end
