class ChangeJobTypeInEmploymentsToJobCategoryId < ActiveRecord::Migration
  def up
    remove_column :employments, :job_type
    add_column :employments, :job_category_id, :integer
  end

  def down
    remove_column :employments, :job_category_id
    add_column :employments, :job_type, :string
  end
end
