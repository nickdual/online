class BackdateOldEmploymentLearningPlanStatus < ActiveRecord::Migration
  def up
    add_column :learning_plans, :active_at, :date

    LearningPlan.all.each do |plan|
      execute("UPDATE learning_plans SET active_at = '#{plan.created_at}'")
    end

    execute("UPDATE employment_learning_plans SET status = 'pending';")

    EmploymentLearningPlan.all.each do |plan|
      plan.set_status!
    end
  end

  def down
  end
end
