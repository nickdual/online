class CreateFacilityProgramLists < ActiveRecord::Migration
  def change
    create_table :facility_program_lists do |t|
      t.integer :facility_program_id
      t.integer :program_id
      t.integer :position

      t.timestamps
    end
  end
end
