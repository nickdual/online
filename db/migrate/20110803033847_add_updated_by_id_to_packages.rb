class AddUpdatedByIdToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :updated_by_id, :integer
  end
end
