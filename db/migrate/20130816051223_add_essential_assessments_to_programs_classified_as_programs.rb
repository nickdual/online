class AddEssentialAssessmentsToProgramsClassifiedAsPrograms < ActiveRecord::Migration
  def up
    Program.where(classification: 'program').find_each do |program|
      if program.assessments.pluck(:assessment_type).include?('Essentials')
        # do nothing
      else
        program.create_essential_assessment
      end
    end
  end

  def down
    # do nothing. Rollback manually.
  end
end
