class AddCompletedAtToEmploymentLearningPlans < ActiveRecord::Migration
  def change
    add_column :employment_learning_plans, :completed_at, :datetime
    
    EmploymentLearningPlan.all.each do |plan|
      execute("UPDATE employment_learning_plans SET completed_at = '#{Time.now}' WHERE id = '#{plan.id}'") if plan.completed?
    end
  end
end