class AddOrderToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :order_id, :integer
    add_index :videos, :order_id

    Video.all.each do |video|
      order_id = video.instance_eval("set_order_id")
      ActiveRecord::Base.connection.execute("UPDATE videos SET order_id = #{order_id} WHERE id = #{video.id}") if order_id
    end
  end
end
