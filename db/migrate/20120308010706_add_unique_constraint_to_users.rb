class AddUniqueConstraintToUsers < ActiveRecord::Migration
  def change
    execute("ALTER TABLE users ADD CONSTRAINT user_email_unique UNIQUE (email);")
  end
end
