class AddPrimaryToProgramThumbs < ActiveRecord::Migration
  def change
    add_column :program_thumbs, :primary, :boolean, :default => false
  end
end
