class AddPersonalToLearningPlans < ActiveRecord::Migration
  def change
    add_column :learning_plans, :personal, :boolean
  end
end
