class AddValidationsToGroupProgramLists < ActiveRecord::Migration
  def change
    change_column(:group_program_lists, :program_id, :integer, :null => false)
    change_column(:group_program_lists, :group_program_id, :integer, :null => false)
    change_column(:group_program_lists, :group_id, :integer, :null => false)

    add_foreign_key(:group_program_lists, :programs)
  end
end
