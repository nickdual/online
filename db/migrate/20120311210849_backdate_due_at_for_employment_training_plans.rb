class BackdateDueAtForEmploymentTrainingPlans < ActiveRecord::Migration
  def up
    rename_table(:employment_training_plans, :employment_learning_plans)
    EmploymentLearningPlan.joins(:learning_plan).all.each do |e|
      due_at = e.created_at.advance(days: e.learning_plan.days_till_due)
      execute("UPDATE employment_learning_plans SET due_at = '#{due_at}' WHERE id = '#{e.id}'")
    end
  end

  def down
    rename_table(:employment_training_plans, :employment_learning_plans)
  end
end
