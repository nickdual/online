class ChangeQuestionInProgramAssessmentTrueOrFalseQuestionsToText < ActiveRecord::Migration
  def up
    change_column :program_assessment_true_or_false_questions, :question, :text
  end

  def down
    change_column :program_assessment_true_or_false_questions, :question, :string
  end
end
