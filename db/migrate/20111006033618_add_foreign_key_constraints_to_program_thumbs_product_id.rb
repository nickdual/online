class AddForeignKeyConstraintsToProgramThumbsProductId < ActiveRecord::Migration
  def change
    execute("ALTER TABLE program_thumbs ADD CONSTRAINT program_thumb_program_fk FOREIGN KEY (program_id) REFERENCES programs(id)")
    change_column(:program_thumbs, :program_id, :integer, :null => false)
  end
end
