class RenameFacilityPackagesToServicePackages < ActiveRecord::Migration
  def up
    rename_table :facilities_packages, :packages_services
    rename_column(:packages_services, :facility_id, :service_id)
  end

  def down
    rename_table :packages_services, :facilities_packages
    rename_column(:facilities_packages, :service_id, :facility_id)
  end
end
