class AddLastEmailStatusFieldsToUser < ActiveRecord::Migration
  def change
    add_column :users, :email_verified_at, :datetime
    add_column :users, :last_email_status, :string
    add_column :users, :last_email_bounce_description, :string
  end
end
