class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name
      t.string :postcode
      t.string :suburb
      t.string :country
      t.string :state

      t.timestamps
    end
  end
end
