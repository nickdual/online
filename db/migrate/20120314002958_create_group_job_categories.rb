class CreateGroupJobCategories < ActiveRecord::Migration
  def change
    create_table :group_job_categories do |t|
      t.string :name
      t.integer :group_id
      t.integer :job_category_id

      t.timestamps
    end
    
    add_foreign_key(:group_job_categories, :groups)
    add_foreign_key(:group_job_categories, :job_categories)
  end
end
