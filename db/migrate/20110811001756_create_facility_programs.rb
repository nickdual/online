class CreateFacilityPrograms < ActiveRecord::Migration
  def self.up
    execute("CREATE VIEW facility_programs AS
              SELECT packages_programs.program_id, facilities_packages.facility_id, facilities_packages.package_id, null as group_id FROM facilities_packages LEFT JOIN packages_programs ON packages_programs.package_id = facilities_packages.package_id LEFT JOIN programs ON programs.id = packages_programs.program_id WHERE programs.processing != true UNION
              SELECT packages_programs.program_id, facilities.id as facility_id, groups_packages.package_id, groups_packages.group_id FROM groups_packages LEFT JOIN facilities ON facilities.group_id = groups_packages.group_id LEFT JOIN packages_programs ON packages_programs.package_id = groups_packages.package_id LEFT JOIN programs ON programs.id = packages_programs.program_id WHERE programs.processing != true")
  end
  
  def self.down
    execute("DROP VIEW facility_programs")
  end
end
