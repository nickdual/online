class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :description
      t.string :file_uid
      t.string :file_name
      t.integer :program_id
      t.timestamps
    end
  end
end
