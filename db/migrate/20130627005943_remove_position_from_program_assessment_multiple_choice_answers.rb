class RemovePositionFromProgramAssessmentMultipleChoiceAnswers < ActiveRecord::Migration
  def up
    remove_column :program_assessment_multiple_choice_answers, :position
  end

  def down
    add_column :program_assessment_multiple_choice_answers, :position, :integer
  end
end
