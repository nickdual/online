class CreateGroupSessions < ActiveRecord::Migration
  def change
    create_table :group_sessions do |t|
      t.string :location
      t.datetime :viewed_at
      t.string :medium
      t.integer :facility_id
      t.timestamps
    end
  end
end
