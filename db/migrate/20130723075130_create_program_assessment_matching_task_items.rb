class CreateProgramAssessmentMatchingTaskItems < ActiveRecord::Migration
  def change
    create_table :program_assessment_matching_task_items do |t|
      t.belongs_to :matching_task_question
      t.string :text
      t.string :answer
      t.timestamps
    end
  end
end
