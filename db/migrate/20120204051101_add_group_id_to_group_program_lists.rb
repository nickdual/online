class AddGroupIdToGroupProgramLists < ActiveRecord::Migration
  def change
    add_column :group_program_lists, :group_id, :integer
  end
end
