class AddCreatedByIdAndUpdatedByIdToLearningResources < ActiveRecord::Migration
  def change
    add_column :learning_resources, :created_by_id, :integer
    add_column :learning_resources, :updated_by_id, :integer
  end
end
