class CreateServiceProgramAssessments < ActiveRecord::Migration
  def change
    create_table :service_program_assessments do |t|
      t.integer :service_id
      t.integer :program_id
      t.boolean :essentials, default: true
      t.boolean :extension
      t.boolean :evidence

      t.timestamps
    end
  end
end
