class CreateProgramAssessmentSequentialAnswerItems < ActiveRecord::Migration
  def change
    create_table :program_assessment_sequential_answer_items do |t|
      t.belongs_to :sequential_question
      t.string :text
      t.string :reference_image_uid
      t.string :reference_image_name
      t.integer :correct_position
      t.integer :position
      t.timestamps
    end
  end
end
