class AddDaysTillDueToTrainingPlans < ActiveRecord::Migration
  def change
    add_column :training_plans, :days_till_due, :integer
  end
end
