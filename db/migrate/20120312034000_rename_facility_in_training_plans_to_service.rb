class RenameFacilityInTrainingPlansToService < ActiveRecord::Migration
  def up
    rename_column(:learning_plans, :facility_id, :service_id)
  end

  def down
    rename_column(:learning_plans, :service_id, :facility_id)
  end
end
