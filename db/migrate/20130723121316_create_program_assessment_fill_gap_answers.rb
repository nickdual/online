class CreateProgramAssessmentFillGapAnswers < ActiveRecord::Migration
  def change
    create_table :program_assessment_fill_gap_answers do |t|
      t.belongs_to :fill_gap_question
      t.string :answer
      t.boolean :correct
      t.integer :position
      t.timestamps
    end
  end
end
