class AddExtensionToLearningPlanPrograms < ActiveRecord::Migration
  def change
    add_column :learning_plan_programs, :extension, :boolean

  end
end
