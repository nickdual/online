class AddVideoDescriptionIndex < ActiveRecord::Migration
  def up
    add_index :videos, :description
  end

  def down
    remove_index :videos, :description
  end
end
