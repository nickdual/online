class AddImageCaptionToProgramAssessmentTrueOrFalseQuestions < ActiveRecord::Migration
  def change
    add_column :program_assessment_true_or_false_questions, :image_caption, :string
  end
end
