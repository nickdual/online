class AddGroupSessionsProgramsJoin < ActiveRecord::Migration
  def up
    create_table :group_sessions_programs, :id => false do |t|
      t.integer :group_session_id
      t.integer :program_id
    end
  end

  def down
    drop_table :group_sessions_programs
  end
end
