class AddCoordinatorIdToProgramAssessments < ActiveRecord::Migration
  def change
    add_column :program_assessments, :coordinator_id, :integer
  end
end
