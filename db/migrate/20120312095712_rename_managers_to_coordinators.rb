class RenameManagersToCoordinators < ActiveRecord::Migration
  def up
    rename_table(:managers, :coordinators)
  end

  def down
    rename_table(:coordinators, :managers)
  end
end
