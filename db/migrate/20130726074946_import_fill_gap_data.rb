class ImportFillGapData < ActiveRecord::Migration
  def up
    unless Rails.env.production?
      load 'db/fixtures/30_program_assessment_fill_gap_question.rb'
    end
  end

  def down
    # Better to roll back manually since you have better control of the data.
  end
end
