class AddCreatedByIdAndUpdatedByIdToProgramCategories < ActiveRecord::Migration
  def change
    add_column :program_categories, :created_by_id, :integer
    add_column :program_categories, :updated_by_id, :integer
  end
end
