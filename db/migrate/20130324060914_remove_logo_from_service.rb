class RemoveLogoFromService < ActiveRecord::Migration
  def up
    remove_column :services, :logo_uid
    remove_column :services, :logo_name
  end

  def down
    add_column :services, :logo_uid,  :string
    add_column :services, :logo_name, :string
  end
end
