class AddIndexToJobsInDragonflyThumbs < ActiveRecord::Migration
  def change
    add_index :dragonfly_thumbs, :job
  end
end
