class AddProgramTypeIdToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :program_type_id, :integer
    add_foreign_key(:programs, :program_types)
  end
end
