class ChangeDueAtToDatetimeInEmploymentLearningPlans < ActiveRecord::Migration
  def up
    change_column :employment_learning_plans, :due_at, :datetime
  end

  def down
    change_column :employment_learning_plans, :due_at, :date
  end
end
