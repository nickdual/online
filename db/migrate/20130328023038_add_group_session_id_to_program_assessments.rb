class AddGroupSessionIdToProgramAssessments < ActiveRecord::Migration
  def change
    add_column :program_assessments, :group_session_id, :integer
  end
end
