class AddRoleToEmployments < ActiveRecord::Migration
  def change
    add_column :employments, :role, :string, :default => "staff"
  end
end
