class ChangeUserIdToEmploymentIdInUserTrainingPlans < ActiveRecord::Migration
  def up
    rename_column :user_training_plans, :user_id, :employment_id
  end

  def down
    rename_column :user_training_plans, :employment_id, :user_id
  end
end
