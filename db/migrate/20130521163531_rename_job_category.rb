class RenameJobCategory < ActiveRecord::Migration
  def change
    rename_table :group_job_categories, :group_job_titles
    rename_table :service_job_categories, :service_job_titles
    rename_column :employments, :job_category_id, :job_title_id
  end
end
