class AddCachedSlugToFacilities < ActiveRecord::Migration
  
  def self.up
    add_column :facilities, :cached_slug, :string
    add_index  :facilities, :cached_slug
  end

  def self.down
    remove_column :facilities, :cached_slug
  end
  
end
