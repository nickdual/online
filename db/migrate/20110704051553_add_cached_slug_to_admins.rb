class AddCachedSlugToAdmins < ActiveRecord::Migration
  
  def self.up
    add_column :admins, :cached_slug, :string
    add_index  :admins, :cached_slug
  end

  def self.down
    remove_column :admins, :cached_slug
  end
  
end
