class AddMissingIndexes < ActiveRecord::Migration
  def self.up
    add_index :admins, :created_by_id
    add_index :admins, :updated_by_id
    add_index :users, :created_by_id
    add_index :users, :updated_by_id
    add_index :services, :group_id
    add_index :services, :disabled_by_id
    add_index :services, :created_by_id
    add_index :services, :updated_by_id
    add_index :coordinators, :user_id
    add_index :coordinators, :group_id
    add_index :coordinators, [:user_id, :group_id]
    add_index :coordinators, [:group_id, :user_id]
    add_index :employments, [:user_id, :service_id]
    add_index :employments, [:service_id, :user_id]
    add_index :employments_group_sessions, [:group_session_id, :employment_id], name: "index_employments_group_sessions"
    add_index :employment_learning_plans, :learning_plan_id, name: "index_employment_learning_plans"
    add_index :employment_learning_plans, :employment_id
    add_index :employment_learning_plans, [:employment_id, :learning_plan_id], name: "index_employment_employment_learning_plans"
    add_index :groups_packages, [:package_id, :group_id]
    add_index :groups_packages, [:group_id, :package_id]
    add_index :groups, :disabled_by_id
    add_index :groups, :created_by_id
    add_index :groups, :updated_by_id
    add_index :group_job_categories, :group_id
    add_index :group_job_categories, :job_category_id
    add_index :group_job_categories, :parent_id
    add_index :group_program_lists, :program_id
    add_index :group_program_lists, [:group_id, :group_program_id]
    add_index :group_sessions, :service_id
    add_index :group_sessions_programs, [:program_id, :group_session_id], name: "index_group_sessions_programs"
    add_index :job_categories, :created_by_id
    add_index :job_categories, :updated_by_id
    add_index :learning_plans, :service_id
    add_index :learning_plans_programs, [:program_id, :learning_plan_id], name: "index_learning_plans_programs"
    add_index :learning_records, :program_id
    add_index :learning_records, :employment_id
    add_index :learning_resources, :created_by_id
    add_index :learning_resources, :updated_by_id
    add_index :learning_resources, :program_id
    add_index :learning_resource_downloads, :employment_id
    add_index :learning_resource_downloads, :learning_resource_id
    add_index :packages, :created_by_id
    add_index :packages, :updated_by_id
    add_index :packages_services, [:service_id, :package_id]
    add_index :package_programs, :package_id
    add_index :package_programs, :program_id
    add_index :package_programs, [:package_id, :program_id]
    add_index :package_program_lists, :package_program_id
    add_index :package_program_lists, :program_id
    add_index :programs, :created_by_id
    add_index :programs, :updated_by_id
    add_index :programs, :category_id
    add_index :programs, :program_type_id
    add_index :program_assessments, :learning_record_id
    add_index :program_assessments, :employment_id
    add_index :program_categories, :created_by_id
    add_index :program_categories, :updated_by_id
    add_index :program_thumbs, :program_id
    add_index :regions, :created_by_id
    add_index :regions, :updated_by_id
    add_index :regions, :group_id
    add_index :regions_services, [:service_id, :region_id]
    add_index :service_job_categories, :service_id
    add_index :service_job_categories, :job_category_id
    add_index :service_job_categories, :parent_id
    add_index :service_program_lists, :program_id
    add_index :service_program_lists, [:service_id, :service_program_id], name: "index_service_program_list"
    add_index :videos, :program_id
  end

  def self.down
    remove_index :admins, :created_by_id
    remove_index :admins, :updated_by_id
    remove_index :users, :created_by_id
    remove_index :users, :updated_by_id
    remove_index :services, :column => [:program_id, :user_id]
    remove_index :services, :group_id
    remove_index :services, :disabled_by_id
    remove_index :services, :created_by_id
    remove_index :services, :updated_by_id
    remove_index :coordinators, :user_id
    remove_index :coordinators, :group_id
    remove_index :coordinators, :column => [:user_id, :group_id]
    remove_index :employments, :user_id
    remove_index :employments, :service_id
    remove_index :employments, :column => [:user_id, :group_session_id]
    remove_index :employments, :column => [:user_id, :learning_plan_id]
    remove_index :employments, :column => [:user_id, :service_id]
    remove_index :employments_group_sessions, :column => [:group_session_id, :employment_id]
    remove_index :employment_learning_plans, :learning_plan_id
    remove_index :employment_learning_plans, :employment_id
    remove_index :employment_learning_plans, :column => [:employment_id, :learning_plan_id]
    remove_index :groups_packages, :column => [:package_id, :group_id]
    remove_index :groups, :disabled_by_id
    remove_index :groups, :created_by_id
    remove_index :groups, :updated_by_id
    remove_index :group_job_categories, :group_id
    remove_index :group_job_categories, :job_category_id
    remove_index :group_job_categories, :parent_id
    remove_index :group_program_lists, :program_id
    remove_index :group_program_lists, :column => [:group_id, :group_program_id]
    remove_index :group_sessions, :service_id
    remove_index :group_sessions_programs, :column => [:program_id, :group_session_id]
    remove_index :job_categories, :created_by_id
    remove_index :job_categories, :updated_by_id
    remove_index :learning_plans, :service_id
    remove_index :learning_plans_programs, :column => [:program_id, :learning_plan_id]
    remove_index :learning_records, :program_id
    remove_index :learning_records, :employment_id
    remove_index :learning_resources, :created_by_id
    remove_index :learning_resources, :updated_by_id
    remove_index :learning_resources, :program_id
    remove_index :learning_resource_downloads, :employment_id
    remove_index :learning_resource_downloads, :learning_resource_id
    remove_index :packages, :created_by_id
    remove_index :packages, :updated_by_id
    remove_index :packages_services, :column => [:service_id, :package_id]
    remove_index :package_programs, :package_id
    remove_index :package_programs, :program_id
    remove_index :package_programs, :column => [:package_id, :program_id]
    remove_index :package_program_lists, :package_program_id
    remove_index :package_program_lists, :program_id
    remove_index :programs, :created_by_id
    remove_index :programs, :updated_by_id
    remove_index :programs, :category_id
    remove_index :programs, :program_type_id
    remove_index :program_assessments, :learning_record_id
    remove_index :program_assessments, :employment_id
    remove_index :program_categories, :created_by_id
    remove_index :program_categories, :updated_by_id
    remove_index :program_thumbs, :program_id
    remove_index :regions, :created_by_id
    remove_index :regions, :updated_by_id
    remove_index :regions, :group_id
    remove_index :regions_services, :column => [:service_id, :region_id]
    remove_index :service_job_categories, :service_id
    remove_index :service_job_categories, :job_category_id
    remove_index :service_job_categories, :parent_id
    remove_index :service_program_lists, :program_id
    remove_index :service_program_lists, :column => [:service_id, :service_program_id]
    remove_index :videos, :program_id
  end
end