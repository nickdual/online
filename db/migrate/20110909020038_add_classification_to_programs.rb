class AddClassificationToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :classification, :string, :default => "program"
  end
end
