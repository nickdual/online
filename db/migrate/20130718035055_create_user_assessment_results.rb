class CreateUserAssessmentResults < ActiveRecord::Migration
  def change
    create_table :user_assessment_results do |t|
      t.integer :assessment_id, null: false
      t.integer :user_id, null: false
      t.boolean :passed, default: false
      t.integer :time_taken

      t.timestamps
    end
  end
end
