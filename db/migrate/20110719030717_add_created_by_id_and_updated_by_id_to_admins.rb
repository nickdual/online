class AddCreatedByIdAndUpdatedByIdToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :created_by_id, :integer
    add_column :admins, :updated_by_id, :integer
  end
end
