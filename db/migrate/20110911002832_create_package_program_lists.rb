class CreatePackageProgramLists < ActiveRecord::Migration
  def change
    create_table :package_program_lists do |t|
      t.integer :program_id
      t.integer :package_program_id

      t.timestamps
    end
  end
end
