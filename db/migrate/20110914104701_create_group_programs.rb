class CreateGroupPrograms < ActiveRecord::Migration
  def change
    execute("CREATE VIEW group_programs AS
              SELECT groups_packages.group_id, groups_packages.package_id, package_programs.program_id FROM groups_packages LEFT JOIN package_programs ON groups_packages.package_id = package_programs.package_id;")
  end
end
