class RenameFacilitiesToServices < ActiveRecord::Migration
  def up
    rename_table(:facilities, :services)
  end

  def down
    rename_table(:services, :facilities)
  end
end
