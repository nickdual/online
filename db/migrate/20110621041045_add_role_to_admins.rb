class AddRoleToAdmins < ActiveRecord::Migration
  def self.up
    add_column :admins, :role, :string
  end

  def self.down
    remove_column :admins, :role
  end
end
