class AddCreatedByIdToPackages < ActiveRecord::Migration
  def change
    add_column :packages, :created_by_id, :integer
  end
end
