class AddGroupSessionIdToLearningRecords < ActiveRecord::Migration
  def change
    add_column :learning_records, :group_session_id, :integer

  end
end
