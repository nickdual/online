class CreateProgramAssessmentMultipleChoiceAnswers < ActiveRecord::Migration
  def change
    create_table :program_assessment_multiple_choice_answers do |t|
      t.belongs_to :multiple_choice_question
      t.string :answer
      t.boolean :correct
      t.integer :position
      t.timestamps
    end
  end
end
