class AddDueAtToEmploymentTrainingPlans < ActiveRecord::Migration
  def change
    add_column :employment_training_plans, :due_at, :date

  end
end
