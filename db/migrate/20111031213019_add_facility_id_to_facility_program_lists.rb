class AddFacilityIdToFacilityProgramLists < ActiveRecord::Migration
  def change
    add_column :facility_program_lists, :facility_id, :integer
  end
end
