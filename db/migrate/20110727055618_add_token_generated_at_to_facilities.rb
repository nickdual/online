class AddTokenGeneratedAtToFacilities < ActiveRecord::Migration
  def change
    add_column :facilities, :token_generated_at, :datetime
  end
end
