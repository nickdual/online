class AddDefaultSectionsToAssessments < ActiveRecord::Migration
  def up
    Program::Assessment.find_each do |assessment|
      assessment.add_default_sections.each do |section|
        section.save!
      end
    end
  end

  def down
    # do nothing
  end
end
