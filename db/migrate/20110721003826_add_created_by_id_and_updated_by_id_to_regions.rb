class AddCreatedByIdAndUpdatedByIdToRegions < ActiveRecord::Migration
  def change
    add_column :regions, :created_by_id, :integer
    add_column :regions, :updated_by_id, :integer
  end
end
