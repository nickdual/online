class RenameFacilitatorsOnlyInLearningResourcesToCoordinatorsOnly < ActiveRecord::Migration
  def up
    rename_column(:learning_resources, :facilitators_only, :coordinators_only)
  end

  def down
    rename_column(:learning_resources, :coordinators_only, :facilitators_only)
  end
end
