class AddGroupIdToFacilities < ActiveRecord::Migration
  def change
    add_column :facilities, :group_id, :integer
  end
end
