class AddCachedSlugToTrainingPlans < ActiveRecord::Migration
  
  def self.up
    add_column :training_plans, :cached_slug, :string
    add_index  :training_plans, :cached_slug
  end

  def self.down
    remove_column :training_plans, :cached_slug
  end
  
end
