class RenameProgramsTrainingPlansToLearningPlansPrograms < ActiveRecord::Migration
  def up
    rename_table(:programs_training_plans, :learning_plans_programs)
    rename_column(:learning_plans_programs, :training_plan_id, :learning_plan_id)
  end

  def down
    rename_table(:learning_plans_programs, :programs_training_plans)
    rename_column(:programs_training_plans, :learning_plan_id, :training_plan_id)
  end
end
