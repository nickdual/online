class ChangeDurationInProgramsToString < ActiveRecord::Migration
  def up
    change_column :programs, :duration, :string
  end

  def down
    change_column :programs, :duration, :integer
  end
end
