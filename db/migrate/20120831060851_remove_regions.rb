class RemoveRegions < ActiveRecord::Migration
  def up
    drop_table :regions
    drop_table :regions_services # HABTM
  end

  def down
    throw ActiveRecord::IrreversibleMigration
  end
end
