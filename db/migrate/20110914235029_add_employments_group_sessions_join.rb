class AddEmploymentsGroupSessionsJoin < ActiveRecord::Migration
  def up
    create_table :employments_group_sessions, :id => false do |t|
      t.integer :employment_id
      t.integer :group_session_id
    end
  end

  def down
    drop_table :employments_group_sessions
  end
end
