class UpdateForeignKeyFromProgramAssessments < ActiveRecord::Migration
  def up
    execute("ALTER TABLE program_assessments DROP CONSTRAINT program_assessments_program_fk;")
    execute("ALTER TABLE program_assessments ADD CONSTRAINT program_assessments_training_record_fk FOREIGN KEY (training_record_id) REFERENCES training_records(id)")
  end

  def down
    execute("ALTER TABLE program_assessments DROP CONSTRAINT program_assessments_training_record_fk;")    
    execute("ALTER TABLE program_assessments ADD CONSTRAINT program_assessments_program_fk FOREIGN KEY (program_id) REFERENCES programs(id)")    
  end
end
