class AddEmploymentIdConstraintsToLearningResourceAssessments < ActiveRecord::Migration
  def change
    execute("ALTER TABLE learning_resource_assessments ADD CONSTRAINT learning_resource_assessments_employment_id_fk FOREIGN KEY (employment_id) REFERENCES employments(id)")
  end
end
