class RemoveGroupFields < ActiveRecord::Migration
  def up
    remove_column :groups, :postcode
    remove_column :groups, :suburb
    remove_column :groups, :state
  end

  def down
    throw ActiveRecord::IrreversibleMigration
  end
end
