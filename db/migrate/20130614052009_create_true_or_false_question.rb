class CreateTrueOrFalseQuestion < ActiveRecord::Migration
  def change
    create_table :program_assessment_true_or_false_questions do |t|
      t.string :question
      t.boolean :answer, default: true
      t.string :reference_image_uid
      t.string :reference_image_name
      t.belongs_to :section
      t.timestamps
    end
  end
end
