class AddCodecToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :codec, :string
  end
end
