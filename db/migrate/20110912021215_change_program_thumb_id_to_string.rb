class ChangeProgramThumbIdToString < ActiveRecord::Migration
  def up
    change_column :program_thumbs, :image_uid, :string
  end

  def down
    change_column :program_thumbs, :image_uid, :integer
  end
end
