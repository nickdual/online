class AddEmploymentIdConstraintToEmploymentTrainingPlans < ActiveRecord::Migration
  def change
    execute("ALTER TABLE employment_training_plans ADD CONSTRAINT employment_training_plans_employment_id_fk FOREIGN KEY (employment_id) REFERENCES employments(id)")
  end
end
