class AddCreatedByIdToFacilities < ActiveRecord::Migration
  def change
    add_column :facilities, :created_by_id, :integer
  end
end
