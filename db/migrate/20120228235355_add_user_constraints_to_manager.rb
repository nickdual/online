class AddUserConstraintsToManager < ActiveRecord::Migration
  def change
    change_column(:managers, :user_id, :integer, :null => false)
    change_column(:managers, :group_id, :integer, :null => false)

    add_foreign_key(:managers, :groups)
    add_foreign_key(:managers, :users)
  end
end
