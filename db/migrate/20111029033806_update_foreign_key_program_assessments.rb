class UpdateForeignKeyProgramAssessments < ActiveRecord::Migration
  def up
    execute("ALTER TABLE program_assessments DROP CONSTRAINT learning_resource_assessments_employment_id_fk")
    execute("ALTER TABLE program_assessments ADD CONSTRAINT program_assessments_employment_fk FOREIGN KEY (employment_id) REFERENCES employments(id)")
  end

  def down
    execute("ALTER TABLE program_assessments DROP CONSTRAINT program_assessments_employment_fk")
    execute("ALTER TABLE program_assessments ADD CONSTRAINT learning_resource_assessments_employment_id_fk FOREIGN KEY (employment_id) REFERENCES employments(id)")
  end
end
