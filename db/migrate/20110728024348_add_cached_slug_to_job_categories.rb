class AddCachedSlugToJobCategories < ActiveRecord::Migration
  
  def self.up
    add_column :job_categories, :cached_slug, :string
    add_index  :job_categories, :cached_slug
  end

  def self.down
    remove_column :job_categories, :cached_slug
  end
  
end
