class RemovePrimaryFromProgramThumbs < ActiveRecord::Migration
  def up
    ProgramThumb.where(primary: nil).each { |thumb| thumb.destroy }
    remove_column :program_thumbs, :primary
  end

  def down
    add_column :program_thumbs, :primary, :boolean
  end
end
