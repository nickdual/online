class RenameLearningResourceAssessmentsToProgramAssessments < ActiveRecord::Migration
  def up
    rename_table(:learning_resource_assessments, :program_assessments)
  end

  def down
    rename_table(:program_assessments, :learning_resource_assessments)
  end
end
