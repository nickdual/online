class RenameFacilitySubscriptionsToServiceSubscriptions < ActiveRecord::Migration
  def up
    execute("DROP VIEW facility_subscriptions;")
    
    execute("CREATE VIEW service_subscriptions AS
              SELECT service_id, package_id, null as group_id FROM packages_services UNION
              SELECT services.id as facility_id, groups_packages.package_id, groups_packages.group_id FROM groups_packages LEFT JOIN services ON services.group_id = groups_packages.group_id")
  end

  def down
    execute("DROP VIEW service_subscriptions;")
    
    execute("CREATE VIEW facility_subscriptions AS
              SELECT facility_id, package_id, null as group_id FROM facilities_packages UNION
              SELECT facilities.id as facility_id, groups_packages.package_id, groups_packages.group_id FROM groups_packages LEFT JOIN facilities ON facilities.group_id = groups_packages.group_id")
  end
end
