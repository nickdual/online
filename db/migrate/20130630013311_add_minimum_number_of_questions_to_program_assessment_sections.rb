class AddMinimumNumberOfQuestionsToProgramAssessmentSections < ActiveRecord::Migration
  def change
    change_table :program_assessment_sections do |t|
      t.integer :minimum_number_of_questions
    end
  end
end
