class AddUpdatedByIdToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :updated_by_id, :integer
  end
end
