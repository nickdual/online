class AddLearningOutcomeIdToProgramAssessmentQuestions < ActiveRecord::Migration
  def change
    change_table :program_assessment_multiple_choice_questions do |t|
      t.belongs_to :learning_outcome
    end

    change_table :program_assessment_true_or_false_questions do |t|
      t.belongs_to :learning_outcome
    end

    change_table :program_assessment_sequential_questions do |t|
      t.belongs_to :learning_outcome
    end
  end
end
