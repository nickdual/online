class RenameLearningResourceIdToProgramIdInProgramAssessments < ActiveRecord::Migration
  def up
    rename_column(:program_assessments, :learning_resource_id, :program_id)
  end

  def down
    rename_column(:program_assessments, :program_id, :learning_resource_id)
  end
end
