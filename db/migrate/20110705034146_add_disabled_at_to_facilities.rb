class AddDisabledAtToFacilities < ActiveRecord::Migration
  def change
    add_column :facilities, :disabled_at, :datetime
  end
end
