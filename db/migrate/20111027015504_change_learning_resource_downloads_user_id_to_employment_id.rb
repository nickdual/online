class ChangeLearningResourceDownloadsUserIdToEmploymentId < ActiveRecord::Migration
  def up
    rename_column(:learning_resource_downloads, :user_id, :employment_id)
  end

  def down
    rename_column(:learning_resource_downloads, :employment_id, :user_id)
  end
end
