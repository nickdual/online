class CreateUserAssessmentResultQuestions < ActiveRecord::Migration
  def change
    create_table :user_assessment_result_questions do |t|
      t.integer :assessment_result_id, null: false
      t.integer :question_id, null: false
      t.boolean :correct, default: false

      t.timestamps
    end
  end
end
