class AddCachedSlugToPackages < ActiveRecord::Migration
  
  def self.up
    add_column :packages, :cached_slug, :string
    add_index  :packages, :cached_slug
  end

  def self.down
    remove_column :packages, :cached_slug
  end
  
end
