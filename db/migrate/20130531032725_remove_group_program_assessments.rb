class RemoveGroupProgramAssessments < ActiveRecord::Migration
  def up
    drop_table :group_program_assessments
  end

  def down
    create_table :group_program_assessments do |t|
      t.integer :group_id
      t.integer :program_id
      t.boolean :essentials, default: true
      t.boolean :extension
      t.boolean :evidence

      t.timestamps
    end
  end
end
