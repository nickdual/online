class AddEvidenceToLearningPlanPrograms < ActiveRecord::Migration
  def change
    add_column :learning_plan_programs, :evidence, :boolean

  end
end
