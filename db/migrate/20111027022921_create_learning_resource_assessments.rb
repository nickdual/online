class CreateLearningResourceAssessments < ActiveRecord::Migration
  def change
    create_table :learning_resource_assessments do |t|
      t.integer :learning_resource_id
      t.integer :employment_id

      t.timestamps
    end
  end
end
