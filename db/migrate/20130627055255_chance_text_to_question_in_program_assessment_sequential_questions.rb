class ChanceTextToQuestionInProgramAssessmentSequentialQuestions < ActiveRecord::Migration
  def up
    rename_column :program_assessment_sequential_questions, :text, :question
  end

  def down
    rename_column :program_assessment_sequential_questions, :question, :text
  end
end
