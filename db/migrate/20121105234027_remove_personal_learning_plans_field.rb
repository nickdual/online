class RemovePersonalLearningPlansField < ActiveRecord::Migration
  def up
    remove_column :learning_plans, :personal
  end

  def down
    add_column :learning_plans, :personal, :boolean
  end
end
