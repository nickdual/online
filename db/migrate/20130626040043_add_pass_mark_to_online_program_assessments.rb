class AddPassMarkToOnlineProgramAssessments < ActiveRecord::Migration
  def change
    add_column :online_program_assessments, :pass_mark, :integer, null: false, default: 50
  end
end
