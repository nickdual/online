class AddProgramsProgramCategoriesJoin < ActiveRecord::Migration
  def self.up
    create_table :program_categories_programs, :id => false do |t|
      t.integer :program_id
      t.integer :program_category_id
    end
  end

  def self.down
    drop_table :program_categories_programs
  end
end
