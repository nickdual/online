class RenameFacilityToServiceInEmployments < ActiveRecord::Migration
  def up
    rename_column(:employments, :facility_id, :service_id)
  end

  def down
    rename_column(:employments, :service_id, :facility_id)
  end
end
