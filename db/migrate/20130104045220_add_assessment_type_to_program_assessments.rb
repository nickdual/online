class AddAssessmentTypeToProgramAssessments < ActiveRecord::Migration
  def change
    add_column :program_assessments, :assessment_type, :string, default: "Essentials"
  end
end
