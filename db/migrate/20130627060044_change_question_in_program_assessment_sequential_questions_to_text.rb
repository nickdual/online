class ChangeQuestionInProgramAssessmentSequentialQuestionsToText < ActiveRecord::Migration
  def up
    change_column :program_assessment_sequential_questions, :question, :text
  end

  def down
    change_column :program_assessment_sequential_questions, :question, :string
  end
end
