class RemoveJobCategories < ActiveRecord::Migration
  def up
    remove_column :service_job_categories, :job_category_id
    remove_column :service_job_categories, :parent_id
    remove_column :group_job_categories, :job_category_id
    remove_column :group_job_categories, :parent_id
    drop_table :job_categories
  end
end
