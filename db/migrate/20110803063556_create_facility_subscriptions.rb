class CreateFacilitySubscriptions < ActiveRecord::Migration
  def change
    execute("CREATE VIEW facility_subscriptions AS
              SELECT facility_id, package_id, null as group_id FROM facilities_packages UNION
              SELECT facilities.id as facility_id, groups_packages.package_id, groups_packages.group_id FROM groups_packages LEFT JOIN facilities ON facilities.group_id = groups_packages.group_id")
  end
end
