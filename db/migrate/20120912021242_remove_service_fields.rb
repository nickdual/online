class RemoveServiceFields < ActiveRecord::Migration
  def up
    remove_column :services, :beds
    remove_column :services, :hours
    remove_column :services, :address1
    remove_column :services, :address2
    remove_column :services, :suburb
    remove_column :services, :postcode
    remove_column :services, :state
  end

  def down
    throw ActiveRecord::IrreversibleMigration
  end
end
