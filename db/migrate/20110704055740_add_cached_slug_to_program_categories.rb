class AddCachedSlugToProgramCategories < ActiveRecord::Migration
  
  def self.up
    add_column :program_categories, :cached_slug, :string
    add_index  :program_categories, :cached_slug
  end

  def self.down
    remove_column :program_categories, :cached_slug
  end
  
end
