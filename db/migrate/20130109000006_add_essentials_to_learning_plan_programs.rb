class AddEssentialsToLearningPlanPrograms < ActiveRecord::Migration
  def change
    add_column :learning_plan_programs, :essentials, :boolean

  end
end
