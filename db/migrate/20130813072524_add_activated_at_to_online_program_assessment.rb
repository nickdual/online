class AddActivatedAtToOnlineProgramAssessment < ActiveRecord::Migration
  def change
    add_column :online_program_assessments, :activated_at, :datetime
  end
end
