class AddProgramsTrainingPlansJoin < ActiveRecord::Migration
  def self.up
    create_table :programs_training_plans, :id => false do |t|
      t.integer :program_id
      t.integer :training_plan_id
    end
  end

  def self.down
    drop_table :programs_training_plan
  end
end
