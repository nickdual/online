class AddForeignKeyConstraintsToLearningResourcesProductId < ActiveRecord::Migration
  def change
    execute("ALTER TABLE learning_resources ADD CONSTRAINT learning_resources_program_fk FOREIGN KEY (program_id) REFERENCES programs(id)")
    change_column(:learning_resources, :program_id, :integer, :null => false)
  end
end
