class RenameFacilityProgramsToServicePrograms < ActiveRecord::Migration
  def up
    execute("DROP VIEW facility_programs")
    
    execute("CREATE VIEW service_programs AS
              SELECT package_programs.program_id, packages_services.service_id, packages_services.package_id, null as group_id FROM packages_services LEFT JOIN package_programs ON package_programs.package_id = packages_services.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE programs.processing != true AND packages_services.service_id IS NOT NULL UNION
              SELECT package_programs.program_id, services.id as service_id, groups_packages.package_id, groups_packages.group_id FROM groups_packages LEFT JOIN services ON services.group_id = groups_packages.group_id LEFT JOIN package_programs ON package_programs.package_id = groups_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE programs.processing != true AND services.id IS NOT NULL")
  end

  def down
    execute("DROP VIEW service_programs")
    
    execute("CREATE VIEW facility_programs AS
              SELECT package_programs.program_id, facilities_packages.facility_id, facilities_packages.package_id, null as group_id FROM facilities_packages LEFT JOIN package_programs ON package_programs.package_id = facilities_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE programs.processing != true AND facilities_packages.facility_id IS NOT NULL UNION
              SELECT package_programs.program_id, facilities.id as facility_id, groups_packages.package_id, groups_packages.group_id FROM groups_packages LEFT JOIN facilities ON facilities.group_id = groups_packages.group_id LEFT JOIN package_programs ON package_programs.package_id = groups_packages.package_id LEFT JOIN programs ON programs.id = package_programs.program_id WHERE programs.processing != true AND facilities.id IS NOT NULL")
  end
end
