class AddCachedSlugToGroups < ActiveRecord::Migration
  
  def self.up
    add_column :groups, :cached_slug, :string
    add_index  :groups, :cached_slug
  end

  def self.down
    remove_column :groups, :cached_slug
  end
  
end
