class RenameTrainingRecordsInProgramAssessmentsToLearningRecords < ActiveRecord::Migration
  def up
    rename_column(:program_assessments, :training_record_id, :learning_record_id)
  end

  def down
    rename_column(:program_assessments, :learning_record_id, :training_record_id)
  end
end
