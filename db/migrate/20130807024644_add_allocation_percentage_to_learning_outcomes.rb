class AddAllocationPercentageToLearningOutcomes < ActiveRecord::Migration
  def change
    add_column :program_assessment_learning_outcomes, :allocation_percentage, :integer, default: 0
  end
end
