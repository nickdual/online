class CreateServiceJobCategories < ActiveRecord::Migration
  def change
    create_table :service_job_categories do |t|
      t.string :name
      t.integer :job_category_id
      t.integer :service_id

      t.timestamps
    end
    
    add_foreign_key(:service_job_categories, :services)
    add_foreign_key(:service_job_categories, :job_categories)
  end
end
