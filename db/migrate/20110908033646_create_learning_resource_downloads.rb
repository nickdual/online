class CreateLearningResourceDownloads < ActiveRecord::Migration
  def change
    create_table :learning_resource_downloads do |t|
      t.integer :user_id
      t.integer :learning_resource_id

      t.timestamps
    end
  end
end
