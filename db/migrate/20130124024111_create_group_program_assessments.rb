class CreateGroupProgramAssessments < ActiveRecord::Migration
  def change
    create_table :group_program_assessments do |t|
      t.integer :group_id
      t.integer :program_id
      t.boolean :essentials, default: true
      t.boolean :extension
      t.boolean :evidence

      t.timestamps
    end
  end
end
