class ChangeActiveAtInDatetimeInLearningPlansToDatetime < ActiveRecord::Migration
  def up
    change_column :learning_plans, :active_at, :datetime
  end

  def down
    change_column :learning_plans, :active_at, :date
  end
end
