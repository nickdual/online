class AddCreatedByIdToProgramAssessments < ActiveRecord::Migration
  def change
    add_column :online_program_assessments, :activated_by_id, :integer
  end
end