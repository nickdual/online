class AddDisabledByIdAndDisabledAtToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :disabled_by_id, :integer
    add_column :groups, :disabled_at, :datetime
  end
end
