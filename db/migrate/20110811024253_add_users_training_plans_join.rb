class AddUsersTrainingPlansJoin < ActiveRecord::Migration
  def self.up
    create_table :training_plans_users, :id => false do |t|
      t.integer :user_id
      t.integer :training_plan_id
    end
  end

  def self.down
    drop_table :training_plans_users
  end
end