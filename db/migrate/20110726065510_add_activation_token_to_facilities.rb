class AddActivationTokenToFacilities < ActiveRecord::Migration
  def change
    add_column :facilities, :activation_token, :string
  end
end
