class CreateUserTrainingPlans < ActiveRecord::Migration
  def change
    drop_table :training_plans_users
    create_table :user_training_plans do |t|
      t.integer :user_id
      t.integer :programs_watched, :default => 0
      t.integer :training_plan_id

      t.timestamps
    end
  end
end
