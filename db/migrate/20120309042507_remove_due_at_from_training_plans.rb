class RemoveDueAtFromTrainingPlans < ActiveRecord::Migration
  def up
    rename_table(:training_plans, :learning_plans)
    rename_column(:employment_training_plans, :training_plan_id, :learning_plan_id)
    LearningPlan.all.each do |plan|
      days = (plan.due_at - plan.created_at.to_date).to_i
      execute("UPDATE learning_plans SET days_till_due = '#{days}' WHERE id = '#{plan.id}'")
    end
    
    remove_column :learning_plans, :due_at
  end

  def down
    execute("UPDATE learning_plans SET days_till_due = null")
    add_column :learning_plans, :due_at, :datetime
    rename_table(:learning_plans, :training_plans)
    rename_column(:employment_training_plans, :learning_plan_id, :training_plan_id)
  end
end
