class AddDragonflyToFacilities < ActiveRecord::Migration
  def change
    add_column :facilities, :logo_uid,  :string
    add_column :facilities, :logo_name, :string
  end
end
