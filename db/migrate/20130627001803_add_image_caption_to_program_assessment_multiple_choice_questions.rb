class AddImageCaptionToProgramAssessmentMultipleChoiceQuestions < ActiveRecord::Migration
  def change
    add_column :program_assessment_multiple_choice_questions, :image_caption, :string
  end
end
