class AddQuestionTypeToUserAssessmentResultQuestions < ActiveRecord::Migration
  def change
    add_column :user_assessment_result_questions, :question_type, :string
  end
end
