class AddEssentialsToGroupSessions < ActiveRecord::Migration
  def change
    add_column :group_sessions, :essentials, :boolean
  end
end
