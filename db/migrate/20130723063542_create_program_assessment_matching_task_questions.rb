class CreateProgramAssessmentMatchingTaskQuestions < ActiveRecord::Migration
  def change
    create_table "program_assessment_matching_task_questions" do |t|
      t.integer  "section_id"
      t.integer  "learning_outcome_id"
      t.text     "directions"
      t.timestamps
    end
  end
end
