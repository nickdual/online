User.seed(:email) do |user|
  user.first_name = 'John'
  user.last_name = 'Doe'
  user.email = "johndoe@example.com"
  user.password = "password"
  user.password_confirmation = "password"
end
  