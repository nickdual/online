program = Program.find_by_code("01-110824")

if program.present?
  assessments = Program::Assessment.seed(:name) do |assessment|
    assessment.program_id = program.id
    assessment.name = 'Programming Languages'
    assessment.time_allowed = 60
  end

  sections = Program::Assessment::Section.seed(:name,
    { name: 'Fill Gap',
      question_type: 'Fill Gap',
      assessment_id: assessments.first.id,
      minimum_number_of_questions: 1
    }
  )

  learning_outcomes = Program::Assessment::LearningOutcome.seed(:description,
    { assessment_id: assessments.first.id,
      description: 'Ability to program in any language',
    }
  )

  fill_gap_questions = Program::Assessment::FillGapQuestion.seed(:question,
    { section_id: sections.first.id,
      learning_outcome_id: learning_outcomes.first.id,
      question: 'The oldest programming language is %% originally developed by IBM',
    }
  )

  fill_gap_answers = Program::Assessment::FillGapAnswer.seed(:answer,
    { fill_gap_question_id: fill_gap_questions.first.id,
      answer: 'PASCAL',
      correct: false
    },
    { fill_gap_question_id: fill_gap_questions.first.id,
      answer: 'PROLOG',
      correct: false
    },
    { fill_gap_question_id: fill_gap_questions.first.id,
      answer: 'BASIC',
      correct: false
    },
    { fill_gap_question_id: fill_gap_questions.first.id,
      answer: 'FORTRAN',
      correct: true
    },
  )
end
