Admin.seed(:email) do |admin|
  admin.first_name = 'John'
  admin.last_name = 'Doe'
  admin.email = "johndoe@example.com"
  admin.password = "password"
  admin.password_confirmation = "password"
  admin.role = 'administrator'
end
  