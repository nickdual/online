# Aged Care Channel Online

This is a Backbone / Ruby on Rails web app to serve Aged Care Channel's learning videos online.

## Getting started

To get started you'll need things like:
*  RVM (or some form of Ruby 1.9.3)
*  Rails (and bundler for everything in Gemfile)
*  PostgreSQL (database)
*  Redis (database)

    bundle install
    cp config/database.example.yml config/database.yml
    cp spec/zendesk_config.example.yml spec/zendesk_config.yml

And edit your config/database.yml configuration to match your local setup.

    rake db:setup
    rails s
    bundle exec sidekiq # If you want to run redis / mailers

Then it's something like:

    rake db:test:clone
    rspec spec && cucumber

## Useful notes

* Checkout stuff in docs/*.md
* Working with Backbone applications (style guide) https://gist.github.com/anathematic/75a03b58999861c76116
* Working with Ruby applications (style guide) https://github.com/bbatsov/ruby-style-guide
