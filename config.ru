# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)
require 'sidekiq'
# require 'split/dashboard'

Sidekiq.configure_client do |config|
  config.redis = { :size => 1 }
end

require 'sidekiq/web'

# Set the AUTH env variable to your basic auth password to protect sidekiq and split.
DEV_USERNAME = "nananana"
DEV_PASSWORD = "batman"
if DEV_USERNAME && DEV_PASSWORD
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == DEV_USERNAME
    password == DEV_PASSWORD
  end
end

run Rack::URLMap.new \
  '/'       => AccOnline::Application,
  '/sidekiq' => Sidekiq::Web.new
