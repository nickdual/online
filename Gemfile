source 'http://rubygems.org'

ruby '1.9.3'

# Root application state
gem "rails", "~> 3.2.11"
gem 'pg'
gem 'rake', '0.9.2'
gem "rack", "1.4.1"
gem "dalli", "~> 2.6.2"
gem "memcachier", "~> 0.0.2"

# Assets related
group :assets do
  gem 'sass-rails', "3.2.4"
  gem "compass-rails", "~> 1.0.0.rc.3"
  gem 'coffee-rails', "3.2.2"
  gem 'uglifier', "1.1.0"

  gem 'therubyracer'

  gem "bootstrap-sass", "~> 2.3.1.3"
  gem "asset_sync", "~> 0.5.4"

  gem "rails-backbone", "~> 0.9.10"
  gem 'execjs'
  gem "haml_coffee_assets", "~> 1.8.2"
end

gem 'cocoon' # Easier handling of nested forms
gem "multi_json", "~> 1.7.2"
gem "oj", "~> 2.0.12"

gem "simple_form"
gem "formtastic", "2.0.0"
gem "jquery-rails", "~> 2.1.3"
gem "haml", "~> 3.1.4"
gem 'roadie'
gem "nokogiri"
gem "state_machine"

gem "gibbon", "~> 0.4.5"

# Database specific
gem "rails_sql_views", :git => "git://github.com/anathematic/rails_sql_views.git"
gem 'foreigner'
gem "composite_primary_keys", "~> 5.0.12"
gem "seed-fu" # Inserting and maintaining seed data


# Application stack for production
gem "bugsnag", "~> 1.3.5"
gem 'heroku'
gem "oink"
gem 'newrelic_rpm'
gem "rack-timeout"
gem "unicorn"

# 3rd Party API
gem "zendesk_remote_auth", :git => "git://github.com/anathematic/zendesk_remote_auth.git"
gem "zendesk_client", :git => "git://github.com/TerraCycleUS/zendesk_client.git"
gem "doc_raptor"
gem "zencoder", :git => "git://github.com/anathematic/zencoder-rb.git"

# Standard Rails stuff / controller and M specific
gem "devise", "~> 2.2.4"
gem "cancan", "~> 1.6.7"
gem "inherited_resources", "~> 1.3.0"
gem "rack-cache", :require => "rack/cache"
gem "dragonfly", "~> 0.9.15"
gem "fog", "~> 1.5.0"
gem "validates_timeliness", "3.0.8"
gem "active_model_serializers", git: "git://github.com/rails-api/active_model_serializers.git"
gem "responders"
gem "slugged", :git => "git://github.com/Sutto/slugged.git"
gem "ancestry"
gem "breadcrumbs_on_rails"
gem 'will_paginate'

# Queueing system
gem "sidekiq", "~> 2.0.3"
gem "slim", "~> 1.2.2"
gem "sinatra", "~> 1.3.2"

# Other
gem "stringex"
gem "whenever"
gem "mime-types"
gem "i18n"
gem "acts_as_list"

# Search
# IMPORTANT: Manually require textacular and ransack because they both add #search
# method to ActiveRecord::Base. See config/initializers/search.rb
gem "ransack", require: false
gem 'textacular', require: false

group :test do
  gem 'fuubar' # The instafailing RSpec progress bar formatter
  gem 'json_expressions' # JSON matchmaking for all your API testing needs
  gem 'rspec-fire'  # Checks the existence of a mocked method if the doubled class has already been loaded.
  gem 'rspec-example_steps' # Given/When/Then steps for RSpec examples
  gem "webmock"
  gem "vcr"

  # home-grown gem for testing the validity of a mock. Hopefully, can replace rspec-fire.
  gem 'verified_double'

end

group :development, :test do
  gem "lol_dba"
  gem "rack-test", "0.6.1"
  gem "machinist", ">=2.0.0.beta2"
  gem "zencoder-fetcher"
  gem "cucumber-rails"
  gem "rspec-rails", "~> 2.12.0"
  gem "capybara"
  gem "selenium-webdriver", "~> 2.35"
  gem "launchy"
  gem "database_cleaner", "~> 1.0.1"
  gem "email_spec"
  gem "allenwei-railroad", :git => "git://github.com/robsonmwoc/railroad.git"
  gem "delorean"
  gem "forgery", "0.3.12"
  gem "thin"
  gem "connection_pool", "~> 0.9.0"
  gem "quiet_assets", "~> 1.0.2"
  gem "pry"
  gem "pry-debugger" # Adds step, next, finish, and continue commands and breakpoints to Pry using debugger.
  gem 'jasmine-rails' # A Jasmine runner for rails projects that's got you covered in both the terminal and the browser
end

group :development do
  gem "pry"
  # gem "pry-debugger" # Adds step, next, finish, and continue commands and breakpoints to Pry using debugger.
end
