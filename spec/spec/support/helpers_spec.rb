require 'spec_helper'

describe "serializable_active_record_double(active_record_class_name, attributes)" do
  let(:active_record_class_name){ 'Group' }

  let(:attributes){
    {
      id: 100,
      name: 'name',
    }
  }

  subject { serializable_active_record_double(active_record_class_name, attributes) }

  it "should stub the read_attribute_for_serialization methods of active_record_class_name" do
    expect(subject.read_attribute_for_serialization(:id)).to eq(attributes[:id])
    expect(subject.read_attribute_for_serialization(:name)).to eq(attributes[:name])
  end

  it "should raise an error if the attribute read for serialization is not defined" do
    expect { subject.read_attribute_for_serialization(:address) }.to raise_error("serializable_active_record_double Group received unexpected message :address")
  end
end

describe "json_representable_double(class_name, attributes).as_json" do
  class Dummy
    attr_accessor :id, :date
  end

  context "without attributes" do
    subject { json_representable_double('Dummy') }
    it "returns {}" do
      expect(subject.as_json).to eq({})
    end
  end

  context "with attributes" do
    let(:attributes){ { id: 35 } }
    subject { json_representable_double('Dummy', attributes) }

    it "returns the attributes" do
      expect(subject.as_json).to match_json_expression(attributes)
    end
  end

  context "with time attributes" do
    let(:time) { Time.now }
    let(:attributes){ { date: time } }

    subject { json_representable_double('Dummy', attributes) }

    it "converts time attributes to json (for easier json expression matching)" do
      expect(time.as_json).to be_a(String)
      expect(subject.as_json).to match_json_expression({ date: time.as_json })
    end
  end

  it "uses attributes[:id] as its params" do
    expect(json_representable_double('Dummy', id: 1).to_param).to eq(1)
  end
end
