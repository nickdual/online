require 'spec_helper'

RSpec.configure do |config|
  config.after :suite do
    VerifiedDouble.report_unverified_signatures(self)
  end
end
