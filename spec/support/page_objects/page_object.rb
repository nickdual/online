class PageObject
  attr_reader :page

  def initialize(page, options = {})
    @page = page
    options.each do |key, value|
      send "#{key}=", value
    end
  end
end

