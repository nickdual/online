class AdminProgramAssessmentPage < PageObject
  def sections
    page.all('.program_assessment_section')
  end
end
