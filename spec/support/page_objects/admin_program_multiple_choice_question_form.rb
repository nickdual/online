class AdminProgramMultipleChoiceQuestionForm < PageObject
  def fill(attributes = {})
    q = Program::Assessment::MultipleChoiceQuestion.make(attributes)

    page.within 'form[id$=program_assessment_multiple_choice_question]' do
      page.find('[id$=question]').set q.question
      page.find('[id$=learning_outcome_id]').find("option[value='#{q.learning_outcome_id}']").select_option
      page.find('[id$=reference_image]').set q.reference_image.path
    end
  end

  def save_and_add_another_question
    page.find_button(I18n.t(:save_and_add_another_question)).click
  end
end
