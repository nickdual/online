class NewAdminProgramFillGapQuestionPage < SimpleDelegator
  attr_accessor :section

  def form
    @form ||= AdminProgramFillGapQuestionForm.new(self)
  end

  def visit!(section)
    visit new_polymorphic_url([:admin, section, Program::Assessment::FillGapQuestion])
  end
end
