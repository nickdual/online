class AdminProgramTrueOrFalseQuestionForm < PageObject
  def fill(attributes = {})
    q = Program::Assessment::TrueOrFalseQuestion.make(attributes)

    page.find_field('program_assessment_true_or_false_question_learning_outcome_id').find("option[value='#{q.learning_outcome_id}']").select_option

    page.find_field('program_assessment_true_or_false_question_question').set q.question
    page.find_field("program_assessment_true_or_false_question_answer_#{q.answer}").click
    page.find_field('program_assessment_true_or_false_question_reference_image').set q.reference_image.path
  end

  def save_and_add_another_question
    page.find_button(I18n.t(:save_and_add_another_question)).click
  end
end

