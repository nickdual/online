class NewAdminProgramMatchingTaskQuestionPage < SimpleDelegator
  def form
    @form ||= AdminProgramMatchingTaskQuestionForm.new(self)
  end

  def visit!(section)
    visit new_polymorphic_url([:admin, section, Program::Assessment::MatchingTaskQuestion])
  end
end
