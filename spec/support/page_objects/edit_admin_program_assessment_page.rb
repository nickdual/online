class EditAdminProgramAssessmentPage < SimpleDelegator
  attr_accessor :assessment

  def visit!
    visit edit_polymorphic_path([:admin, assessment.program, assessment])
  end

  def publish_button
    find("input[type=submit][value=#{I18n.t('program.assessment.activate')}]")
  end
end
