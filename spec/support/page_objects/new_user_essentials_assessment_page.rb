class NewUserEssentialsAssessmentPage < SimpleDelegator
  def start
    find("[role=next]").click
  end
end
