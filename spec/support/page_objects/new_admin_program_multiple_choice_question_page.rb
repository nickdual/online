class NewAdminProgramMultipleChoiceQuestionPage < PageObject
  attr_accessor :section

  def form
    @form ||= AdminProgramMultipleChoiceQuestionForm.new(page)
  end

  def visit
    page.visit page.new_polymorphic_url([:admin, section, Program::Assessment::MultipleChoiceQuestion])
  end
end
