class TrueOrFalseQuestionPage < SimpleDelegator
  def submit(answer)
    within "#questions" do
      choose answer.to_s.titleize
    end

    find("[type=submit]").click
    find("[role=next]").click
  end
end
