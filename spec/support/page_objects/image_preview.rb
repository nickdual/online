class ImagePreview < SimpleDelegator
  PLACEHOLDER_URL = "http://placehold.it/539x302"

  def self.encode64(image_path)
    Base64.encode64(File.open(image_path, 'rb').read).gsub("\n", '')
  end

  def delete_action
    find('[data-ImagePreview-DeleteAction]')
  end

  def image_input
    find('[data-ImagePreview-ImageInput]')
  end
  
  def preview
    find('[data-ImagePreview-preview]')
  end

  def remove_image_input
    find('[data-ImagePreview-RemoveImageInput]')
  end

  def visit!
    visit admin_style_guide_path('image_preview')
  end
end
