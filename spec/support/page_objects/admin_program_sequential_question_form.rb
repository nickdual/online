class AdminProgramSequentialQuestionForm < PageObject
  def fill(attributes = {})
    q = Program::Assessment::SequentialQuestion.make(attributes)

    page.within 'form[id$=program_assessment_sequential_question]' do
      page.find('[id$=question]').set q.question
      page.find('[id$=learning_outcome_id]').find("option[value='#{q.learning_outcome_id}']").select_option

      page.all("[name$='[text]']").each_with_index do |answer_item_text_field, i|
        answer_item_text_field.set i.to_s
      end
    end
  end

  def save_and_add_another_question
    page.find_button(I18n.t(:save_and_add_another_question)).click
  end
end
