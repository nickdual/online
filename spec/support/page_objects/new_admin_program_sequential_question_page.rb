class NewAdminProgramSequentialQuestionPage < PageObject
  attr_accessor :section

  def form
    @form ||= AdminProgramSequentialQuestionForm.new(page)
  end

  def visit
    page.visit page.new_polymorphic_url([:admin, section, Program::Assessment::SequentialQuestion])
  end
end
