class UserProgramPage < SimpleDelegator
  attr_accessor :program

  def visit!
    visit("/programs/#{program.id}")
  end

  def start_assessment_button
    find("[role=start-assessment]")
  end

  def essentials_assessment_section
    find('[data-essentials-assessment]')
  end

  def start_essentials_assessment
    start_assessment_button.click
  end
end
