class AdminProgramFillGapQuestionForm < SimpleDelegator
  def fill(attributes = {})
    q = Program::Assessment::FillGapQuestion.make(attributes)

    within 'form[id$=program_assessment_fill_gap_question]' do
      find('[id$=question]').set q.question
      find('[id$=learning_outcome_id]').find("option[value='#{q.learning_outcome_id}']").select_option
    end
  end

  def save_and_add_another_question
    find_button(I18n.t(:save_and_add_another_question)).click
  end
end
