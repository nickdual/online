class UserLearningPlanPage < SimpleDelegator
  def visit!(employment_learning_plan)
    visit "/my-learning/plan/#{employment_learning_plan.id}"
  end

  def pending_program
    Program.new(find("#pendingprogramslist .pending-programs"))
  end

  class Program < SimpleDelegator
    def essentials_assessment
      Assessment.new(find('[data-essentials-assessment]'))
    end

    class Assessment < SimpleDelegator
      def badge
        find('.badge')
      end
    end
  end
end
