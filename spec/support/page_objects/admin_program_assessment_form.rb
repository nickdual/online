class AdminProgramAssessmentForm < PageObject
  def fill(o = {})
    default = Program::Assessment.make

    page.find('[name$="[name]"]').set o[:name] || default.name
    page.find('[name$="[time_allowed]"]').set o[:time_allowed] || default.time_allowed
    page.find('[name$="[pass_mark]"]').set o[:pass_mark] || default.pass_mark

    sections.each do |section|
      section.minimum_number_of_questions_field.set 0
    end
  end

  def sections
    page.all('.program_assessment_section').map {|div| SectionDiv.new(div) }
  end

  def submit
    page.find("input[type=submit]").click
  end

  class SectionDiv < PageObject
    def minimum_number_of_questions_field
      page.find("[name$='[minimum_number_of_questions]']")
    end
  end
end
