class NewAdminProgramTrueOrFalseQuestionPage < PageObject
  attr_accessor :section

  def form
    @form ||= AdminProgramTrueOrFalseQuestionForm.new(page)
  end

  def visit
    page.visit page.new_polymorphic_url([:admin, section, Program::Assessment::TrueOrFalseQuestion])
  end
end

