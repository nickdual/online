class Fixture
  def initialize(*args)
    args.each do |arg|
      send arg
    end
  end

  def employment
    @employment ||= service.employments.create!(user: user)
  end

  def employment_learning_plan
    employment
    learning_plan
    @employment_learning_plan ||= user.employment_learning_plans.first.tap do |elp|
      elp.status = 'active'
      elp.save!
    end
  end

  def learning_plan
    @learning_plan ||= LearningPlan.create!(
      :service_id => service.id,
      :name => "Learning Plan",
      :description => 'Learning plan description',
      :days_till_due => 5,
      :active_at => 2.days.ago,
      :employments => [employment],
      :programs => [program])
  end

  def learning_plan_program
    return @learning_plan_program if @learning_plan_program

    @learning_plan_program = LearningPlanProgram.where(
      program_id: program,
      learning_plan_id: learning_plan).first

    if @learning_plan_program.present?
      @learning_plan_program.essentials = true
      @learning_plan_program.save!
    else
      @learning_plan_program = LearningPlanProgram.create!(
        program: program,
        learning_plan: learning_plan,
        essentials: true)
    end

    @learning_plan_program
  end

  def learning_outcome
    @learning_outcome ||= program_assessment.learning_outcomes
      .create!(description: 'Ability to learn as an outcome')
  end

  def matching_task_section
    @matching_task_section ||= create_section(Program::Assessment::MatchingTaskQuestion)
  end

  def matching_task_question
    @matching_task_question ||= create_question(matching_task_section) do
      answer_options = [
        Program::Assessment::MatchingTaskItem.new(text: "One", answer: "First"),
        Program::Assessment::MatchingTaskItem.new(text: "Two", answer: "Second"),
        Program::Assessment::MatchingTaskItem.new(text: "Three", answer: "Third")]

      matching_task_section.matching_task_questions.create!(
        learning_outcome: learning_outcome,
        directions: "Match numbers",
        items: answer_options)
    end
  end

  def multiple_choice_section
    @multiple_choice_section ||= create_section(Program::Assessment::MultipleChoiceQuestion)
  end

  def multiple_choice_question
    @multiple_choice_question ||= create_question(multiple_choice_section) do
      answer_options = [
        Program::Assessment::MultipleChoiceAnswer.new(answer: "Blue", correct: true),
        Program::Assessment::MultipleChoiceAnswer.new(answer: "Yellow", correct: false),
        Program::Assessment::MultipleChoiceAnswer.new(answer: "Green", correct: false)]

      multiple_choice_section.multiple_choice_questions.create!(
        learning_outcome: learning_outcome,
        question: "What colour is the ocean?",
        answers: answer_options)
    end
  end

  def package
    @package ||= Package.make!
  end

  def program
    @program ||= Program.make!(:processed, title: "Fire Safety").tap do |program|
      package.programs = [program]
      package.save!
    end
  end

  def program_assessment
    @program_assessment ||= program.assessments.first.tap do |assessment|
      assessment.time_allowed = 60
      assessment.activated_at = Time.now
      assessment.save!
    end
  end

  def section
    @section ||= create_section(Program::Assessment::TrueOrFalseQuestion)
  end

  def service
    @service ||= Service.make!.tap do |service|
      package.services = [service]
      package.save!
    end
  end

  def user
    @user ||= User.make!
  end

  def sequence_section
    @sequence_section ||= create_section(Program::Assessment::SequentialQuestion)
  end

  def sequence_question
    @sequence_question ||= create_question(sequence_section) do
      answer_options = [
        Program::Assessment::SequentialAnswerItem.new(text: "Three", position: 2, correct_position: 3),
        Program::Assessment::SequentialAnswerItem.new(text: "One", position: 3, correct_position: 1),
        Program::Assessment::SequentialAnswerItem.new(text: "Two", position: 1, correct_position: 2)]

      sequence_section.sequential_questions.create!(
        learning_outcome: learning_outcome,
        question: "What colour is the ocean?",
        answer_items: answer_options,
        question_type: "Text")
    end
  end

  def true_or_false_question
    @true_or_false_question ||= create_question(section) do
      section.true_or_false_questions.create!(
        learning_outcome: learning_outcome,
        question: "The sky is blue",
        answer: true)
    end
  end

  private

  def create_question(section)
    question = yield
    section.minimum_number_of_questions = 1
    section.save!
    question
  end

  def create_section(question_class)
    section = program_assessment.sections.detect{|sequence_section|
      sequence_section.question_type == question_class.question_type}
    section.minimum_number_of_questions = 0
    section.save!
    section
  end
end
