def stub_zencoder
  Zencoder::Job.stub(:create).and_return(OpenStruct.new(:body => {"id" => "1"}, :errors => []))
  thumbnails = {"group_label"=>"screenshot", "format"=>"jpg", "created_at"=>"2011-09-07T16:01:43+10:00", "updated_at"=>"2011-09-07T16:01:43+10:00", "url"=>"http://acc.dev/rails.png", "id"=>19502144, "height"=>240, "file_size_bytes"=>8770, "width"=>320}
  
  zencoder_job = OpenStruct.new(:body => { "job" => {"input_media_file" => {"width" => 320, "height" => 240}}})
  Zencoder::Job.stub(:details).and_return(zencoder_job)
    
  output = OpenStruct.new(:body => {"height" => "1234", "width" => "12345"})
  Zencoder::Output.stub(:details).and_return(output)
  
  Video.any_instance.stub(:remote_url).and_return("http://accprogramstaging.s3.amazonaws.com/2011/09/07/07/32/09/595/meow.mp4")  
end