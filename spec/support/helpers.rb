def create_service
  VCR.use_cassette "create_service" do
    Service.make!
  end
end

def create_employment(service = Service.make!, user = User.make!)
  @user = user
  @service = service
  @employment = Employment.create!(:user => @user, :service => @service)
end

def json_representable_double(class_name, attributes = {})
  attributes = HashWithIndifferentAccess.new(attributes)
  fire_double(class_name, attributes.merge(
    as_json: attributes.as_json,
    to_param: attributes[:id]))
end

def login_as_admin
  @admin = Admin.make!(:administrator)
  visit admin_path

  fill_in "Email", with: @admin.email
  fill_in "Password", with: "123456"
  click_button "Sign in"
  page.should have_content("Signed in successfully.")
end

def login_as_group_coordinator
  @user = User.make!
  @group = Group.make!
  @service = Service.make!(group: @group)
  @group.coordinators.create!(user: @user)

  visit root_path

  fill_in "Email", with: @user.email
  fill_in "Password", with: "123456"
  click_button "Sign In"
  page.should have_content("Welcome, #{@user.name}")
end

def login_as_coordinator
  @user = User.make!
  @service = Service.make!
  @employment = Employment.create!(user: @user, service: @service, role: "coordinator")

  visit root_path

  fill_in "Email", with: @user.email
  fill_in "Password", with: "123456"
  click_button "Sign In"
  page.should have_content("Welcome, #{@user.name}")
end

def login_as_user
  create_user_and_service
  sign_in_to_login_page
end

def sign_in_to_login_page(user = @user)
  visit "/"
  fill_in "Email", with: user.email
  fill_in "Password", with: "123456"
  click_button "Sign In"
end

def create_user_and_service
  @user = User.make!
  @service = Service.make!
  @service.employments.create!(user: @user)
end

def create_program_and_assessment
  @program = Program.make!(:processed, title: "Fire Safety")
  @package = Package.make!(programs: [@program], services: [@service])

  @assessment = @program.assessments.first
  @assessment.time_allowed = 60
  @assessment.activated_at = Time.now
  @assessment.save!

  @assessment.learning_outcomes.create!(description: 'Ability to learn as an outcome')
end

def serializable_active_record_double(active_record_class_name, attributes = {})
  fire_double(active_record_class_name, attributes).tap do |d|
    d.stub(:read_attribute_for_serialization) do |arg|
      if attributes.keys.include?(arg)
        attributes[arg]
      else
        raise "serializable_active_record_double #{active_record_class_name} received unexpected message :#{arg}"
      end
    end
  end
end
