require 'vcr'
require 'webmock'

VCR.configure do |c|
  c.cassette_library_dir = "#{Rails.root}/spec/fixtures/cassette_library"
  c.hook_into :webmock
  c.default_cassette_options = { :record => :once }
  c.ignore_localhost         = true
end