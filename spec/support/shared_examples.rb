shared_examples 'a program assessment question' do
  describe "#possible_learning_outcomes" do
    let(:learning_outcome){
      Program::Assessment::LearningOutcome.new }

    let(:assessment){
      Program::Assessment.new(learning_outcomes: [learning_outcome]) }

    let(:section){
      Program::Assessment::Section.new(assessment: assessment) }

    let(:question){
      described_class.new(section: section) }

    subject { question.possible_learning_outcomes }

    it { should eq([learning_outcome]) }
  end

  describe "#question_id" do
    let(:id){ 2 }

    it "prefixes the id with the question type code name and pads the id with zero's" do
      subject.should_receive(:id).and_return(id)
      described_class.should_receive(:count).and_return(100)
      expect(subject.question_id).to eq(described_class.code_name + "00#{id}")
    end
  end

  describe ".question_type" do
    it "is implemented by the class" do
      expect(described_class.question_type).to be_a(String)
    end
  end

  describe ".code_name" do
    it "is implemented by the class" do
      expect(described_class.code_name).to be_a(String)
    end
  end

  context "where the question has a number of user assessment result questions" do
    let(:unsuccessful_results){
      VerifiedDouble.of_class('User::AssessmentResultQuestion') }

    let(:successful_results){
      VerifiedDouble.of_class('User::AssessmentResultQuestion') }

    let(:question_results){
      VerifiedDouble.of_class('User::AssessmentResultQuestion') }

    before do
      unsuccessful_results.stub(:count).and_return(2)

      subject.stub(:user_assessment_result_questions).and_return(question_results)
    end
    
    describe "#attempts_count" do
      it "is the number of user assessments results of the question" do
        question_results.should_receive(:count).and_return(3)
        expect(subject.attempts_count).to eq(3)
      end
    end

    describe "#successful_attempts_count" do
      it "is the number of correct user assessment results of the question" do
        question_results.should_receive(:correct).and_return(successful_results)
        successful_results.should_receive(:count).and_return(68)
        expect(subject.successful_attempts_count).to eq(68)
      end
    end

    describe "#unsuccessful_attempts_count" do
      it "is the number of incorrect user assessment results of the questions" do
        question_results.should_receive(:incorrect).and_return(unsuccessful_results)
        unsuccessful_results.should_receive(:count).and_return(76)
        expect(subject.unsuccessful_attempts_count).to eq(76)
      end
    end

    describe "#pass_rate" do
      it "is the ratio of successful attempts over attempts" do
        question_results.should_receive(:count).and_return(2)
        question_results.should_receive(:correct).and_return(successful_results)
        successful_results.should_receive(:count).and_return(1)

        expect(subject.pass_rate).to eq(0.5)
      end
    end
    
  end

end

shared_examples "serializer delegating its attributes to a model class" do |model_class|
  describe "._attributes.keys" do
    let(:model) { model_class.new }

    subject { described_class.new(model) }

    it "can be delegated to either #{model_class} or the serializer" do
      described_class._attributes.keys.each do |attribute|
        expect(model.respond_to?(attribute) || subject.respond_to?(attribute)).to be_true
      end

      described_class._associations.keys.each do |attribute|
        expect(model.respond_to?(attribute) || subject.respond_to?(attribute)).to be_true
      end
    end

    it "are symbols" do
      described_class._attributes.keys.each do |attribute|
        expect(attribute).to be_a(Symbol)
      end
    end

    it "is the active model serializer of #{model_class}" do
      expect(model_class.new.active_model_serializer).to eq(described_class)
    end
  end
end
