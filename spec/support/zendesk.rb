def setup_zendesk_connection
  unless ENV["ZENDESK_URL"]
    @zendesk_config = YAML.load_file "#{Rails.root}/spec/zendesk_config.yml"
  end

  @zendesk = Zendesk::Client.new do |config|
    config.account = ENV["ZENDESK_URL"] || @zendesk_config["url"]
    config.basic_auth (ENV["ZENDESK_USERNAME"] || @zendesk_config["username"]), (ENV["ZENDESK_PASSWORD"] || @zendesk_config["password"])
  end
end

def remove_last_zendesk_organization
  begin
    @zendesk.organizations(subject.last.id).delete if @service.persisted?
  rescue MultiJson::DecodeError
  end
end