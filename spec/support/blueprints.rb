require 'machinist/active_record'
include Rake::DSL

# Add your blueprints here.
#
# e.g.
#   Post.blueprint do
#     title { "Post #{sn}" }
#     body  { "Lorem ipsum..." }
#   end

Admin.blueprint do
  name { Forgery(:name).full_name }
  email { Forgery(:email).address }
  password { "123456" }
  password_confirmation { "123456" }
end

Admin.blueprint(:administrator) do
  role {"administrator"}
end

Coordinator.blueprint do
end

Employment.blueprint do
  service { Service.first }
  user
end

User.blueprint do
  name                  { Forgery(:name).full_name }
  email                 { Forgery(:email).address }
  password              { "123456" }
  password_confirmation { "123456" }
end

User.blueprint(:with_active_group) do
  groups { [Group.make(disabled_at: nil)] }
end

User.blueprint(:with_program) do
  employments { [Employment.make!(service: Service.make!(:with_program))] }
end

User::Invited.blueprint do
end

Group.blueprint do
  name      { Forgery(:address).city + " Home"}
end

GroupSession.blueprint do
  service
  created_by  { User.make! }
  medium      { "Online" }
  viewed_at   { Time.now }
end

Service.blueprint do
  name      { Forgery(:address).city + " Home"}
  country   { "Australia" }
end

Service.blueprint(:with_program) do
  packages { [Package.make(:programs => [Program.make(:processed)])] }
end

LearningResource.blueprint do
  name { Forgery(:name).full_name }
  resource { File.open("spec/fixtures/cat.jpg") }
  program
end

Program.blueprint do
  title { "Program " + Forgery(:basic).text }
  code { Forgery(:basic).text }
  description { Forgery(:lorem_ipsum).sentences(2) }
  classification { "program" }
end

Program::Assessment.blueprint do
  name { "Assessment #{Forgery(:basic).text}" }
  time_allowed { 1 }
  program
end

Program::Assessment::FillGapQuestion.blueprint do
  question { 'Warm is for %%' }
end

Program::Assessment::LearningOutcome.blueprint do
end

Program::Assessment::Section.blueprint do
  name { "Section #{Forgery(:basic).text}" }
  minimum_number_of_questions { 0 }
  question_type { "True/False" }
end

Program::Assessment::MatchingTaskQuestion.blueprint do
end

Program::Assessment::MultipleChoiceQuestion.blueprint do
  question { 'Is made of warm for sits?' }
  reference_image  { File.open("spec/fixtures/cat.jpg") }
end

Program::Assessment::SequentialQuestion.blueprint do
  question { 'Arrange the items correctly' }
end

Program::Assessment::TrueOrFalseQuestion.blueprint do
  question { 'Is made of warm for sits?' }
  answer { true }
  reference_image  { File.open("spec/fixtures/cat.jpg") }
end

Program.blueprint(:introduction) do
  classification { "introduction" }
end

Program.blueprint(:closer) do
  classification { "closer" }
end

Program.blueprint(:processed) do
  processing { false }
end

ProgramCategory.blueprint do
  name { "Program Category" + Forgery(:basic).text }
end

ProgramType.blueprint do
  name { "Program Type" + Forgery(:basic).text }
end

Package.blueprint do
  name { "Package " + Forgery(:basic).text }
end

UserProgram.blueprint do
  # Attributes here
end

ServiceProgram.blueprint do
  # Attributes here
end

LearningRecord.blueprint do
  program { Program.make! }
  employment { Employment.make! }
end

LearningPlan.blueprint do
  service
  name            { Forgery(:basic).text }
  days_till_due   { 5 }
  active_at       { Time.zone.today }
end

EmploymentLearningPlan.blueprint do
  learning_plan
  employment
end

ServiceProgramList.blueprint do
  # Attributes here
end

Group::JobTitle.blueprint do
  group
  name            { "Group Job Title " + Forgery(:basic).text }
end

Service::JobTitle.blueprint do
  service
  name            { "Service Job Title " + Forgery(:basic).text }
end

Video.blueprint {
  program
  file        { File.open("#{Rails.root}/spec/fixtures/meow.mp4") }
  description { Forgery(:lorem_ipsum).sentences(2) + rand(1000).to_s }
  zencoder_id { 1 }
}

ProgramAssessment.blueprint do
  employment
  program
  coordinator { User.make! }
end

User::AssessmentResult.blueprint do
  time_taken { 0 }
  # Attributes here
end

User::AssessmentResultQuestion.blueprint do
  # Attributes here
end
