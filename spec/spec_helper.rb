# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/example_steps'
require 'rspec/fire'
require 'rspec/rails'
require "email_spec"
require 'json_expressions/rspec'
require 'sidekiq'
require 'sidekiq/testing'
require 'sidekiq/testing/inline'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
require "support/page_objects/page_object"
Dir[Rails.root.join("spec/support/**/*.rb")].each {|f| require f}

RSpec.configure do |config|
  config.include(EmailSpec::Helpers)
  config.include(EmailSpec::Matchers)
  config.include Delorean
  config.include Devise::TestHelpers, :type => :controller
  config.include RSpec::Fire
  config.include VerifiedDouble::Matchers

  # == Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr
  config.mock_with :rspec
  config.extend VCR::RSpec::Macros
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  config.before(:each) do
    reset_mailer
    stub_zencoder

    # We don't have access to clear temp on CI
    unless ENV['SEMAPHORE']
      Rails.cache.clear
    end

    # Some things are setting the timezone everywhere, best to just make everything UTC
    Time.zone = "UTC"
  end

  config.after(:each) { back_to_the_present }
  config.after(:all) { Rails.cache.clear }

  ## Enable only if needed
  #config.after do
    #if example.exception
      #save_and_open_page
      #binding.pry
    #end
  #end

  config.before(:each) {
    DatabaseCleaner.strategy = :deletion
    DatabaseCleaner.start
  }

  config.after(:each) {
    DatabaseCleaner.clean
  }

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = false

end

RSpec::Fire.configure do |config|
  config.verify_constant_names = true
end
