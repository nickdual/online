require 'spec_helper'

describe "admin/programs/assessments/sections/sequential_questions/new.html.haml" do
  let(:resource){
    Program::Assessment::SequentialQuestion.make(
      answer_items: [Program::Assessment::SequentialAnswerItem.new],
      section: Program::Assessment::Section.make(
        assessment: Program::Assessment.make)) }

  let(:page){ render; Capybara.string(rendered) }
      
  it "implements the reference_image and remove_reference_image inputs as an ImagePreview" do
    view.stub(:resource_class){ Program::Assessment::SequentialQuestion }
    view.stub(:resource){ resource }
    view.stub(:child_class){ Program::Assessment::SequentialAnswerItem }
    view.stub(:url_for).and_return('')

    expect(page).to have_selector("div[data-imagepreview] input[type=file][data-imagepreview-imageinput]")
    expect(page)
      .to have_selector("div[data-imagepreview] input[type=checkbox][data-imagepreview-removeimageinput]")
    expect(page).to have_selector("div[data-imagepreview] a[data-imagepreview-deleteaction]")
    expect(page).to have_selector("div[data-imagepreview] img[data-imagepreview-preview]")
  end
end
