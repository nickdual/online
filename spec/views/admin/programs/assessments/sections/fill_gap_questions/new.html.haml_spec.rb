require 'spec_helper'

describe "admin/programs/assessments/sections/fill_gap_questions/new.html.haml" do
  let(:resource){
    Program::Assessment::FillGapQuestion.make(
      answers: [Program::Assessment::FillGapAnswer.new],
      section: Program::Assessment::Section.make(
        assessment: Program::Assessment.make)) }

  let(:page){ render; Capybara.string(rendered) }
      
  it "implements the radio buttons in the answers subform as a boolean radio group set" do
    view.stub(:resource_class){ Program::Assessment::FillGapQuestion }
    view.stub(:resource){ resource }
    view.stub(:child_class){ Program::Assessment::FillGapAnswer }
    view.stub(:url_for).and_return('')

    expect(page).to have_selector("#answers[data-cocoon]")
    expect(page.all('input[name$="[correct]"][data-boolean-radio-group-set=correct-answer]').size).to eq(2) # true and false
  end
end
