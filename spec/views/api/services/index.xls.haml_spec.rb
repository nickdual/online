require 'spec_helper'

describe 'api/services/index.xls.haml' do
  class ExpectedTable < OpenStruct
    def first_cell_of_column(i)
      Capybara.string(body).all("table[name='#{self.name}'] tbody tr td")[i]
    end

    def first_cell_of_id_column
      first_cell_of_column(self.id_column)
    end
  end

  let(:employment) {
    fire_double('Employment',
      employment_learning_plans: [],
      learning_records: [],
      user_id: user.id,
      id: 46,
      employee_number: 47,
      service_id: service.id,
      user: user,
      started_at: Time.now,
      ended_at: Time.now,
      status: 'status',
      role: 'role',
      job_title: 'job_title') }

  let(:employment_learning_plan){
    fire_double('EmploymentLearningPlan',
      learning_plan_id: learning_plan.id,
      status: 'status') }

  let(:employment_learning_plans_relation){
    double(:employment_learning_plans_relation,
      overdue: [],
      completed: [],
      pending: []) }

  let(:group){ fire_double('Group',
    employments: [],
    users: []) }

  let(:learning_plan){
    fire_double('LearningPlan',
      id: 30,
      name: 'name',
      service: service,
      service_id: service.id,
      days_till_due: 40,
      employment_learning_plans: employment_learning_plans_relation) }

  let(:program){
    fire_double('Program',
      id: 50,
      code: 'code',
      title: 'title',
      category: program_category,
      program_type: 'program_type',
      accreditation: 'accreditation',
      duration: 'duration') }

  let(:program_category){ fire_double('ProgramCategory', parent: 'parent') }

  let(:service){
    fire_double('Service',
      id: 8,
      name: 'name',
      country: 'country',
      activation_token: 'activation_token',
      employments: [],
      learning_plans: [],
      programs: []) }

  let(:services){ [] }

  let(:user){ fire_double('User',
    id: 12,
    first_name: 'first_name',
    last_name: 'last_name',
    email: 'email@example.com',
    formatted_born_at: Time.now.to_s,
    gender: 'M',
    sign_in_count: 21,
    current_sign_in_at: Time.now,
    employments: user_employments_relation) }

  let(:user_employments_relation) {
    double(:user_employments_relation, where: []) }

  before do
    pending # To be fixed in #55687666
    assign :group, group
    assign :services, services
  end

  subject { render; rendered }

  context "where the group has users" do
    let(:users_table){
      ExpectedTable.new(name: 'Users Overview', id_column: 0, body: subject) }

    let(:group){ fire_double('Group',
      employments: [],
      users: [user]) }

    it { expect(users_table.first_cell_of_id_column.text).to eq(user.id.to_s) }

    context "and the users are employed by the services" do
      let(:services){ [service] }

      before do
        user_employments_relation
          .should_receive(:where)
          .at_least(:once)
          .with(service_id: [service.id])
          .and_return([employment])
      end

      it "expects the Users Overview to include the employments count and employee numbers of the user" do
        expect(users_table.first_cell_of_column(8).text).to eq('1')
        expect(users_table.first_cell_of_column(9).text).to eq(employment.employee_number.to_s)
      end
    end
  end

  context "where the group has employments" do
    let(:employments_table){
      ExpectedTable.new(name: 'Employments Overview', id_column: 1, body: subject) }

    let(:group){ fire_double('Group',
      employments: [employment],
      users: []) }

    it { expect(employments_table.first_cell_of_id_column.text).to eq(employment.id.to_s) }
  end

  context "with services" do
    let(:services_table){
      ExpectedTable.new(name: 'Services', id_column: 0, body: subject) }

    let(:services){ [service] }

    it { expect(services_table.first_cell_of_id_column.text).to eq(service.id.to_s) }
  end

  context "with employment learning plans" do
    let(:employment_learning_plans_table){
      ExpectedTable.new(name: 'Assigned Learning Plans', id_column: 6, body: subject) }

    before do
      group.stub(:employments).and_return([employment])
      employment.stub(:employment_learning_plans).and_return([employment_learning_plan])
    end

    it { expect(employment_learning_plans_table.first_cell_of_id_column.text).to eq(learning_plan.id.to_s) }
  end

  context "with learning plans" do
    let(:learning_plans_table){
      ExpectedTable.new(name: 'Learning Plans', id_column: 0, body: subject) }

    let(:services){ [service] }

    before do
      service.stub(:learning_plans).and_return([learning_plan])
    end

    it { expect(learning_plans_table.first_cell_of_id_column.text).to eq(learning_plan.id.to_s) }
  end

  context "with programs" do
    let(:programs_table){
      ExpectedTable.new(name: 'Programs', id_column: 0, body: subject) }

    let(:services){ [service] }

    before do
      service.stub(:programs).and_return([program])
    end

    it { expect(programs_table.first_cell_of_id_column.text).to eq(program.id.to_s) }
  end
end

