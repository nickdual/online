require 'spec_helper'

describe 'api/services/show.xls.haml' do
  class ExpectedTable < OpenStruct
    def first_cell_of_column(i)
      Capybara.string(body).all("table[name='#{self.name}'] tbody tr td")[i]
    end

    def first_cell_of_id_column
      first_cell_of_column(self.id_column)
    end
  end

  let(:employment) {
    fire_double('Employment',
      employment_learning_plans: employment_learning_plans_relation,
      learning_records: [],
      user_id: user.id,
      id: 46,
      employee_number: 47,
      user: user,
      started_at: Time.now,
      ended_at: Time.now,
      status: 'status',
      role: 'role',
      job_title: 'job_title') }

  let(:employment_learning_plan){
    fire_double('EmploymentLearningPlan',
      learning_plan_id: learning_plan.id,
      status: 'status') }

  let(:employment_learning_plans_relation){
    double(:employment_learning_plans_relation,
      overdue: [],
      completed: [],
      pending: []) }

  let(:learning_plan){
    fire_double('LearningPlan',
      id: 30,
      name: 'name',
      service: service,
      service_id: service.id,
      days_till_due: 40) }

  let(:learning_record){
    fire_double('LearningRecord',
      id: 53,
      program_id: program.id) }

  let(:program){
    fire_double('Program',
      id: 50,
      code: 'code',
      title: 'title',
      category: program_category,
      program_type: 'program_type',
      accreditation: 'accreditation',
      duration: 'duration') }

  let(:program_category){ fire_double('ProgramCategory', parent: 'parent') }

  let(:service){
    fire_double('Service',
      id: 8,
      name: 'name',
      country: 'country',
      activation_token: 'activation_token',
      employments: [],
      learning_plans: [],
      programs: []) }

  let(:user){ fire_double('User',
    id: 12,
    first_name: 'first_name',
    last_name: 'last_name',
    name: 'first_name last_name',
    email: 'email@example.com',
    formatted_born_at: Time.now.to_s,
    gender: 'M',
    sign_in_count: 21,
    current_sign_in_at: Time.now,
    employments: user_employments_relation) }

  let(:user_employments_relation) {
    double(:user_employments_relation, where: []) }

  before do
    pending # To be fixed in #55687666
    assign :service, service
  end

  subject { render; rendered }

  context "where the service has employments" do
    let(:employments_table){
      ExpectedTable.new(name: 'Employments Overview', id_column: 0, body: subject) }

    before do
      service.stub(:employments).and_return([employment])
    end

    it { expect(employments_table.first_cell_of_id_column.text).to eq(employment.id.to_s) }
  end

  context "with employment learning plans" do
    let(:employment_learning_plans_table){
      ExpectedTable.new(name: 'Assigned Learning Plans', id_column: 0, body: subject) }

    let(:employment_learning_plans_relation){
      double(:employment_learning_plans_relation,
        overdue: [employment_learning_plan],
        completed: [employment_learning_plan, employment_learning_plan, employment_learning_plan],
        pending: [employment_learning_plan, employment_learning_plan]) }

    let(:id){ employment_learning_plan.learning_plan_id }

    before do
      service.stub(:employments).and_return([employment])
    end

    it { expect(employment_learning_plans_table.first_cell_of_column(3).text).to eq('3') }
    it { expect(employment_learning_plans_table.first_cell_of_column(4).text).to eq('2') }
    it { expect(employment_learning_plans_table.first_cell_of_column(5).text).to eq('1') }
    it { expect(employment_learning_plans_table.first_cell_of_column(6).text).to eq("#{id},#{id},#{id}") }
    it { expect(employment_learning_plans_table.first_cell_of_column(7).text).to eq("#{id},#{id}") }
    it { expect(employment_learning_plans_table.first_cell_of_column(8).text).to eq(id.to_s) }

  end

  context "with learning records" do
    let(:learning_records_table){
      ExpectedTable.new(name: 'Learning Records', id_column: 0, body: subject) }

    before do
      service.stub(:employments).and_return([employment])
      employment.stub(:learning_records).and_return([learning_record])
    end

    it { expect(learning_records_table.first_cell_of_column(4).text).to eq('1') }
    it { expect(learning_records_table.first_cell_of_column(5).text).to eq(program.id.to_s) }
  end

  context "with learning plans" do
    let(:learning_plans_table){
      ExpectedTable.new(name: 'Learning Plans', id_column: 0, body: subject) }

    before do
      service.stub(:learning_plans).and_return([learning_plan])
      learning_plan.stub(:employment_learning_plans).and_return(employment_learning_plans_relation)
    end

    it { expect(learning_plans_table.first_cell_of_id_column.text).to eq(learning_plan.id.to_s) }
  end

  context "with programs" do
    let(:programs_table){
      ExpectedTable.new(name: 'Programs', id_column: 0, body: subject) }

    before do
      service.stub(:programs).and_return([program])
    end

    it { expect(programs_table.first_cell_of_id_column.text).to eq(program.id.to_s) }
  end
end

