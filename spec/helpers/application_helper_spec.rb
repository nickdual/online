require 'spec_helper'

describe ApplicationHelper do
  include ApplicationHelper
  describe "#number_to_percentage_or_na(number)" do
    context "where number is NAN" do
      it "should be N/A" do
        expect(number_to_percentage_or_na(0.0/0)).to eq('N/A')
      end
    end

    context "where number is not 0" do
      it "should be the number as percentage" do
        expect(number_to_percentage_or_na(50.0)).to eq('50%')
      end
    end

    it "should accept integers" do
      expect(number_to_percentage_or_na(50)).to eq('50%')
    end
  end
end