require 'spec_helper'

describe Admin::AdminsHelper do
  include Admin::AdminsHelper

  describe "#last_logged_in(user)" do
    shared_examples "a helper for displaying the last logged in of class_name" do |class_name|
      let!(:time_now){ Time.now }

      subject { last_logged_in(user) }

      context "where user already signed in" do
        let(:user){
          fire_double(class_name,
            name: 'George',
            current_sign_in_at: time_now - 1.hour) }

        it "includes the name of the user and the time that passed since he logged in" do
          Time.stub(:now).and_return(time_now)
          expect(subject).to eq("#{user.name}, about 1 hour ago")
        end
      end

      context "where user has not signed in" do
        let(:user){ fire_double(class_name,
          name: 'George',
          current_sign_in_at: nil) }

        it { expect(subject).to eq("#{user.name} has not signed in before") }
      end
    end

    it_behaves_like "a helper for displaying the last logged in of class_name", 'User'
    it_behaves_like "a helper for displaying the last logged in of class_name", 'Employment'
    it_behaves_like "a helper for displaying the last logged in of class_name", 'Coordinator'
    it_behaves_like "a helper for displaying the last logged in of class_name", 'Admin'
  end
end