require "spec_helper"

describe "Admin::Users::ActivitiesController" do
  use_vcr_cassette "create_service"

  before :each do
    @user = User.make!
    service = Service.make!
    employment = Employment.make! user: @user, service: service
    @learning_record = LearningRecord.make! employment: employment
    program = Program.make!
    @group_session_record = LearningRecord.make!(
      program: program,
      employment: employment,
      group_session: GroupSession.make!(program: program, service: employment.service)
    )
    program = Program.make!
    assessment_record = LearningRecord.make!(
      program: program,
      employment: employment,
    )
    @assessment = ProgramAssessment.make!(
      employment: employment,
      program: program,
    )
    login_as_admin
    visit admin_user_path(@user)
    click_link "Show Learning Records"
  end

  describe "index" do
    it "lists all types of activity for a user" do
      page.should have_link("Show User")
      page.should have_content(@learning_record.title)
      page.should have_content(@group_session_record.title)
      page.should have_content(@assessment.title)
    end
  end
end
