require "spec_helper"

describe "Admin::Users::EmploymentLearningPlansController" do
  use_vcr_cassette "create_service"

  before :each do
    @user = User.make!
    service = Service.make!
    employment = Employment.make!(user: @user, service: service)
    learning_plan = LearningPlan.make!(service: service)
    @employment_learning_plan = EmploymentLearningPlan.make!(
      learning_plan: learning_plan,
      employment: employment,
    )
    login_as_admin
  end

  describe "index" do
    before :each do
      visit admin_user_learning_plans_path(@user)
    end

    it "lists employment learning plans" do
      page.should have_link("Show User", href: admin_user_path(@user))
      page.should have_link(
        @employment_learning_plan.name,
        href: admin_user_learning_plan_path(@user, @employment_learning_plan)
      )
    end
  end

  describe "show" do
    before :each do
      visit admin_user_learning_plan_path(@user, @employment_learning_plan)
    end

    it "lists employment learning plan details" do
      page.should have_link(
        "Show Learning Plans",
        href: admin_user_learning_plans_path(@user)
      )
      page.should have_content(@employment_learning_plan.name)
    end
  end
end
