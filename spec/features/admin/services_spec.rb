require "spec_helper"

def create_service_and_view
  visit admin_services_path

  click_link "New Service"
  fill_in "Name", with: "OldHome"
  select "Australia", from: "Country"
  select "Package", from: "Packages"
  click_button "Create Service"
end

describe "ServicesController" do
  use_vcr_cassette "create_service"
  before(:each) { login_as_admin }

  context "Create Service" do
    it "I can create a service" do
      Package.make!(name: "Package")
      click_link "Services"
      create_service_and_view
      page.should have_content "Service was successfully created"
      click_link "OldHome"
      page.should have_content "Subscriptions"
    end

    it "I can create a service under a group" do
      group = Group.make!
      package = Package.make!
      click_link "Groups"
      click_link group.name
      create_service_and_view # Create service in the group
      page.should have_content "Service was successfully created"
    end
  end

  describe "Management" do
    before(:each) do
      @service = Service.make! name: "Carrington"
    end

    context "Update Service" do
      use_vcr_cassette "update_service"

      it "I can edit a service" do
        click_link "Services"
        click_link "Edit"
        fill_in "Name", with: "OldHomeChange"
        click_button "Update Service"
        page.should have_content "Service was successfully updated."
        page.should have_content "OldHomeChange"
      end

      it "I can reset a service's activation token" do
        click_link "Services"
        click_link "Carrington"
        click_link "Reset Activation Token"
        page.should have_content "Service's activation token was successfully reset."
      end

      it "I can disable and enable a service" do
        click_link "Services"
        click_link "Carrington"
        click_link "Disable Service"
        page.should have_content "Successfully disabled service."
        page.should have_content "disabled by #{@admin.name}"

        click_link "Enable Service"
        page.should have_content "Successfully enabled service."
        page.should have_content "active"
      end
    end
  end

  describe "exports" do
    before(:each) { @service = Service.make! name: "Carrington" }

    context "services" do
      it "should export a list of services" do
        click_link "Services"
        click_link "Export"
        page.should have_content "name,country,disabled_at,activation_token,group,employments_count"
        page.should have_content "Carrington"
      end
    end

    context "job titles" do
      before(:each) do
        @job = @service.job_titles.create!(name: "Mr. Manager")
      end

      it "should export a list of job titles" do
        visit admin_service_path(@service)
        click_link "Export Job Titles"

        page.should have_content "id,title"
        page.should have_content [@job.id, @job.name].join(",")
      end
    end
  end

  describe "Introductions and closers" do
    let(:program) { Program.make!(processing: false) }
    let(:package) { Package.make!(programs: [program]) }
    let(:service) { Service.make!(packages: [package]) }
    let(:intro) { Program.make!(:introduction, processing: false) }
    let(:closer) { Program.make!(:closer, processing: false) }

    before(:each) { intro; closer }

    it "should be able to assign introductions and closers to a program" do
      visit admin_service_path(service)
      click_link "Show Programs"
      click_link "Edit"

      check intro.title
      check closer.title
      click_button "Update Service Program"

      page.should have_content("Successfully updated program")
      ServiceProgram.first.program_lists.count.should == 2

      click_link "Edit"
      page.has_checked_field?(intro.title).should be_true
      page.has_checked_field?(closer.title).should be_true
    end
  end
end
