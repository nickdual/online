require "spec_helper"

describe "ProgramsController" do
  before(:each) do
    login_as_admin
    @program_category = ProgramCategory.make!(name: "Cake")
    @program_type = ProgramType.make!(name: "Community")
  end

  it "I can upload a program" do
    click_link "Programs"
    click_link "New Program"
    fill_in "Title", with: "Something"
    fill_in "Code", with: "12345"
    fill_in "Description", with: "Something else"
    select "S1", from: "Accreditation / Quality Standard"
    fill_in "Duration", with: "5"
    attach_file "Video", "spec/fixtures/meow.mp4"
    attach_file "Thumbnail", "spec/fixtures/cat.jpg"
    select "Community", from: "Type"
    click_button "Create Program"
    page.should have_content "Program was successfully created"
  end

  it "I can update a program" do
    @program = Program.make!(title: "Something")
    click_link "Programs"
    click_link "Something"
    click_link "Edit Program"
    fill_in "Title", with: "Derp"
    click_button "Update Program"
    page.should have_content "Program was successfully updated"

    click_link "Derp"
    click_link "Delete Program"
    page.should have_content "Program was successfully destroyed."
    page.should_not have_content "Derp"
  end

  it "I can receive updates from zencoder with new videos" do
    @program = Program.make!(title: "Something", created_by: @admin)
    click_link "Programs"
    click_link "Something"
    page.should have_content "We're still processing this program - we'll send you an email when it's finished"
    reset_mailer

    # Send mock Zencoder updates
    10.times {|index| page.driver.post "/webhooks/zencoder/#{@program.id}", {:output => {"state"=>"finished", "label"=>"browser_webm_#{index}", "url"=>"http://accprogramstaging.s3.amazonaws.com/2011/09/07/07/32/09/595/meow.mp4", "id"=>7287488}, "job"=>{"test"=>true, "state"=>"finished", "id"=>6424425}}}
    unread_emails_for(@admin.email).first.subject.should == "Something has finished Encoding"

    visit admin_path
    click_link "Programs"
    click_link "Something"
    page.should have_content "This program has been processed by Zencoder"
  end

  it "I can export programs as a csv" do
    @program = Program.make!(title: "Something", created_by: @admin)
    click_link "Programs"
    click_link "Export"
    page.should have_content "Program Title,Program Code,Processing,Category,Created at,Updated at"
    page.should have_content "Something"
  end
end
