require "spec_helper"

describe "ProgramCategoriesController" do
  before(:each) { login_as_admin }

  it "I can create a program category" do
    click_link "Program Categories"
    click_link "New Program Category"
    fill_in "Name", with: "Something Else"
    click_button "Create Program category"
    page.should have_content "Program category was successfully created."
  end

  it "I can upate a program category" do
    ProgramCategory.make! name: "Cake"

    click_link "Program Categories"
    click_link "Edit"
    fill_in "Name", with: "Else else"
    click_button "Update Program category"
    page.should have_content "Program category was successfully updated."

    click_link "Else else"
    click_link "Delete Program Category"
    page.should have_content "Program category was successfully destroyed."
    page.should_not have_content "Else else"
  end

  it "I can export program categories as a csv" do
    ProgramCategory.make! name: "Cake"

    click_link "Program Categories"
    click_link "Export"
    page.should have_content "name,parent_category,programs_count"
    page.should have_content "Cake"
  end
end