require 'spec_helper'

describe "Feature: An admin can specify an assessment for a program" do
  let(:program){ Program.make!(created_by: @admin) }

  let(:name){ 'name' }
  let(:time_allowed){ 8 }
  let(:pass_mark) { 50 }
  let(:assessment_type){ 'Essentials' }

  Steps do
    Given "I logged in as an administrator" do
      pending # to be fixed in #55687666
      login_as_admin
    end

    And "a program exists" do
      program
    end

    When "I view the program assessments under the program" do
      visit admin_program_path(program)
    end

    And "I create a new program assessment" do
      find_link('New Assessment').click

      AdminProgramAssessmentForm.new(self).tap do |form|
        form.fill name: name, pass_mark: pass_mark, time_allowed: time_allowed
        form.submit
      end
    end

    Then "I should see the program assessment" do
      page.should have_content name
      page.should have_content time_allowed

      pending "Feedback from Grant"
    end

    When "I view the program assessments" do
      within '#breadcrumb' do
        find_link(program.title).click
      end
    end

    Then "I should see the program assessment in the program assessments" do
      expect(find('table.program_assessments')).to have_content(name)
    end

    When "I edit the program assessment" do
      within 'table.program_assessments' do
        find_link('Edit').click
      end

      find_field('Name').set 'new ' + name
      find("input[type=submit]").click
    end

    Then "I should see that the program assessment has been modified" do
      expect(find('table.assessment tr.name td')).to have_content('new ' + name)
    end
  end

  describe "Scenario: administrating the learning outcomes of an assessment", js: true do
    let(:assessment){
      Program::Assessment.make!(program: program) }

    let(:description){
      'Power to kill a yak from 200 yards away... with mind bullets!' }

    let(:modified_description){
      'Power to move you' }

    Steps do
      Given "I logged in as an administrator" do
        login_as_admin
      end

      And "an assessment exists under a program" do
        assessment
      end

      When "I add a learning outcome to the assessment" do
        visit edit_polymorphic_url([:admin, program, assessment], only_path: true)

        within "#learning_outcomes" do
          find_link('Add').click
          find("[name$='[description]']").set description
        end

        find("input[type=submit]").click
      end

      Then "I should see the learning outcome in the assessment" do
        expect(page).to have_content(description)
      end

      When "I edit the assessment" do
        find('a[data-original-title="Edit Assessment"]').click

        within "#learning_outcomes" do
          find("[name$='[description]']").set modified_description
        end

        find("input[type=submit]").click
      end

      Then "I should see that the assessment has been modified" do
        expect(page).to_not have_content(description)
        expect(page).to have_content(modified_description)
      end

      When "I remove the assessment" do
        find('a[data-original-title="Edit Assessment"]').click

        within "#learning_outcomes" do
          find_link('Remove').click
        end

        find("input[type=submit]").click
      end

      Then "I should not see the learning_outcome in the assessment" do
        expect(page).to_not have_content(modified_description)
      end
    end
  end

  describe "Scenario: I can view and edit the question type allocation of an assessment section" do
    let(:minimum_number_of_questions){ 19 }

    Steps do
      Given "I am creating a new program assessment" do
        pending # to be fixed in #55687666
        login_as_admin
        visit admin_program_path(program)
        find_link('New Assessment').click
      end

      When "I specify that a section of the assessment must have at least has n questions" do
        AdminProgramAssessmentForm.new(self).tap do |form|
          form.fill
          form.sections[0].minimum_number_of_questions_field.set minimum_number_of_questions
          form.submit
        end
      end

      Then "I should see that the section is required to have at least n questions" do
        expect(AdminProgramAssessmentPage.new(self).sections[0]).to have_content(minimum_number_of_questions)
      end

      And "I should see that the section has 0 questions" do
        last_section = Program::Assessment::Section.last
        expect(last_section.questions).to have(0).questions
      end
    end
  end
end
