require 'spec_helper'

describe "Feature: An admin can create a multiple choice question for users" do
  let(:program){ Program.make! }
  let(:assessment){
    Program::Assessment.make!(
      program: program,
      learning_outcomes: [learning_outcome]) }

  let(:learning_outcome){
    Program::Assessment::LearningOutcome.make(
      description: 'Warms are for sits!') }

  let(:section){ Program::Assessment::Section.make!(assessment: assessment) }

  let(:question){ 'Is made of warm for sits?' }
  let(:reference_image_basename){ 'cat.jpg' }
  let(:reference_image_path) { "spec/fixtures/#{reference_image_basename}" }

  Steps "Scenario: administrating multiple choice questions" do
    Given "I logged in as an administrator" do
      login_as_admin
    end

    And "an section under a program assessment exists" do
      section
    end

    When "I view the multiple choice questions under the section" do
      visit url_for([:admin, section, :program_assessment_multiple_choice_questions])
    end

    And "I create a question" do
      find_link('New Multiple choice question').click
      find_field('program_assessment_multiple_choice_question_learning_outcome_id').find("option[value='#{learning_outcome.id}']").select_option
      find_field('program_assessment_multiple_choice_question_question').set question
      find_field('program_assessment_multiple_choice_question_reference_image').set reference_image_path
      find('input[type=submit]').click
    end

    Then "I should see the question" do
      within '.program_assessment_multiple_choice_question' do
        expect(page).to have_content(question)
        expect(find('.reference_image img')['src']).to be_end_with(reference_image_basename)
      end
    end

    When "I view the questions" do
      within '#breadcrumb' do
        find_link('Multiple choice questions').click
      end
    end

    Then "I should see the question in the questions" do
      within 'table tr.program_assessment_multiple_choice_question' do
        expect(find('td.question')).to have_content(question)
        expect(find('td.reference_image img')['src']).to be_end_with(reference_image_basename)
      end
    end

    When "I download the questions as csv" do
      click_link 'Export Multiple choice questions'
    end

    Then "I should see the question in the csv" do
      CSV.parse(find('p').text, headers: true).tap do |table|
        expect(table[0]['question']).to eq(question)
        expect(table[0]['reference_image_uid']).to be_end_with(reference_image_basename)
        expect(table[0]['reference_image_name']).to eq(reference_image_basename)
      end
    end

    When "I edit the question" do
      visit url_for([:admin, section, :program_assessment_multiple_choice_questions])
      within 'table tr.program_assessment_multiple_choice_question td.actions' do
        find_link('Edit').click
      end

      find_field('Question').set 'Cat asks: is made of warm for sits?'
      find('input[type=submit]').click
    end

    Then "I should see that the question has been modified" do
      expect(find('.program_assessment_multiple_choice_question')).to have_content('Cat asks: is made of warm for sits?')
    end

    When "I destroy the question" do
      find_link('Delete Multiple choice question').click
    end

    Then "I should not see the question in the questions" do
      expect(page.status_code).to eq(200)
      expect(page).to_not have_css('table tr.program_assessment_multiple_choice_question')
    end
  end

  describe "Scenario: administrating multiple choice answers in an existing question", js: true do
    let(:question_text) { 'What is the meaning of life?' }
    let(:answer_text) { 42 }
    let(:modified_answer_text) { "Forty-two" }

    let(:question){
      Program::Assessment::MultipleChoiceQuestion.create(
        question: 'What is the meaning of life?',
        learning_outcome: learning_outcome,
        section: section) }

    Steps do
      Given "I logged in as an administrator" do
        login_as_admin
      end

      And "a question exists" do
        expect(question).to_not be_new_record
      end

      When "I add an answer to the question" do
        visit edit_polymorphic_url([:admin, section, question], only_path: true)

        within "#answers" do
          find_link('Add Option').click
          find("[name$='[answer]']").set answer_text
          find("[name$='[correct]']").click
        end

        find('input[type=submit]').click
      end

      Then "I should see the answer in the question" do
        within '.program_assessment_multiple_choice_question' do
          expect(page).to have_content(answer_text)
        end
      end

      When "I edit the answer" do
        find('a[data-original-title="Edit Multiple choice question"]').click

        within "#answers" do
          find("[name$='[answer]']").set modified_answer_text
        end

        find('input[type=submit]').click
      end

      Then "I should see that the answer has been modified" do
        within '.program_assessment_multiple_choice_question' do
          expect(page).to have_content(modified_answer_text)
        end
      end

      When "I remove the answer" do
        find('a[data-original-title="Edit Multiple choice question"]').click

        within "#answers" do
          find_link('Delete').click
        end

        find('input[type=submit]').click
      end

      Then "I should not see the answer in the question" do
        within '.program_assessment_multiple_choice_question' do
          expect(page).to_not have_content(modified_answer_text)
        end
      end
    end
  end

  describe "Create question and add another question" do
    Steps  do
      Given "I am logged in as administrator" do
        login_as_admin
      end

      And "an section under a program assessment exists" do
        section
      end

      When "I create a multiple choice question via 'Save and Continue'" do
        NewAdminProgramMultipleChoiceQuestionPage.new(self, section: section).tap do |p|
          p.visit
          p.form.fill(learning_outcome_id: learning_outcome.id)
          p.form.save_and_add_another_question
        end
      end

      Then "I should see that the question has been created" do
        expect(page).to have_content('Multiple choice question was successfully created.')
      end

      And "I should see the page for submitting a new multiple choice question" do
        expect(page).to have_content('New Multiple choice question')
      end
    end
  end
end
