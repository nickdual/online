require 'spec_helper'

describe "Feature: An admin can add / remove Sequencing Questions to a section" do
  let(:program){ Program.make! }
  let(:assessment){
    Program::Assessment.make!(
      program: program,
      learning_outcomes: [learning_outcome]) }

  let(:learning_outcome){
    Program::Assessment::LearningOutcome.make!(
      description: 'Able to remember Eraserheads discography') }

  let(:section){ Program::Assessment::Section.make!(assessment: assessment) }

  let(:text){ 'Arrange the following albums in the order of release' }

  let(:question_type){ 'Text' }

  Steps "Scenario: administrating sequential questions" do
    Given "I logged in as an administrator" do
      login_as_admin
    end

    And "an section under a program assessment exists" do
      section
    end

    When "I view the sequential questions under the section" do
      visit url_for([:admin, section, :program_assessment_sequential_questions])
    end

    And "I create a question" do
      find_link('New Sequencing Question').click
      find_field('program_assessment_sequential_question_learning_outcome_id').find("option[value='#{learning_outcome.id}']").select_option
      find_field('program_assessment_sequential_question_question').set text

      all("[name$='[text]']").each_with_index do |answer_item_text_field, i|
        answer_item_text_field.set i.to_s
      end

      find('input[type=submit]').click
    end

    Then "I should see the question" do
      within '.program_assessment_sequential_question' do
        expect(find('[data-value=question]')).to have_content(text)
      end
    end

    When "I view the questions" do
      within '#breadcrumb' do
        find_link('Sequencing Questions').click
      end
    end

    Then "I should see the question text in the questions" do
      within 'table tr.program_assessment_sequential_question' do
        expect(find('td.question')).to have_content(text)
        expect(find('td.question_type')).to have_content(question_type)
      end
    end

    When "I download the questions as csv" do
      click_link 'Export Sequencing Questions'
    end

    Then "I should see the question in the csv" do
      CSV.parse(find('p').text, headers: true).tap do |table|
        expect(table[0]['question']).to eq(text)
        expect(table[0]['question_type']).to be_end_with(question_type)
      end
    end

    When "I edit the question" do
      visit url_for([:admin, section, :program_assessment_sequential_questions])
      within 'table tr.program_assessment_sequential_question td.actions' do
        find_link('Edit').click
      end

      find_field('program_assessment_sequential_question_question').set 'Arrange the following albums by release'
      find('input[type=submit]').click
    end

    Then "I should see that the question has been modified" do
      expect(find('.program_assessment_sequential_question *[data-value=question]')).to have_content('Arrange the following albums by release')
    end

    When "I destroy the question" do
      find_link('Delete Sequencing Question').click
    end

    Then "I should not see the question in the questions" do
      expect(page.status_code).to eq(200)
      expect(page).to_not have_css('table tr.program_assessment_sequential_question')
    end
  end

  describe "Scenario: administrating sequential answers in an existing question", js: true do
    class Album < OpenStruct
      def path
        File.join(Rails.root, "spec/fixtures", basename)
      end
    end

    let(:question){
      Program::Assessment::SequentialQuestion.create(
        learning_outcome: learning_outcome,
        question: text,
        question_type: question_type,
        section: section) }

    let(:albums){
      [ Album.new(basename: '220px-Ultraelectromagnetic.jpg', correct_position: 0),
        Album.new(basename: '220px-Eraserheads_circus.jpg', correct_position: 1),
        Album.new(basename: '220px-Cutterpillow.jpg', correct_position: 2),
        Album.new(basename: '220px-Fruitcake96.jpg', correct_position: 3) ] }

    Steps do
      Given "I logged in as an administrator" do
        login_as_admin
      end

      And "a question exists" do
        expect(question).to_not be_new_record
      end

      When "I add answers to the question" do
        visit edit_polymorphic_url([:admin, section, question], only_path: true)

        find_field('program_assessment_sequential_question_upload_images').click

        within "#answer_items" do
          albums.each do |album|
            find_link('Add Option').click
            all("[name$='[reference_image]']").last.set album.path
            all("[name$='[correct_position]']").last.set album.correct_position
          end
        end

        find('input[type=submit]').click
      end

      Then "I should see the answers of the question" do
        within '.program_assessment_sequential_question' do
          expect(all('.program_assessment_sequential_answer_item').map(&:text))
            .to eq(albums.sort_by(&:correct_position).map(&:basename))
        end
      end

      When "I edit the answer" do
        find('a[data-original-title="Edit Sequencing Question"]').click

        albums.first.basename = "220px-AlbumArt_(Sticker_Happy).jpg"

        within "#answer_items" do
          find("[name$='[reference_image]']").set albums.first.path
        end

        find('input[type=submit]').click
      end

      Then "I should see that the answer has been modified" do
        within '.program_assessment_sequential_question' do
          expect(all('.program_assessment_sequential_answer_item').map(&:text)).to include("220px-AlbumArt_(Sticker_Happy).jpg")

          expect(all('.program_assessment_sequential_answer_item').map(&:text))
            .to eq(albums.sort_by(&:correct_position).map(&:basename))
        end
      end

      When "I remove the answer" do
        find('a[data-original-title="Edit Sequencing Question"]').click

        within "#answer_items" do
          find_link('Delete').click
        end

        find('input[type=submit]').click
      end

      Then "I should not see the answer in the question" do
        within '.program_assessment_sequential_question' do
          expect(all('.program_assessment_sequential_answer_item').map(&:text)).to have_content(albums[1, 3].sort_by(&:correct_position).map(&:basename))
        end
      end
    end
  end

  describe "Create question and add another question" do
   
    Steps  do
      Given "I am logged in as administrator" do
        login_as_admin
      end

      And "an section under a program assessment exists" do
        section
      end

      When "I create a sequential choice question via 'Save and Continue'" do
        NewAdminProgramSequentialQuestionPage.new(self, section: section).tap do |p|
          p.visit
          p.form.fill(learning_outcome_id: learning_outcome.id)
          p.form.save_and_add_another_question
        end
      end

      Then "I should see that the question has been created" do
        expect(page).to have_content('Sequencing Question was successfully created.')
      end

      And "I should see the page for submitting a new multiple choice question" do
        expect(page).to have_content('New Sequencing Question')
      end
    end
  end
end
