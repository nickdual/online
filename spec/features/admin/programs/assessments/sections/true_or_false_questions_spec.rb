require 'spec_helper'

describe "Feature: An admin can create a true / false question (in text) for users" do
  let(:program){ Program.make! }
  let(:assessment){
    Program::Assessment.make!(
      program: program,
      learning_outcomes: [learning_outcome]) }

  let(:learning_outcome){
    Program::Assessment::LearningOutcome.make(
      description: 'Warms are for sits!') }

  let(:section){ Program::Assessment::Section.make!(assessment: assessment) }

  let(:question){ 'Is made of warm for sits?' }
  let(:answer){ true }
  let(:reference_image_basename){ 'cat.jpg' }
  let(:reference_image_path) { "spec/fixtures/#{reference_image_basename}" }


  Steps do
    Given "I logged in as an administrator" do
      login_as_admin
    end

    And "an section under a program assessment exists" do
      section
    end

    When "I view the true/false questions under the section" do
      visit url_for([:admin, section, :program_assessment_true_or_false_questions])
    end

    And "I create a question" do
      find_link('New True/False question').click
      find_field('program_assessment_true_or_false_question_learning_outcome_id').find("option[value='#{learning_outcome.id}']").select_option
      find_field('program_assessment_true_or_false_question_question').set question
      find_field('program_assessment_true_or_false_question_answer_true').click
      find_field('program_assessment_true_or_false_question_reference_image').set reference_image_path
      find('input[type=submit]').click
    end

    Then "I should see the question" do
      within '.program_assessment_true_or_false_question' do
        expect(page).to have_content(question)
        expect(page).to have_content(answer.to_s.titleize)
        expect(find('table.reference_image img')['src']).to be_end_with(reference_image_basename)
      end
    end

    When "I view the questions" do
      within '#breadcrumb' do
        find_link('True/False questions').click
      end
    end

    Then "I should see the question in the questions" do
      within 'table tr.program_assessment_true_or_false_question' do
        expect(find('td.question')).to have_content(question)
        expect(find('td.answer')).to have_content(answer)
        expect(find('td.reference_image img')['src']).to be_end_with(reference_image_basename)
      end
    end

    When "I download the questions as csv" do
      click_link 'Export True/False questions'
    end

    Then "I should see the question in the csv" do
      CSV.parse(find('p').text, headers: true).tap do |table|
        expect(table[0]['question']).to eq(question)
        expect(table[0]['answer']).to eq(answer.to_s)
        expect(table[0]['reference_image_uid']).to be_end_with(reference_image_basename)
        expect(table[0]['reference_image_name']).to eq(reference_image_basename)
      end
    end

    When "I edit the question" do
      visit url_for([:admin, section, :program_assessment_true_or_false_questions])
      within 'table tr.program_assessment_true_or_false_question td.actions' do
        find_link('Edit').click
      end

      find_field('Question').set 'Cat asks: is made of warm for sits?'
      find('input[type=submit]').click
    end

    Then "I should see that the question has been modified" do
      expect(page).to have_content('Cat asks: is made of warm for sits?')
    end

    When "I destroy the question" do
      find_link('Delete True/False question').click
    end

    Then "I should not see the question in the questions" do
      expect(page.status_code).to eq(200)
      expect(page).to_not have_css('table tr.program_assessment_true_or_false_question')
    end
  end

  Steps "Create question and add another question" do
    Given "I am logged in as administrator" do
      login_as_admin
    end

    And "an section under a program assessment exists" do
      section
    end

    When "I create a true or false question via 'Save and Continue'" do
      NewAdminProgramTrueOrFalseQuestionPage.new(self, section: section).tap do |p|
        p.visit
        p.form.fill(learning_outcome_id: learning_outcome.id)
        p.form.save_and_add_another_question
      end
    end

    Then "I should see that the question has been created" do
      expect(page).to have_content('True/False question was successfully created.')
    end

    And "I should see the page for submitting a new true or false question" do
      expect(page).to have_content('New True/False question')
    end
  end
end
