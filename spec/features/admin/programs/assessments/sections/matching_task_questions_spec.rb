require 'spec_helper'

describe "Feature: An admin can create a matching task question for users" do
  let(:program){ Program.make! }
  let(:assessment){
    Program::Assessment.make!(
      program: program,
      learning_outcomes: [learning_outcome]) }

  let(:learning_outcome){
    Program::Assessment::LearningOutcome.make(
      description: 'Warms are for sits!') }

  let(:section){ Program::Assessment::Section.make!(
    question_type: 'Matching Task',
    assessment: assessment) }

  let(:directions){ 'Match the tasks on the right with their corresponding answers.' }
  let(:revised_directions){ "Directions: #{directions}" }

  Steps "Scenario: administrating matching task questions" do
    Given "I logged in as an administrator" do
      login_as_admin
    end

    And "an section under a program assessment exists" do
      section
    end

    When "I view the matching task questions under the section" do
      visit url_for([:admin, program, assessment])
      find("a[data-questions-link='#{Program::Assessment::MatchingTaskQuestion}']").click
    end

    And "I create a question" do
      find_link('New Matching task question').click
      find_field('program_assessment_matching_task_question_learning_outcome_id').find("option[value='#{learning_outcome.id}']").select_option
      find_field('program_assessment_matching_task_question_directions').set directions
      find('input[type=submit]').click
    end

    Then "I should see the question" do
      within '.program_assessment_matching_task_question' do
        expect(page).to have_content(directions)
      end
    end

    When "I view the questions" do
      within '#breadcrumb' do
        find_link('Matching task questions').click
      end
    end

    Then "I should see the question in the questions" do
      within 'table tr.program_assessment_matching_task_question' do
        expect(page).to have_content(directions)
      end
    end

    When "I download the questions as csv" do
      click_link 'Export Matching task questions'
    end

    Then "I should see the question in the csv" do
      CSV.parse(find('p').text, headers: true).tap do |table|
        expect(table[0]['directions']).to eq(directions)
      end
    end

    When "I edit the question" do
      visit url_for([:admin, section, :program_assessment_matching_task_questions])
      within 'table tr.program_assessment_matching_task_question td.actions' do
        find_link('Edit').click
      end

      find_field('program_assessment_matching_task_question_directions').set revised_directions
      find('input[type=submit]').click
    end

    Then "I should see that the question has been modified" do
      expect(find('.program_assessment_matching_task_question')).to have_content(revised_directions)
    end

    When "I destroy the question" do
      find_link('Delete Matching task question').click
    end

    Then "I should not see the question in the questions" do
      expect(page.status_code).to eq(200)
      expect(page).to_not have_css('table tr.program_assessment_matching_task_question')
    end
  end

  describe "Scenario: administrating matching task answers in an existing question", js: true do
    let(:question_directions) { 'Match name to year of birth' }
    let(:item_text) { 'George' }
    let(:item_answer) { "1980" }
    let(:modified_item_answer) { "1981" }

    let(:question){
      Program::Assessment::MatchingTaskQuestion.create(
        directions: question_directions,
        learning_outcome: learning_outcome,
        section: section) }

    Steps do
      Given "I logged in as an administrator" do
        login_as_admin
      end

      And "a question exists" do
        expect(question).to_not be_new_record
      end

      When "I add an item to the question" do
        visit edit_polymorphic_url([:admin, section, question], only_path: true)

        within "#items" do
          find_link('Add').click
          find("[name$='[text]']").set item_text
          find("[name$='[answer]']").set item_answer
        end

        find('input[type=submit]').click
      end

      Then "I should see the item in the question" do
        within '.program_assessment_matching_task_question' do
          expect(page).to have_content(item_text)
          expect(page).to have_content(item_answer)
        end
      end

      When "I edit the item" do
        find('a[data-original-title="Edit Matching task question"]').click

        within "#items" do
          find("[name$='[answer]']").set modified_item_answer
        end

        find('input[type=submit]').click
      end

      Then "I should see that the item has been modified" do
        within '.program_assessment_matching_task_question' do
          expect(page).to have_content(modified_item_answer)
        end
      end

      When "I remove the answer" do
        find('a[data-original-title="Edit Matching task question"]').click

        within "#items" do
          find_link('Delete').click
        end

        find('input[type=submit]').click
      end

      Then "I should not see the answer in the question" do
        within '.program_assessment_matching_task_question' do
          expect(page).to_not have_content(modified_item_answer)
        end
      end
    end
  end

  describe "Create question and add another question" do
    Steps  do
      Given "I am logged in as administrator" do
        login_as_admin
      end

      And "an section under a program assessment exists" do
        section
      end

      When "I create a matching task question via 'Save and Continue'" do
        NewAdminProgramMatchingTaskQuestionPage.new(self).tap do |p|
          p.visit!(section)
          p.form.fill(learning_outcome_id: learning_outcome.id)
          p.form.save_and_add_another_question
        end
      end

      Then "I should see that the question has been created" do
        expect(page).to have_content('Matching task question was successfully created.')
      end

      And "I should see the page for submitting a new matching task question" do
        expect(page).to have_content('New Matching task question')
      end
    end
  end
end
