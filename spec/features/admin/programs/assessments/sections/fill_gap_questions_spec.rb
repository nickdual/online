require 'spec_helper'

describe "Feature: An admin can create a fill gap question for users" do
  let(:program){ Program.make! }
  let(:assessment){
    Program::Assessment.make!(
      program: program,
      learning_outcomes: [learning_outcome]) }

  let(:learning_outcome){
    Program::Assessment::LearningOutcome.make(
      description: 'Warms are for sits!') }

  let(:section){ Program::Assessment::Section.make!(assessment: assessment) }

  let(:question){ 'Warms are for %%!' }
  let(:revised_question){ '%% are for sits!' }

  Steps "Scenario: administrating fill gap questions" do
    Given "I logged in as an administrator" do
      login_as_admin
    end

    And "an section under a program assessment exists" do
      section
    end

    When "I view the fill gap questions under the section" do
      visit url_for([:admin, section, :program_assessment_fill_gap_questions])
    end

    And "I create a question" do
      find_link('New Fill gap question').click
      find_field('program_assessment_fill_gap_question_learning_outcome_id').find("option[value='#{learning_outcome.id}']").select_option
      find_field('program_assessment_fill_gap_question_question').set question
      find('input[type=submit]').click
    end

    Then "I should see the question" do
      within '.program_assessment_fill_gap_question' do
        expect(page).to have_content(question.gsub("%%", ''))
      end
    end

    When "I view the questions" do
      within '#breadcrumb' do
        find_link('Fill gap questions').click
      end
    end

    Then "I should see the question in the questions" do
      within 'table tr.program_assessment_fill_gap_question' do
        expect(find('td.question')).to have_content(question)
      end
    end

    When "I download the questions as csv" do
      click_link 'Export Fill gap questions'
    end

    Then "I should see the question in the csv" do
      CSV.parse(find('p').text, headers: true).tap do |table|
        expect(table[0]['question']).to eq(question)
      end
    end

    When "I edit the question" do
      visit url_for([:admin, section, :program_assessment_fill_gap_questions])
      within 'table tr.program_assessment_fill_gap_question td.actions' do
        find_link('Edit').click
      end

      find_field('Question').set revised_question
      find('input[type=submit]').click
    end

    Then "I should see that the question has been modified" do
      expect(find('.program_assessment_fill_gap_question')).to have_content(revised_question.gsub("%%", ''))
    end

    When "I destroy the question" do
      find_link('Delete Fill gap question').click
    end

    Then "I should not see the question in the questions" do
      expect(page.status_code).to eq(200)
      expect(page).to_not have_css('table tr.program_assessment_fill_gap_question')
    end
  end

  describe "Scenario: administrating fill gap answers in an existing question", js: true do
    let(:question_text) { 'The meaning of life is %%' }
    let(:answer_text) { 42 }
    let(:modified_answer_text) { "Forty-two" }

    let(:question){
      Program::Assessment::FillGapQuestion.create(
        question: question_text,
        learning_outcome: learning_outcome,
        section: section) }

    Steps do
      Given "I logged in as an administrator" do
        login_as_admin
      end

      And "a question exists" do
        expect(question).to_not be_new_record
      end

      When "I add an answer to the question" do
        visit edit_polymorphic_url([:admin, section, question], only_path: true)

        within "#answers" do
          find_link('Add').click
          find("[name$='[answer]']").set answer_text
          find("[name$='[correct]']").click
        end

        find('input[type=submit]').click
      end

      Then "I should see the answer in the question" do
        within '.program_assessment_fill_gap_question' do
          expect(page).to have_content(answer_text)
        end
      end

      When "I edit the answer" do
        find('a[data-original-title="Edit Fill gap question"]').click

        within "#answers" do
          find("[name$='[answer]']").set modified_answer_text
        end

        find('input[type=submit]').click
      end

      Then "I should see that the answer has been modified" do
        within '.program_assessment_fill_gap_question' do
          expect(page).to have_content(modified_answer_text)
        end
      end

      When "I remove the answer" do
        find('a[data-original-title="Edit Fill gap question"]').click

        within "#answers" do
          find_link('Delete').click
        end

        find('input[type=submit]').click
      end

      Then "I should not see the answer in the question" do
        within '.program_assessment_fill_gap_question' do
          expect(page).to_not have_content(modified_answer_text)
        end
      end
    end
  end

  describe "Create question and add another question" do
    Steps  do
      Given "I am logged in as administrator" do
        login_as_admin
      end

      And "an section under a program assessment exists" do
        section
      end

      When "I create a fill gap question via 'Save and Continue'" do
        NewAdminProgramFillGapQuestionPage.new(self).tap do |p|
          p.visit!(section)
          p.form.fill(learning_outcome_id: learning_outcome.id)
          p.form.save_and_add_another_question
        end
      end

      Then "I should see that the question has been created" do
        expect(page).to have_content('Fill gap question was successfully created.')
      end

      And "I should see the page for submitting a new fill gap question" do
        expect(page).to have_content('New Fill gap question')
      end
    end
  end
end
