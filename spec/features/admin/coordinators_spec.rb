require "spec_helper"

describe "CoordinatorsController" do
  use_vcr_cassette "create_service"
  before(:each) do
    @group = Group.make! name: "Some Group"
    @service = Service.make! name: "Some Service"
    @group.services = [@service]
    login_as_admin
    click_link "Groups"
    click_link "Some Group"
  end

  it "I can add a coordinator to a service" do
    click_link "New Group Coordinator"
    fill_in "First name", with: "Harry"
    fill_in "Last name", with: "Potter"
    fill_in "Email", with: "harry@icdesign.com.au"
    click_button "Create Group Coordinator"
    page.should have_content "Coordinator was successfully invited."
    unread_emails_for("harry@icdesign.com.au").count.should >= 1

    click_button "Remove Group Coordinator"
    page.should have_content "Coordinator was successfully removed."
  end

  it "I can invite a coordinator to a group (vs. creating a new user)" do
    User.make! email: "harry@icdesign.com.au"
    click_link "New Group Coordinator"
    fill_in "Email", with: "harry@icdesign.com.au"
    click_button "Create Group Coordinator"
    page.should have_content "This email address already belongs to an active member. Would you like to invite them to this group?"

    click_button "Invite User"
    page.should have_content "Coordinator was successfully invited."
    unread_emails_for("harry@icdesign.com.au").first.subject.should == "You are now a Group Coordinator for #{@group}"
  end
end
