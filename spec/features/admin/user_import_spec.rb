require "spec_helper"

describe "UserImportsController" do
  use_vcr_cassette "create_service"

  before(:each) do
    login_as_admin
    @service = Service.make!
    reset_mailer
    visit new_admin_service_user_import_path(@service)
  end

  it "I can bulk import users" do
    attach_file("CSV file", "spec/fixtures/users-complete.csv")
    click_button "Import Users"

    page.should have_content("Successfully received csv file, we'll email #{@admin.email} when it's complete")
    unread_emails_for(@admin.email).size.should == 1
    unread_emails_for(@admin.email).first.subject.should == "Completed User Import for #{@service}"

    open_email(@admin.email, with_subject: "Completed User Import for #{@service}")
    current_email.default_part_body.to_s.should include("<strong>2 of 2</strong> users were successfully imported.")
  end

  it "I can bulk import users" do
    attach_file("CSV file", "spec/fixtures/users-incomplete.csv")
    click_button "Import Users"

    page.should have_content("Successfully received csv file, we'll email #{@admin.email} when it's complete")
    unread_emails_for(@admin.email).size.should == 1
    unread_emails_for(@admin.email).first.subject.should == "Completed User Import for #{@service}"

    open_email(@admin.email, with_subject: "Completed User Import for #{@service}")
    current_email.default_part_body.to_s.should include("<strong>0 of 2</strong> users were successfully imported.")
    current_email.default_part_body.to_s.should include("Sinclair (First name can't be blank)")
  end
end