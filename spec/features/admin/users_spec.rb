require "spec_helper"

describe "UsersController" do
  use_vcr_cassette "create_service"
  let(:service) { Service.make!(name: "Carrington") }
  before(:each) { login_as_admin }

  it "I can create and update a user (via a service)" do
    service
    click_link "Services"
    click_link service.name

    click_link "Add User"
    fill_in "First name", with: "Harry"
    fill_in "Last name", with: "Potter"
    fill_in "Email", with: "harry@icdesign.com.au"
    click_button "Create Employment"

    page.should have_content "User was successfully created."
    unread_emails_for("harry@icdesign.com.au").first.subject.should == "Your Account has been Created for Carrington"
    User.last.activation_code_set_at.should_not be_nil

    page.should have_content  "Current Employments"
    page.should have_content "Carrington"

    click_link "Edit"
    fill_in "First name", with: "James"
    click_button "Update User"
    page.should have_content "User was successfully updated"
    unread_emails_for("harry@icdesign.com.au").first.subject.should == "Your Account has been Created for Carrington"

    click_link "James Potter"
    click_link "Delete User"
    page.should have_content "User was successfully destroyed."
    page.should_not have_content "James Potter"
  end

  it "I can invite an existing user" do
    service
    user = User.make!(email: "harry@icdesign.com.au")

    click_link "Services"
    click_link service.name

    click_link "Add User"
    fill_in "First name", with: "Harry"
    fill_in "Last name", with: "Potter"
    fill_in "Email", with: "harry@icdesign.com.au"
    click_button "Create Employment"

    page.should have_content "User was successfully created."
    unread_emails_for("harry@icdesign.com.au").first.subject.should == "Your Account is now Active with Carrington"
  end

  it "I can reset a users password" do
    user = User.make! email: "harry@icdesign.com.au"
    click_link "Users"
    click_link user.name
    click_link "Reset Password"
    page.should have_content "Successfully sent password reset instructions to the user's email."
    unread_emails_for("harry@icdesign.com.au").first.subject.should == "Reset password instructions"
  end

  it "I can de-activate an employment" do
    employment = Employment.make!(service: service)
    visit admin_user_path(employment.user)

    click_link "Modify Employment"
    click_link "Terminate Employment"

    page.should have_content "Employment was successfully terminated."
    Employment.inactive.count.should == 1
  end

  it "I can export users as a csv" do
    user = User.make! email: "harry@icdesign.com.au"
    click_link "Users"
    click_link "Export"
    page.should have_content "first_name,last_name,email,born_at,gender,sign_in_count,last_sign_in_at"
    page.should have_content "harry@icdesign.com.au"
  end

  describe "I can search for a user" do
    let(:users){ ['George', 'Lenny'].map{|first_name| User.make!(first_name: first_name) } }

    Steps do
      Given "two user exist with different names" do
        users
      end

      And "I am in the admin users page" do
        visit admin_users_path
      end

      When "I search for the first user" do
        within '#search form' do
          page.find_field('q').set users[0].first_name
          page.find('button').click
        end
      end

      Then "I should see the first user" do
        expect(find('table.users')).to have_content(users[0].first_name)
      end

      Then "I should not see the second user" do
        expect(find('table.users')).to_not have_content(users[1].first_name)
      end
    end
  end
end


