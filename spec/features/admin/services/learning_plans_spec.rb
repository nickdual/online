require "spec_helper"

describe "Admin::Services::LearningPlansController" do
  use_vcr_cassette "create_service"

  before(:each) do
    login_as_admin
    @service = Service.make!
    @learning_plan = LearningPlan.make! service: @service
    visit admin_service_path(@service)
  end

  context "#index" do
    it "lists learning plans" do
      click_link "Show Learning Plans"
      page.should have_link(@learning_plan.name)
    end

    it "exports learning plans as a csv" do
      click_link "Show Learning Plans"
      click_link "Export Learning Plans"
      page.should have_content "name,status,days_till_due,programs,users,activation_date,created_at,updated_at"
      page.should have_content "#{@learning_plan.name}"
    end
  end

  context "#show" do
    it "lists programs for specific learning plans" do
      @program = Program.make!
      @learning_plan.programs << @program

      click_link "Show Learning Plans"
      click_link @learning_plan.name
      page.should have_link(@program.code)
    end
  end
end
