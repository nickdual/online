require "spec_helper"

describe "Admin::Services::JobTitlesController" do
  use_vcr_cassette "create_service"

  context "with a group" do
    before(:each) do
      login_as_admin
      @group = Group.make!
      @service = Service.make! group: @group
      @job_title = Service::JobTitle.make! service: @service
    end

    it "should redirect to group job title index" do
      visit admin_service_job_titles_path(@service)
      current_path.should == admin_group_job_titles_path(@group)
      page.should have_content "Job Titles are administered through this Service's Group"
    end
  end

  context "without a group" do

    before(:each) do
      login_as_admin
      @service = Service.make!
      @job_title = Service::JobTitle.make! service: @service
    end

    context "index action" do
      it "lists job titles" do
        visit admin_service_path(@service)
        click_link "Job Titles"
        page.should have_content @job_title.name
      end
    end

    context "show action" do
      it "lists users" do
        user = User.make!
        Employment.make! service: @service, user: user, job_title_id: @job_title.id
        visit admin_service_job_title_path(@service, @job_title)
        page.should have_link user.name
      end
    end

    context "administration" do
      it "can create a job title" do
        visit admin_service_job_titles_path(@service)
        click_link "New Job Title"
        fill_in "Name", with: "The Godfather"
        click_button "Create Job title"
        page.should have_content "Job Title was successfully created"
      end

      it "can edit a Job Title" do
        visit admin_service_job_title_path(@service, @job_title)
        click_link "Edit"
        fill_in "Name", with: "Muppeteer"
        click_button "Update Job title"
        page.should have_content "Job Title was successfully updated"
      end

      it "can delete a Job Title" do
        visit admin_service_job_title_path(@service, @job_title)
        click_link "Delete Job Title"
        page.should have_content "Job Title was successfully deleted"
      end
    end
  end
end
