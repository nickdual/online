require "spec_helper"

describe "Admin::Groups::JobTitlesController" do

  before(:each) do
    login_as_admin
    @group = Group.make!
    @job_title = Group::JobTitle.make! group: @group
  end

  context "index action" do
    it "lists job titles" do
      visit admin_group_path(@group)
      click_link "Job Titles"
      page.should have_content @job_title.name
    end
  end

  context "show action" do
    use_vcr_cassette "create_service"

    it "lists users for service" do
      service = Service.make! group: @group
      user = User.make!
      Employment.make! service: service, user: user, job_title_id: @job_title.id
      visit admin_group_job_title_path(@group, @job_title)
      page.should have_link user.name
    end
  end

  context "administration" do
    it "can create a job title" do
      visit admin_group_job_titles_path(@group)
      click_link "New Job Title"
      fill_in "Name", with: "Aardvark Herder"
      click_button "Create Job title"
      page.should have_content "Job Title was successfully created"
    end

    it "can edit a Job Title" do
      visit admin_group_job_title_path(@group, @job_title)
      click_link "Edit"
      fill_in "Name", with: "Nerf Herder"
      click_button "Update Job title"
      page.should have_content "Job Title was successfully updated"
    end

    it "can delete a Job Title" do
      visit admin_group_job_title_path(@group, @job_title)
      click_link "Delete Job Title"
      page.should have_content "Job Title was successfully deleted"
    end
  end
end
