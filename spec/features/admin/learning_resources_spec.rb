require "spec_helper"

describe "LearningResourcesController" do
  before(:each) do
    login_as_admin
  end

  it "I can create a learning resource" do
    Program.make! title: "Something"
    click_link "Programs"
    click_link "Something"
    click_link "New Learning Resource"
    fill_in "Name", with: "Book of Spells"
    attach_file "Resource", "spec/fixtures/cat.jpg"
    click_button "Create Learning resource"
    page.should have_content "Learning resource was successfully created."
  end

  it "I can edit and remove a learning resource" do
    program = Program.make! title: "Something"
    LearningResource.make!(program: program)
    click_link "Programs"
    click_link "Something"

    within "#learning_resources" do
      click_link "Edit"
    end

    fill_in "Name", with: "Books of Smells"
    click_button "Update Learning resource"
    page.should have_content "Learning resource was successfully updated."
    page.should have_content "Books of Smells"

    click_link "Delete"
    page.should have_content "Learning resource was successfully destroyed."
    page.should_not have_content "Books of Smells"
  end
end