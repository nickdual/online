require "spec_helper"

describe "GroupsController" do
  before(:each) { login_as_admin }

  it "I can create a group" do
    Package.make!(name: "Package")
    click_link "Groups"
    click_link "New Group"
    fill_in "Name", with: "Best Group"
    select "Australia", from: "Country"
    select "Package", from: "Packages"
    click_button "Create Group"

    page.should have_content("Group was successfully created.")
    page.should have_content("Packages")
  end

  context "Management" do
    before(:each) do
      Group.make!(name: "Best Group")
      click_link "Groups"
    end

    it "I can edit a group" do
      click_link "Edit"
      fill_in "Name", with: "Bestest Group"
      click_button "Update Group"
      page.should have_content("Group was successfully updated.")
    end

    it "I can disable and enable a group" do
      click_link "Best Group"
      click_link "Disable Group"
      page.should have_content "Successfully disabled group."
      page.should have_content "Disabled by #{@admin.name.downcase}"

      click_link "Enable Group"
      page.should have_content "Successfully enabled group."
      page.should have_content "Active"
    end

  end

  describe "exports" do
    before(:each) { @group = Group.make!(name: "Best Group") }

    context "users" do
      it "I can export groups as a csv" do
        visit admin_groups_path
        click_link "Export"
        page.should have_content "name,country,disabled_at,services_count,packages_count"
        page.should have_content "Best Group"
      end
    end

    context "job titles" do
      before(:each) do
        @job = @group.job_titles.create!(name: "Mr. Manager")
      end

      it "should export a list of job titles" do
        visit admin_group_path(@group)
        click_link "Export Job Titles"

        page.should have_content "id,title"
        page.should have_content [@job.id, @job.name].join(",")
      end
    end
  end

  describe "intro and closers" do
    let(:package) { Package.make!(programs: [program]) }
    let(:program) { Program.make!(processing: false) }
    let(:intro) { Program.make!(:introduction, processing: false) }
    let(:closer) { Program.make!(:closer, processing: false) }

    before(:each) do
      intro; closer
      @group = Group.make!(packages: [package])
    end

    it "can intros and closers to a program on a group" do
      visit admin_group_path(@group)
      click_link "Show Programs"
      click_link "Edit"

      check intro.title
      check closer.title
      click_button "Update Group Program"

      page.should have_content("Successfully updated program")
      GroupProgram.first.program_lists.count.should == 2

      click_link "Edit"
      page.has_checked_field?(intro.title).should be_true
      page.has_checked_field?(closer.title).should be_true
    end
  end
end
