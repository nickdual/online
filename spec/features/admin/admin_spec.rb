require "spec_helper"

describe "AdminController" do
  before(:each) { login_as_admin }

  context "Signing in (personal actions)" do
    it "I can login as an admin" do
      page.should have_content("Signed in successfully.")
    end

    it "I can logout as an admin" do
      click_link "Sign out"
      current_path.should == new_user_session_path
    end

    it "I can change my login details (name / email)" do
      click_link "Profile settings"
      fill_in "First name", with: "derp"
      click_button "Update Profile"

      page.should have_content "Admin was successfully updated."
      @admin.reload.first_name.should == "derp"
    end

    it "I can change my password" do
      click_link "Profile settings"
      fill_in "New password", with: "catdog"
      fill_in "Confirm new password", with: "catdog"
      click_button "Update Profile"

      page.should have_content "Admin was successfully updated."
      click_link "Sign out"

      # Sign in with new password
      visit admin_path
      fill_in "Email", with: @admin.email
      fill_in "Password", with: "catdog"
      click_button "Sign in"
      page.should have_content("Signed in successfully.")
    end
  end

  describe "Management" do
    context "new admin" do
      it "I can invite other administrator" do
        click_link "Admins"
        click_link "New Admin"

        fill_in "First name", with: "Bob"
        fill_in "Last name", with: "Huynh"
        fill_in "Email", with: "bob@icdesign.com.au"
        choose "admin_role_moderator"
        click_button "Create Admin"

        page.should have_content("Admin was successfully invited.")
        unread_emails_for("bob@icdesign.com.au").count.should == 1
        unread_emails_for("bob@icdesign.com.au").first.subject.should == "You have been invited to become an ACC Admin"
      end
    end

    context "existing admin(s)" do
      before(:each) do
        @admin1 = Admin.make!(email: "bob@icdesign.com.au", first_name: "Zee")
        click_link "Admins"
      end

      it "An admin can set their password when they are invited to the system" do
        click_link "Sign out"

        open_email("bob@icdesign.com.au", subject: "You've been invited to be an admin for ACC Online")
        visit_in_email "Set Admin Password"
        fill_in "New password", with: "123456"
        fill_in "Confirm your new password", with: "123456"
        click_button "Change my password"

        page.should have_content("Your password was changed successfully. You are now signed in.")
      end

      it "I can manage other admin's accounts" do
        within "table tr:nth-child(2)" do
          click_link "Edit"
        end

        choose "admin_role_viewer"
        click_button "Update Admin"

        page.should have_content("Admin was successfully updated.")
        # Admin.last.updated_by_id.should == @admin.id
      end

      it "I can remove an admin" do
        click_link @admin1.name
        click_link "Delete Admin"
        page.should have_content("Admin was successfully destroyed.")
        Admin.count.should == 1
      end

      it "I can export admins as a csv" do
        click_link "Export"
        page.should have_content("first_name,last_name,email,sign_in_count,last_sign_in_at")
        page.should have_content(@admin.email)
      end
    end
  end
end