require "spec_helper"

describe "PackagesController" do
  before(:each) { login_as_admin }

  it "I can create packages (and associate with a program)" do
    Program.make! title: "Something"
    click_link "Packages"
    click_link "New Package"
    fill_in "Name", with: "Package"
    check "Something"
    click_button "Create Package"
    page.should have_content "Package was successfully created"
  end

  it "I can edit a package" do
    Program.make! title: "Something"
    Package.make! name: "Package"
    click_link "Packages"
    click_link "Edit"
    fill_in "Name", with: "Package 1"
    click_button "Update Package"
    page.should have_content "Package was successfully updated."

    click_link "Package 1"
    click_link "Delete Package"
    page.should have_content "Package was successfully destroyed."
    page.should_not have_content "Package 1"
  end

  it "I can export packages as a csv" do
    Package.make! name: "Package"
    click_link "Packages"
    click_link "Export"
    page.should have_content "name,programs_count,created_at,updated_at"
    page.should have_content "Package"
  end

  describe "Introductions and closers" do
    let(:package) { Package.make!(programs: [program]) }
    let(:program) { Program.make!(processing: false) }
    let(:intro) { Program.make!(:introduction, processing: false) }
    let(:closer) { Program.make!(:closer, processing: false) }

    before(:each) { intro; closer }

    it "should allow assigning of introductions and closers to a program" do
      visit admin_package_path(package)
      click_link "Edit"

      check intro.title
      check closer.title
      click_button "Update Package Program"

      page.should have_content("Package program was successfully updated.")
      PackageProgram.first.program_lists.count.should == 2

      click_link "Edit"
      page.has_checked_field?(intro.title).should be_true
      page.has_checked_field?(closer.title).should be_true
    end
  end
end
