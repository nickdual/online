require 'spec_helper'

describe "ImagePreview", js: true do
  let(:image_preview){
    ImagePreview.new(self) }

  let(:image_path){
    'spec/fixtures/cat.jpg' }

  Steps do
    Given "I am viewing an uploaded image with the image preview widget" do
      image_preview.visit!
    end
    
    Then "I should see a preview of the image" do
      expect(image_preview.preview['src']).to match(%r{/assets/logo.png$})
    end
    
    And "I should see that the image file input is blank" do
      expect(image_preview.image_input.value).to be_blank
    end
    
    And "I should see that the Remove Image Input is unselected" do
      expect(image_preview.remove_image_input['checked']).to be_nil
    end

    When "I enter the path of a new image in the image file input" do
      image_preview.image_input.set File.join(Rails.root, image_path)
    end
    
    Then "I should see a preview of the new image" do
      expect(image_preview.preview['src']).to include(ImagePreview.encode64(image_path))
    end

    And "I should see that the Remove Image Input is unselected" do
      expect(image_preview.remove_image_input['checked']).to be_nil
    end

    When "I click on the Delete Image action" do
      image_preview.delete_action.click
    end
    
    Then "I should see a placeholder for the image" do
      expect(image_preview.preview['src']).to eq(ImagePreview::PLACEHOLDER_URL)
    end
    
    And "I should see that the Remove Image Input is selected" do
      expect(image_preview.remove_image_input['checked']).to be_true
    end
  end
end
