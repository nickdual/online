require 'spec_helper'

feature "Coordinator management", %q{
  In order maintain allow users to access group level details
  As an group coordinator
  I want to create and maintain other coordinators
} do

  use_vcr_cassette "create_service_from_group"
  background do
    login_as_group_coordinator
    visit "/coordinator/groups/#{@group.id}/coordinators"

    sleep(0.5)
  end

  scenario 'I can create a new coordinator', js: true do
    click_link "New Coordinator"

    fill_in "First name", with: "Albus"
    fill_in "Last name", with: "Dumbledore"
    fill_in "Email", with: "a.dumbledore@hogwarts.co.uk"

    click_button "Invite Coordinator"

    page.should have_content "Successfully added coordinator"
    Coordinator.count.should == 2
    unread_emails_for("a.dumbledore@hogwarts.co.uk").last.subject.should == "Your Group Coordinator Account has been Created for #{@group}"

    # scenario 'I can remove an existing coordinator'
    find("[role=remove]").click
    page.driver.browser.switch_to.alert.accept

    page.should have_content "Successfully removed coordinator"
    Coordinator.count.should == 1
  end

  scenario 'I can invite an existing user to a group', js: true do
    User.make!(email: "a.dumbledore@hogwarts.co.uk")
    click_link "New Coordinator"

    fill_in "First name", with: "Albus"
    fill_in "Last name", with: "Dumbledore"
    fill_in "Email", with: "a.dumbledore@hogwarts.co.uk"

    click_button "Invite Coordinator"

    page.should have_content "Successfully added coordinator"
    Coordinator.count.should == 2
    unread_emails_for("a.dumbledore@hogwarts.co.uk").last.subject.should == "You are now a Group Coordinator for #{@group}"
  end
end