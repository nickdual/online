require 'spec_helper'

feature "Learning Plans", %q{
  In order to train users in particular areas
  As an coordinator
  I want to create and maintain learning plans
} do

  use_vcr_cassette "create_service"
  background do
    login_as_coordinator

    @program = Program.make!(:processed, title: "Fire Safety")
    @package = Package.make!(programs: [@program])
    @service.update_attributes(packages: [@package])

    visit "/coordinator/services/#{@service.id}/learning-plans"
    sleep(0.5)
  end

  scenario 'I change the assessment criteria for a program for a new learning plan', js: true do
    find('[role=new]').click

    fill_in "name", with: "Fire Training Plan"
    fill_in "days_till_due", with: 5
    fill_in "active_at", with: Date.today.strftime("%d/%m/%Y")

    within "#programsearch" do
      fill_in 'search', with: Program.first.title
      find('#programs-form-results li:first').click
      find('a.add').click
    end

    all("#programCriteria tbody > tr.assessment").count.should == 1

    # Essentials are already checked
    all(".essentials input").first.should be_checked
    all(".essentials input").last.should  be_checked

    # The extension column should be toggled
    check "legend_extension"
    all(".extension input").first.should be_checked
    all(".extension input").last.should  be_checked

    uncheck "legend_extension"
    all(".extension input").first.should_not be_checked
    all(".extension input").last.should_not  be_checked

    within "#programsearch" do
      find('#programs-form-chosen li:first').click
      find('a.remove').click
    end

    all("#programCriteria tbody > tr.assessment").count.should == 0

    within "#programsearch" do
      fill_in 'search', with: Program.first.title
      find('#programs-form-results li:first').click
      find('a.add').click
    end

    within "#programCriteria" do
      check "extension"
    end

    click_button "Create"

    page.should have_content("Successfully created learning plan")
    @learning_plan = LearningPlan.last
    @learning_plan.learning_plan_programs.count.should == 1
    @learning_plan.learning_plan_programs.first.extension.should == true
  end
end