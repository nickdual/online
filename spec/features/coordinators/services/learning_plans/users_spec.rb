require 'spec_helper'

feature "Learning Plans", %q{
  In order to train users in particular areas
  As an coordinator
  I want to create and maintain learning plans
} do

  use_vcr_cassette "create_service"
  background do
    login_as_coordinator

    @program1 = Program.make!(:processed, title: "Fire Safety")
    @program2 = Program.make!(:processed, title: "Infection Control")
    @package = Package.make!(programs: [@program1, @program2])
    @plan = @service.learning_plans.make!(programs: [@program1], employments: [@employment], service: @service)
    LearningPlanProgram.first.update_attributes(essentials: true)
    EmploymentLearningPlan.first.active!
  end

  scenario 'I can mark program assessments', js: true do
    visit "/coordinator/services/#{@service.id}/learning-plans/#{@plan.id}/users"
    sleep(0.5)

    click_link @employment.name
    sleep(0.5)
    click_link "Approve"

    sleep(0.5)
    page.should have_content "Successfully assessed program"
    ProgramAssessment.count.should == 1
    all('[role="access"]').count.should == 0 # Approve button should be hidden
  end

  scenario 'I can add and remove a user from a plan', js: true do
    visit "/coordinator/services/#{@service.id}/learning-plans/#{@plan.id}/users"
    sleep(0.5)

    find('[role="remove"]').click
    page.driver.browser.switch_to.alert.accept

    sleep(0.5)
    page.should have_content "Successfully removed user"
    @plan.users.count.should == 0
  end
end