require 'spec_helper'

feature "Learning Plans", %q{
  In order to train users in particular areas
  As an coordinator
  I want to create and maintain learning plans
} do

  use_vcr_cassette "create_service"
  background do
    login_as_coordinator

    @program = Program.make!(:processed, title: "Fire Safety")
    @package = Package.make!(programs: [@program])
    @service.update_attributes(packages: [@package])

    visit "/coordinator/services/#{@service.id}/learning-plans"
    sleep(0.5)
  end

  scenario 'I can view learning plans', js: true do
    within "ul#planslist" do
      page.should have_content "There are no learning plans for this service"
    end
  end

  scenario 'I can create learning plan', js: true do
    find('[role=new]').click 

    fill_in "name", with: "Fire Training Plan"
    fill_in "days_till_due", with: 5
    fill_in "active_at", with: Date.today.strftime("%d/%m/%Y")

    within "#programsearch" do
      fill_in 'search', with: Program.first.title
      find('#programs-form-results li:first').click
      find('a.add').click
    end

    within "#usersearch" do
      fill_in 'search', with: Employment.first.name
      find('#users-form-results li:last').click
      find('a.add').click
    end

    click_button "Create"

    page.should have_content("Successfully created learning plan")
    @learning_plan = LearningPlan.last
    @learning_plan.programs.count.should == 1
    @learning_plan.employment_learning_plans.count.should == 1
  end

  scenario 'I can edit a learning plan', js: true do
    @plan = LearningPlan.make!(service: @service, programs: [@program], employments: [Employment.first])
    visit "/coordinator/services/#{@service.id}/learning-plans/#{@plan.id}/users"
    sleep(0.5)

    click_link "Edit"
    within "#usersearch" do
      find('#users-form-chosen li:last').click
      find('a.remove').click

      fill_in 'search', with: Employment.first.name
      find('#users-form-results li:last').click
      find('a.add').click
    end

    click_button "Update Learning Plan"
    page.should have_content("Successfully updated learning plan")
  end
end