require 'spec_helper'

feature "Group Sessions", %q{
  In order to create learning records for users in a service
  As an coordinator
  I want to create group sessions
} do

  use_vcr_cassette "create_service"
  background do
    login_as_coordinator

    @program = Program.make!(:processed, title: "Fire Safety")
    @package = Package.make!(programs: [@program], services: [@service])

    visit "/coordinator/services/#{@service.id}"
    sleep(0.5)
    click_link "Sessions"
  end

  scenario 'I can mark program assessments', js: true do
    sleep(0.5)

    # Selecting program
    fill_in "programs-autocomplete", with: "fire"
    find('ul#programs-results li').click

    # Filling in date
    fill_in "viewed_at_date", with: "21/05/1988"

    # Other meta-data
    check "Add program record?"
    choose "Online"

    check "Add assessment record?"
    check "Essentials"
    check "Evidence"

    # Selecting users
    fill_in "users-autocomplete", with: @user.first_name
    find('ul#users-form-results li').click
    find('.add').click
    click_button "Create Group Session"

    page.should have_content("Successfully created group session")
    LearningRecord.count.should == 1
    ProgramAssessment.count.should == 2

    @session = GroupSession.first
    @session.employments.count.should == 1
  end

  scenario 'I can attempt to create an invalid group session', js: true do
    click_button "Create Group Session"

    page.should have_content "ERROR: There were errors while trying to create a group session. Please check issues below."
    page.should have_content "ERROR: You must select a program"
    page.should have_content "ERROR: Date must be present"
    page.should have_content "ERROR: You must choose to add a record for a PROGRAM, an ASSESSMENT, or BOTH"
    page.should have_content "ERROR: You must choose AT LEAST ONE USER"
  end
end