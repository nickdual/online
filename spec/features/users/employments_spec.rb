require 'spec_helper'

feature "Profile editing", %q{
  In order to save coordinators having to keep everyone's information up to date
  As an employment
  I want maintain my own employment information
} do

  use_vcr_cassette "create_service"
  background do
    login_as_user
    click_link "Settings"
    sleep(0.5)
  end

  use_vcr_cassette "create_multiple_services"
  scenario 'I can join a new employment', js: true do
    @service = Service.make!

    click_link "Settings"
    click_link "New Employment"
    fill_in "activation_token", with: @service.activation_token
    click_button "Join Service"

    page.should have_content("Successfully joined employment")
    page.should have_content(@service.name)
    Employment.count.should == 2
  end

  scenario 'I can edit my employment details', js: true do
    find('a[role="edit"]').click
    fill_in "employee_number", with: "123456"
    click_button "Update"

    page.should have_content("Successfully updated employment")
  end

  scenario 'I can leave my employmnet', js: true do
    find('a[role="leave"]').click
    page.driver.browser.switch_to.alert.accept
    page.should have_content("Successfully left service")
  end
end