require 'spec_helper'

feature "User Registration", %q{
  In order to give users access to the ACC Online interface, without giving access to everyone
  As a User
  I want User access privileges
} do

  use_vcr_cassette "create_service"
  background { @service = Service.make! }

  scenario 'I can register an account' do
    visit root_path
    click_link "Register"

    fill_in "First name",       with: "Thomas"
    fill_in "Last name",        with: "Sinclair"
    fill_in "Email",            with: "thomas@icdesign.com.au"
    fill_in "Activation token", with: @service.activation_token

    click_button "Sign Up"

    current_path.should == "/"
    @user = User.last

    unread_emails_for(@user.email).count.should == 1
    unread_emails_for(@user.email).first.subject.should == "Your Account has been Successfully Activated with #{@service}"
  end
end