require 'spec_helper'

feature "Profile editing", %q{
  In order to keep my details updated
  As a user
  I want update my profile
} do

  use_vcr_cassette "create_service"
  background { login_as_user }

  scenario 'I can register an account', js: true do
    click_link "Settings"
    sleep(0.5)

    fill_in "first_name", with: "Harry"
    fill_in "last_name", with: "Potter"
    select "Male", from: "gender"
    click_button "Save"

    page.should have_content("Successfully updated profile")
  end
end