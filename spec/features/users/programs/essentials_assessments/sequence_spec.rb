require 'spec_helper'

feature "Essentials Assessment" do

  use_vcr_cassette "create_service"

  let(:fixture){ Fixture.new(
    :sequence_question,
    :employment_learning_plan,
    :learning_plan_program) }

  background do

    sign_in_to_login_page fixture.user
    visit("/programs/#{fixture.program.id}")
    click_link "Start Assessment"
    find("[role=next]").click
  end

  scenario 'Pass a multiple choice section', js: true do
    # Since drag_by doesn't work with jQuery.ui out of the box, we're hacking around it in js
    within "#questions" do
      page.execute_script("$('#questions .sortable .ui-state-default:nth-child(1)').attr({'data-answer-id': #{Program::Assessment::SequentialAnswerItem.correct_order.first.id}})")
      page.execute_script("$('#questions .sortable .ui-state-default:nth-child(2)').attr({'data-answer-id': #{Program::Assessment::SequentialAnswerItem.correct_order.second.id}})")
      page.execute_script("$('#questions .sortable .ui-state-default:nth-child(3)').attr({'data-answer-id': #{Program::Assessment::SequentialAnswerItem.correct_order.third.id}})")
      page.execute_script("ACC.Programs.get(#{fixture.program.id}).EssentialsAssessment.get(#{fixture.program_assessment.id}).Sections.get(#{fixture.sequence_section.id}).Questions.get(#{fixture.sequence_question.id}).set({correct: true})")
      page.execute_script("$('form input.disabled').removeClass('disabled')")
    end

    find("[type=submit]").click
    page.should have_content "Congratulations!"
    page.should have_content "You scored 100% in the Sequencing section."

    find("[role=next]").click
    page.should have_content "You scored 100%"

    visit root_path # ensure database updates are done before checks below

    User::AssessmentResult.count.should == 1
    User::AssessmentResult.first.passed.should == true

    User::AssessmentResultQuestion.count.should == 1
    User::AssessmentResultQuestion.first.correct.should == true
  end

  scenario 'Fail a multiple choice section', js: true do
    page.execute_script("$('form input.disabled').removeClass('disabled')")
    page.execute_script("ACC.Programs.get(#{fixture.program.id}).EssentialsAssessment.get(#{fixture.program_assessment.id}).Sections.get(#{fixture.sequence_section.id}).Questions.get(#{fixture.sequence_question.id}).set({correct: false})")
    page.execute_script("ACC.Programs.get(#{fixture.program.id}).EssentialsAssessment.get(#{fixture.program_assessment.id}).Sections.get(#{fixture.sequence_section.id}).Questions.get(#{fixture.sequence_question.id}).set({answers: [{id: 1, answer: 'Nothing', correct: true}]})")
    page.execute_script("ACC.Programs.get(#{fixture.program.id}).EssentialsAssessment.get(#{fixture.program_assessment.id}).Sections.get(#{fixture.sequence_section.id}).Questions.get(#{fixture.sequence_question.id}).set({userAnswer: 1})")
    page.execute_script("ACC.Programs.get(#{fixture.program.id}).EssentialsAssessment.get(#{fixture.program_assessment.id}).Sections.get(#{fixture.sequence_section.id}).Questions.get(#{fixture.sequence_question.id}).set({yourAnswerOrder: [1]})")
    page.execute_script("ACC.Programs.get(#{fixture.program.id}).EssentialsAssessment.get(#{fixture.program_assessment.id}).Sections.get(#{fixture.sequence_section.id}).Questions.get(#{fixture.sequence_question.id}).set({correctAnswerOrder: [1]})")

    sleep(0.5)
    find("[type=submit]").click

    all(".question.feedback.incorrect").count.should == 1
    page.should_not have_content "Congratulations!"

    find("[role=next]").click
    page.should have_content "You scored 0%"

    visit root_path # ensure database updates are done before checks below

    User::AssessmentResult.count.should == 1
    User::AssessmentResult.first.passed.should == false

    User::AssessmentResultQuestion.count.should == 1
    User::AssessmentResultQuestion.first.correct.should == false
    User::AssessmentResultQuestion.first.question_type.should ==
      Program::Assessment::SequentialQuestion.question_type
  end
end
