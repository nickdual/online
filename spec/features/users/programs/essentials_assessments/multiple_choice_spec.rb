require 'spec_helper'

feature "Essentials Assessment" do
  use_vcr_cassette "create_service"

  let(:fixture){ Fixture.new(
    :multiple_choice_question,
    :employment_learning_plan,
    :learning_plan_program) }

  background do
    sign_in_to_login_page fixture.user
    visit("/programs/#{fixture.program.id}")
    click_link "Start Assessment"
    find("[role=next]").click
  end

  scenario 'Pass a multiple choice section', js: true do
    within "#questions" do
      choose fixture.multiple_choice_question.answers.where(correct: true).first.answer
    end

    find("[type=submit]").click
    page.should have_content "Congratulations!"
    page.should have_content "You scored 100% in the Multiple Choice section."

    find("[role=next]").click
    page.should have_content "You scored 100%"

    visit root_path # ensure database updates are done before checks below

    User::AssessmentResult.count.should == 1
    User::AssessmentResult.first.passed.should == true

    User::AssessmentResultQuestion.count.should == 1
    User::AssessmentResultQuestion.first.correct.should == true
  end

  scenario 'Fail a multiple choice section', js: true do
    within "#questions" do
      choose fixture.multiple_choice_question.answers.where(correct: false).first.answer
    end

    find("[type=submit]").click
    all(".question.feedback.incorrect").count.should == 1
    page.should_not have_content "Congratulations!"

    find("[role=next]").click
    page.should have_content "You scored 0%"

    visit root_path # ensure database updates are done before checks below

    User::AssessmentResult.count.should == 1
    User::AssessmentResult.first.passed.should == false

    User::AssessmentResultQuestion.count.should == 1
    User::AssessmentResultQuestion.first.correct.should == false
    User::AssessmentResultQuestion.first.question_type.should ==
      Program::Assessment::MultipleChoiceQuestion.question_type
  end
end
