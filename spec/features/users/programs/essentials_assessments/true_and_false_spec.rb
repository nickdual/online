require 'spec_helper'

feature "Essentials Assessment" do

  use_vcr_cassette "create_service"

  let(:fixture){ Fixture.new(
    :true_or_false_question,
    :employment_learning_plan,
    :learning_plan_program) }

  background do
    sign_in_to_login_page fixture.user
    visit("/programs/#{fixture.program.id}")
    click_link "Start Assessment"
    find("[role=next]").click
  end

  scenario 'Pass a true or false section', js: true do
    within "#questions" do
      choose "True"
    end

    find("[type=submit]").click
    page.should have_content "Congratulations!"
    page.should have_content "You scored 100% in the True/False section."

    find("[role=next]").click
    page.should have_content "You scored 100%"

    visit root_path # ensure database updates are done before checks below

    User::AssessmentResult.count.should == 1
    User::AssessmentResult.first.passed.should == true

    User::AssessmentResultQuestion.count.should == 1
    User::AssessmentResultQuestion.first.correct.should == true
  end

  scenario 'Fail a true or false section', js: true do
    within "#questions" do
      choose "False"
    end

    find("[type=submit]").click
    all(".question.feedback.incorrect").count.should == 1
    page.should_not have_content "Congratulations!"

    find("[role=next]").click
    page.should have_content "You scored 0%"

    visit root_path # ensure database updates are done before checks below

    User::AssessmentResult.count.should == 1
    User::AssessmentResult.first.passed.should == false

    User::AssessmentResultQuestion.count.should == 1
    User::AssessmentResultQuestion.first.correct.should == false
    User::AssessmentResultQuestion.first.question_type.should ==
      Program::Assessment::TrueOrFalseQuestion.question_type
  end
end
