require 'spec_helper'

describe '/programs' do
  use_vcr_cassette "create_service"

  describe "Scenario: Only active assessments are available to users", js: true do
    let(:fixture){
      Fixture.new(:program_assessment, :employment_learning_plan, :learning_plan_program).tap do |fixture|
        fixture.program_assessment.activated_at = nil
        fixture.program_assessment.save!
      end
    }

    let(:user_program_page){
      UserProgramPage.new(self).tap {|page|
        page.program = fixture.program } }

    let(:edit_admin_program_assessment_page){
      EditAdminProgramAssessmentPage.new(self).tap {|page|
        page.assessment = fixture.program_assessment } }

    Steps do
      Given "an assessment is inactive" do
        expect(fixture.program_assessment).not_to be_active
      end

      When "I view the program as a user" do
        sign_in_to_login_page fixture.user
        user_program_page.visit!
      end

      Then "I should not be able to see the assessment" do
        expect(lambda { user_program_page.start_assessment_button }).to raise_error(Capybara::ElementNotFound)
      end

      When "I publish the assessment" do
        login_as_admin
        edit_admin_program_assessment_page.visit!
        edit_admin_program_assessment_page.publish_button.click
      end

      Then "I should see that the assessment is active" do
        fixture.program_assessment.reload
        expect(fixture.program_assessment).to be_active
      end

      And 'I should see that I can no longer "republish" the assessment' do
        edit_admin_program_assessment_page.visit!
        expect(edit_admin_program_assessment_page.publish_button['disabled']).to eq('true')
      end

      When 'I view the program as a user' do
        user_program_page.visit!
      end

      Then 'I should be able to see the assessment' do
        expect(user_program_page.start_assessment_button).not_to be_nil
      end

      And 'I should be able to start the assessment' do
        user_program_page.start_assessment_button.click
      end
    end
  end

  describe "Scenario: unsuccessful assessments", js: true do
    let(:fixture){
      Fixture.new(:true_or_false_question, :employment_learning_plan) }

    let(:user_program_page){
      UserProgramPage.new(self).tap {|page|
        page.program = fixture.program } }

    let(:edit_admin_program_assessment_page){
      EditAdminProgramAssessmentPage.new(self).tap {|page|
        page.assessment = fixture.program_assessment } }

    let(:new_user_essentials_assessment_page){
      NewUserEssentialsAssessmentPage.new(self) }

    let(:true_or_false_question_page){
      TrueOrFalseQuestionPage.new(self) }

    let(:user_learning_plan_page){
      UserLearningPlanPage.new(self) }

    Steps do
      Given "an essential assessment is active and is required" do
        expect(fixture.program_assessment.assessment_type).to eq('Essentials')
        expect(fixture.program_assessment).to be_active
        expect(fixture.learning_plan_program.essentials).to be_true
      end

      When "I view the program as a user" do
        sign_in_to_login_page fixture.user
        user_program_page.visit!
      end

      Then "I should see that the assessment is pending" do
        expect(user_program_page.essentials_assessment_section).to have_css('.badge-pending')
      end

      When "I am unsuccessful in completing an essentials assessment" do
        user_program_page.start_essentials_assessment
        new_user_essentials_assessment_page.start
        true_or_false_question_page.submit(! fixture.true_or_false_question.answer)
        user_program_page.visit!
      end

      Then "I should see that the assessment is unsuccessful" do
        expect(user_program_page.essentials_assessment_section).to have_css('.badge-important')
        expect(user_program_page.essentials_assessment_section).to have_content('Unsuccessful')
      end

      Then "I should also see that the assessment is unsuccessful in my learning plan page" do
        user_learning_plan_page.visit!(fixture.employment_learning_plan)
        expect(user_learning_plan_page.pending_program.essentials_assessment.badge['class']).to include('badge-important')
        expect(user_learning_plan_page.pending_program.essentials_assessment.text).to include('Unsuccessful')
      end
    end
  end
end

