require 'spec_helper'

describe Video do
  include EmailSpec::Helpers
  include EmailSpec::Matchers
  let(:admin) { Admin.make! }
  let(:video) { program.videos.new(file: File.open("#{Rails.root}/spec/fixtures/meow.mp4"), description: "Description") }
  let(:program) { Program.create!(:title => "derp", :code => "derp", :description => "derp", :created_by => admin, :updated_by => admin) }
  before(:each) { reset_mailer }
  subject { video }

  context "create a video" do
    it { subject.save.should == true }
  end

  context "height / width" do
    context "zencoder id passed" do
      subject { video }
      before(:each) { video.zencoder_id = 1; video.save! }

      it { subject.height.should eql(1234) }
      it { subject.width.should eql(12345) }
    end
  end

  describe "Codec handling" do
    subject { video.codec }
    context "browser_h264_hd" do
      before(:each) do
        video.description = "browser_h264_hd"
        video.save!
      end

      it { subject.should == "video/mp4; codecs='avc1.64001E, mp4a.40.2'" }
    end

    context "browser_webm_hd" do
      before(:each) do
        video.description = "browser_webm_hd"
        video.save!
      end

      it { subject.should == "video/webm; codecs='vp8, vorbis'" }
    end

    context "browser_h264_sd" do
      before(:each) do
        video.description = "browser_h264_sd"
        video.save!
      end

      it { subject.should == "video/mp4" }
    end

    context "browser_webm_sd" do
      before(:each) do
        video.description = "browser_webm_sd"
        video.save!
      end

      it { subject.should == "video/webm" }
    end
  end

  describe "order videos based on description" do
    context "browser_webm_sd" do
      before(:each) do
        video.description = "browser_webm_sd"
        video.save
      end

      it { subject.order_id.should == 3 }
    end

    context "browser_webm_hd" do
      before(:each) do
        video.description = "browser_webm_hd"
        video.save
      end

      it { subject.order_id.should == 4 }
    end
  end

  describe "Scopes" do
    let(:video1) { program.videos.new(:file => File.open("#{Rails.root}/spec/fixtures/meow.mp4"), :zencoder_id => 1) }
    before(:each) { video.save!; video1.save! }

    context ".zencoder_processed" do
      it { Video.zencoder_processed.count.should == 1 }
    end

    context ".sd" do
      it { Video.sd.count.should == 0 }

      context "with" do
        before(:each) { program.videos.create!(:description => "browser_h264_sd") }
        it { Video.sd.count.should eql(1) }
      end
    end

    context ".hd" do
      it { Video.hd.count.should == 0 }

      context "with" do
        before(:each) { program.videos.create!(:description => "browser_h264_hd") }
        it { Video.hd.count.should == 1 }
      end
    end

    context ".mobile" do
      it { Video.mobile.count.should eql(0) }

      context "with" do
        before(:each) { program.videos.create!(:description => "playlist") }
        it { Video.mobile.count.should eql(1) }
      end
    end
  end
end
