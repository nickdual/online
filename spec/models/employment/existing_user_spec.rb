require 'spec_helper'

describe Employment::ExistingUser do
  use_vcr_cassette "create_service"
  let(:service)       { Service.make! }
  let(:user)          { User.make! }

  let(:valid_attributes) {
    {
      service_id: service.id,
      user_id:    user.id,
      role:       "staff"
    }
  }

  let(:employment) { Employment::ExistingUser.new(valid_attributes) }

  before(:each) { reset_mailer; employment.save }
  subject       { unread_emails_for(user.email) }

  it { subject.count.should == 1 }
  it { subject.first.subject.should == "Your Account is now Active with #{service}" }
end