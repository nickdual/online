require 'spec_helper'

describe PackageProgramList do
  before(:each) do
    @program = Program.make!(:processed)
    @package = Package.make!
    @program_package = @package.package_programs.create!(:program => @program)
    @package_program_list = @program_package.program_lists.new(:program => Program.make!(:introduction))
  end
  
  it "should be able to create a valid instance of PackageProgramList" do
    @package_program_list.save!
  end
  
  describe "intros vs. closers" do
    before(:each) { @package_program_list.save! }
    
    it "should order the PackageProgramList automatically based on the classification" do
      @package_program_list.position.should eql(1)
    end
    
    it "should increase the order of my package" do
      @package_program_list = @program_package.program_lists.create!(:program => Program.make!(:introduction))
      @package_program_list.position.should eql(2)
    end
    
    it "should scope positions via intros vs. closers" do
      @package_program_list = @program_package.program_lists.create!(:program => Program.make!(:closer))
      @package_program_list.position.should eql(1)
    end
  end
end
