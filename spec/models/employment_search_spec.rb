require 'spec_helper'

def search(params, service)
  EmploymentSearch.results({q: params}, service)
end

describe EmploymentSearch do
  use_vcr_cassette "create_service"
  let(:service) { Service.make! }

  describe "ransack methods" do
    context "role_eq" do
      before(:each) do
        @employment = Employment.make!(service: service)
        2.times { Employment.make!(role: "coordinator", service: service) }
      end

      subject { search({role_eq: "staff"}, service) }
      it { subject.count.should == 1 }
      it { subject.first.should == @employment }
    end

    context "job_title_id_eq" do
      let(:job_title) { Service::JobTitle.create!(name: "Wizard", service: service) }
      before(:each) do
        @employment = Employment.make!(job_title_id: job_title.id, service: service)
        2.times { Employment.make!(service: service) }
      end

      subject { search({job_title_id_eq: job_title.id}, service) }
      it { subject.count.should == 1 }
      it { subject.first.should == @employment }

      context "service" do
        use_vcr_cassette "create_service_via_employment"
        before(:each) { Employment.make!(job_title_id: job_title.id, service: Service.make!) }
        it { subject.count.should == 1 }
      end
    end
  end

  describe "learning records" do
    let(:program) { Program.make! }
    let(:employment) { Employment.make!(service: service) }

    before(:each) do
      employment.learning_records.create!(program: program)
      Employment.make!(service: service)
    end

    context "user_learning_records_program_id_eq" do
      subject { search({user_learning_records_program_id_eq: program.id}, service) }
      it { subject.count.should == 1 }
      it { subject.first.should == employment }
    end

    context "user_learning_records_program_id_does_not_exist" do
      before(:each) { @employment = Employment.last }
      subject { search({user_learning_records_program_id_does_not_exist: program.id}, service) }
      it { subject.count.should == 1 }
      it { subject.first.should == @employment }

      context "service" do
        use_vcr_cassette "create_service_via_employment"
        before(:each) { Employment.make!(service: Service.make!) }
        it { subject.count.should == 1 }
      end
    end
  end

  describe "learning plans" do
    let(:learning_plan) { LearningPlan.make!(service: service) }
    let(:employment) { Employment.make!(service: service) }
    let(:program) { Program.make! }

    before(:each) do
      learning_plan.update_attributes(employments: [employment, Employment.make!(service: service)], programs: [program])
      EmploymentLearningPlan.where(employment_id: employment.id).first.complete!
      Employment.make!(service: service)
    end

    context "completed_employment_learning_plans_learning_plan_id_eq" do
      before(:each) do
        EmploymentLearningPlan.all.each { |e| e.active! }
        LearningRecord.create!(employment: employment, program: program)
        ProgramAssessment.create!(employment: employment, program: program, assessment_type: "Essentials", coordinator: employment.user)
      end

      subject { search({completed_employment_learning_plans_learning_plan_id_eq: learning_plan.id}, service) }
      it { subject.count.should == 1 }
      it { subject.first.should == employment }
    end

    context "completed_employment_learning_plans_learning_plan_id_does_not_exist" do
      before(:each) do
        EmploymentLearningPlan.all.each { |e| e.active! }
        LearningRecord.create!(employment: employment, program: program)
        ProgramAssessment.create!(employment: employment, program: program, assessment_type: "Essentials", coordinator: employment.user)
        EmploymentLearningPlan.completed.count.should == 1
        EmploymentLearningPlan.completed.first.employment.should == employment
        service.employments.count.should == 3
      end

      subject { search({completed_employment_learning_plans_learning_plan_id_does_not_exist: learning_plan.id}, service) }

      it { subject.count.should == 2 }
      it { subject.should_not include(employment) }

      context "service" do
        use_vcr_cassette "create_service_via_employment"
        before(:each) { Employment.make!(service: Service.make!) }
        it { subject.count.should == 2 }
      end
    end
  end

  describe "learning assessments" do
    let(:program) { Program.make! }
    let(:employment) { Employment.make!(service: service) }
    before(:each) do
      employment.program_assessments.create!(program: program, coordinator: employment.user)
      2.times { Employment.make!(service: service) }
    end

    context "program_assessments_program_id_eq" do
      subject { search({program_assessments_program_id_eq: program.id}, service) }
      it { subject.count.should == 1 }
      it { subject.first.should == employment }
    end

    context "program_assessments_program_id_does_not_exist" do
      before(:each) { @employment = Employment.last }

      subject { search({program_assessments_program_id_does_not_exist: program.id}, service) }
      it { subject.count.should == 2 }
      it { subject.should_not include(employment) }

      context "service" do
        use_vcr_cassette "create_service_via_employment"
        before(:each) { @employment = Employment.make!(service: Service.make!) }
        it { subject.count.should == 2 }
      end
    end
  end
end
