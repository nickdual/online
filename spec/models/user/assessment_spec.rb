require 'spec_helper'

def active_employee_plan
  plan.employment_learning_plans.first.active!
end

def overdue_employee_plan
  plan.employment_learning_plans.first.overdue!
end

def complete_employee_plan
  active_employee_plan

  ["Essentials", "Evidence", "Extension"].each do |assessment_type|
    attributes = { assessment_type: assessment_type, program_id: program.id, coordinator_id: user.id}
    employment.program_assessments.create!(attributes)
  end
end

def create_second_learning_plan
  service = Service.make!
  employment = Employment.make!(user: user, service: service)
  learning_plan = LearningPlan.make!(service: service, programs: [program], employments: [employment])
  learning_plan.learning_plan_programs.first.update_attributes(attributes)
  learning_plan.employment_learning_plans.first.active!
end

describe User::Assessment, verifies_contract: 'User#assessments(Program)=>Hash' do
  use_vcr_cassette "create_multiple_services"
  let(:user)          { User.make! }
  let(:service)       { create_service }
  let(:employment)    { Employment.make!(service: service, user: user) }
  let(:program)       { Program.make!(:processed) }
  let(:plan)          { LearningPlan.make!(service: service, programs: [program], employments: [employment]) }
  let(:plan_program)  { plan.learning_plan_programs.first }
  let(:attributes)    { { essentials: true, evidence: true, extension: true } }

  before(:each) { plan_program.update_attributes(attributes) }
  subject { user.assessments(program) }

  describe "required" do
    before(:each) { active_employee_plan }

    it { subject[:essentials_required].should == true }
    it { subject[:evidence_required].should   == true }
    it { subject[:extension_required].should  == true }
  end

  describe "completed" do
    before(:each) { complete_employee_plan }

    it { subject[:essentials_completed].should  == true }
    it { subject[:evidence_completed].should    == true }
    it { subject[:extension_completed].should   == true }

    context "not completed if there is more than one learning plan" do
      before(:each) { create_second_learning_plan }

      it { subject[:essentials_completed].should_not  == true }
      it { subject[:evidence_completed].should_not    == true }
      it { subject[:extension_completed].should_not   == true }
    end
  end

  describe "overdue" do
    before(:each) { overdue_employee_plan }

    it { subject[:essentials_overdue].should  == true }
    it { subject[:evidence_overdue].should    == true }
    it { subject[:extension_overdue].should   == true }
  end
end

describe User::Assessment do
  describe "#essentials_failed" do
    let(:user){ VerifiedDouble.of_instance('User') }

    let(:essentials_assessment){
      VerifiedDouble.of_instance('Program::Assessment') }

    let(:program){
      VerifiedDouble.of_instance('Program',
        essentials_assessment: essentials_assessment) }

    let(:user_assessment){ described_class.new(user, program) }

    subject { user_assessment.essentials_failed }

    context "where user failed the program's essential assessment" do
      before do
        essentials_assessment.should_receive(:failed_by_user?).with(user).and_return(true)
      end

      it { should be_true }
    end

    context "where user passed the program's essential assessment" do
      before do
        essentials_assessment.should_receive(:failed_by_user?).with(user).and_return(false)
      end

      it { should be_false }
    end
  end
end
