require 'spec_helper'

describe User::Activity do
  let(:service)     { Service.make! }
  let(:user)        { User.make! }
  let(:employment)  { Employment.make!(service: service, user: user) }
  let(:program)     { Program.make!(:processed) }
  use_vcr_cassette "create_service"
  subject { user.activities.first }

  context "lists programs viewed online" do
    before(:each) do
      employment.learning_records.create!(program: program)
    end

    it { subject[:activity_type].should == "Viewed Program" }
    it { subject[:title].should         == program.title }
    it { subject[:medium].should        == "Viewed Online" }
  end

  context "lists programs viewed via group sessions" do
    let(:coordinator) { User.make! }
    before(:each) do
      attributes = {
        service:      service,
        employments:  [employment],
        program:      program,
        medium:       "DVD",
        created_by:   coordinator
      }

      service.group_sessions.make!(attributes)
    end

    it { subject[:activity_type].should     == "Viewed Program" }
    it { subject[:record_created_by].should == coordinator.name }
    it { subject[:medium].should            == "Viewed via DVD" }
  end

  context "lists assessments completed" do
    before(:each) do
      employment.program_assessments.create!(program: program, coordinator: employment.user)
    end

    it { subject[:activity_type].should   == "Essentials Assessment Completed" }
    it { subject[:title].should           == program.title }
    it { subject[:medium].should          == "Assessed Online" }
    it { subject[:assessment_type].should == "Essentials" }
  end

  context "lists assessments completed where coordinator is nil" do
    before(:each) do
      employment.program_assessments.create!(program: program, coordinator: nil)
    end

    it { expect(subject[:record_created_by]).to eq(user.name) }
  end
end
