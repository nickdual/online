require 'spec_helper'

describe User::SelfRegister do
  use_vcr_cassette "create_service"
  let(:service) { Service.make! }
  let(:user) { User::SelfRegister.new(valid_attributes)  }

  let(:valid_attributes) {
    {
      first_name:       "Harry",
      last_name:        "Potter",
      email:            "harry@icdesign.com.au",
      activation_token: service.activation_token
    } 
  }

  subject { user }

  context "notifications" do
    subject { unread_emails_for(user.email) }
    before(:each) { user.save! }

    it { subject.count.should == 1 }
    it { subject.first.subject.should == "Your Account has been Successfully Activated with #{service}" }
  end

  context "activation_token" do
    context "requires valid" do
      before(:each) { user.activation_token = "derp" }
      it { subject.should_not be_valid }
    end
  end

  context "default attributes" do
    before(:each) { subject.save! }
    it { subject.employments.first.role.should == "staff" }
  end
end