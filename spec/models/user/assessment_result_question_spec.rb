require 'spec_helper'

describe User::AssessmentResultQuestion do
  let(:question) { User::AssessmentResultQuestion.create(assessment_result_id: 1, question_id: 1) }

  context "create a User::AssessmentResultQuestion" do
    it { question.save.should == true }
  end

  describe ".count" do
    it "is the number of saved user assessment question results", verifies_contract: 'User::AssessmentResultQuestion.count()=>Fixnum' do
      question.save!
      expect(User::AssessmentResultQuestion.count()).to eq(1)
    end
  end

  describe ".correct" do
    it "should be the user assment question results that are correct", verifies_contract: 'User::AssessmentResultQuestion.correct()=>Class' do
      question.correct = true
      question.save!
      expect(User::AssessmentResultQuestion.correct).to eq([question])
    end
  end

  describe ".incorrect" do
    it "should be the user assment question results that are incorrect", verifies_contract: 'User::AssessmentResultQuestion.incorrect()=>Class' do
      question.correct = false
      question.save!
      expect(User::AssessmentResultQuestion.incorrect).to eq([question])
    end
  end
end
