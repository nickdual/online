require 'spec_helper'

describe User::Stats do
  use_vcr_cassette "create_service"
  let(:user) { User.make! }
  let(:service) { Service.make! }
  let(:employment) { Employment.make!(user: user, service: service) }
  let(:program) { Program.make! }
  subject { user }

  describe "plans_overdue_count" do
    before(:each) do
      LearningPlan.make!(service: service, programs: [program], employments: [employment])
      EmploymentLearningPlan.first.overdue!
    end

    it { subject.plans_overdue_count.should == 1 }
  end

  describe "assessments_completed_count" do
    before(:each) do
      employment.program_assessments.create!(program: program, employment: employment, coordinator: employment.user)
    end

    it { subject.assessments_completed_count.should == 1 }
  end

  describe "programs_viewed_count" do
    before(:each) do
      employment.learning_records.create!(program: program)
    end

    it { subject.programs_viewed_count.should == 1 }
  end

  describe "programs_awaiting_completion_count" do
    before(:each) do
      LearningPlan.make!(service: service, programs: [program], employments: [employment])
    end

    it { subject.programs_awaiting_completion_count.should == 1 }
  end

  describe "active_employments_count" do
    before(:each) { employment }
    it { subject.active_employments_count.should == 1 }
  end
end