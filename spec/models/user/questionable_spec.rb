require 'spec_helper'

describe User::Questionable do
  use_vcr_cassette "create_service"
  let(:service) { Service.make! }

  describe "New user" do
    let(:attributes) {
      {
        first_name:       "Harry",
        last_name:        "Potter",
        email:            "harry.potter@hogwarts.co.uk",
        activation_token: service.activation_token
      }
    }

    let(:questionable) { User::Questionable.new(attributes) }
    before(:each) { questionable.save }

    it { User.count.should == 1 }
    it { Employment.count.should == 1 }

    context "mailers" do
      subject { unread_emails_for("harry.potter@hogwarts.co.uk") }
      it { subject.count.should == 1 }
      it { subject.first.subject.should == "Your Account has been Created for #{service}" }
    end
  end

  describe "New employment" do
    let(:user) { User.make! }
    let(:attributes) {
      {
        first_name: user.first_name,
        last_name:  user.last_name,
        email:      user.email,
        service_id: service.id
      }
    }

    let(:questionable) { User::Questionable.new(attributes) }
    before(:each) { questionable.save }

    it { User.count.should == 1 }
    it { Employment.count.should == 1 }

    context "mailers" do
      subject { unread_emails_for(user.email) }
      it { subject.count.should == 1 }
      it { subject.first.subject.should == "Your Account is now Active with #{service}" }
    end
  end
end