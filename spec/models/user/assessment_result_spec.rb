require 'spec_helper'

describe User::AssessmentResult do
  use_vcr_cassette "create_service"
  let(:service)     { Service.make! }
  let(:user)        { User.make! }
  let(:program)     { Program.make! }
  let(:assessment)  { Program::Assessment.make!(program: program) }
  let(:result)      { User::AssessmentResult.new(user_id: user.id, assessment_id: assessment.id, time_taken: 5) }

  context "create an User::AssessmentResult" do
    it { result.should be_valid }
  end

  describe "program_assessment" do
    before(:each) { Employment.make!(user: user, service: service) }
    subject { user.program_assessments }

    context "none by default" do
      before(:each) { result.save }
      it { subject.count.should == 0 }
    end

    context "create a program_assessment if passed" do
      before(:each) do
        result.passed = true; result.save
      end

      it { subject.count.should == 1 }
    end
  end

  describe ".passed" do
    let!(:passing_result) {
      described_class.make!(
        passed: true,
        assessment: assessment,
        user: user) }

    it 'should be the results which passed', verifies_contract: 'User::AssessmentResult.passed()=>Class' do

      expect(described_class.passed).to eq([passing_result])
    end
  end

  describe ".count", verifies_contract: 'User::Assessment.count()=>Fixnum' do
    before do
      result.save!
    end

    it "is the number of results",
      verifies_contract: 'User::AssessmentResult.count()=>Fixnum' do

      expect(described_class.count).to eq(1)
    end
  end

  describe ".failed" do
    let!(:failing_result) {
      described_class.make!(
        passed: false,
        assessment: assessment,
        user: user) }

    it 'should be the results which failed' do
      expect(described_class.failed).to eq([failing_result])
    end
  end

  describe "#failed?" do
    subject { result.failed? }

    context "where passed is true" do
      let(:result){ described_class.new(passed: true) }
      it { should be_false }
    end

    context "where passed is false" do
      let(:result){ described_class.new(passed: false) }
      it { should be_true }
    end
  end
end
