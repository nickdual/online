require 'spec_helper'

describe User::Invited do
  use_vcr_cassette "create_service"
  let(:service) { Service.make! }

  let(:valid_attributes) {
    {
      first_name:       "Harry",
      last_name:        "Potter",
      email:            "harry@icdesign.com.au",
      activation_token: service.activation_token
    }
  }

  let(:user) { User::Invited.create(valid_attributes) }

  context "notifiers" do
    subject { unread_emails_for(user.email) }

    it { subject.count.should == 1 }
    it { subject.first.subject.should == "Your Account has been Created for #{service}" }
  end

end