require 'spec_helper'

describe ProgramAssessment do
  use_vcr_cassette "create_service"
  let(:service)    { Service.make! }
  let(:employment) { Employment.make!(service: service) }
  let(:program)    { Program.make! }
  subject { ProgramAssessment.new(program: program, employment: employment, coordinator: employment.user) }

  context "create a program assessment" do
    before(:each) { subject.save! }
    it { subject.save! }
  end
end
