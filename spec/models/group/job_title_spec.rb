require 'spec_helper'

describe Group::JobTitle do
  use_vcr_cassette "create_service"
  before(:each) do
    @group = Group.make!
    
    @valid_attributes = {
      group: @group, 
      name: "Poop", 
    }

    @group_job_title = Group::JobTitle.new(@valid_attributes)
  end

  it "should be able to create an instance of Group::JobTitle" do
    @group_job_title.save!
  end
  
  it "should reset the job title for employments if the group job is removed" do
    @group_job_title.save
    @service = Service.make!(group: @group)
    @employment = Employment.make!(job_title_id: @group_job_title.id, service: @service)

    @group_job_title.destroy
    @employment.reload.job_title_id.should be_nil
  end
end
