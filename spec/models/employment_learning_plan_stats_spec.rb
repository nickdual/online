require 'spec_helper'

describe "EmploymentLearningPlanStats" do
  use_vcr_cassette "create_service_via_employment"
  let(:service)       { Service.make! }
  let(:program)       { Program.make! }
  let(:user)          { User.make! }
  let(:employment)    { Employment.make!(service: service, user: user) }
  let(:learning_plan) { LearningPlan.make!(service: service, learning_plan_programs_attributes: [{program_id: program.id, essentials: true}]) }
  let(:plan) { EmploymentLearningPlan.new(employment: employment, learning_plan: learning_plan, active_at: 2.days.ago) }

  # The sleep is to get around some caching issues
  before(:each) { plan.save!; sleep(1); plan.active! }

  describe "program viewings" do
    context "programs_viewed_pending_ids" do
      subject { plan.programs_viewed_pending_ids }
      it { should == [LearningPlanProgram.first.id] }
    end

    context "programs_viewed_overdue_ids" do
      before(:each) { plan.overdue! }
      subject { plan.programs_viewed_overdue_ids }
      it { should == [LearningPlanProgram.first.id] }
    end

    context "programs_viewed_completed_ids" do
      before(:each) { LearningRecord.create!(program: program, employment: employment) }
      subject { plan.reload.programs_viewed_completed_ids }
      it { should == [LearningPlanProgram.first.id] }
    end
  end

  describe "program assessments" do
    context "assessment_not_required_count" do
      subject { plan.assessment_not_required_count }
      it { should == 2 }
    end

    context "program_assessments_completed_ids" do
      before(:each) { ProgramAssessment.create!(program: program, employment: employment, assessment_type: "Essentials", coordinator: user) }
      subject { plan.program_assessments_completed_ids }
      it { should == [LearningPlanProgram.first.id] }
    end

    context "program_assessments_pending_ids" do
      subject { plan.program_assessments_pending_ids }
      it { should == [LearningPlanProgram.first.id] }
    end

    context "program_assessments_overdue_ids" do
      before(:each) { plan.overdue! }
      subject { plan.program_assessments_overdue_ids }
      it { should == [LearningPlanProgram.first.id] }
    end
  end
end