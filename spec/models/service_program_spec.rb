require 'spec_helper'

def create_group_and_assign_to(service)
  @group = Group.make!(:packages => [Package.make!(:programs => [Program.make!(:processed)])])
  service.update_attributes(:group_id => @group.id)
end

describe ServiceProgram do
  use_vcr_cassette "create_service"
  before(:each) do
    @program = Program.make!(:processed)
    @service = Service.make!(:packages => [Package.make!(:programs => [@program, Program.make!(:processed)])])
  end
  
  it "should allow retrieval of the programs under a service easily" do
    @service.programs.count.should eql(2)
  end
  
  it "should not return uncompleted programs" do
    Package.first.programs << Program.make!
    @service.programs.count.should eql(2)
  end
  
  it "should not include duplicate programs" do
    create_group_and_assign_to(@service)
    @group.update_attributes(:packages => [Package.make!(:programs => [@program])])
    
    @service.service_programs.all.count.should eql(2)
  end
  
  it "should be able to return programs based on the group" do
    create_group_and_assign_to(@service)
    @service.programs.count.should eql(3)
  end
  
  it "should not include duplicate programs" do
    create_group_and_assign_to(@service)
    @group.update_attributes(:packages => [Package.make!(:programs => [@program])])
    @service.programs.all.count.should eql(2)
  end
end
