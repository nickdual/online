require 'spec_helper'
require 'cancan/matchers'

describe ProgramCategory do
  it "should be able to create ProgramCategory" do
    ProgramCategory.create!(:name => "Category")
  end

  describe "parents" do
    let(:parent) { ProgramCategory.create!(name: "Parent") }
    subject { ProgramCategory.create!(name: "Child", parent: parent) }

    it "should save the parent name" do
      subject.parent_category_name.should == parent.name
    end
  end

  describe "#all_programs_count" do
    let!(:program_class){ VerifiedDouble.of_class('Program') }
    let(:descendant) { subject.children.new(name: "Descendant").tap{|d| d.save! } }
    let(:programs){ [:program] }

    subject { ProgramCategory.create!(name: "Parent") }

    it "is the number of programs under the category and its descendants" do
      program_class
        .should_receive(:where)
        .with(category_id: [descendant.id, subject.id])
        .and_return(programs)

      expect(subject.all_programs_count).to eq(programs.count)
    end
  end
end
