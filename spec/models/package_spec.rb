require 'spec_helper'

describe Package do
  it "should be able to create a package" do
    Package.create!(:name => "wat")
  end
  
  describe "Programs" do
    before(:each) do
      @program = Program.make!
      @package = Package.make!(:programs => [@program])
    end
    
    it "should be able to accept programs" do
      @package.programs.count.should eql(1)
    end
    
    it "should allow me to order the programs" do
      @package.update_attributes(:programs => [@program, Program.make!])
      @package.programs.last.should_not eql(@program)
    end
    
    it "should be able to retreieve ProgramLists" do
      @package.package_programs.first.program_lists.create!(:program => Program.make!)
      @package.program_lists.count.should eql(1)
    end
  end
end
