require 'spec_helper'

describe ProgramThumb do
  before(:each) do
    @program = Program.make!
    @thumb = @program.build_thumb(:image => Rails.root.join("spec", "fixtures", "cat.jpg"))
  end
  
  it "should be able to create a thumb from program" do
    @thumb.save!
  end
end
