require 'spec_helper'

describe Coordinator do
  use_vcr_cassette "create_service"
  before(:each) do
    @user = User.make!
    @group = Group.make!
  end

  it "should be able to create a coordinator" do
    Coordinator.create!(:user => @user, :group => @group)
  end

  describe "Service Employments" do
    before(:each) do
      @service = @group.services.make!
      @coordinator = Coordinator.new(:user => @user, :group => @group)
    end

    it "should assign a coordinator as a coordinator under all the services for a group" do
      @coordinator.save!
      @service.employments.count.should eql(1)
      @service.employments.first.user.should eql(@user)
    end

    it "should disable the employment for all services when a coordinator's relationship is removed from a group" do
      @coordinator.save
      @coordinator.destroy
      @service.employments.count.should eql(1)
      @service.employments.first.status.should eql("inactive")
    end

    it "should update existing employments to coordinator if the user is already employed as a service" do
      @service.employments.create(:user => @user)
      @coordinator.save
      @service.employments.first.role.should eql("group coordinator")
      @service.employments.count.should eql(1)
    end

    it "should not include ex-employments" do
      @service.employments.create(user: @user, ended_at: Time.zone.now)
      @coordinator.save
      @service.employments.count.should eql(2)
    end

    context "mailers" do
      subject { unread_emails_for(@user.email) }
      before(:each) { @coordinator.save! }

      it "should not send the coordinator an email for each employment" do
        subject.count.should == 0
      end
    end

    context "group services" do
      use_vcr_cassette "create_service_from_group"
      it "should add a coordinator onto future services for a group" do
        @coordinator.save
        @service = @group.services.make!
        @service.employments.count.should eql(1)
      end
    end
  end

  describe "#save_with_user" do
    let(:group){ Group.make }
    let(:coordinator){ Coordinator.make(user: user, group: group) }

    context "where user is a new valid user" do
      let(:user){ User.make }

      it "creates the user" do
        expect(coordinator.save_with_user).to be_true
        expect(coordinator.user).to_not be_new_record
      end
    end

    context "where user is a new invalid user" do
      let(:user){ User.make(email: nil) }

      it "adds the error of the user to the errors of the coordinator" do
        expect(coordinator.save_with_user).to be_false
        expect(coordinator.user).to be_new_record
        expect(coordinator.errors[:email]).to eq(["can't be blank"])
      end
    end

    context "where user is a existing modified valid user" do
      let(:user){ User.make! }
      let(:new_first_name){ "New #{user.first_name}" }

      it "updates the user" do
        user.first_name = new_first_name
        coordinator.user = user
        expect(coordinator.save_with_user).to be_true
        expect(coordinator.user.reload.first_name).to eq(new_first_name)
      end
    end

    context "where user is a existing modified invalid user" do
      let(:user){ User.make! }
      let(:new_first_name){ "New #{user.first_name}" }

      it "adds the error of the user to the errors of the coordinator" do
        user.email = nil
        user.first_name = new_first_name
        expect(coordinator.save_with_user).to be_false
        expect(coordinator.user.first_name).to eq(new_first_name)
        expect(coordinator.errors[:email]).to eq(["can't be blank"])
      end
    end
  end
end
