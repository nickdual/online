require 'spec_helper'

describe ServiceSubscription do
  use_vcr_cassette "create_service"
  before(:each) do
    @service = Service.make!(:packages => [Package.make])
  end
  
  it "should be able to get a list of subscriptions" do
    @service.subscriptions.count.should eql(1)
  end
  
  it "should include groups in it's subscriptions" do
    @group = Group.make!(:packages => [Package.make!])
    @service.update_attributes(:group => @group)
    
    @service.subscriptions.count.should eql(2)
  end
  
  it "should not include groups which are not assigned to any facilties" do
    @group = Group.make!(:packages => [Package.make!])
    @service.subscriptions.count.should eql(1)
  end
end
