require 'spec_helper'

describe LearningPlan do
  use_vcr_cassette "create_service"
  before(:each) do
    @service = Service.make!
    @employment = Employment.make!(:service => @service)
    @program = Program.make!(:processed)

    @valid_attributes = {
      :service_id => @service.id,
      :name => "Learning Plan",
      :description => 'Learning plan description',
      :days_till_due => 5,
      :active_at => 2.days.ago,
      :employments => [@employment],
      :programs => [@program]
    }

    @learning_plan = LearningPlan.new(@valid_attributes)
  end

  context "allow creation of a learning plan" do
    it { @learning_plan.save! }
  end

  describe "#update_employment_learning_plans" do
    subject { @learning_plan.employment_learning_plans.first }

    context "employment learning plan is inactive" do
      before(:each) do
        @learning_plan.active_at = 2.days.from_now
        @learning_plan.save
      end

      it { subject.reload.pending?.should == true }

      context "does update days_till_due" do
        before(:each) do
          @learning_plan.update_attributes(days_till_due: 2)
        end

        it { @learning_plan.active_at.to_date.should == 2.days.from_now.to_date }
        it { subject.reload.due_at.to_date.should == 4.days.from_now.to_date }
      end
    end

    context "employment learning plan is the next day" do
      before(:each) do
        @learning_plan.save; subject.active!
      end

      it { subject.reload.active?.should == true }

      context "doesn't update days_till_due" do
        before(:each) do
          @learning_plan.update_attributes(days_till_due: 1)
        end

        # Starting from the next day, we're not active straight away.
        it { subject.reload.due_at.to_date.should == 5.days.from_now.to_date }
      end
    end
  end

  describe "Scopes" do
    use_vcr_cassette "create_service_via_learning_record"
    before(:each) do 
      LearningPlan.make!(:service => @service, :programs => [Program.make!], :employments => [Employment.make!])
    end

    it ".for_service" do
      LearningPlan.for_service(123456789).count.should eql(0)
      LearningPlan.for_service(@service.id).count.should eql(1)
    end
  end
end
