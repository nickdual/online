require 'spec_helper'

describe GroupProgramList do
  use_vcr_cassette "create_service"  
  before(:each) do
    @program = Program.make!(:processed)
    @package = Package.make!(:programs => [@program])
    @group = Group.make!(:packages => [@package])
    @group_program = @group.group_programs.first
    
    @valid_attributes = {
      :program => Program.make(:introduction),
      :group_program_id => @program.id,
      :group_id => @group.id
    }
    
    @group_program_list = @group_program.program_lists.new(@valid_attributes)
  end
  
  it "should be able to add to program lists" do
    @group_program_list.save!
  end
  
  describe "intros vs. closers" do
    before(:each) { @group_program_list.save! }
    
    it "should order the PackageProgramList automatically based on the classification" do
      @group_program_list.position.should eql(1)
    end
    
    it "should increase the order of my package" do
      @group_program_list = @group_program.program_lists.create!(:program => Program.make!(:introduction), :group_id => @group.id, :group_program_id => @program.id)
      @group_program_list.position.should eql(2)
    end
    
    it "should scope positions via intros vs. closers" do
      @group_program_list = @group_program.program_lists.create!(:program => Program.make!(:closer), :group_id => @group.id, :group_program_id => @program.id)
      @group_program_list.position.should eql(1)
    end
  end
end
