require 'spec_helper'

describe CoordinatorReminder do
  use_vcr_cassette "create_service"
  before(:each) do
    @service = Service.make!
    @user = User.make!
    Time.stub(:now).and_return(Time.new(2012,5,18,1,1,1))
    Time.now.stub(:friday?).and_return(true)
  end

  describe ".coordinator_weekly_report" do
    before(:each) do
      @employment = Employment.make!(service: @service, user: @user, role: "coordinator")
      reset_mailer
      Coordinator.coordinator_weekly_report
    end

    it "should email coordinators" do
      unread_emails_for(@user.email).count.should == 1
    end
  end
  
  describe ".group_coordinator_weekly_report" do
    before(:each) do
      @group = Group.make!
      @service.update_attributes(group: @group)
      @coordinator = Coordinator.create!(user: @user, group: @group)
      reset_mailer
      Coordinator.group_coordinator_weekly_report
    end
    
    it "should email group coordinators" do
      unread_emails_for(@user.email).count.should == 1
    end
  end
end