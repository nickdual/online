require "spec_helper"
require "cancan/matchers"

describe AdminAbility do
  use_vcr_cassette "create_service"
  before(:each) do
    admin = Admin.make!(:role => "viewer")
    @ability = AdminAbility.new(admin)
  end

  it "can read all models as a viewer (service as an example)" do
    @ability.should be_able_to(:read, Service.create(:name => "Hogwartz"))
  end

end
