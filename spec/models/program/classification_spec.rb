require 'spec_helper'

describe Program::Classification do
  describe ".all" do
    it "should be classification types of all the ids" do
      expect(described_class::IDS).to_not be_empty
      expect(described_class.all.map(&:id)).to eq(described_class::IDS)
      expect(described_class.all.map(&:class).uniq).to eq([described_class])
    end
  end

  describe ".find(id)" do
    let(:id){ described_class::IDS.first }
    subject { described_class.find(id) }

    it "returns the type matching the id" do
      expect(subject.id).to eq(id)
      expect(subject.class).to eq(described_class)
    end
  end

  describe "#to_s" do
    let(:id){ described_class::IDS.first }
    subject { described_class.find(id) }

    it "titleizes the id" do
      expect(subject.to_s).to eq(id.titleize)
    end
  end
end