require 'spec_helper'

describe Program::Assessment::QuestionTypeGroup do
  describe "#id" do
    it "doesn't change" do
      expect(subject.id).to_not be_nil
      expect(subject.id).to eq(subject.id)
    end
  end

  describe "#persisted?" do
    it "is true so that rails' dom_id helper can use the group's id" do
      expect(subject).to be_persisted
    end
  end
end
