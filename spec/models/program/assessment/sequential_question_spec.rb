require 'spec_helper'

describe Program::Assessment::SequentialQuestion do
  it_behaves_like 'a program assessment question'

  describe "#upload_images" do
    context "where #question_type is Image" do
      subject { described_class.new(question_type: "Image").upload_images }
      it { should be_true }
    end

    context "where #question_type is Text" do
      subject { described_class.new(question_type: "Text").upload_images }
      it { should be_false }
    end
  end

  describe "#upload_images=(value)" do
    before do
      subject.upload_images = value
    end

    context "where value is true" do
      let(:value){ true }

      it "sets #question_type to 'Image'" do
        expect(subject.question_type).to eq('Image')
      end
    end

    context "where value is false" do
      let(:value){ false }

      it "sets #question_type to 'Text'" do
        expect(subject.question_type).to eq('Text')
      end
    end

    context "where value is falsy" do
      let(:value){ '0' }

      it "sets #question_type to 'Text'" do
        expect(subject.question_type).to eq('Text')
      end
    end
  end

  describe "#add_default_answer_items" do
    it "adds three answer items with set correct positions", verifies_contract: "Program::Assessment::SequentialQuestion#add_default_answer_items" do
      subject.add_default_answer_items
      expect(subject.answer_items.map(&:correct_position)).to eq([1, 2, 3])
    end
  end
end
