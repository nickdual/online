require 'spec_helper'

describe Program::Assessment::MultipleChoiceQuestion do
  subject {
    Program::Assessment::MultipleChoiceQuestion.new(
      learning_outcome_id: 7,
      question: 'Call me?',
      section_id: 7) }

  describe "answers_attributes=(attributes_hash)" do
    context "where the question is a new record" do
      let(:answer_attributes) {
        { "answer"=>"Yes",
          "correct"=>"0",
          "_destroy"=>""} }

      it "should not require multiple_choice_questiod as an attribute" do
        subject.answers_attributes = {'0' => answer_attributes }
        expect(subject).to be_valid
      end
    end
  end


  it_behaves_like 'a program assessment question'
end
