require 'spec_helper'

describe Program::Assessment::SequentialAnswerItem do
  describe "#name" do
    subject {
      described_class.new(
        sequential_question: Program::Assessment::SequentialQuestion.new(upload_images: upload_images),
        text: 'text',
        reference_image: File.open('spec/fixtures/cat.jpg')) }

    context "where the parent question is set to upload images" do
      let(:upload_images){ true }

      context "and the answer item only has reference image url" do
        subject {
          described_class.new(
            sequential_question: Program::Assessment::SequentialQuestion.new(upload_images: upload_images),
            text: '',
            reference_image: File.open('spec/fixtures/cat.jpg')) }

        it { expect(subject.name).to eq(subject.reference_image_name) }
      end

      context "and the answer item has both text and reference image url" do
        it "should include both url and text" do
          expect(subject.name).to eq("#{subject.reference_image_name} (#{subject.text})")
        end
      end
    end


    context "where the parent question is not set to upload images" do
      let(:upload_images){ false }
      it { expect(subject.name).to eq(subject.text) }
    end
  end

  describe "#text" do
    subject {
      described_class.new(
        sequential_question: Program::Assessment::SequentialQuestion.new(upload_images: upload_images)) }

    context "where the parent question is set to upload images" do
      let(:upload_images){ true }

      it "is not required" do
        subject.valid?
        expect(subject.errors[:text]).to_not include("can't be blank")
      end
    end

    context "where the parent question is not set to upload images" do
      let(:upload_images){ false }

      it "is required" do
        expect(subject.text).to be_blank
        expect(subject).to_not be_valid
        expect(subject.errors[:text]).to include("can't be blank")
      end
    end
  end

  describe "#reference_image" do
    subject {
      described_class.new(
        sequential_question: Program::Assessment::SequentialQuestion.new(upload_images: upload_images)) }

    describe "where the parent question is set to upload images" do
      let(:upload_images){ true }

      it "is required" do
        expect(subject.reference_image).to be_blank
        expect(subject).to_not be_valid
        expect(subject.errors[:reference_image]).to include("can't be blank")
      end
    end

    describe "where the parent question is not set to upload images" do
      let(:upload_images){ false }

      it "is not required" do
        subject.valid?
        expect(subject.errors[:reference_image]).to_not include("can't be blank")
      end
    end
  end
end
