require 'spec_helper'

describe Program::Assessment::LearningOutcome do
  describe "#to_s" do
    subject { described_class.new(description: 'test') }
    it { expect(subject.to_s).to eq(subject.description) }
  end

  #TODO: implement
  describe "#percentage" do
    it "is a random number between 1 and 0 for now" do
      expect(subject.percentage).to be < 1
      expect(subject.percentage).to be >= 0
    end
  end

  describe "#question_type_groups" do
    let(:question){ Program::Assessment::TrueOrFalseQuestion.new }
    subject { described_class.new(true_or_false_questions: [question]) }

    it "builds questions groups for each question type under the learning outcome" do
      expect(subject.question_type_groups).to have(1).group
      expect(subject.question_type_groups[0].learning_outcome).to eq(subject)
      expect(subject.question_type_groups[0].questions).to eq([question])
      expect(subject.question_type_groups[0].question_class).to eq(question.class)
    end
  end

  describe "#questions" do
    let(:true_or_false_question){ Program::Assessment::TrueOrFalseQuestion.new }
    let(:multiple_choice_question){ Program::Assessment::MultipleChoiceQuestion.new }
    let(:sequential_question){ Program::Assessment::SequentialQuestion.new }

    subject {
      described_class.new(
        true_or_false_questions: [true_or_false_question],
        sequential_questions: [sequential_question],
        multiple_choice_questions: [multiple_choice_question]) }

    it "can include true or false questions" do
      expect(subject.questions).to include(true_or_false_question)
    end

    it "can include sequential questions" do
      expect(subject.questions).to include(sequential_question)
    end

    it "can include multiple choice questions" do
      expect(subject.questions).to include(multiple_choice_question)
    end
  end
end
