require 'spec_helper'

describe Program::Assessment::Section do
  describe "#sequential_questions" do
    it 'is an ActiveRecord::Associations', verifies_contract: "Program::Assessment::Section#sequential_questions()=>ActiveRecord::Associations" do
      expect(subject.sequential_questions.build).to be_a(Program::Assessment::SequentialQuestion)
    end
  end

  context "with a saved section" do
    subject { described_class.make!(name: 'name', assessment: Program::Assessment.make!(program: Program.make!)) }

    it { should verify_reader_contract("Program::Assessment::Section#id()=>Fixnum") }
    it { should verify_reader_contract("Program::Assessment::Section#assessment()=>Program::Assessment") }

    describe ".find(section.id)" do
      it "returns the section", verifies_contract: "Program::Assessment::Section.find(String)=>Program::Assessment::Section" do
        expect(described_class.find(subject.id)).to eq(subject)
      end
    end
  end

  describe "#questions" do
    let(:true_or_false_question) { Program::Assessment::TrueOrFalseQuestion.new }
    let(:sequential_question) { Program::Assessment::SequentialQuestion.new }
    let(:multiple_choice_question) { Program::Assessment::MultipleChoiceQuestion.new }
    let(:matching_task_question) { Program::Assessment::MatchingTaskQuestion.new }

    let(:section){ described_class.new }

    subject { section.questions }

    before do
      section.true_or_false_questions << true_or_false_question
      section.sequential_questions << sequential_question
      section.multiple_choice_questions << multiple_choice_question
      section.matching_task_questions << matching_task_question
    end

    it { should include(true_or_false_question) }
    it { should include(sequential_question) }
    it { should include(multiple_choice_question) }
    it { should include(matching_task_question) }
  end

  describe "#question_class" do
    it "is the question class matching the value" do
      subject.question_type = 'Multiple Choice'
      expect(subject.question_class).to eq(Program::Assessment::MultipleChoiceQuestion)
    end

    context "where question_type doesn't match any question class" do
      it "should raise QuestionTypeDoesntHaveQuestionClass" do
        subject.question_type = 'Question Type Without A Question Class'
        expect(lambda { subject.question_class })
          .to raise_error(described_class::QuestionTypeDoesntHaveQuestionClass)
      end
    end
  end

  describe "#class" do
    subject { described_class }
    it { should verify_reader_contract('Program::Assessment::Section.model_name()=>ActiveModel::Name') }
  end

  describe "#question_ratio" do
    let(:learning_outcome){
      Program::Assessment::LearningOutcome.make! }

    subject { Program::Assessment::Section.make!(
      minimum_number_of_questions: 1,
      true_or_false_questions: [
        Program::Assessment::TrueOrFalseQuestion.make(learning_outcome: learning_outcome),
        Program::Assessment::TrueOrFalseQuestion.make(learning_outcome: learning_outcome)]) }

    it "is the ratio of number of questions to the total questions" do
      expect(subject.question_ratio).to eq(0.50)
    end
  end

  describe "#valid?" do
    before do
      subject.valid?
    end

    context "where the assessment is active and the section does not have enough questions" do
      subject {
        described_class.make(
          assessment: assessment,
          minimum_number_of_questions: 1) }

      let(:assessment) { Program::Assessment.make(
        activated_at: Time.now) }

      it { expect(subject.errors[:minimum_number_of_questions]).not_to be_empty }
    end

    context "where the assessment is active and has a section has more than enough questions" do
      subject {
        described_class.make(
          assessment: assessment,
          minimum_number_of_questions: 0) }

      let(:assessment) { Program::Assessment.make(
        activated_at: Time.now) }

      it { expect(subject.errors[:minimum_number_of_questions]).to be_empty }
    end

    context "where the assessment is inactive and has a section with not enough questions" do
      subject {
        described_class.make(
          assessment: assessment,
          minimum_number_of_questions: 1) }

      let(:assessment) { Program::Assessment.make(
        activated_at: nil) }

      it { expect(subject.errors[:minimum_number_of_questions]).to be_empty }
    end
  end

end

