require 'spec_helper'

describe Program::Assessment do
  it { should verify_reader_contract('Program::Assessment#class()=>Class') }

  describe "#activated_at=(time)", verifies_contract: 'Program::Assessment#activated_at=(Time)' do
    it "sets activated_at to the time" do
      time = Time.now
      subject.activated_at = time
      expect(subject.activated_at).to eq(time)
    end
  end

  describe "#update_attributes" do
    it "is a method of Program::Assessment", verifies_contract: 'Program::Assessment#update_attributes()' do
      subject.update_attributes({})
    end
  end

  context "with a saved assessment" do
    subject { described_class.make!(name: 'name', program: Program.make!) }

    it { should verify_reader_contract("Program::Assessment#id()=>Fixnum") }
    it { should verify_reader_contract("Program::Assessment#name()=>String") }
    it { should verify_reader_contract("Program::Assessment#program()=>Program") }
  end

  describe "#add_default_sections", verifies_contract: 'Program::Assessment#add_default_sections()' do
    it "adds a section for each question type in Section::QUESTION_TYPES" do
      expect(subject.sections).to be_empty

      subject.add_default_sections

      question_types = Program::Assessment::Section::QUESTION_CLASSES.map(&:question_type)

      expect(subject.sections.size).to eq(question_types.size)
      expect(subject.sections.map(&:name)).to eq(question_types)
      expect(subject.sections.map(&:question_type)).to eq(question_types)
      expect(subject.sections.map(&:minimum_number_of_questions).uniq).to eq([0])
    end

    context "where the assessment already has a section" do
      let(:existing_question_type){ Program::Assessment::MultipleChoiceQuestion.question_type }

      it "doesn't duplicate that section" do
        subject.sections.build name: existing_question_type, question_type: existing_question_type

        subject.add_default_sections

        expect(subject.sections.select{|section| section.question_type == existing_question_type })
          .to have(1).section
      end
    end
  end

  describe "#pass_rate" do
    let(:successful_attempts_count) { 2 }
    let(:attempts_count) { 5 }

    let(:successful_user_assessment_results){
      VerifiedDouble.of_class('User::AssessmentResult') }

    let(:user_assessment_results){
      VerifiedDouble.of_class('User::AssessmentResult') }

    it "is the ratio of successful attempts to all attempts" do
      subject.should_receive(:user_assessment_results).at_least(:once).and_return(user_assessment_results)
      user_assessment_results.should_receive(:passed).and_return(successful_user_assessment_results)
      user_assessment_results.should_receive(:count).and_return(attempts_count)
      successful_user_assessment_results.should_receive(:count).and_return(successful_attempts_count)

      expect(subject.pass_rate)
        .to be_within(0.05)
        .of(successful_attempts_count.to_f / attempts_count)
    end
  end

  describe "#active?" do
    it "is true if and only if the assessment has an activation timestamp" do
      subject.activated_at = nil
      expect(subject).not_to be_active

      subject.activated_at = Time.now
      expect(subject).to be_active
    end
  end

  describe "#status" do
    context "where the assessment has been activated" do
      it "is Active" do
        subject.activated_at = Time.now
        expect(subject.status).to eq('Active')
      end
    end

    context "where the assessment has not been activated" do
      it "is Inactive" do
        subject.activated_at = nil
        expect(subject.status).to eq('Inactive')
      end
    end
  end

  describe ".active", verifies_contract: 'Program::Assessment.active()=>Class' do
    subject { described_class.make!(program: Program.make!, activated_at: Time.now) }

    it "includes only active assessments" do
      expect(described_class.active).to eq([subject])
    end
  end

  describe ".count", verifies_contract: 'Program::Assessment.count()=>Fixnum' do
    it "is the number of assessments" do
      expect(described_class.count).to eq(0)
    end
  end

  describe ".essentials", verifies_contract: 'Program::Assessment.essentials()=>Array' do
    fixture_class = Class.new(Fixture) do
      def essentials_assessment
        program_assessment
      end

      def non_essentials_assessment
        @non_essentials_assessment ||= program.assessments.create!(
          name: 'Extensions',
          assessment_type: 'Extensions',
          time_allowed: 60,
          activated_at: Time.now)
      end
    end

    let!(:fixture){ fixture_class.new(:essentials_assessment, :non_essentials_assessment) }

    it "includes only assessments with assessment_type='Essentials'" do
      expect(described_class.essentials.all).to eq([fixture.essentials_assessment])
    end
  end

  describe "#failed_by_user?(user)", verifies_contract: 'Program::Assessment#failed_by_user?(User)=>VerifiedDouble::Boolean' do

    fixture_class = Class.new(Fixture) do
      def passed_result
        @passed_result ||= User::AssessmentResult.make!(
          user: user,
          assessment: program_assessment, passed: true)
      end

      def failed_result
        @failed_result ||= User::AssessmentResult.make!(
          user: user,
          assessment: program_assessment, passed: false)
      end
    end

    subject { fixture.program_assessment.failed_by_user?(fixture.user) }

    context "where the user passed on his last attempt of the assessment" do
      let!(:fixture){ fixture_class.new(:failed_result, :passed_result) }
      it { should be_false }
    end

    context "where the user failed on his last attempt of the assessment" do
      let!(:fixture){ fixture_class.new(:passed_result, :failed_result) }
      it { should be_true }
    end

    context "where the user has not taken the assessment" do
      let!(:fixture){ fixture_class.new(:program_assessment) }

      before do
        expect(fixture.program_assessment.user_assessment_results).to be_empty
      end

      it { should be_false }
    end
  end
end

