require 'spec_helper'

describe Employment do
  use_vcr_cassette "create_service"
  let(:service)       { Service.make! }
  let(:user)          { User.make! }

  let(:valid_attributes) {
    {
      service_id: service.id,
      user_id:    user.id,
      role:       "staff"
    }
  }

  let(:employment) { Employment.new(valid_attributes) }
  subject { Employment.new(valid_attributes) }

  context "valid employment" do
    it { subject.should be_valid }
  end

  context "defaults" do
    context "role to staff (if nil)" do
      before(:each) {
        subject.role = nil; subject.save
      }

      it { subject.reload.role.should == "staff" }
    end

    context "started_at to Time.now" do
      before(:each) { subject.save }
      it { subject.reload.started_at.should_not == nil }
    end
  end

  context "validations" do
    context "#ensure_only_one_current_employment" do
      before(:each) { employment.save! }
      it { subject.should_not be_valid }
    end

    context "activation_token / service" do
      before(:each) { subject.service_id = nil }

      context "no validation token or service" do
        it { subject.should_not be_valid }
      end

      context "activation token is rubbish" do
        before(:each) { subject.activation_token = "derp" }
        it { subject.should_not be_valid }
      end
    end
  end

  context "Activation Token" do
    before(:each) {
      subject.activation_token = service.activation_token
      subject.save
    }

    it { subject.reload.service.should == service }
  end

  describe "email setter", verifies_contract: 'Employment#user()=>User' do
    before(:each) do
      subject.save
      subject.update_attributes(email: "ron.weasley@hogwarts.co.uk")
    end

    context "should change the email for the user" do
      it { subject.user.email.should == "ron.weasley@hogwarts.co.uk" }
    end
  end

  describe "EmploymentLearningPlans" do
    let(:program) { Program.make!(:processed) }
    let(:learning_plan) { LearningPlan.make!(programs: [program], employments: [subject], active_at: 2.days.ago, service: service) }

    before(:each) { subject.save; learning_plan }

    context "#pending_learning_plan_programs" do
      before(:each) { EmploymentLearningPlan.first.active! }
      it { subject.pending_learning_plan_programs.count.should == 1 }
    end

    context "#overdue_learning_plan_programs" do
      before(:each) do
        subject.save; learning_plan
        EmploymentLearningPlan.first.overdue!
      end

      it { subject.overdue_learning_plan_programs.count.should == 1 }
    end
  end

  describe "Finishing Employments" do
    context "mark fields" do
      before(:each) {
        subject.save!; subject.finish!
      }

      it { subject.status.should == "inactive" }
      it { subject.ended_at.should_not == nil }
    end

    context "remove learning_plans" do
      let(:program) { Program.make! }

      before(:each) {
        LearningPlan.make!(programs: [program], employments: [subject], service: service)
        subject.save!; subject.finish!
      }

      it { subject.reload.learning_plans.count.should == 0 }
    end
  end
end
