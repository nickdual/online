require 'spec_helper'

describe UserProgram do
  use_vcr_cassette "create_service"
  before(:each) do
    @user = User.make!
    @service = Service.make!(:packages => [Package.make!(:programs => [Program.make!(:processed), Program.make!(:processed)])])
    @user.employments.create!(:service => @service)
  end
  
  it "should be able to retrieve a list of programs" do
    @user.programs.count.should eql(2)
  end
  
  it "should retrieve a list of programs based on groups" do
    @group = Group.make!(:packages => [Package.make!(:programs => [Program.make!(:processed)])])
    @service.update_attributes(:group => @group)
    @user.programs.count.should eql(3)
  end
  
  
  describe "Filters" do
    it "should not include unfinished programs (still being processed by zencoder)" do
      @service.packages << Package.make!(:programs => [Program.make!])
      @user.programs.count.should eql(2)
    end

    it "should not include duplicate programs" do
      @program = Program.first
      @group = Group.make!(:packages => [Package.make!(:programs => [@program])])    
      @service.update_attributes(:group => @group)
      @user.programs.all.count.should eql(2)
    end
    
    context "groups" do
      before(:each) do
        @group = Group.make!(:packages => [Package.make!(:programs => [Program.make!])])    
        @service.update_attributes(:group => @group)
        @group.update_attributes(:disabled_at => Time.zone.now)
      end
      
      it "should not include programs from a group which is disabled" do
        @user.programs.count.should eql(0)
      end
    end
    
    context "service" do
      it "should not include programs from a service which is disabled" do
        @service.update_attributes(:disabled_at => Time.zone.now)
        @user.programs.count.should eql(0)
      end
      
      it "should not include programs from a service where a user is no longer employed" do
        @user.employments.first.update_attributes(:ended_at => Time.zone.now)
        @user.employments.active.count.should eql(0)
        @user.programs.count.should eql(0)
      end
    end
  end
end
