require 'spec_helper'
require 'cancan/matchers'

describe Service do
  use_vcr_cassette "create_service"
  before(:each) do
    @admin = Admin.make!
    @service = Service.new(name: "OldHome", country: "Australia")
  end
  
  it "should create a service" do
    @service.save!
  end

  describe "Activation Token" do
    before(:each) { @service = Service.make! }

    it "should generate an activation token when the service is created" do
      @service.save!
      @service.reload
      @service.activation_token.should_not eql(nil)
      @service.token_generated_at.should_not eql(nil)
    end

    it "should save the time at which a token is generated" do
      @service.update_column(:token_generated_at, 2.days.ago)
      
      @service.reset_token
      @service.reload.token_generated_at.should_not eql(2.days.ago)
    end

    it "should reset all tokens which were generated more than 30 days ago" do
      @old_token = @service.activation_token
      @old_time = @service.token_generated_at
      
      time_travel_to(31.days.from_now)
      Service.reset_old_tokens!
      @service.reload
      @service.activation_token.should_not eql(@old_token)
    end

  end
  
  describe "Scopes" do
    before(:each) { Service.make! }
    it "tokens_reset_30_days_ago" do
      Service.tokens_reset_30_days_ago.count.should eql(0)

      time_travel_to(32.days.from_now)
      Service.tokens_reset_30_days_ago.count.should eql(1)
    end
  end
  
  describe "Disabling and enabling a service" do
    before(:each) { @service.save! }
    context "Disable" do
      it "should allow a service to be enabled by default" do
        @service.disabled?.should eql(nil)
      end

      it "should be able to disable a service" do
        @service.disable!(@admin)
        @service.disabled?.should_not eql(nil)
      end

      it "should be able to be enabled once disabled" do
        @service.disable!(@admin)
        @service.enable!
        @service.disabled?.should eql(nil)
        @service.disabled_by_id.should eql(nil)
      end

      it "should disable all employments for a service" do
        @employment = @service.employments.make!(:service => @service)
        @service.disable!(@admin)
        @employment.reload
        @employment.status.should eql("inactive")
      end
    end
    
    context "enable" do
      before(:each) do
        @group = Group.make!
        @service.update_attributes(:group => @group)
        @user = User.make!
        @group.coordinators.create!(:user => @user)
      end
      
      it "should enable coordinators for a service when the service is enabled again" do
        @service.disable!(@admin)
        @user.employments.first.ended_at.should_not eql(nil)
        @service.enable!
        @user.employments.first.ended_at.should eql(nil)
      end
    end
  end
  
  describe "Group Packages" do
    before(:each) do
      @program = Program.make!(:processed)
      @package = Package.make!
      @program_package = @package.package_programs.create!(:program => @program)
      @package_program_list = @program_package.program_lists.create!(:program => Program.make!(:introduction))
      @service.packages = [@package]
      @service.save!
    end
    
    subject { @service.service_programs.first }
    
    it "should update all the program lists when a group is assigned a package" do
      subject.program_lists.count.should eql(1)
    end
  end

  describe "zendesk integration" do
    require "zendesk"
    before(:all) { setup_zendesk_connection }
    before(:each) { @service = Service.make! }
    subject { @zendesk.organizations }

    context "create" do
      it { subject.count.should == 1 }
      it { @service.zendesk_id.should_not be_nil }
    end

    context "update" do
      use_vcr_cassette "update_service"
      before(:each) { @service.update_attributes(name: "Fraggle Rock") }
      it { subject.count.should == 1 }
      it { subject.last.name.should == "Fraggle Rock" }
    end
  end
end
