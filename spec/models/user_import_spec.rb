require 'spec_helper'

describe UserImport do
  use_vcr_cassette "create_service"
  let(:service) { Service.make! }
  let(:admin) { Admin.make! }

  let(:complete_csv)            { "#{Rails.root}/spec/fixtures/users-complete.csv" }
  let(:incomplete_csv)          { "#{Rails.root}/spec/fixtures/users-incomplete.csv" }
  let(:complete_job_title_csv)  { "#{Rails.root}/spec/fixtures/users-jobs-complete.csv" }

  let(:attributes) {
    {
      service_id: service.id,
      admin_id: admin.id,
      csv_file: File.read(complete_csv)
    }
  }

  let(:user_import) { UserImport.new(attributes) }
  subject { user_import }

  context "can import a bunch of users" do
    before(:each) { subject.save }

    it { subject.total_users.should == 2 }
    it { service.users.count.should == 2 }
  end

  describe "users returned" do
    context "#successful_invited_users" do
      before(:each) { subject.save }
      it { subject.successful_invited_users.count.should eql(2) }
      it { subject.successful_invited_users.first.should eql(User::Invited.first) }
    end

    context "#unsuccessful_users" do
      before(:each) do
        subject.csv_file = File.read(incomplete_csv)
        subject.save
      end

      it { subject.unsuccessful_users.count.should eql(2) }
      it { subject.unsuccessful_users.first.new_record?.should eql(true) }
    end
  end

  describe "#job_title" do
    before(:each) do
      subject.csv_file = File.read(complete_job_title_csv)
    end

    context "service" do
      before(:each) do
        @job_title = service.job_titles.create!(name: "Mr. Manager")
        subject.save
      end

      context "should allow job titles to be set" do
        it { Employment.first.job_title_id.should == @job_title.id }
      end

      context "return numbers for who failed to enter" do
        it { subject.failed_job_titles.count.should == 1 }
        it { subject.failed_job_titles.first.first_name.should == "Harry" }
        it { subject.failed_job_titles.first.job_title.should == "Derp derp" }
      end
    end

    context "group" do
      before(:each) do
        @group = Group.make!
        service.update_attributes(group_id: @group.id)
        @job_title = @group.job_titles.create!(name: "Mr. Manager")
        subject.save
      end

      context "should allow job titles to be set" do
        it { Employment.first.job_title_id.should == @job_title.id }
      end
    end
  end

  describe "#notify_admin" do
    before(:each) { admin; reset_mailer }

    context "successful import" do
      before(:each) {
        user_import.save
      }

      subject { unread_emails_for(admin.email) }
      it { subject.count.should == 1 }

      context "email content" do
        before(:each) { open_email(admin.email, with_subject: "Completed User Import for #{service}") }
        it { current_email.default_part_body.to_s.should include("<strong>2 of 2</strong> users were successfully imported.") }
      end
    end

    context "failed import" do
      before(:each) {
        user_import.csv_file = File.read(incomplete_csv)
        user_import.save
        open_email(admin.email, with_subject: "Completed User Import for #{service}")
      }

      it { current_email.default_part_body.to_s.should include("<strong>0 of 2</strong> users were successfully imported.") }
      it { current_email.default_part_body.to_s.should include("Sinclair (First name can't be blank)") }
    end

    context "failed because of employment errors" do
      before(:each) {
        user_import.save; reset_mailer
        user_import = UserImport.new(attributes).save

        open_email(admin.email, with_subject: "Completed User Import for #{service}")
      }

      it { current_email.default_part_body.to_s.should include("Emmet Rogan (User is already employed at service)") }
    end

    context "job titles failed to import" do
      before(:each) {
        user_import.csv_file = File.read(complete_job_title_csv)
        user_import.save
        open_email(admin.email, with_subject: "Completed User Import for #{service}")
      }

      it { current_email.default_part_body.to_s.should include("Emmet Rogan (Mr. Manager)") }
    end
  end

  describe "validations" do
    context "requires a csv file" do
      before(:each) { subject.csv_file = nil }
      it { subject.should_not be_valid }
    end

    # TODO: Also consider moving a bunch of this into a new class
    context "existing users" do
      use_vcr_cassette "create_multiple_services"
      let(:service2) { Service.make! }

      context "no extra users, just extra employments" do
        before(:each) {
          user_import = UserImport.new(attributes)
          user_import.service_id = service2.id
          user_import.save

          subject.save
        }

        it { subject.successful_invited_users.count.should == 2 }
        it { User.count.should == 2 }
        it { Employment.count.should == 4 }
      end

      context "not creating employments for existing users" do
        before(:each) {
          subject.save
          @user_import = UserImport.new(attributes)
          @user_import.save
        }

        it { User.count.should == 2 }
        it { Employment.count.should == 2 }
        it { @user_import.unsuccessful_users.count.should == 2 }
      end
    end
  end

  # TODO: A lot of this needs to be moved into a new class for handling importing users
  describe "mailers" do
    subject { unread_emails_for("harry.potter@hogwarts.co.uk") }

    context "existing user" do
      before(:each) do
        @user = User.make!(email: "harry.potter@hogwarts.co.uk")
        reset_mailer; user_import.save
      end

      it { subject.count.should == 1 }
      it { subject.first.subject.should == "Your Account is now Active with #{service}" }
    end

    context "new user" do
      before(:each) { user_import.save }
      it { subject.count.should == 1 }
      it { subject.first.subject.should == "Your Account has been Created for #{service}" }
    end

    context "invalid user" do
      before(:each) do
        user_import.csv_file = File.read(incomplete_csv)
        user_import.save
      end

      it { subject.count.should == 0 }
    end
  end
end
