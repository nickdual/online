require 'spec_helper'

def create_video_for(program)
  Video.make!(program: program)
end

describe Program do
  use_vcr_cassette "create_service"
  before(:each) do
    @program_category = ProgramCategory.create!(:name => "Derp")
    @admin = Admin.create!(:name => "Thomas Sinclair", :email => "thomas@icdesign.com.au", :password => 123456, :password_confirmation => 123456)

    @valid_attributes = {
      :title => "derp",
      :code => "derp",
      :description => "derp",
      :category => @program_category,
      :created_by => @admin,
      :updated_by => @admin
    }

    @program = Program.new(@valid_attributes)
  end

  it { should verify_reader_contract('Program#class()=>Class') }

  context "with a saved program" do
    subject { Program.make!(title: 'title') }

    it { should verify_reader_contract("Program#id()=>Fixnum") }
    it { should verify_reader_contract("Program#title()=>String") }

    it "verifies contract Program#assessments()=>ActiveRecord::Associations", verifies_contract: "Program#assessments()=>ActiveRecord::Associations" do
      expect(subject.assessments.build).to be_a(Program::Assessment)
    end
  end

  it "should be able to create a program" do
    @program.save!
    @program.created_by.should eql(@admin)
    @program.updated_by.should eql(@admin)
  end

  it "should default the classification to program" do
    @program.save
    @program.classification.should eql("program")
  end

  describe "scopes" do
    before(:each) do
      2.times { Program.make!(:processed) }
      Program.make!
    end

    it ".processing" do
      Program.processing.count.should eql(1)
    end

    it "processed" do
      Program.processed.count.should eql(2)
    end

    context "classification" do
      before(:each) do
        3.times { Program.make!(:closer) }
        Program.make!(:introduction)
      end

      it "should be able to retrieve programs based on classification" do
        Program.closers.count.should eql(3)
        Program.programs.count.should eql(3)
        Program.introductions.count.should eql(1)
        Program.introductions_or_closers.count.should eql(4)
      end
    end

  end

  describe "zencoder" do
    before(:each) do
      @program.video = File.open("#{Rails.root}/spec/fixtures/meow.mp4")
      @program.save
    end

    subject { @program }

    it "should assume videos are pending processing by default" do
      subject.processing?.should eql(true)
    end

    it "should upload the initial video as the original" do
      subject.videos.count.should eql(1)
      subject.original_video.should_not eql(nil)
    end

    it "should upload the video to Zencoder for processing" do
      subject.zencoder_id.should_not eql(nil)
    end

    it "should mark a program as processed once there are 10 versions of the video" do
      9.times { create_video_for(@program) }
      Video.first.update_attributes(description: "browser_webm_sd", width: 5, height: 5)
      create_video_for(@program)

      @program.videos.count.should == 11
      subject.reload
      subject.processing?.should eql(false)
      subject.video_width.should_not be_nil
      subject.video_height.should_not be_nil
    end
  end

  describe ".classifications" do
    it "matches the ids of Program::Classification" do
      expect(described_class.classifications).to eq(Program::Classification::IDS)
    end
  end

  describe ".where" do
    let!(:program) { Program.create!(category: category) }
    let(:category) { ProgramCategory.create!(name: "Parent") }

    it "can filter by array of category_ids", verifies_contract: 'Program.where(Hash)=>Array' do
      expect(Program.where(category_id: [category.id]).all).to eq([program])
    end
  end

  describe ".find_using_slug!(slug)" do
    let(:title){ 'test' }
    let!(:program){ Program.make!(title: title) }

    it "returns the program matching the slug", verifies_contract: 'Program.find_using_slug!(String)=>Program' do
      expect(Program.find_using_slug!(title)).to eq(program)
    end
  end

  describe "#class" do
    subject { described_class }
    it { should verify_reader_contract('Program.model_name()=>ActiveModel::Name') }
  end

  describe "#create" do
    context "where the program is classified as a Program" do
      subject { described_class.make(classification: 'program') }

      it "should automatically have an essential assessment" do
        subject.save!

        expect(subject.assessments).to have(1).assessment
        assessment = subject.assessments.first

        expect(assessment.assessment_type).to eq('Essentials')
        expect(assessment.name).to eq('Essentials')
        expect(assessment.time_allowed).to eq(0)

        expect(assessment.sections.map(&:question_type))
          .to eq(Program::Assessment::Section::QUESTION_CLASSES.map(&:question_type))

        expect(assessment.sections).to be_all{|section|
          ! section.new_record? }
      end
    end

    context "where the program is not classified as a Program" do
      subject { described_class.make(classification: 'closer') }

      it "should not have an essential assessment" do
        subject.save!
        expect(subject.assessments).to be_empty
      end
    end
  end

  describe "#classified_as_program?" do
    it "is true IFF the classification is a program" do
      subject.classification = 'program'
      expect(subject).to be_classified_as_program

      subject.classification = 'intro'
      expect(subject).not_to be_classified_as_program
    end
  end

  describe "#active_assessments_count" do
    let(:active_assessments_count){ 179 }

    let(:assessments){
      VerifiedDouble.of_class('Program::Assessment') }

    it "is the number of active assessments" do
      assessments.should_receive(:active).and_return(assessments)
      assessments.should_receive(:count).and_return(active_assessments_count)
      subject.should_receive(:assessments).and_return(assessments)

      expect(subject.active_assessments_count).to eq(active_assessments_count)
    end
  end

  describe "#essentials_assessment" do
    let(:assessment){ VerifiedDouble.of_instance('Program::Assessment') }
    let(:assessments){ VerifiedDouble.of_class('Program::Assessment') }

    it "is the first essentials assessment of the program", verifies_contract: 'Program#essentials_assessment()=>Program::Assessment' do
      subject.should_receive(:assessments).and_return(assessments)
      assessments.should_receive(:essentials).and_return([assessment])

      expect(subject.essentials_assessment).to eq(assessment)
    end
  end
end
