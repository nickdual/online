require 'spec_helper'

describe "PlaylistHelper" do
  use_vcr_cassette "create_service"
  before(:each) do
    @program = Program.make!(:processed)
    @introduction = Program.make!(:processed, classification: "introduction")
    @package = Package.make!(programs: [@program])
    @package_program = @package.package_programs.first
    @package_program_list = @package_program.program_lists.create!(program: @introduction)
  end
  
  let(:closer) { Program.make!(:processed, classification: "closer") }
  
  context "group" do
    before(:each) { @group = Group.make!(packages: [@package]) }
    subject { @group.group_programs.flat_map(&:program_lists) }
    
    it "should create GroupProgramList records if a group receives a package" do
      subject.count.should eql(1)
    end
    
    it "should receive new GroupProgramList records as programs are added to the package_program" do
      @package_program.program_lists.create!(program: closer)
      subject.count.should eql(2)
    end
    
    it "should remove GroupProgramList records as programs are removed from the PackageProgramList" do
      @package_program_list.destroy
      subject.count.should eql(0)
    end
    
    it "should not create duplicates for records in PackageProgramList" do
      @package_program.program_lists.create!(program: @introduction)
      subject.count.should eql(1)
    end
    
    context "conflicting groups and packages" do
      before(:each) do
        @package1 = Package.make!(programs: [@program])
        @group1 = Group.make!(packages: [@package1])
        @package1.package_programs.first.program_lists.create!(program: @introduction)
        @package1.package_programs.first.program_lists.create!(program: closer)
      end
      
      it "should not inherit introductions from other packages" do
        subject.count.should eql(1)
      end
      
      it "should include removing" do
        @package1.package_programs.first.program_lists.each { |p| p.destroy }
        subject.count.should eql(1)
      end
    end
  end
  
  context "service" do
    context "with group" do
      use_vcr_cassette "create_service_from_group"
      before(:each) do
        @group = Group.make!(packages: [@package])
        @service = Service.make!(group: @group)
      end
      
      subject { @service.service_programs.flat_map(&:program_lists) }
      
      it "should create ServiceProgramList based on belonging to a group with the package" do
        subject.count.should eql(1)
      end
      
      it "should receive new ServiceProgramList based on the group receiving a new GroupProgramList from PackageProgramList" do
        @package_program.program_lists.create!(program: closer)
        subject.count.should eql(2)
      end
      
      it "should remove a ServiceProgramList based on the group removing a GroupProgramList" do
        @group.group_programs.first.program_lists.first.destroy
        subject.count.should eql(0)
      end
      
      it "should remove a ServiceProgramList based on the package remove a GroupProgramList" do
        @package_program_list.destroy
        subject.count.should eql(0)
      end
      
      it "should not create duplicates for records inherited from GroupProgramList" do
        @group.group_programs.first.program_lists.create!(program: @introduction, :group_program_id => @program.id, :group_id => @group.id)
        subject.count.should eql(1)
      end
    end
    
    context "without group" do
      before(:each) { @service = Service.make!(packages: [@package]) }
      subject { @service.service_programs.flat_map(&:program_lists) }
      
      it "should create ServiceProgramList records if a service records a package" do
        subject.count.should eql(1)
      end
      
      it "should receive new ServiceProgramList records as programs are added to the package_program" do
        @package_program.program_lists.create!(program: closer)
        subject.count.should eql(2)
      end
      
      it "should remove ServiceProgramList records as programs are removed from PackageProgramList" do
        @package_program_list.destroy
        subject.count.should eql(0)
      end
      
      it "should not create duplicates for records in PackageProgramList" do
        @package_program.program_lists.create!(program: @introduction)
        subject.count.should eql(1)
      end

      use_vcr_cassette "create_service_from_group"
      it "should not receive introductions from groups, just because it has the same page" do
        @group = Group.make!(packages: [@package])
        @service1 = Service.make!(group: @group)
        @group.group_programs.first.program_lists.create!(program: Program.make!(:processed, classification: "introduction"), :group_program_id => @program.id, :group_id => @group.id)

        subject.count.should == 1
      end

      context "conflicting services and packages" do
        before(:each) do
          @package1 = Package.make!(programs: [@program])
          @package1.package_programs.first.program_lists.create!(program: @introduction)
          @package1.package_programs.first.program_lists.create!(program: closer)
        end

        it "should not inherit introductions from other packages" do
          subject.count.should eql(1)
        end

        it "should include removing" do
          @package1.package_programs.first.program_lists.each { |p| p.destroy }
          subject.count.should eql(1)
        end
      end
    end
  end
end