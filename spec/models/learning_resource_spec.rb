require 'spec_helper'

describe LearningResource do
  use_vcr_cassette "create_service"
  before(:each) do
    @program = Program.make!(:processed)
    
    @valid_attributes = {
      program: @program,
      name: "test",
      resource: File.open("spec/fixtures/cat.jpg")
    }
    
    @learning_resource = LearningResource.new(@valid_attributes)
  end
  
  it "should be able to create a learning resource" do
    @learning_resource.save!
  end
  
  it "should not allow a learning resource for a program classification outside of program" do
    @learning_resource.program = Program.make!(:introduction)
    @learning_resource.save
    @learning_resource.should have(1).errors_on(:program)
  end
  
  describe ".create_downloads_by_user!" do
    before(:each) do
      @learning_resource = LearningResource.make!(:program => @program)
      @package = Package.make!(:programs => [@program])
      @service = Service.make!(:packages => [@package])
      @user = User.make!
      @employment = Employment.create!(:service => @service, :user => @user)
    end
    
    it "should be able to create a LearningResourceDownload for an employment based off the program" do
      @learning_resource.create_downloads_by_user!(@user)
      @learning_resource.downloads.count.should eql(1)
    end
  end
  
  describe "scopes" do
    before(:each) { @learning_resource.save! }
    
    it ".all_staff" do
      LearningResource.all_staff.count.should eql(1)
    end
    
    it ".coordinators_only" do
      2.times { LearningResource.make!(:coordinators_only => true) }
      LearningResource.coordinators_only.count.should eql(2)
    end
  end
end
