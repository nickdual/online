require 'spec_helper'

describe EmploymentLearningPlanProgram do
  context "with an employment plan" do
    use_vcr_cassette "create_service"
    let(:service)     { Service.make! }
    let(:program)     { Program.make!(:processed) }
    let(:employment)  { Employment.make!(service: service) }
    let(:plan)        { LearningPlan.make!(service: service) }
    let(:employment_plan) { EmploymentLearningPlan.make!(employment: employment, learning_plan: plan) }
    before(:each) { employment_plan.update_attributes(active_at: Time.zone.now.yesterday) }

    describe "pending" do
      subject { employment_plan.programs_pending }
      it { subject.count.should == 0 }

      context "overdue" do
        before(:each) do
          plan.learning_plan_programs.create!(program_id: program.id, essentials: true)
          employment_plan.overdue!
        end

        subject { employment_plan.programs_pending.first }
        it { subject.overdue_essentials.should == true }
      end

      context "learning records" do
        before(:each) do
          plan.learning_plan_programs.create!(program: program, essentials: true)
        end

        it { subject.count.should == 1 }

        context "learning records created" do
          before(:each) do
            employment.learning_records.create!(program: program)
          end

          it { subject.count.should == 1 }
        end
      end

      context "program assessments" do
        let(:program1) { Program.make! }

        before(:each) do
          plan.learning_plan_programs.create!(program: program, essentials: true)
          plan.learning_plan_programs.create!(program: program1, essentials: true)

          employment.learning_records.create!(program: program)
          employment.learning_records.create!(program: program1)
          employment.program_assessments.create!(program: program1, assessment_type: "Essentials", coordinator: employment.user)
        end

        it { subject.count.should == 1 }
        it { subject.first.program_id.should == program.id }
      end
    end

    describe "completed" do
      subject { employment_plan.reload.programs_completed }
      it { subject.count.should == 0 }

      context "learning records" do
        before(:each) do
          plan.learning_plan_programs.create!(program: program, essentials: true)
        end

        it { subject.count.should == 0 }

        context "learning records created" do
          before(:each) do
            employment.learning_records.create!(program: program)
          end

          it { subject.count.should == 0 }

          context "learning record and essential assessment created" do
            before(:each) do
              employment.program_assessments.create!(program: program, assessment_type: "Essentials", coordinator: employment.user)
            end

            it { subject.count.should == 1 }
            it { subject.first.require_essentials.should == true }
            it { subject.first.completed_essentials.should == true }
          end

          context "learning record and evidence assessment created" do
            before(:each) do
              LearningPlanProgram.first.update_attributes(evidence: true, essentials: false)
              employment.program_assessments.create!(program: program, assessment_type: "Evidence", coordinator: employment.user)
            end

            it { subject.count.should == 1 }
            it { subject.first.require_evidence.should == true }
            it { subject.first.completed_evidence.should == true }
          end

          context "learning record and extension assessment created" do
            before(:each) do
              LearningPlanProgram.first.update_attributes(extension: true, essentials: false)
              employment.program_assessments.create!(program: program, assessment_type: "Extension", coordinator: employment.user)
            end

            it { subject.count.should == 1 }
            it { subject.first.require_extension.should == true }
            it { subject.first.completed_extension.should == true }
          end
        end
      end
    end
  end

  describe '#is_essential_assessment_unsuccessful' do
    let(:user){ VerifiedDouble.of_instance('User') }

    let(:user_assessment_hash){
      { essentials_failed: true } }

    let(:employment) {
      VerifiedDouble.of_instance('Employment', user: user) }

    let(:program){
      VerifiedDouble.of_instance('Program') }

    it "should be the essentials_failed result of the user's assessment on the program" do
      subject.stub(:employment).and_return(employment)
      subject.stub(:program).and_return(program)

      user.should_receive(:assessments).with(program).and_return(user_assessment_hash)

      expect(subject.is_essential_assessment_unsuccessful).to eq(user_assessment_hash[:essentials_failed])
    end
  end
end
