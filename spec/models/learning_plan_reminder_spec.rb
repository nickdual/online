require 'spec_helper'

describe LearningPlanReminder do
  use_vcr_cassette "create_service_via_employment"

  let(:program) { Program.make!(processing: false) }
  let(:employment) { create_employment }
  let(:learning_plan) { LearningPlan.make!(service: @service, programs: [program], employments: [employment], days_till_due: 5, active_at: 2.days.ago) }
  subject { learning_plan.employment_learning_plans.first }

  describe ".overdue_reminders" do
    before(:each) do
      create_employment
      subject.active!
      subject.update_attributes(due_at: Time.zone.now.yesterday)
      LearningPlan.overdue_reminders
    end

    context "send set the learning plan to overdue" do
      it { subject.reload.overdue?.should == true }
    end
  end

  describe ".activate_plans" do
    before(:each) do
      create_employment
      subject.update_attributes(active_at: Time.zone.now)
      LearningPlan.activate_plans
    end

    it { subject.reload.active?.should == true }
  end

  describe ".due_in_x" do
    before(:each) do
      create_employment
      learning_plan; reset_mailer
    end

    subject { unread_emails_for(employment.email) }

    context "no mailer to be sent" do
      before(:each) { LearningPlan.due_in_x(1) }
      it { subject.count.should == 0 }
    end

    context "should send a mailer reminding users" do
      before(:each) do
        EmploymentLearningPlan.first.active!
        reset_mailer; LearningPlan.due_in_x(5)
      end

      it { subject.last.subject.should eql("Your Learning Plan #{learning_plan} is due in 5 days") }
    end
  end
end