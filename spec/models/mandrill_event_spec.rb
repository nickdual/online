require 'spec_helper'

describe MandrillEvent do
  describe "#update_user!" do
    let(:user){ fire_double('User') }

    context "where an email was successfully sent to the user" do
      let(:expected_status){ 'sent' }

      let(:subject){ described_class.new(
        email: 'gsmendoza@gmail.com', occurred_at: Time.now, status: expected_status) }

      it "records the email verification date and clears the last bounce description" do
        User.should_receive(:find_by_email).with(subject.email).and_return(user)

        user.should_receive(:update_attributes).with(
          email_verified_at: subject.occurred_at,
          last_email_bounce_description: nil,
          last_email_status: expected_status)

        subject.update_user!
      end
    end

    context "where an email sent to the user soft-bounced" do
      let(:subject){
        described_class.new(
          email: 'gsmendoza@gmail.com',
          occurred_at: Time.now,
          status: 'soft-bounced',
          bounce_description: 'mailbox_full') }

      it "records the last bounce description and retains the email verification date" do
        User.should_receive(:find_by_email).with(subject.email).and_return(user)

        user.should_receive(:update_attributes).with(
          last_email_bounce_description: subject.bounce_description,
          last_email_status: subject.status)

        subject.update_user!
      end
    end

    context "where an email sent to the user hard-bounced" do
      let(:subject){
        described_class.new(
          email: 'gsmendoza@gmail.com',
          occurred_at: Time.now,
          status: 'bounced',
          bounce_description: 'bad_mailbox') }

      it "records the bounce description and clears the email verification date" do
        User.should_receive(:find_by_email).with(subject.email).and_return(user)

        user.should_receive(:update_attributes).with(
          email_verified_at: nil,
          last_email_bounce_description: subject.bounce_description,
          last_email_status: subject.status)

        subject.update_user!
      end
    end

    context "where the event has an unknown status" do
      let(:subject){
        described_class.new(
          email: 'gsmendoza@gmail.com',
          occurred_at: Time.now,
          status: 'rejected') }

      it "raises UnknownStatus" do
        User.should_receive(:find_by_email).with(subject.email).and_return(user)

        expect { subject.update_user! }.to raise_error(described_class::UnknownStatus)

      end
    end
  end
end
