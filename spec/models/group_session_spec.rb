require 'spec_helper'

describe GroupSession do
  use_vcr_cassette "create_service"
  let(:service) { Service.make! }
  let(:program) { Program.make! }
  let(:user) { User.make! }
  let(:employment) { Employment.make!(user: user, service: service) }

  before(:each) do
    @valid_attributes = {
      employments:    [employment],
      program_id:     program.id,
      viewed_at:      2.days.from_now.beginning_of_day,
      created_by_id:  1
    }

    @group_session = GroupSession.new(@valid_attributes)
  end

  context "create a group session" do
    it { @group_session.save! }
  end

  describe "#create_learning_records" do
    subject { user.learning_records }

    before(:each) do
      @group_session.medium = "Online"
      @group_session.save!
    end

    context "should mark of learning records" do
      it { subject.count.should == 1 }
      it { subject.first.group_session_id.should == @group_session.id }
      it { subject.first.viewed_at.should == 2.days.from_now.beginning_of_day }
      it { user.program_assessments.count.should == 0 }
    end
  end

  describe "#create_assessment_records" do
    before(:each) do
      @group_session.essentials = true
      @group_session.evidence = true
      @group_session.save!
    end

    subject { employment.program_assessments }

    context "should create assessment records" do
      it { subject.count.should == 2 }
      it { subject.first.group_session_id.should == @group_session.id }
      it { subject.map(&:assessment_type).should include("Evidence") }
      it { subject.map(&:assessment_type).should include("Essentials") }
      it { subject.first.assessed_at.should == 2.days.from_now.beginning_of_day }
      it { user.learning_records.count.should == 0 }
    end
  end

  describe "viewed_at" do
    before(:each) do
      @future_date = 5.days.from_now.strftime("%d/%m/%Y")
      @group_session.viewed_at_date = @future_date
      @group_session.viewed_at_time = "11:00am"
      @group_session.save!
    end

    subject { @group_session.viewed_at }

    context "viewed_at_date" do
      it { subject.strftime("%d/%m/%Y").should == @future_date }
      it { @group_session.viewed_at_date.strftime("%d/%m/%Y").should == @future_date }
    end

    context "viewed_at_time" do
      it { subject.strftime("%l:%M%P").should == "11:00am" }
    end

    context "invalid data" do
      it "should handle no am / pm" do
        @group_session = GroupSession.new(@valid_attributes)

        @group_session.viewed_at = nil
        @group_session.viewed_at_date = "21/5/1988"
        @group_session.viewed_at_time = "9:13"
        @group_session.save!

        @group_session.reload.viewed_at.strftime("%l:%M%P").should == " 9:13am"
      end
    end
  end
end
