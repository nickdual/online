require 'spec_helper'

describe LearningRecord do
  use_vcr_cassette "create_service"
  let(:service) { Service.make! }

  before(:each) do
    @program = Program.make!
    @employment = Employment.make!(service: service)

    @learning_record = LearningRecord.new(:program => @program, :employment => @employment)
  end
  
  it "should be able to create a learning record" do
    @learning_record.save!
  end
  
  it "should fill the viewed_at if blank" do
    @learning_record.viewed_at = nil
    @learning_record.save!
    @learning_record.viewed_at.should_not eql(nil)
  end

  describe "scopes" do
    use_vcr_cassette "create_service_via_employment"
    context "group_by_medium" do
      before(:each) { 2.times { LearningRecord.make!(:medium => "online") }}
      subject { LearningRecord.group_by_medium }
      
      it "should be able to return the mediums grouped" do
        subject.count["online"].should eql(2)
        subject.first.medium.should eql("online")
      end
    end

    context "group_by_viewed_at" do
      use_vcr_cassette "create_service_via_learning_record"
      before(:each) do
        2.times { LearningRecord.make!(:viewed_at => 2.days.from_now) }
        LearningRecord.make!
      end
      subject { LearningRecord.group_by_viewed_at }

      it "should be able to separate by time" do
        subject.all.size.should eql(3)
      end
      
      use_vcr_cassette "create_service_via_learning_record_and_program"
      it "should not group programs together" do
        LearningRecord.make!(program: Program.first)
        subject.all.size.should eql(4)
      end
    end
  end
end
