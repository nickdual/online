require 'spec_helper'

describe Service::JobTitle do
  use_vcr_cassette "create_service"
  before(:each) do
    @service = Service.make!
    
    @valid_attributes = {
      service: @service,
      name: "Derp",
    }

    @service_job_title = Service::JobTitle.new(@valid_attributes)
  end
  
  it "should be able to create an instance of Service::JobTitle" do
    @service_job_title.save!
  end
  
  it "should reset the job title for employments if the group job is removed" do
    @service_job_title.save
    @employment = Employment.make!(job_title_id: @service_job_title.id, service: @service)

    @service_job_title.destroy
    @employment.reload.job_title_id.should be_nil
  end
  
end
