require 'spec_helper'
require 'cancan/matchers'

describe Admin do
  before(:each) do
    @valid_attributes = {
      :email => "bob.huynh@gmail.com", 
      :password => 123456, 
      :password_confirmation => 123456, 
      :first_name => "Bob", 
      :last_name => "Huynh",
      :role => "viewer"
    }
    
    @admin = Admin.new(@valid_attributes)
  end
  
  it "should be able to create an admin" do
    @admin.save!
  end
  
  it "should send the admin an email upon admin creation" do
    @admin.save!
    unread_emails_for(@admin.email).size.should eql(1)
  end
  
  it "should make admins a viewer by default" do
    @admin.role = nil
    @admin.save
    @admin.role.should eql("viewer")
  end
  
  it "should be able to pass the first and last name together, and automatically split it" do
    @admin.first_name = nil
    @admin.last_name = nil
    @admin.name = "Bob Huynh"
    @admin.save
    @admin.first_name.should eql("Bob")
    @admin.last_name.should eql("Huynh")
  end
end
