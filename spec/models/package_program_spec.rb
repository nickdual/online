require 'spec_helper'

describe PackageProgram do
  before(:each) do
    @program = Program.make!
    @package = Package.make!
    @package_program = PackageProgram.new(:program => @program, :package => @package)
  end
  
  it "should be able to make a PackageProgram" do
    @package_program.save!
  end
  
  describe "Position" do
    before(:each) { @package_program.save }
    
    it "should order my programs by default" do
      @package_program.position.should eql(1)
    end
    
    it "should expand on that position" do
      @package_program = PackageProgram.create!(:program => Program.make!, :package => @package)
      @package_program.position.should eql(2)
    end
    
    it "should position my package programs when them as a group" do
      @package.package_programs = [PackageProgram.new(:position => 1, :program => Program.make!), PackageProgram.new(:position => 1, :program => @program)]
      @package.save
      @package.package_programs.last.program.should eql(@program)
    end
    
    it "should position my package based on my accepts_nested_attributes_for" do
      @package.package_programs_attributes = [{:position => 2, :program => Program.make!, :selected => "1"}, {:position => 1, :program => @program, :selected => "1"}]
      @package.save
      @package.package_programs.first.program.should eql(@program)
    end
  end
  
  describe "Nested Attributes" do
    it "should not include package programs which do not have selected set as 1" do
      @package.package_programs_attributes = [{"program_id" => Program.make!.id, "position" => "1", "selected" => "1"}, {"program_id" => @program.id, "position" => "1"}]
      @package.save
      @package.package_programs.count.should eql(1)
    end
  end
  
  describe "Program Lists" do
    before(:each) { @package_program.save }
    
    it "should allow me to add programs to another program in a list" do
      2.times { @package_program.program_lists.create!(:program => Program.make!(:introduction)) }
      @package_program.program_lists.count.should eql(2)
    end
  end
end
