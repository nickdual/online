require 'spec_helper'

describe "CoordinatorMailChimp" do
  let(:group)   { Group.make! }
  let(:service) { Service.make!(group: group) }
  let(:user)    { User.make! }

  before(:each) do
    Coordinator.create!(group: group, user: user)
  end

  context "create a normal coordinator" do
    it { 
      pending
      Gibbon.lists({filters: {list_name: "Group Coordinators"}}).users.count.should == 1
    }
  end

  context "unsubscribe on deletion" do
    it {
      pending
      Gibbon.lists({filters: {list_name: "Group Coordinators"}}).users.count.should == 0
    }
  end
end