require 'spec_helper'

describe ServiceProgramList do
  use_vcr_cassette "create_service"
  before(:each) do
    @program = Program.make!(:processed)
    @package = Package.make!(:programs => [@program])
    @service = Service.make!(:packages => [@package])
    @service_program = @service.service_programs.first
    
    @valid_attributes = {
      :program => Program.make!(:introduction), 
      :service_id => @service_program.service_id, 
      :service_program_id => @program.id
    }
    
    @service_program_list = @service_program.program_lists.new(@valid_attributes)
  end
  
  it "should be able to add to program lists" do
    @service_program_list.save!
  end
  
  describe "intros vs. closers" do
    before(:each) { @service_program_list.save! }
    
    it "should order the PackageProgramList automatically based on the classification" do
      @service_program_list.position.should eql(1)
    end
    
    it "should increase the order of my package" do
      @service_program_list = @service_program.program_lists.create!(:program => Program.make!(:introduction))
      @service_program_list.position.should eql(2)
    end
    
    it "should scope positions via intros vs. closers" do
      @service_program_list = @service_program.program_lists.create!(:program => Program.make!(:closer))
      @service_program_list.position.should eql(1)
    end
  end
  
  describe "scopes" do
    before(:each) do
      @service_program_list.save! # introduction
      2.times { @service_program.program_lists.create!(:program => Program.make!(:closer)) }
    end
    
    it ".introductions" do
      ServiceProgramList.introductions.count.should eql(1)
    end
    
    it ".closers" do
      ServiceProgramList.closers.count.should eql(2)
    end
  end
end
