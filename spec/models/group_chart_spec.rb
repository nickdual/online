require 'spec_helper'

describe GroupChart do
  let(:group) { fire_double('Group') }

  describe ".number_of_users_by_service(group)" do
    let(:service) { double('Service') }
    let(:service_employments_active_count) { 7 }

    let(:chart_data) {
      { title: 'Number of Users by Service',
        series: [{
          name: 'Number of users',
          data: [
            [ service.to_s, service_employments_active_count ] ] }] } }

    subject { GroupChart.number_of_users_by_service(group) }

    before do
      group.should_receive(:services).and_return([service])
      service.stub_chain(:employments, :active, :count).and_return(service_employments_active_count)
    end

    it { expect(subject).to match_json_expression(chart_data) }
  end

  describe ".learning_records_created_in_24_hours(group)" do
    let(:learning_record_class) { fire_class_double('LearningRecord').as_replaced_constant }
    let(:employment_class) { fire_class_double('Employment').as_replaced_constant }

    let(:service){ fire_double('Service', id: 360) }
    let(:employment){ fire_double('Employment', id: 370) }

    let(:learning_records){ [:learning_record] }

    let(:hours) {
      (-23..0).collect { |hours_ago| Time.zone.now.advance(hours: hours_ago) } }

    let(:chart_data) {
      { title: 'Learning Records created in the past 24 hours',
        yAxisTitle: 'Number of Programs',
        categories: hours.map{|hour| hour.strftime("%l%P") },
        series: [{
          name: "programs",
          data: hours.map{ learning_records.count } }] } }

    subject { GroupChart.learning_records_created_in_24_hours(group) }

    before do
      group.should_receive(:services).and_return([service])
      employment_class.should_receive(:where).with(service_id: [service.id]).and_return([employment])

      learning_record_class.should_receive(:where).
        with(employment_id: [employment.id]).exactly(hours.count).and_return(learning_records)

      learning_records.should_receive(:where).
        with(viewed_at: [anything()..anything()]).exactly(hours.count).and_return(learning_records)
    end

    it { expect(subject).to match_json_expression(chart_data) }
  end

  describe ".learning_records_past_30_days_by_service(group)" do
    let(:learning_records) { [:learning_record] }
    let(:service) { fire_double('Service', name: 'name') }

    let(:chart_data) {
      { title: 'Learning Records created in the past 30 days',
        yAxisTitle: 'Number of Programs',
        categories: [service.name],
        series: [{ name: "programs", data: [learning_records.count] }] } }

    subject { GroupChart.learning_records_past_30_days_by_service(group) }

    before do
      Time.stub(:now).and_return(Time.new)
      group.stub_chain(:services, :default_order).at_least(:once).and_return([service])
      service.should_receive(:learning_records).and_return(learning_records)
      learning_records.should_receive(:where).with(viewed_at: [30.days.ago..Time.zone.now]).and_return(learning_records)
    end

    it { expect(subject).to match_json_expression(chart_data) }
  end

  describe ".learning_plans_completed_over_30_days" do
    let(:learning_plans) { [:learning_plan] }
    let(:service) { fire_double('Service', name: 'name') }

    let(:chart_data) {
      { title: 'Learning Plans completed in the past 30 days',
        yAxisTitle: 'Number of Learning Plans completed',
        categories: [service.name],
        series: [{ name: "learning plans", data: [learning_plans.count] }] } }

    subject { GroupChart.learning_plans_completed_over_30_days(group) }

    before do
      Time.stub(:now).and_return(Time.new)
      group.stub_chain(:services, :default_order).at_least(:once).and_return([service])
      service.stub_chain(:employment_learning_plans, :completed).and_return(learning_plans)
      learning_plans.should_receive(:where).with(updated_at: [30.days.ago..Time.zone.now]).and_return(learning_plans)
    end

    it { expect(subject).to match_json_expression(chart_data) }
  end

  describe ".learning_plan_pending_vs_overdue(group)" do
    let(:employment_class) { fire_class_double('Employment').as_replaced_constant }

    let(:service) { fire_double('Service', id: 1080) }
    let(:employment) { fire_double('Employment') }
    let(:overdue_learning_plan_programs) { [:overdue_learning_plan_program, :overdue_learning_plan_program] }
    let(:pending_learning_plan_programs) { [:pending_learning_plan_program] }

    let(:chart_data) {
      { title: 'Learning Plans pending vs. overdue',
        series: [{
          name: 'Number of programs',
          data: [
            ["Overdue", overdue_learning_plan_programs.size],
            ["Pending Completion", pending_learning_plan_programs.size] ] }] } }

    subject { GroupChart.learning_plan_pending_vs_overdue(group) }

    before do
      expect(overdue_learning_plan_programs.size).to_not eq(pending_learning_plan_programs)
      group.should_receive(:services).and_return([service])

      employment_class.should_receive(:where).with(service_id: [service.id]).at_least(:once).and_return([employment])
      employment.should_receive(:overdue_learning_plan_programs).and_return(overdue_learning_plan_programs)
      employment.should_receive(:pending_learning_plan_programs).and_return(pending_learning_plan_programs)
    end

    it { expect(subject).to match_json_expression(chart_data) }
  end
end
