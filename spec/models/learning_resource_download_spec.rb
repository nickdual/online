require 'spec_helper'

describe LearningResourceDownload do
  use_vcr_cassette "create_service"
  before(:each) do
    @user = User.make!
    @program = Program.make!(:processed)
    @learning_resource = LearningResource.make!(:program => @program)
  end
  
  it "should be able to create a learning resource download" do
    LearningResourceDownload.create!(:user => @user, :learning_resource => @learning_resource)
  end
end
