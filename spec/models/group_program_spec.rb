require 'spec_helper'

describe GroupProgram do
  use_vcr_cassette "create_service"
  before(:each) do
    @package = Package.make!(:programs => [Program.make!])
    @group = Group.make!(:packages => [@package])
  end
  
  it "should tell me how many programs are in a group" do
    @group.programs.count.should eql(1)
  end
end
