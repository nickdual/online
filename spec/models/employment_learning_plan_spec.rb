require 'spec_helper'

describe EmploymentLearningPlan do
  use_vcr_cassette "create_service_via_employment"
  let(:service)     { Service.make! }
  let(:program)     { Program.make! }
  let(:user)        { User.make! }
  let(:employment)  { Employment.make!(service: service, user: user) }

  before(:each) do
    @valid_attributes = {
      name: "Fake learning plan!",
      programs: [program],
      employments: [employment],
      days_till_due: 5,
      active_at: Time.zone.now,
      service: service
    }

    @learning_plan = LearningPlan.make!(@valid_attributes)
  end

  subject { EmploymentLearningPlan.first }

  context "create a user learning plan" do
    it { subject.should be_valid }
    it { subject.programs.count.should == 1 }

    # Plan is active, we start on the next day
    context "#days_remaining for active plan" do
      it { subject.days_remaining.should == 6 }
    end

    context "#days_till_active" do
      it { subject.days_till_active.should == 1 }
    end
  end

  describe "#active_at" do
    before(:each) do
      @learning_plan = LearningPlan.new(@valid_attributes)
    end

    subject { EmploymentLearningPlan.first }

    context "not be activated till the next day" do
      before(:each) { @learning_plan.save }
      it { subject.active_at.to_date.should == Time.zone.now.tomorrow.to_date }
    end

    context "not activated until activation date" do
      before(:each) do
        @learning_plan.active_at = 5.days.from_now
        @learning_plan.save!
      end

      subject { EmploymentLearningPlan.last }
      it { subject.active_at.to_date.should == 5.days.from_now.to_date }
    end
  end

  describe "#status" do
    before(:each) do
      # Making the learning plan active_at earlier
      EmploymentLearningPlan.first.update_attributes(active_at: 5.hours.ago)
    end

    context "pending" do
      it { subject.pending?.should == true }
    end

    context "active" do
      before(:each) do
        @learning_plan.update_attributes(active_at: 2.days.ago)
        EmploymentLearningPlan.destroy_all
        plan = EmploymentLearningPlan.create!(employment: employment, learning_plan: @learning_plan)
        reset_mailer
        plan.active!
      end

      subject { EmploymentLearningPlan.first }
      it { subject.reload.active?.should == true }
      it { unread_emails_for(subject.employment.user.email).last.subject
          .should == "You have a New Learning Plan: #{subject.learning_plan}" }
    end

    context "overdue" do
      before(:each) { reset_mailer; subject.overdue! }
      it { subject.overdue?.should == true }
      it { unread_emails_for(subject.user.email).first.subject
          .should eql("Your Learning Plan #{subject.learning_plan} is now Overdue") }
    end

    context "completed" do
      before(:each) do
        sleep(1); subject.active!
        employment.learning_records.create!(program: program)
        employment.program_assessments.create!(program: program, assessment_type: "Essentials", coordinator: user)
      end

      it { subject.reload.completed?.should == true }
      it { subject.reload.completed_at.should_not == nil }
    end

    context "completed with no assessments" do
      before(:each) do
        sleep(1); subject.active!
        LearningPlanProgram.first.update_attributes(essentials: false)
        employment.learning_records.create!(program: program)
      end

      it { subject.reload.completed?.should == true }
      it { subject.reload.completed_at.should_not == nil }
    end
  end

  describe "#set_due_at!" do
    before(:each) { subject.set_due_at! }

    # Starts at the beginning of the next day
    it { subject.due_at.to_date.should == 6.days.from_now.to_date }

    context "should update due_at automatically" do
      context "in future" do
        before(:each) do
          @learning_plan.update_attributes(days_till_due: 10)
          subject.set_due_at!
        end

        # Starts at the beginning of the next day
        it { subject.due_at.to_date.should == 11.days.from_now.to_date }
      end
    end
  end

  describe "Scopes" do
    before(:each) do
      @learning_plan.update_attributes(active_at: 2.days.ago)
    end

    context ".overdue" do
      subject { EmploymentLearningPlan.overdue }

      before(:each) { EmploymentLearningPlan.first.overdue! }
      it { subject.count.should == 1 }
    end

    context ".upcoming" do
      before(:each) do
        EmploymentLearningPlan.first.update_attributes(active_at: 5.days.ago, status: "active")
      end

      subject { EmploymentLearningPlan.upcoming }
      it { subject.count.should == 1 }
    end
  end
end
