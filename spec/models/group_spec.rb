require 'spec_helper'
require 'cancan/matchers'

describe Group do
  use_vcr_cassette "create_service"
  before(:each) do
    @admin = Admin.make!

    @valid_attributes = {
      :name => "McDonalds",
      :country => "Australia"
    }

    @group = Group.new(@valid_attributes)
  end

  it "should create a group" do
    @group.save!
  end

  describe "Disabling and enabling a group" do
    before(:each) do
      @group.save!
    end

    it "should allow a group to be enabled by default" do
      @group.disabled?.should eql(nil)
    end

    it "should be able to disable a group" do
      @group.disable!(@admin)
      @group.disabled?.should_not eql(nil)
    end

    it "should be able to be enabled once disabled" do
      @group.disable!(@admin)
      @group.enable!
      @group.disabled?.should eql(nil)
      @group.disabled_by_id.should eql(nil)
    end

    context "Services" do
      before(:each) { @service = @group.services.make!(name: "Something", group: @group) }

      it "should disable all services when a group is disabled" do
        @group.disable!(@admin)
        @service.reload.disabled?.should_not eql(nil)
      end

      it "should enable all services when a group is enabled" do
        @group.disable!(@admin)
        @service.reload.disabled?.should_not eql(nil)
        @group.enable!
        @service.reload.disabled?.should eql(nil)
      end

      it "should finish all employments under the services" do
        @employment = @service.employments.make!(:service => @service)
        @group.disable!(@admin)

        @employment.reload
        @employment.status.should eql("inactive")
      end
    end
  end

  describe "#coordinators.new(user: user)" do
    let(:group){ described_class.new }
    let(:coordinator){ group.coordinators.new(user: user) }

    context "where user is a new user" do
      let(:user){ User.new }
      it "builds a new coordinator with the user" do
        expect(coordinator.user).to be_new_record
        expect(coordinator.user).to eq(user)
        expect(coordinator.user_id).to be_nil
      end
    end

    context "where user is an existing user" do
      let(:user){ User.make! }
      it "builds a new coordinator with a user_id set to the user's id" do
        expect(coordinator.user).to eq(user)
        expect(coordinator.user_id).to eq(user.id)
      end
    end
  end
end
