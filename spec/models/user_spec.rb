require 'spec_helper'
require 'cancan/matchers'

describe User do
  use_vcr_cassette "create_service"
  let(:service) { Service.make! }
  before(:each) do
    @valid_attributes = {
      :email => "harry@icdesign.com.au",
      :password => 123456,
      :password_confirmation => 123456,
      :first_name => "Harry",
      :last_name => "Potter"
    }

    @user = User.new(@valid_attributes)
  end

  subject { @user }
  it { subject.save! }

  context "valid email addresses" do
    context "'" do
      before(:each) { @user.email = "harry'potter@icdesign.com.au" }
      it { subject.should be_valid }
    end

    context "_" do
      before(:each) { @user.email = "julie_mcd_@hotmail.com" }
      it { subject.should be_valid }
    end

    context "no spaces" do
      before(:each) { @user.email = "harry potter@icdesign.com.au" }
      it { subject.should_not be_valid }
    end
  end

  describe "#timezone" do
    it "should default to nil if left blank" do
      @user.time_zone = ""
      @user.save
      @user.reload.time_zone.should be_nil
    end
  end

  describe "Employment" do
    before(:each) { @employment = @user.employments.build(service: service) }
    it "should be able to associate a user to a service via employment" do
      @user.save
      service.users.count.should eql(1)
    end

    it "should mark an employment role as a user by default" do
      @user.save
      service.staff.count.should eql(1)
      service.coordinators.count.should eql(0)
    end

    it "should be able to scope out coordinators" do
      @employment.role = "coordinator"
      @user.save
      service.coordinators.count.should eql(1)
    end
  end

  describe "#create_learning_record!" do
    let(:service) { Service.make! }
    let(:program) { Program.make! }
    subject { @user }

    before(:each) do
      Employment.make!(user: @user, service: service)
    end

    context "can create a learning record" do
      before(:each) { @user.create_learning_record!(program) }
      it { subject.learning_records.count.should == 1 }
    end

    context "can create multiple learning records" do
      let(:service1) { Service.make! }
      use_vcr_cassette "create_multiple_services"

      before(:each) do
        Employment.make!(user: @user, service: service1)
        @user.create_learning_record!(program)
      end

      subject { @user.learning_records }
      it { subject.count.should == 2 }
    end

    context "can pass attributes to learning records" do
      before(:each) do
        @user.create_learning_record!(program, "DVD", 5.hours.ago)
      end

      subject { @user.learning_records.first }
      it { subject.medium.should == "DVD" }
      it { subject.viewed_at.to_s(:time).should == 5.hours.ago.to_s(:time) }
    end
  end

  describe "find_or_build_user" do
    let(:service) { Service.make! }
    before(:each) do
      @attributes = {
        email: "thomas@icdesign.com.au",
        first_name: "Thomas",
        last_name: "Sinclair",
        employments_attributes: [{role: "coordinator", activation_token: service.activation_token}]
      }
    end
    subject { User.find_or_build_user(@attributes, service) }
    it { subject; User.count.should == 1 }
    it { subject.should == Employment.first }
    it { subject.role.should == "coordinator" }

    context "existing user" do
      before(:each) do
        @user = User.make!(email: "thomas@icdesign.com.au")
      end

      it { subject; User.count.should == 1 }
      it { subject.should == Employment.first }
      it { subject.role.should == "coordinator" }
    end
  end

  describe "Existing user (via email)" do
    before(:each) { @user.save! }
    context "#existing_email?" do
      it "should tell me there is no existing user" do
        @user.existing_email?.should_not eql(true)
      end

      it "should tell me there is an existing user via email" do
        @new_user = User.make(:email => @user.email)
        @new_user.save
        @new_user.existing_email?.should eql(true)
      end
    end

    context "existing_user" do
      it "should return the user which already exists based on email" do
        @user = User.make(:email => "harry@icdesign.com.au")
        @user.save
        @user.existing_user.should eql(User.first)
      end
    end
  end

  context "when email is changed" do
    let(:service) { Service.make! }
    let!(:invited_user) { User::Invited.make!(activation_token: service.activation_token) }
    let!(:user_saved_at){ Time.now }

    # reload to test if callback is called through User class instead of User::Invited
    let(:user) { User.find(invited_user.id) }

    before do
      reset_mailer
      Time.should_receive(:now).at_least(:once).and_return(user_saved_at)
      user.update_attribute :email, "new_#{user.email}"
    end

    it { expect(unread_emails_for(user.email).count).to eq(1) }
    it { expect(unread_emails_for(user.email).first.subject).to eq("Your Account has been Created for #{service}") }
    it { expect(user.welcome_email_resent_at).to eq user_saved_at }
  end

  describe ".find_or_initialize_by_email(email)" do
    let(:email) { Forgery(:email).address }

    context "where email is an email of an existing user" do
      let(:existing_user) { User.make!(email: email) }
      subject { User.find_or_initialize_by_email(existing_user.email) }

      it { expect(subject).to eq(existing_user) }
    end

    context "where there is no user matching the mail" do
      it "initializes a new user with the email" do
        user = User.find_or_initialize_by_email(email)
        expect(user).to be_new_record
        expect(user.email).to eq(email)
      end
    end
  end

  describe "#attributes=(hash)" do
    let(:email) { 'gsmendoza@gmail.com' }
    it "sets the attributes of the user to the hash" do
      user = User.new
      user.attributes = {email: email}
      expect(user.email).to eq(email)
    end
  end
end
