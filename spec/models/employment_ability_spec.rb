require "spec_helper"
require "cancan/matchers"

describe EmploymentAbility do
  use_vcr_cassette "create_service"
  before(:each) do
    @user = User.make!
    @ability = EmploymentAbility.new(@user)
  end
  
  describe "Employment" do
    before(:each) { @service = Service.make! }
    context "coordinator" do
      before(:each) { @employment = @user.employments.create!(:service => @service, :role => "coordinator") }
    
      it "should be able to manage an employment if the user is a coordinator at this service" do
        @ability.should be_able_to(:manage, @employment)
      end

      it "should not be able to access an employment if we pass an Employment class" do
        @ability.should_not be_able_to(:manage, Employment)
      end
    end
    
    context "coordinator" do
      before(:each) do
        @user.employments.create!(:service => @service, :role => "coordinator") # Marking the user as a coordinator
        @employment = Employment.create!(:user => User.make!, :service => @service)
      end
      
      it "should be able to manage an employment if the user is a coordinator at this service" do
        @ability.should be_able_to(:manage, @employment)
      end
      
      it "should not be able to access an employment if we pass an Employment class" do
        @ability.should_not be_able_to(:manage, Employment)
      end
    end
  end
  
  describe "LearningResource" do
    before(:each) do
      @program = Program.make!(:processed)
      @learning_resource = LearningResource.make!(:program => @program)
      
      @service = Service.make!(:packages => [Package.make!(:programs => [@program])])
      @user.employments.create!(:service => @service)
    end
    
    it "should allow a user to access a learning resource" do
      @ability.should be_able_to(:read, @learning_resource)
    end
    
    it "shouldn't allow a user to access a learning resource if they don't have that program assigned to them" do
      @user.employments.first.update_attributes(:ended_at => Time.zone.now)
      @ability.should_not be_able_to(:read, @learning_resource)
    end
    
    it "should allow a user to download coordinator learning resources" do
      @user.employments.first.update_attributes(:role => "coordinator")
      @ability.should be_able_to(:read_coordinator_resources, @program)
    end

    it "should not allow a user to download coordinator learning resources" do
      @ability.should_not be_able_to(:read_coordinator_resources, @program)
    end

  end

end
