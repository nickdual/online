require 'spec_helper'

describe LearningPlanProgram do
  use_vcr_cassette "create_service"
  let(:program) { Program.make!(:processed) }
  let(:service) { Service.make! }
  let(:plan)    { LearningPlan.make!(service: service) }
  subject { LearningPlanProgram.new(program_id: program.id, learning_plan_id: plan.id) }

  context "create learning plan program" do
    it { subject.save! }
  end
end