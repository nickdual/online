@staging_services = [
  {:name => "Deerhurst", :country => "United Kingdom", :group => Group.find_by_name("Brunelcare")},
  {:name => "Glastonbury Care Home", :country => "United Kingdom", :group => Group.find_by_name("Brunelcare")},
  {:name => "Saffron", :country => "United Kingdom", :group => Group.find_by_name("Brunelcare")},
  {:name => "Trevarna", :country => "United Kingdom", :group => Group.find_by_name("Cornwall Care Ltd")},
  {:name => "Cedar Grange", :country => "United Kingdom", :group => Group.find_by_name("Cornwall Care Ltd")},
  {:name => "Chyvarhas", :country => "United Kingdom", :group => Group.find_by_name("Cornwall Care Ltd")},
  {:name => "Arden House", :country => "United Kingdom", :group => Group.find_by_name("Greensleeves Homes Trust")},
  {:name => "St Cross Grange", :country => "United Kingdom", :group => Group.find_by_name("Greensleeves Homes Trust")},
  {:name => "Tickford Abbey", :country => "United Kingdom", :group => Group.find_by_name("Greensleeves Homes Trust")},
  {:name => "Berelands House", :country => "United Kingdom", :group => Group.find_by_name("Priory Group")},
  {:name => "Charles Court Care Home", :country => "United Kingdom", :group => Group.find_by_name("Priory Group")},
  {:name => "Ben Madigan Nursing Home", :country => "United Kingdom", :group => Group.find_by_name("Priory Group")},
  {:name => "Calway House", :country => "United Kingdom", :group => Group.find_by_name("Somerset Care Ltd")},
  {:name => "Rowden House", :country => "United Kingdom", :group => Group.find_by_name("Somerset Care Ltd")},
  {:name => "Field House", :country => "United Kingdom", :group => Group.find_by_name("Somerset Care Ltd")}
]
