require 'spec_helper'

describe Api::Profiles::EmploymentLearningPlansController do
  let(:employment) { fire_double('Employment', id: 4) }
  let(:user){ fire_double('User',
    employments: [employment],
    time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#index" do
    let(:employment_learning_plan) {
      json_representable_double('EmploymentLearningPlan',
        id: 60,
        status_id: 18,
        due_at: Time.now) }

    let(:employment_learning_plans_relation) {
      double(:employment_learning_plans_relation).tap {|relation|
        relation.stub(:includes).and_return(relation)
        relation.stub(:not_pending).and_return(relation) } }

    let(:employment_learning_plan_class) { fire_class_double('EmploymentLearningPlan').as_replaced_constant }

    context "with params[:active]" do
      subject { get :index, active: true, format: 'json' }

      it "responds with noncompleted plans of the current user ordered by due_at" do
        user.employments.should_receive(:active).and_return(user.employments)
        user.employments.should_receive(:pluck).with(:id).and_return([employment.id])

        employment_learning_plan_class
          .should_receive(:where).with(employment_id: [employment.id])
          .and_return(employment_learning_plans_relation)

        employment_learning_plans_relation
          .should_receive(:not_completed)
          .and_return(employment_learning_plans_relation)

        employment_learning_plans_relation
          .should_receive(:order)
          .with('due_at')
          .and_return(employment_learning_plans_relation)

        employment_learning_plans_relation
          .should_receive(:limit)
          .with(999)
          .and_return([employment_learning_plan])

        expect(subject.body).to match_json_expression([employment_learning_plan.as_json])
      end
    end

    context "without params[:active]" do
      subject { get :index, format: 'json' }

      it "includes the completed plans of the current user in the response" do
        user.employments.should_receive(:active).and_return(user.employments)
        user.employments.should_receive(:pluck).with(:id).and_return([employment.id])

        employment_learning_plan_class
          .should_receive(:where).with(employment_id: [employment.id])
          .and_return(employment_learning_plans_relation)

        employment_learning_plans_relation
          .should_receive(:limit)
          .with(999)
          .and_return([employment_learning_plan])

        expect(subject.body).to match_json_expression([employment_learning_plan.as_json])
      end
    end
  end
end
