require "spec_helper"

describe Api::Profiles::ActivitiesController do
  let(:activities) { Array.new(3){ json_representable_double('User::Activity') } }

  let(:user){ fire_double('User',
    activities: activities,
    time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#index" do
    context "without a params[:limit]" do
      subject { get :index, format: 'json' }

      it "responds with all the activities of the current user" do
        expect(subject.body).to match_json_expression(activities.as_json)
      end
    end

    context "with a params[:limit]" do
      let(:limit) { 2 }
      subject { get :index, limit: limit, format: 'json' }

      it "responds with only first <limit> activities of the current user" do
        expect(subject.body).to match_json_expression([activities[0], activities[1]].as_json)
      end
    end
  end
end
