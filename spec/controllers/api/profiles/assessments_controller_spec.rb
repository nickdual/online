require "spec_helper"

describe Api::Profiles::AssessmentsController do
  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#show" do
    let(:assessment) { { essentials_required:  true } }
    let(:program) { fire_double('Program', 'id' => 7) }
    let(:program_class){ fire_class_double('Program').as_replaced_constant }

    subject { get :show, id: program.id, format: 'json' }

    it "responds with the assessment of the user matching the program id" do
      program_class.should_receive(:find).with(program.id.to_s).and_return(program)
      user.should_receive(:assessments).with(program).and_return(assessment)

      expect(subject.body).to match_json_expression(assessment)
    end
  end
end
