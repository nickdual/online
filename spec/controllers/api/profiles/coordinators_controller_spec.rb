require "spec_helper"

describe Api::Profiles::CoordinatorsController do
  let(:coordinator) { json_representable_double('Coordinator', id: 4) }

  let(:user){ fire_double('User',
    coordinators: [coordinator],
    time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#index" do
    subject { get :index, format: 'json' }

    it "responds with the coordinators of the current user" do
      expect(subject.body).to match_json_expression([coordinator.as_json])
    end
  end

  describe "#destroy" do
    subject { delete :destroy, id: coordinator.id, format: 'json' }

    it "removes the coordinator from the current user" do
      user.coordinators.should_receive(:find).with(coordinator.id.to_s).and_return(coordinator)
      coordinator.should_receive(:destroy)
      expect(subject.body).to be_blank
    end
  end
end
