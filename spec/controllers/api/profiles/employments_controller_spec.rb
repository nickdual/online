require "spec_helper"

describe Api::Profiles::EmploymentsController do
  let(:employment_attributes) { { 'id' => 130 } }
  let(:employment) {
    json_representable_double('Employment', employment_attributes) }

  let(:employments) { [employment] }

  let(:user){ fire_double('User',
    time_zone: Time.zone,
    employments: employments) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#index" do
    subject { get :index, format: 'json' }

    it "responds with the active non-user coordinator employments of the user, ordered by service" do
      employments.stub_chain(:active, :not_group_coordinator, :includes, :order_by_service).and_return(employments)
      expect(subject.body).to match_json_expression([employment_attributes])
    end
  end

  describe "#create" do
    subject { post :create, employment: employment_attributes, format: 'json' }

    it "adds the employment to the user" do
      user.employments.should_receive(:new).with(employment_attributes).and_return(employment)
      employment.should_receive(:save).and_return(true)
      expect(subject.body).to match_json_expression(employment_attributes)
    end

    context "with invalid params" do
      let(:expected_errors){ {:service_id=>["can't be blank"]} }

      let(:employment_attributes){ {'id' => nil} }

      it "responds with the errors" do
        user.employments.should_receive(:new).with(employment_attributes).and_return(employment)

        employment.should_receive(:save)
        employment.should_receive(:errors).at_least(:once).and_return(expected_errors)
        expect(subject.body).to match_json_expression(errors: expected_errors)
      end
    end
  end

  describe "#show" do
    subject { get :show, id: employment.id, format: 'json' }

    it "responds with the employment under the user" do
      user.employments.should_receive(:find).with(employment.id.to_s).and_return(employment)
      expect(subject.body).to match_json_expression(employment_attributes)
    end
  end

  describe "#update" do
    subject { put :update, id: employment.id, employment: employment_attributes, format: 'json' }

    it "updates the employment under the user with params[:employment]" do
      user.employments.should_receive(:find).with(employment.id.to_s).and_return(employment)
      employment.should_receive(:update_attributes).with(employment_attributes).and_return(true)

      expect(subject.body).to match_json_expression(employment_attributes)
    end
  end

  describe "#destroy" do
    subject { delete :destroy, id: employment.id, format: 'json' }

    it "finishes the employment of the user" do
      user.employments.should_receive(:find).with(employment.id.to_s).and_return(employment)
      employment.should_receive(:finish!)
      expect(subject.body).to be_blank
    end
  end
end
