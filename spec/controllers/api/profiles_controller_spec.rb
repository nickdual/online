require 'spec_helper'

describe Api::ProfilesController do
  let(:user) { User.make! }

  before do
    sign_in user
  end

  describe "#show" do
    let(:user_json){ UserSerializer.new(user).as_json[:user] }
    subject { get :show, format: 'json' }
    it { expect(subject.body).to match_json_expression(user_json) }
  end

  describe "#update" do
    let(:new_first_name) { user.first_name * 2 }

    before do
      put :update, user: { first_name: new_first_name }, format: 'json'
    end

    it { expect(user.reload.first_name).to eq(new_first_name) }
    it { expect(response.body).to match_json_expression({first_name: new_first_name}.forgiving!) }
  end
end
