require "spec_helper"

describe Api::Services::GroupSessionsController do
  let(:group_session) {
    json_representable_double('GroupSession', group_session_attributes) }

  let(:group_session_attributes) { { 'id' => 130 } }

  let(:service) {
    fire_double('Service',
      id: 11,
      group_sessions: double('service_group_sessions_relation')) }

  let(:service_class){ fire_class_double('Service').as_replaced_constant }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    service_class.should_receive(:find_using_slug!).with(service.id.to_s).and_return(service)
  end

  describe "#create" do
    subject { post :create, group_session: group_session_attributes, service_id: service.id, format: 'json' }

    it "adds the group session to the service" do
      service.group_sessions.should_receive(:new).with(group_session_attributes.stringify_keys).and_return(group_session)
      group_session.should_receive(:save)
      expect(subject.body).to match_json_expression(group_session.as_json)
    end
  end
end
