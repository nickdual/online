require "spec_helper"

describe Api::Services::EmploymentLearningPlansController do
  let(:employment_learning_plan) {
    json_representable_double('EmploymentLearningPlan', employment_learning_plan_attributes) }

  let(:employment_learning_plan_attributes) {
    { id: 14,
      status_id: 10,
      due_at: Time.now } }

  let(:employment) {
    fire_double('Employment',
      id: 13,
      employment_learning_plans: double('employment.employment_learning_plans_relation')) }

  let(:employment_class) { fire_class_double('Employment').as_replaced_constant }
  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    employment_class.should_receive(:find).with(employment.id.to_s).and_return(employment)
  end

  describe "#index" do
    subject { get :index, employment_id: employment.id, employment_id: employment.id, format: 'json' }

    it "responds with the employment_learning_plans under the employment" do
      employment.employment_learning_plans.should_receive(:includes)
        .with(:service, :user, :employment, :learning_records, :program_assessments, :employment_learning_plan_programs)
        .and_return([employment_learning_plan])

      expect(subject.body).to match_json_expression([employment_learning_plan.as_json])
    end
  end

  describe "#show" do
    subject { get :show, employment_id: employment.id, id: employment_learning_plan.id, format: 'json' }

    it "responds with the employment_learning_plan matching params[:id] under params[:employment_id]" do
      employment.employment_learning_plans
        .should_receive(:find)
        .with(employment_learning_plan.id.to_s)
        .and_return(employment_learning_plan)

      expect(subject.body).to match_json_expression(employment_learning_plan.as_json)
    end
  end

  describe "#create" do
    subject { post :create,
      employment_id: employment.id,
      employment_learning_plan: employment_learning_plan_attributes,
      format: 'json' }

    it "adds params[:employment_learning_plan] to the employment" do
      employment.employment_learning_plans
        .should_receive(:new)
        .with(employment_learning_plan_attributes.stringify_keys)
        .and_return(employment_learning_plan)

      employment_learning_plan.should_receive(:save).and_return(true)

      expect(subject.body).to match_json_expression(employment_learning_plan_attributes.as_json)
    end
  end
end
