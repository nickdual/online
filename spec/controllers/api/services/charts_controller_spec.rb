require 'spec_helper'

describe Api::Services::ChartsController do
  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#index" do
    class ServiceChartDouble < OpenStruct
      def expected_json
        { type: self.type, name: self.name, data: self.fake_data }
      end

      def fake_data
        { title: self.name }
      end
    end

    let(:service_class) { fire_class_double('Service').as_replaced_constant }
    let(:service_chart_class) { fire_class_double('ServiceChart').as_replaced_constant }

    let(:service) { fire_double('Service', id: 120) }

    let(:charts){
      [ ServiceChartDouble.new(
          type: 'column', name: 'learning_records_in_24_hours', data_method: :learning_records_over_24_period),
        ServiceChartDouble.new(
          type: 'column', name: 'learning_records_in_7_days', data_method: :learning_records_past_7_days),
        ServiceChartDouble.new(
          type: 'column', name: 'learning_records_in_30_days', data_method: :learning_records_over_30_days),
        ServiceChartDouble.new(
          type: 'column', name: 'learning_plans_in_30_days', data_method: :learning_plans_completed_over_30_days),
        ServiceChartDouble.new(
          type: 'pie', name: 'learning_plans_pending_vs_overdue', data_method: :learning_plan_pending_vs_overdue)]}

    subject { get :index, service_id: service.id, format: 'json' }

    it "returns a list of charts" do
      service_class.should_receive('find_using_slug!').with(service.id.to_s).and_return(service)

      charts.each do |chart|
        service_chart_class.should_receive(chart.data_method).and_return(chart.fake_data)
      end

      expect(subject.body).to match_json_expression(charts.map(&:expected_json))
    end
  end
end
