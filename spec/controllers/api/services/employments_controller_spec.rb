require "spec_helper"

describe Api::Services::EmploymentsController do
  let(:employment) {
    json_representable_double('Employment', employment_attributes) }

  let(:employment_attributes) { { 'id' => 130 } }
  let(:employment_class) { fire_class_double('Employment').as_replaced_constant }
  let(:employments_relation) { double('employments_relation') }
  let(:employment_search_class) { fire_class_double('EmploymentSearch').as_replaced_constant }

  let(:service) {
    fire_double('Service',
      id: 11,
      employments: double('service_employments_relation')) }

  let(:service_class){ fire_class_double('Service').as_replaced_constant }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    service_class.should_receive(:find_using_slug!).with(service.id.to_s).and_return(service)
  end

  describe "#index" do
    subject { get :index, service_id: service.id, format: 'json' }

    it "responds with the active employments of the service, ordered by name" do
      employment_class
        .should_receive(:where)
        .with(service_id: service.id)
        .and_return(employments_relation)

      employments_relation.should_receive(:active).and_return(employments_relation)
      employments_relation.should_receive(:includes).with(:service, :user).and_return(employments_relation)
      employments_relation.should_receive(:order_by_name).and_return([employment])

      expect(subject.body).to match_json_expression([employment.as_json])
    end
  end

  describe "#advanced" do
    let(:expected_params) {
      { "service_id"=> service.id.to_s,
        "format"=>"json",
        "controller"=>"api/services/employments",
        "action"=>"advanced"} }

    subject { get :advanced, service_id: service.id, format: 'json' }

    it "responds with employments of the service matching the EmploymentSearch params" do
      employment_search_class
        .should_receive(:results)
        .with(expected_params, service)
        .and_return([employment])

      expect(subject.body).to match_json_expression([employment.as_json])
    end
  end

  describe "#create" do
    let(:questionable){
      fire_double('User::Questionable',
        employment: employment,
        user: user_of_questionable) }
    let(:questionable_class) { fire_class_double('User::Questionable').as_replaced_constant }

    let(:service) {
      fire_double('Service', id: 11, activation_token: 'activation_token') }

    let(:user_of_questionable){ json_representable_double('User', id: 69) }

    let(:user_attributes) { {} }
    let(:user_attributes_with_questionable_params) {
      { service_id: service.id, activation_token: service.activation_token } }

    subject { post :create, user: user_attributes, service_id: service.id, format: 'json' }

    before do
      questionable_class
        .should_receive(:new).with(user_attributes_with_questionable_params.stringify_keys)
        .and_return(questionable)
    end

    context "where the questionable object represented by params[:user] is saveable" do
      it "responds with the employment of the questionable object" do
        questionable.should_receive(:save).and_return(true)
        expect(subject.body).to match_json_expression(questionable.employment.as_json)
      end
    end

    context "where the questionable object represented by params[:user] is not saveable" do
      it "responds with the actual user of the questionable object" do
        questionable.should_receive(:save).and_return(false)
        expect(subject.body).to match_json_expression(questionable.user.as_json)
      end
    end

    it "sets the service_id and activation_token of params[:user] to the service" do
      questionable.stub(:save).and_return(true)
      subject
      expect(controller.params[:user]).to eq(user_attributes_with_questionable_params.stringify_keys)
    end
  end

  describe "#show" do
    subject { get :show, id: employment.id, service_id: service.id, format: 'json' }

    it "responds with the employment under the service" do
      service.employments.should_receive(:find).with(employment.id.to_s).and_return(employment)
      expect(subject.body).to match_json_expression(employment.as_json)
    end
  end

  describe "#update" do
    subject { put :update, id: employment.id, employment: employment_attributes, service_id: service.id, format: 'json' }

    it "updates the employment under the service with params[:employment]" do
      service.employments.should_receive(:find).with(employment.id.to_s).and_return(employment)
      employment.should_receive(:touch).and_return(employment)
      employment.should_receive(:update_attributes).with(employment_attributes.stringify_keys)
      expect(subject.body).to match_json_expression(employment.as_json)
    end
  end

  describe "#destroy" do
    let(:current_time) { Time.zone.now }

    subject { delete :destroy, id: employment.id, service_id: service.id, format: 'json' }

    it "sets the end time of the employment to the current time" do
      Time.zone.should_receive(:now).at_least(:once).and_return(current_time)
      service.employments.should_receive(:find).with(employment.id.to_s).and_return(employment)
      employment.should_receive(:update_attribute).with(:ended_at, current_time)
      expect(subject.body).to be_blank
    end
  end
end
