require "spec_helper"

describe Api::Services::JobTitlesController do
  let(:service) {
    fire_double('Service',
      id: 4,
      job_titles: double('service_job_titles_relation')) }

  let(:service_class) { fire_class_double('Service').as_replaced_constant(transfer_nested_constants: true) }

  let(:service_job_title) {
    json_representable_double('Service::JobTitle', service_job_title_attributes ) }

  let(:service_job_title_attributes) { { 'id' => 7 } }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    service_class.should_receive(:find_using_slug!).with(service.id.to_s).and_return(service)
  end

  describe "#index" do
    subject { get :index, service_id: service.id, format: 'json' }

    context "where the service has a group" do
      let(:group){
        fire_double('Group',
          id: 29,
          job_titles: double('group_job_titles_relation')) }

      let(:group_job_title) {
        json_representable_double('Group::JobTitle', id: 29) }

      let(:service) {
        fire_double('Service',
          id: 33,
          group: group,
          group_id: group.id,
          job_titles: double('service_job_titles_relation')) }

      it "responds with the job titles of the group of the service" do
        group.job_titles.should_receive(:default_order).and_return([group_job_title])
        expect(subject.body).to match_json_expression([group_job_title.as_json])
      end
    end

    context "where the service does not have a service" do
      let(:service) {
        fire_double('Service',
          id: 4,
          group_id: nil,
          job_titles: double('service_job_titles_relation')) }

      it "responds with the job titles of the service" do
        service.job_titles.should_receive(:default_order).and_return([service_job_title])
        expect(subject.body).to match_json_expression([service_job_title.as_json])
      end
    end
  end

  describe "#create" do
    subject { post :create, service_id: service.id, job_title: service_job_title_attributes, format: 'json' }

    it "adds the job title to the service" do
      service.job_titles
        .should_receive(:new)
        .with(service_job_title_attributes.stringify_keys)
        .and_return(service_job_title)
      service_job_title.should_receive(:save)
      expect(subject.body).to match_json_expression(service_job_title.as_json)
    end

    context "with invalid params" do
      let(:service_job_title_attributes) { { 'id' => nil } }

      let(:expected_errors){ {"name"=>["can't be blank"]} }

      it "responds with the errors" do
        service.job_titles
          .should_receive(:new)
          .with(service_job_title_attributes)
          .and_return(service_job_title)

        service_job_title.should_receive(:save)
        service_job_title.should_receive(:errors).at_least(:once).and_return(expected_errors)
        expect(subject.body).to match_json_expression(errors: expected_errors)
      end
    end
  end

  describe "#show" do
    subject { get :show, id: service_job_title.id, service_id: service.id, format: 'json' }

    it "responds with the job title under the service" do
      service.job_titles.should_receive(:find).with(service_job_title.id.to_s).and_return(service_job_title)
      expect(subject.body).to match_json_expression(service_job_title.as_json)
    end
  end

  describe "#update" do
    subject { put :update, id: service_job_title.id, service_id: service.id, job_title: service_job_title_attributes, format: 'json' }

    it "updates the job title under the service with params[:job_title]" do
      service.job_titles.should_receive(:find).with(service_job_title.id.to_s).and_return(service_job_title)
      service_job_title.should_receive(:update_attributes).with(service_job_title_attributes)

      expect(subject.body).to match_json_expression(service_job_title.as_json)
    end
  end

  describe "#destroy" do
    subject { delete :destroy, id: service_job_title.id, service_id: service.id, format: 'json' }

    it "removes the job title from the service" do
      service.job_titles.should_receive(:find).with(service_job_title.id.to_s).and_return(service_job_title)
      service_job_title.should_receive(:destroy)

      expect(subject.body).to be_blank
    end
  end
end
