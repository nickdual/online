require "spec_helper"

describe Api::Services::LearningPlansController do
  let(:learning_plan) {
    json_representable_double('LearningPlan', learning_plan_attributes ) }

  let(:learning_plan_attributes) { { 'id' => 7 } }

  let(:learning_plan_class) { fire_class_double('LearningPlan').as_replaced_constant }

  let(:service) {
    fire_double('Service',
      id: 4,
      learning_plans: double('learning_plans_relation')) }

  let(:service_class) { fire_class_double('Service').as_replaced_constant }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    service_class.should_receive(:find_using_slug!).with(service.id.to_s).and_return(service)
  end

  describe "#index" do
    subject { get :index, service_id: service.id, format: 'json' }

    it "responds with the learning plans of the service" do
      service.learning_plans.should_receive(:default_order).and_return([learning_plan])
      expect(subject.body).to match_json_expression([learning_plan.as_json])
    end
  end

  describe "#create" do
    subject { post :create, service_id: service.id, learning_plan: learning_plan_attributes, format: 'json' }

    it "adds the learning plan to the service" do
      learning_plan_class
        .should_receive(:new)
        .with(learning_plan_attributes)
        .and_return(learning_plan)

      learning_plan.should_receive("service_id=").with(service.id)
      learning_plan.should_receive(:save)

      expect(subject.body).to match_json_expression(learning_plan.as_json)
    end

    context "with invalid params" do
      let(:learning_plan_attributes) { { 'id' => nil } }

      let(:expected_errors){ {"name"=>["can't be blank"]} }

      it "responds with the errors" do
        learning_plan_class
          .should_receive(:new)
          .with(learning_plan_attributes)
          .and_return(learning_plan)

        learning_plan.should_receive("service_id=").with(service.id)
        learning_plan.should_receive(:save)
        learning_plan.should_receive(:errors).at_least(:once).and_return(expected_errors)
        expect(subject.body).to match_json_expression(errors: expected_errors)
      end
    end
  end

  describe "#show" do
    subject { get :show, id: learning_plan.id, service_id: service.id, format: 'json' }

    it "responds with the learning plan under the service" do
      service.learning_plans.should_receive(:find).with(learning_plan.id.to_s).and_return(learning_plan)
      expect(subject.body).to match_json_expression(learning_plan.as_json)
    end
  end

  describe "#update" do
    subject { put :update, id: learning_plan.id, service_id: service.id, learning_plan: learning_plan_attributes, format: 'json' }

    it "updates the learning plan under the service with params[:learning_plan]" do
      service.learning_plans.should_receive(:find).with(learning_plan.id.to_s).and_return(learning_plan)
      learning_plan.should_receive(:update_attributes).with(learning_plan_attributes)

      expect(subject.body).to match_json_expression(learning_plan.as_json)
    end
  end

  describe "#destroy" do
    subject { delete :destroy, id: learning_plan.id, service_id: service.id, format: 'json' }

    it "removes the learning plan from the service" do
      service.learning_plans.should_receive(:find).with(learning_plan.id.to_s).and_return(learning_plan)
      learning_plan.should_receive(:destroy)

      expect(subject.body).to be_blank
    end
  end
end
