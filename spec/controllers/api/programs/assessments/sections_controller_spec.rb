require "spec_helper"

describe Api::Programs::Assessments::SectionsController do
  let(:section_attributes) { { 'id' => 4 } }
  let(:section) {
    json_representable_double('Program::Assessment::Section', section_attributes) }

  let(:sections){
    double('sections').tap {|sections|
      sections.stub(:requires_answers).and_return(sections)
      sections.stub(:as_json).and_return([section.as_json]) }}

  let(:program_assessment) {
    fire_double('Program::Assessment',
      id: 10,
      sections: sections) }

  let!(:program_assessment_class) {
    fire_class_double('Program::Assessment').as_replaced_constant(transfer_nested_constants: true) }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    program_assessment_class.should_receive(:find).with(program_assessment.id.to_s).and_return(program_assessment)
  end

  describe "#index" do
    subject { get :index, program_assessment_id: program_assessment.id, format: 'json' }

    it "responds with the sections of the program_assessment" do
      expect(subject.body).to match_json_expression([section.as_json])
    end
  end

  describe "#show" do
    subject { get :show, program_assessment_id: program_assessment.id, id: section.id, format: 'json' }

    it "responds with the section under the program_assessment" do
      program_assessment.sections.should_receive(:find).with(section.id.to_s).and_return(section)
      expect(subject.body).to match_json_expression(section_attributes)
    end
  end
end
