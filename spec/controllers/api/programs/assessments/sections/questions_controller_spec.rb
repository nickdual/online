require "spec_helper"
require 'program/assessment/multiple_choice_question' # zeus gem can't seem to find this

describe Api::Programs::Assessments::Sections::QuestionsController do
  let(:question_attributes) { { 'id' => 4 } }
  let(:question) {
    json_representable_double('Program::Assessment::MultipleChoiceQuestion', question_attributes) }

  let(:questions){ [question] }

  let(:section) {
    fire_double('Program::Assessment::Section', id: 10) }

  let!(:section_class) {
    fire_class_double('Program::Assessment::Section').as_replaced_constant }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    section_class.should_receive(:find).with(section.id.to_s).and_return(section)
  end

  describe "#index" do
    subject { get :index, program_assessment_section_id: section.id, format: 'json' }

    it "responds with the questions of the section" do
      section.should_receive(:questions).and_return(questions)
      questions.should_receive(:shuffle).and_return(questions)
      expect(subject.body).to match_json_expression([question.as_json])
    end
  end
end
