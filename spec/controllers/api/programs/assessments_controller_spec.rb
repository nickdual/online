require "spec_helper"

describe Api::Programs::AssessmentsController do
  let(:assessment_attributes) { { 'id' => 4 } }
  let(:assessment) {
    json_representable_double('Program::Assessment', assessment_attributes) }

  let(:program) {
    fire_double('Program',
      id: 10,
      assessments: [assessment]) }

  let!(:program_class) { fire_class_double('Program').as_replaced_constant(transfer_nested_constants: true) }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    program_class.should_receive(:find_using_slug!).with(program.id.to_s).and_return(program)
  end

  describe "#index" do
    let(:assessment) {
      json_representable_double('Program::Assessment', { id: 26 }) }

    subject { get :index, program_id: program.id, format: 'json' }

    it "responds with the assessments of the program" do
      expect(subject.body).to match_json_expression([assessment.as_json])
    end
  end

  describe "#show" do
    subject { get :show, program_id: program.id, id: assessment.id, format: 'json' }

    it "responds with the assessment under the program" do
      program.assessments.should_receive(:find).with(assessment.id.to_s).and_return(assessment)
      expect(subject.body).to match_json_expression(assessment_attributes)
    end
  end
end
