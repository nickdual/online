require "spec_helper"

describe Api::Programs::LearningRecordsController do
  let(:learning_record_attributes) { { 'id' => 4 } }
  let(:learning_record) {
    json_representable_double('LearningRecord', learning_record_attributes) }

  let(:learning_records_relation) { double('ActiveRecord::Relation') }

  let(:program) {
    fire_double('Program',
      id: 10,
      learning_records: learning_records_relation) }
  let(:program_class) { fire_class_double('Program').as_replaced_constant }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    program_class.should_receive(:find_using_slug!).with(program.id.to_s).and_return(program)
  end

  describe "#create" do
    subject { post :create, program_id: program.id, format: 'json' }

    it "adds to the user a learning_record based on the program " do
      user.should_receive(:create_learning_record!).with(program).and_return(learning_record)
      expect(subject.body).to match_json_expression(learning_record_attributes)
    end
  end

  describe "#show" do
    subject { get :show, program_id: program.id, id: learning_record.id, format: 'json' }

    it "responds with the learning_record under the program" do
      program.learning_records.should_receive(:find).with(learning_record.id.to_s).and_return(learning_record)
      expect(subject.body).to match_json_expression(learning_record_attributes)
    end
  end
end
