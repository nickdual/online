require "spec_helper"

describe Api::Programs::VideosController do
  let(:program_class) { fire_class_double('Program').as_replaced_constant }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    program_class.should_receive(:find_using_slug!).with(program.id.to_s).and_return(program)
  end

  describe "#index" do
    let(:program) {
      fire_double('Program',
        id: 10,
        videos: double('Program.videos')) }

    let(:video) { json_representable_double('Video', id: 16) }

    subject { get :index, program_id: program.id, format: 'json' }

    it "responds with non-original videos under the program" do
      program.videos.should_receive(:not_original).and_return([video])
      expect(subject.body).to match_json_expression([video.as_json])
    end
  end
end
