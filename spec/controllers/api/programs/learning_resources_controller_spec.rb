require "spec_helper"

describe Api::Programs::LearningResourcesController do
  let(:program) {
    fire_double('Program',
      id: 10,
      learning_resources: double('ActiveRecord::Relation')) }
  let(:program_class) { fire_class_double('Program').as_replaced_constant }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    program_class.should_receive(:find_using_slug!).with(program.id.to_s).and_return(program)
  end

  describe "#index" do
    let(:learning_resource) {
      json_representable_double('LearningResource', { id: 4 }) }

    context "with params[:coordinator]" do
      subject { get :index, coordinator: true, program_id: program.id, format: 'json' }

      it "responds with coordinatory-only learning resources of the program" do
        program.learning_resources.should_receive(:coordinators_only).and_return(program.learning_resources)
        program.learning_resources.stub(:default_order).and_return([learning_resource])
        expect(subject.body).to match_json_expression([learning_resource.as_json])
      end
    end

    context "without params[:coordinator]" do
      subject { get :index, program_id: program.id, format: 'json' }

      it "responds with all-staff learning resources of the program" do
        program.learning_resources.should_receive(:all_staff).and_return(program.learning_resources)
        program.learning_resources.stub(:default_order).and_return([learning_resource])
        expect(subject.body).to match_json_expression([learning_resource.as_json])
      end
    end
  end

  describe "#show" do
    let(:file) { File.open('spec/fixtures/cat.jpg') }

    let(:learning_resource) {
      json_representable_double('LearningResource', {
        id: 4,
        resource_name: file.path,
        file: file }) }

    it "sends a file of the learning_resource under the program to the user" do
      program.learning_resources.should_receive(:find_using_slug!).with(learning_resource.id.to_s).and_return(learning_resource)
      learning_resource.should_receive(:create_downloads_by_user!).with(user)

      get :show, program_id: program.id, id: learning_resource.id, format: 'json'

      # Check if the action is sending the file
      expect(controller.response_body.to_path).to eq(file)
      expect(response).to be_success
    end
  end
end
