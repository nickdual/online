require "spec_helper"

describe Api::Programs::RelatedController do
  let(:program_class) { fire_class_double('Program').as_replaced_constant }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    program_class.should_receive(:find_using_slug!).with(program.id.to_s).and_return(program)
  end

  describe "#index" do
    let(:program) { fire_double('Program', id: 10) }

    let(:available_related_program) {
      json_representable_double('Program', { id: 18 }) }

    let(:unavailable_related_program) {
      json_representable_double('Program', { id: 21 }) }

    let(:available_unrelated_program) {
      json_representable_double('Program', { id: 24 }) }

    subject { get :index, program_id: program.id, format: 'json' }

    it "responds with programs available to the current that are related to the program" do
      user.should_receive(:programs).and_return([available_related_program, available_unrelated_program])
      program.should_receive(:related).and_return([available_related_program, unavailable_related_program])

      expect(subject.body).to match_json_expression([available_related_program.as_json])
    end
  end
end
