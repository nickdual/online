require 'spec_helper'

describe Api::ProgramsController do
  use_vcr_cassette "create_service"

  let(:user) { User.make! }

  before do
    sign_in user
  end

  describe "#index" do
    context "when a service_id is given" do
      let(:service) { Service.make!(:with_program) }
      let(:programs_json){ ActiveModel::ArraySerializer.new(service.programs).as_json }

      subject { get :index, format: 'json', service_id: service }

      it "returns the programs under that service_id" do
        expect(subject.body).to match_json_expression(programs_json)
      end
    end

    context "when no service_id is given" do
      let(:user) { User.make!(:with_program) }
      let(:programs_json){ ActiveModel::ArraySerializer.new(user.programs).as_json }

      subject { get :index, format: 'json' }

      it "returns the programs belonging to the current user" do
        expect(subject.body).to match_json_expression(programs_json)
      end
    end
  end

  describe "#show" do
    subject { get :show, format: 'json', id: program.id }

    context "when the program belongs to the current user" do
      let(:user) { User.make!(:with_program) }
      let(:program) { user.programs.first }
      let(:program_json){ ProgramSerializer.new(program).as_json[:program] }

      it "shows the program" do
        expect(subject.body).to match_json_expression(program_json)
      end
    end
  end
end
