require 'spec_helper'

describe Api::ServicesController do
  let(:user){ fire_double('User', time_zone: Time.zone) }

  let(:service_as_json){ { id: 1 } }
  let(:service){ stub_model(Service, service_as_json) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#index" do
    let(:group_class){ fire_class_double('Group').as_replaced_constant }
    let(:service_class){ fire_class_double('Service').as_replaced_constant }

    let(:services){ [service] }

    let(:employment) { fire_double('Employment', service_id: service.id) }

    context "when group_id is provided" do
      let(:group){ fire_double('Group', id: 10, services: [service]) }

      subject { get :index, group_id: group.id, format: :json }

      it "returns the active services of the group" do
        group_class.should_receive(:find_using_slug!).with(group.id.to_s).and_return(group)
        service_class.should_receive(:where).with(id: [service.id]).and_return(services)
        services.stub_chain(:active, :default_order).and_return(services)

        expect(subject.body).to match_json_expression([service_as_json.forgiving!])
      end
    end

    context "when role_id is provided" do
      let(:role_id) { 35 }

      subject { get :index, role_id: role_id, format: :json }

      it "returns the active services of the current user's active non-staff employments" do
        user.stub_chain(:employments, :active, :non_staff).and_return([employment])
        service_class.should_receive(:where).with(id: [service.id]).and_return(services)
        services.stub_chain(:active, :default_order).and_return(services)

        expect(subject.body).to match_json_expression([service_as_json.forgiving!])
      end
    end

    context "when neither group_id or role_id is provided" do
      subject { get :index, format: :json }

      it "returns the active services of the current user's active employments" do
        user.stub_chain(:employments, :active).and_return([employment])
        service_class.should_receive(:where).with(id: [service.id]).and_return(services)
        services.stub_chain(:active, :default_order).and_return(services)

        expect(subject.body).to match_json_expression([service_as_json.forgiving!])
      end
    end
  end

  describe "#reset_token" do
    subject { put :reset_token, id: service.id, format: :json }

    it "resets the token of the service" do
      Service.should_receive(:find_using_slug!).with(service.id.to_s).and_return(service)
      service.should_receive(:reset_token)

      expect(subject.body).to match_json_expression(service: service_as_json.forgiving!)
    end
  end
end
