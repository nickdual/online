require "spec_helper"

describe Api::Groups::JobTitlesController do
  let(:group_class) { fire_class_double('Group').as_replaced_constant(transfer_nested_constants: true) }

  let(:group) { fire_double('Group', id: 120, job_titles: [group_job_title]) }
  let(:group_job_title_attributes) { { 'id' => 130 } }

  let(:group_job_title) {
    fire_double('Group::JobTitle',
      group_job_title_attributes.merge(as_json: group_job_title_attributes) ) }

  let(:new_group_job_title) { json_representable_double('Group::JobTitle', id: nil) }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    group_class.should_receive(:find_using_slug!).with(group.id.to_s).and_return(group)
  end

  describe "#index" do
    subject { get :index, group_id: group.id, format: 'json' }

    it "responds with the job categories of the group" do
      group.job_titles.should_receive(:default_order).and_return(group.job_titles)
      expect(subject.body).to match_json_expression([group_job_title_attributes])
    end
  end

  describe "#create" do
    subject { post :create, group_id: group.id, job_title: group_job_title_attributes, format: 'json' }

    it "adds the job category to the group" do
      group.job_titles
        .should_receive(:new)
        .with(group_job_title_attributes)
        .and_return(group_job_title)

      group_job_title.should_receive(:save).and_return(true)
      expect(subject.body).to match_json_expression(group_job_title.as_json)
    end

    context "with invalid params" do
      let(:expected_errors){ {"name"=>["can't be blank"]} }

      it "responds with the errors" do
        group.job_titles
          .should_receive(:new)
          .with(group_job_title_attributes)
          .and_return(new_group_job_title)

        new_group_job_title.should_receive(:save)
        new_group_job_title.should_receive(:errors).at_least(:once).and_return(expected_errors)
        expect(subject.body).to match_json_expression(errors: expected_errors)
      end
    end
  end

  describe "#show" do
    subject { get :show, id: group_job_title.id, group_id: group.id, format: 'json' }

    it "responds with the job category under the group" do
      group.job_titles.should_receive(:find).with(group_job_title.id.to_s).and_return(group_job_title)
      expect(subject.body).to match_json_expression(group_job_title_attributes)
    end
  end

  describe "#update" do
    subject { put :update, id: group_job_title.id, group_id: group.id, job_title: group_job_title_attributes, format: 'json' }

    before do
      group.job_titles.should_receive(:find).with(group_job_title.id.to_s).and_return(group_job_title)
    end

    it "updates the job category under the group with params[:job_title]" do
      group_job_title.should_receive(:update_attributes).with(group_job_title_attributes).and_return(true)

      expect(subject.body).to match_json_expression(group_job_title_attributes)
    end

    context "with invalid params" do
      let(:expected_errors){ {"name"=>["can't be blank"]} }

      it "responds with the errors" do
        group_job_title.should_receive(:update_attributes).with(group_job_title_attributes)
        group_job_title.should_receive(:errors).at_least(:once).and_return(expected_errors)
        expect(subject.body).to match_json_expression(errors: expected_errors)
      end
    end
  end

  describe "#destroy" do
    subject { delete :destroy, id: group_job_title.id, group_id: group.id, format: 'json' }

    it "removes the job category from the group" do
      group.job_titles.should_receive(:find).with(group_job_title.id.to_s).and_return(group_job_title)
      group_job_title.should_receive(:destroy)

      # https://rails.lighthouseapp.com/projects/8994/tickets/5199-respond_with-returns-on-put-and-delete-verb
      # responds_with seems to ignore the @job_title and just renders an empty string.
      expect(subject.body).to be_blank
    end
  end
end
