require "spec_helper"

describe Api::Groups::CoordinatorsController do
  let!(:group_class) { fire_class_double('Group').as_replaced_constant(transfer_nested_constants: true) }

  let(:group) { fire_double('Group', id: 120, coordinators: [coordinator]) }

  let(:coordinator_attributes) { { 'id' => 130 } }
  let(:coordinator) {
    fire_double('Coordinator',
      coordinator_attributes.merge(as_json: coordinator_attributes) ) }

  let(:new_coordinator) { json_representable_double('Coordinator', id: nil) }

  let(:user_attributes){ {
    email: 'gsmendoza@gmail.com',
    time_zone: Time.zone } }

  let(:user){ fire_double('User', user_attributes) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    group_class.should_receive(:find_using_slug!).with(group.id.to_s).and_return(group)
  end

  describe "#index" do
    subject { get :index, group_id: group.id, format: 'json' }

    it "responds with the coordinators of the group" do
      group.coordinators.should_receive(:default_order).and_return(group.coordinators)
      expect(subject.body).to match_json_expression([coordinator.as_json])
    end
  end

  describe "#create" do
    subject { post :create, group_id: group.id, coordinator: user_attributes, format: 'json' }

    before do
      User
        .should_receive(:find_or_initialize_by_email)
        .with(user.email)
        .and_return(user)

      user.should_receive("attributes=").with(user_attributes.stringify_keys)
      user.should_receive(:new_record?).and_return(true)
      user.should_receive("activation_code_set_at=").and_return(Time.zone.now)
      user.should_receive("password=").and_return("password")
      user.should_receive("password_confirmation=").and_return("password")
    end

    it "adds a coordinator for params[:user] to the group" do
      group.coordinators
        .should_receive(:new)
        .with(user: user)
        .and_return(coordinator)

      object = OpenStruct.new
      UserNotifier.should_receive(:new_user_has_joined_group).and_return(object)
      object.should_receive(:deliver).and_return(true)

      coordinator.should_receive(:save_with_user).and_return(true)
      expect(subject.body).to match_json_expression(coordinator.as_json)
    end

    context "with invalid params" do
      let(:expected_errors){ {"name"=>["can't be blank"]} }

      it "responds with the errors" do
        group.coordinators
          .should_receive(:new)
          .with(user: user)
          .and_return(new_coordinator)

        new_coordinator.should_receive(:save_with_user).and_return(false)
        new_coordinator.should_receive(:errors).at_least(:once).and_return(expected_errors)
        expect(subject.body).to match_json_expression(errors: expected_errors)
      end
    end
  end

  describe "#destroy" do
    subject { delete :destroy, id: coordinator.id, group_id: group.id, format: 'json' }

    it "removes the coordinator from the group" do
      group.coordinators.should_receive(:find).with(coordinator.id.to_s).and_return(coordinator)
      coordinator.should_receive(:destroy)
      expect(subject.body).to be_blank
    end
  end
end
