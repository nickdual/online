require 'spec_helper'

describe Api::Groups::ChartsController do
  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#index" do
    let(:group_class) { fire_class_double('Group').as_replaced_constant }
    let(:group_chart_class) { fire_class_double('GroupChart').as_replaced_constant }


    let(:learning_records_created_in_24_hours) { { title: "learning_records_created_in_24_hours" } }
    let(:learning_records_past_30_days_by_service) { { title: "learning_records_past_30_days_by_service" } }
    let(:learning_plans_completed_over_30_days) { { title: "learning_plans_completed_over_30_days" } }
    let(:number_of_users_by_service) { { title: "number_of_users_by_service" } }
    let(:learning_plan_pending_vs_overdue) { { title: "learning_plan_pending_vs_overdue" } }

    let(:group) { fire_double('Group', id: 120) }

    let(:charts_as_json) {
      [ { type: 'column', name: 'learning_records_in_24_hours', data: learning_records_created_in_24_hours },
        { type: 'column', name: 'learning_records_in_30_days', data: learning_records_past_30_days_by_service },
        { type: 'column', name: 'learning_plans_in_30_days', data: learning_plans_completed_over_30_days },
        { type: 'pie', name: 'users_by_service', data: number_of_users_by_service },
        { type: 'pie', name: 'learning_plans_pending_vs_overdue', data: learning_plan_pending_vs_overdue } ]
    }

    subject { get :index, group_id: group.id, format: 'json' }

    it "returns a list of charts" do
      group_class.should_receive('find_using_slug!').with(group.id.to_s).and_return(group)

      group_chart_class.should_receive(:learning_records_created_in_24_hours).with(group).and_return(learning_records_created_in_24_hours)
      group_chart_class.should_receive(:learning_records_past_30_days_by_service).with(group).and_return(learning_records_past_30_days_by_service)
      group_chart_class.should_receive(:learning_plans_completed_over_30_days).with(group).and_return(learning_plans_completed_over_30_days)
      group_chart_class.should_receive(:number_of_users_by_service).with(group).and_return(number_of_users_by_service)
      group_chart_class.should_receive(:learning_plan_pending_vs_overdue).with(group).and_return(learning_plan_pending_vs_overdue)

      expect(subject.body).to match_json_expression(charts_as_json)
    end

  end
end
