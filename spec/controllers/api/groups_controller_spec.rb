require 'spec_helper'

describe Api::GroupsController do
  describe "#index" do
    before do
      sign_in User.make!(:with_active_group)
    end

    let(:group_json){ ActiveModel::ArraySerializer.new(assigns(:groups)).as_json }
    subject { get :index, format: 'json' }

    it { expect(subject.body).to match_json_expression(group_json) }
  end
end
