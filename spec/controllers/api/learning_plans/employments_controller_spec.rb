require "spec_helper"

describe Api::LearningPlans::EmploymentsController do
  let(:employment) {
    fire_double('Employment',
      employment_attributes.merge(
        as_json: employment_attributes)) }

  let(:employment_attributes) {
    { id: 14 }.stringify_keys }

  let(:employment_learning_plan) {
    fire_double('EmploymentLearningPlan',
      employment_learning_plan_attributes.merge(
        as_json: employment_learning_plan_attributes)) }

  let(:employment_learning_plan_attributes) {
    { id: 18 }.stringify_keys }

  let(:learning_plan) {
    fire_double('LearningPlan',
      id: 13,
      employments: [employment],
      employment_learning_plans: [employment_learning_plan],
      ) }

  let(:learning_plan_class) { fire_class_double('LearningPlan').as_replaced_constant }
  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    learning_plan_class.should_receive(:find_using_slug!).with(learning_plan.id.to_s).and_return(learning_plan)
  end

  describe "#index" do
    subject { get :index, learning_plan_id: learning_plan.id, format: 'json' }

    it "responds with the employments under the learning_plan" do
      expect(subject.body).to match_json_expression([employment_attributes.as_json])
    end
  end

  describe "#destroy" do
  let(:other_employment_learning_plan) { fire_double('EmploymentLearningPlan') }

    subject { delete :destroy, learning_plan_id: learning_plan.id, id: employment.id, format: 'json' }

    it "removes the first employment learning plan matching the employment from learning_plan.employment_learning_plans" do
      learning_plan.employment_learning_plans
        .should_receive(:where)
        .with('employment_id = ?', employment.id.to_s)
        .and_return([employment_learning_plan, other_employment_learning_plan])

      employment_learning_plan.should_receive(:destroy)
      other_employment_learning_plan.should_not_receive(:destroy)

      expect(subject.body).to be_blank
    end
  end
end
