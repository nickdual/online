require "spec_helper"

describe Api::LearningPlans::EmploymentLearningPlansController do
  let(:employment_learning_plan) {
    fire_double('EmploymentLearningPlan',
      employment_learning_plan_attributes.merge(
        as_json: employment_learning_plan_attributes)) }

  let(:employment_learning_plan_attributes) {
    { id: 14,
      status_id: 10,
      due_at: Time.now }.stringify_keys }

  let(:learning_plan) {
    fire_double('LearningPlan',
      id: 13,
      employment_learning_plans: [employment_learning_plan]) }

  let(:learning_plan_class) { fire_class_double('LearningPlan').as_replaced_constant }
  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    learning_plan_class.should_receive(:find_using_slug!).with(learning_plan.id.to_s).and_return(learning_plan)
  end

  describe "#index" do
    subject { get :index, learning_plan_id: learning_plan.id, format: 'json' }

    before do
      learning_plan.employment_learning_plans.should_receive(:includes)
        .with(:service, :user, :employment, :learning_records, :program_assessments, :employment_learning_plan_programs)
        .and_return(learning_plan.employment_learning_plans)
    end

    it "responds with the employment_learning_plans under the learning_plan" do
      expect(subject.body).to match_json_expression([employment_learning_plan_attributes.as_json])
    end
  end

  describe "#show" do
    subject { get :show, learning_plan_id: learning_plan.id, id: employment_learning_plan.id, format: 'json' }

    it "responds with the employment_learning_plan matching params[:id] under params[:learning_plan_id]" do
      learning_plan.employment_learning_plans
        .should_receive(:find)
        .with(employment_learning_plan.id.to_s)
        .and_return(employment_learning_plan)

      expect(subject.body).to match_json_expression(employment_learning_plan_attributes.as_json)
    end
  end

  describe "#create" do
    subject { post :create,
      learning_plan_id: learning_plan.id,
      employment_learning_plan: employment_learning_plan_attributes,
      format: 'json' }

    it "adds params[:employment_learning_plan] to learning_plan.employment_learning_plans" do
      learning_plan.employment_learning_plans
        .should_receive(:new)
        .with(employment_learning_plan_attributes)
        .and_return(employment_learning_plan)

      employment_learning_plan.should_receive(:save)

      expect(subject.body).to match_json_expression(employment_learning_plan_attributes.as_json)
    end

    context "with invalid params" do
      let(:learning_plan_attributes) { { 'id' => nil } }

      let(:expected_errors){ {"employment_id"=>["is not unique"]} }

      it "responds with the validation errors" do
        learning_plan.employment_learning_plans
          .should_receive(:new)
          .with(employment_learning_plan_attributes)
          .and_return(employment_learning_plan)

        employment_learning_plan.should_receive(:save)
        employment_learning_plan.should_receive(:errors).at_least(:once).and_return(expected_errors)
        expect(subject.body).to match_json_expression(errors: expected_errors)
      end
    end
  end

  describe "#destroy" do
    subject { delete :destroy, learning_plan_id: learning_plan.id, id: employment_learning_plan.id, format: 'json' }

    it "removes the employment_learning_plan from learning_plan.employment_learning_plans" do
      learning_plan.employment_learning_plans
        .should_receive(:find)
        .with(employment_learning_plan.id.to_s)
        .and_return(employment_learning_plan)

      employment_learning_plan.should_receive(:destroy)

      expect(subject.body).to be_blank
    end
  end
end
