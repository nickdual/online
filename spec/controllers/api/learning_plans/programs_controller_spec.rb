require "spec_helper"

describe Api::LearningPlans::ProgramsController do
  let(:learning_plan) {
    fire_double('LearningPlan',
      id: 13,
      programs: [program]) }

  let(:learning_plan_class) { fire_class_double('LearningPlan').as_replaced_constant }

  let(:program) {
    fire_double('Program',
      program_attributes.merge(
        as_json: program_attributes)) }

  let(:program_attributes) {
    { id: 14 }.stringify_keys }

  let(:program_class) { fire_class_double('Program').as_replaced_constant }

  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
    learning_plan_class.should_receive(:find_using_slug!).with(learning_plan.id.to_s).and_return(learning_plan)
  end

  describe "#index" do
    subject { get :index, learning_plan_id: learning_plan.id, format: 'json' }

    before do
      learning_plan.programs.should_receive(:default_order).and_return(learning_plan.programs)
      learning_plan.programs.should_receive(:includes)
        .with(:category, :program_type, :thumb)
        .and_return(learning_plan.programs)
    end

    it "responds with the programs under the learning_plan" do
      expect(subject.body).to match_json_expression([program_attributes.as_json])
    end
  end

  describe "#create" do
    let(:learning_plan) {
      fire_double('LearningPlan',
        id: 13,
        programs: []) }

    subject { post :create,
      learning_plan_id: learning_plan.id,
      program_id: program.id,
      format: 'json' }

    it "looks for the program matching params[:program_id] and adds it to the learning_plan" do
      expect(learning_plan.programs).to be_empty

      program_class.should_receive(:find_using_slug!).with(program.id).and_return(program)
      learning_plan.should_receive(:save).and_return(true)

      expect(subject.body).to match_json_expression(program_attributes.as_json)
      expect(learning_plan.programs).to eq([program])
    end
  end

  describe "#destroy" do
    let(:learning_plan) {
      fire_double('LearningPlan',
        id: 13,
        programs: [program]) }

    subject { delete :destroy, learning_plan_id: learning_plan.id, id: program.id, format: 'json' }

    it "looks for the program matching params[:program_id] and removes it from the learning_plan" do
      expect(learning_plan.programs).to eq([program])

      program_class.should_receive(:find_using_slug!).with(program.id.to_s).and_return(program)

      expect(subject.body).to be_blank
      expect(learning_plan.programs).to be_empty
    end
  end
end
