require 'spec_helper'

describe Api::Employments::ActivitiesController do
  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#index" do
    let(:activity) { { activity_type: "Viewed Program" } }
    let(:employment) { fire_double('Employment', id: 60) }
    let(:employment_class) { fire_class_double('Employment').as_replaced_constant }

    subject { get :index, employment_id: employment.id, format: 'json' }

    it "responds with the activities of the employment's user" do
      employment_class.should_receive(:find).with(employment.id.to_s).and_return(employment)
      employment.should_receive(:user).and_return(user)
      user.should_receive(:activities).and_return([activity])

      expect(subject.body).to match_json_expression([activity.as_json])
    end
  end
end
