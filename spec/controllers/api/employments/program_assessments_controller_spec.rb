require 'spec_helper'

describe Api::Employments::ProgramAssessmentsController do
  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#create" do
    let(:employment) { fire_double('Employment', id: 130, program_assessments: []) }
    let(:employment_class) { fire_class_double('Employment').as_replaced_constant }
    let(:program_assessment_attributes) { { "assessment_type" =>  'Essentials' } }
    let(:new_program_assessment) { fire_double('ProgramAssessment') }

    subject { post :create, employment_id: employment.id, program_assessment: program_assessment_attributes, format: 'json' }

    it "adds the program assessment to the employment" do
      employment_class.should_receive(:find).with(employment.id.to_s).and_return(employment)
      employment.program_assessments.should_receive(:new).with(program_assessment_attributes).and_return(new_program_assessment)
      new_program_assessment.should_receive(:save!)
      new_program_assessment.should_receive(:as_json).and_return(program_assessment_attributes)

      expect(subject.body).to match_json_expression(program_assessment_attributes)
    end
  end
end
