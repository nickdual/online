require 'spec_helper'

describe Api::EmploymentLearningPlans::ProgramsPendingController do
  let(:user){ fire_double('User', time_zone: Time.zone) }

  before do
    controller.stub(:authenticate_user!).and_return(true)
    controller.stub(:current_user).and_return(user)
  end

  describe "#index" do
    let(:employment_learning_plan_class) { fire_class_double('EmploymentLearningPlan').as_replaced_constant }

    let(:pending_program) { fire_double('EmploymentLearningPlanProgram', as_json: { title: 'title' }) }
    let(:employment_learning_plan) { fire_double('EmploymentLearningPlan', id: 60) }

    subject { get :index, employment_learning_plan_id: employment_learning_plan.id, format: 'json' }

    it "responds with the pending_programs of the employment_learning_plan" do
      employment_learning_plan_class.should_receive(:find).with(employment_learning_plan.id.to_s).and_return(employment_learning_plan)
      employment_learning_plan.should_receive(:programs_pending).and_return([pending_program])

      expect(subject.body).to match_json_expression([pending_program.as_json])
    end
  end
end
