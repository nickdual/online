require 'spec_helper'

describe Admin::Programs::AssessmentsController do
  let!(:program_class){
    model_name = Program.model_name
    VerifiedDouble.of_class('Program').tap{|klass|
      klass.stub(:model_name).and_return(model_name) } }

  let(:admin){ VerifiedDouble.of_instance('Admin') }

  let(:program){
    VerifiedDouble.of_instance('Program',
      id: 7,
      assessments: program_assessments_association,
      title: 'program-title',
      class: program_class) }

  let(:program_assessments_association){
    double('ActiveRecord::Associations',
      build: assessment) }

  before do
    pending # To be fixed in #55687666
    program_class.should_receive(:find_using_slug!).with(program.id.to_s).and_return(program)

    controller.stub(:authenticate_admin!).and_return(true)
    controller.stub(:current_admin).and_return(admin)
  end


  describe "#new" do
    let(:assessment){ VerifiedDouble.of_instance('Program::Assessment') }

    it "should add default sections to the new assessment" do
      assessment.should_receive(:add_default_sections)
      get :new, program_id: program.id
    end
  end

  describe "#update" do
    let(:assessment){
      VerifiedDouble.of_instance('Program::Assessment',
        id: 45,
        name: 'Essential',
        class: Program::Assessment) }

    before do
      assessment.should_receive(:update_attributes)
    end

    context "where params[:commit] is t('program.assessment.activate')" do
      let!(:time){ Time.now }


      before do
        Time.stub(:now).and_return(time)
      end

      it "should set the activation timestamp of the assessment" do
        program_assessments_association.should_receive(:find)
          .with(assessment.id.to_s).and_return(assessment)

        assessment.should_receive('activated_at=').with(time)
        put :update, id: assessment.id, program_id: program.id, commit: I18n.t('program.assessment.activate')
      end
    end

    context "where params[:commit] is not t('program.assessment.activate')" do
      it "should not set the activation timestamp of the assessment" do
        program_assessments_association.should_receive(:find)
          .with(assessment.id.to_s).and_return(assessment)

        assessment.should_not_receive('activated_at=')
        put :update, id: assessment.id, program_id: program.id
      end
    end
  end
end
