require 'spec_helper'

describe Admin::Programs::Assessments::Sections::SequentialQuestionsController do
  let!(:section_class){
    VerifiedDouble.of_class('Program::Assessment::Section', model_name: Program::Assessment::Section.model_name) }

  let(:admin){ VerifiedDouble.of_instance('Admin') }

  let(:assessment){
    VerifiedDouble.of_instance('Program::Assessment',
      class: Program::Assessment,      
      name: 'assessment-name',
      program: program,
      id: 11) }

  let(:program){
    VerifiedDouble.of_instance('Program',
      id: 17, title: 'program-title', class: Program) }

  let(:question){ VerifiedDouble.of_instance('Program::Assessment::SequentialQuestion') }

  let(:questions){ double('ActiveRecord::Associations', build: question) }

  let(:section){
    VerifiedDouble.of_instance('Program::Assessment::Section',
      id: 18,
      assessment: assessment,
      sequential_questions: questions) }

  before do
    section_class.should_receive(:find).with(section.id.to_s).and_return(section)

    controller.stub(:authenticate_admin!).and_return(true)
    controller.stub(:current_admin).and_return(admin)
  end

  describe "#new" do
    it "should add default sections to the new sequential question" do
      question.should_receive(:add_default_answer_items)
      get :new, program_assessment_section_id: section.id
    end
  end
end
