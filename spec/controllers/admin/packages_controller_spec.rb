require 'spec_helper'

describe Admin::PackagesController do
  let(:admin){ fire_double('Admin') }

  let(:package){
    fire_double('Package', id: 6, name: 'package-name', :class => package_class) }

  let!(:package_class){
    fire_class_double('Package').as_replaced_constant.tap {|klass|
      klass.extend ActiveModel::Naming  } }

  before do
    controller.stub(:authenticate_admin!).and_return(true)
    controller.stub(:current_admin).and_return(admin)
    package_class.should_receive(:find_using_slug).with(package.id.to_s).and_return(package)
  end

  describe "#update" do
    it "should update the updated_by of the package" do
      package_class.should_receive(:find_using_slug!).with(package.id.to_s).and_return(package)
      package.should_receive("updated_by=").with(admin)
      package.should_receive(:update_attributes).and_return(true)

      put :update, package: {}, id: package.id
    end
  end
end
