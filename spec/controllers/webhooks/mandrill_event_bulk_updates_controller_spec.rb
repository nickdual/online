require 'spec_helper'

describe Webhooks::MandrillEventBulkUpdatesController do
  context "Scenario: System records delivery time of successfully delivered emails" do
    let(:expected_state) { 'sent' }
    let(:unix_timestamp) { 1365109999 }

    let(:user){ User.make!(
      email: "example.webhook@mandrillapp.com",
      last_email_bounce_description: 'mailbox_full',
      last_email_status: 'soft-bounced') }

    let(:mandrill_event_as_json){
      { "event" => "send",
        "msg" => {
          "_id" => "exampleaaaaaaaaaaaaaaaaaaaaaaaaa",
          "_version" => "exampleaaaaaaaaaaaaaaa",
          "clicks" => [],
          "email" => user.email,
          "metadata" => { "user_id" => user.id },
          "opens" => [],
          "sender" => "example.sender@mandrillapp.com",
          "state" => expected_state,
          "subject" => "This an example webhook message",
          "tags" => [ "webhook-example" ],
          "ts" => unix_timestamp } } }

    Steps do
      Given "the system called Mandrill to send an email to a user" do
        user
      end

      When "Mandrill notifies the system that it was able to send the email" do
        post :create, mandrill_events: [mandrill_event_as_json].to_json
      end

      Then "the system should record the delivery time as the email verified date of the user" do
        user.reload
        expect(user.email_verified_at).to eq(Time.at(unix_timestamp))
        expect(user.last_email_status).to eq(expected_state)
      end

      And "the system should clear the last email rejected message of the user (for example, if the previous one soft-bounced)" do
        expect(user.last_email_bounce_description).to be_nil
      end
    end
  end

  context "Scenario: System records error message of soft-bounced emails" do
    let(:last_email_verified_at) { Time.zone.now }
    let(:soft_bounce_description) { 'mailbox_full' }
    let(:expected_state) { "soft-bounced" }

    let(:user){ User.make!(
      email: "example.webhook@mandrillapp.com",
      last_email_bounce_description: nil,
      last_email_status: 'sent',
      email_verified_at: last_email_verified_at) }

    let(:mandrill_event_as_json){
      { "event" => "soft_bounce",
        "msg" => {
          "_id" => "exampleaaaaaaaaaaaaaaaaaaaaaaaaa",
          "_version" => "exampleaaaaaaaaaaaaaaa",
          "bgtools_code" => 22,
          "bounce_description" => soft_bounce_description,
          "diag" => "smtp;552 5.2.2 Over Quota",
          "email" => user.email,
          "metadata" => { "user_id" => user.id },
          "sender" => "example.sender@mandrillapp.com",
          "state" => expected_state,
          "subject" => "This an example webhook message",
          "tags" => [ "webhook-example" ],
          "ts" => 1365109999 } } }

    Steps do
      Given "the system called Mandrill to send an email to a user" do
        user
      end

      When "Mandrill notifies the system that the email soft-bounced" do
        post :create, mandrill_events: [mandrill_event_as_json].to_json
      end

      Then "the system should record the error rejected message in the user's data" do
        user.reload
        expect(user.last_email_bounce_description).to eq(soft_bounce_description)
        expect(user.last_email_status).to eq(expected_state)
      end

      And "it should NOT clear the email verified date of the user (because it's just a soft-bounce)" do
        expect(user.email_verified_at).to be_within(1).of(last_email_verified_at)
      end
    end
  end

  context "Scenario: System records error message of hard-bounced emails" do
    let(:hard_bounce_description) { "bad_mailbox" }
    let(:expected_state) { "bounced" }

    let(:user){ User.make!(
      email: "example.webhook@mandrillapp.com",
      last_email_bounce_description: nil,
      last_email_status: 'sent',
      email_verified_at: Time.zone.now) }

    let(:mandrill_event_as_json){
      { "event" => "hard_bounce",
        "msg" => {
          "_id" => "exampleaaaaaaaaaaaaaaaaaaaaaaaaa",
          "_version" => "exampleaaaaaaaaaaaaaaa",
          "bgtools_code" => 10,
          "bounce_description" => hard_bounce_description,
          "diag" => "smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.",
          "email" => user.email,
          "metadata" => { "user_id" => user.id },
          "sender" => "example.sender@mandrillapp.com",
          "state" => expected_state,
          "subject" => "This an example webhook message",
          "tags" => [ "webhook-example" ],
          "ts" => 1365109999 } } }

    Steps do
      Given "the system called Mandrill to send an email to a user" do
        user
      end

      When "Mandrill notifies the system that the email hard-bounced" do
        post :create, mandrill_events: [mandrill_event_as_json].to_json
      end

      Then "the system should record the error rejected message in the user's data" do
        user.reload
        expect(user.last_email_bounce_description).to eq(hard_bounce_description)
        expect(user.last_email_status).to eq(expected_state)
      end

      And "it should clear the email verified date of the user (because it's no longer clear if the email is valid)" do
        expect(user.email_verified_at).to be_nil
      end
    end
  end
end
