require 'spec_helper'

describe UserSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", User
end
