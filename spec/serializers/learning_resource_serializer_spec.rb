require 'spec_helper'

describe LearningResourceSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", LearningResource
end
