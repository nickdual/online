require 'spec_helper'

describe VideoSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Video
end
