require 'spec_helper'

describe ServiceSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Service
end
