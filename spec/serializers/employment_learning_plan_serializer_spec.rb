require 'spec_helper'

describe EmploymentLearningPlanSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", EmploymentLearningPlan
end
