require 'spec_helper'

describe CoordinatorSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Coordinator
end
