require 'spec_helper'

describe EmploymentLearningPlanProgramSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", EmploymentLearningPlanProgram
end
