require 'spec_helper'

describe Group::JobTitleSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Group::JobTitle
end
