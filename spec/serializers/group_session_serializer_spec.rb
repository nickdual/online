require 'spec_helper'

describe GroupSessionSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", GroupSession
end
