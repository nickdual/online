require 'spec_helper'

describe EmploymentSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Employment
end
