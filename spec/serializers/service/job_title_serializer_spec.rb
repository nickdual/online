require 'spec_helper'

describe Service::JobTitleSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Service::JobTitle
end
