require 'spec_helper'

describe ProgramSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Program
end
