require 'spec_helper'

describe LearningPlanSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", LearningPlan
end
