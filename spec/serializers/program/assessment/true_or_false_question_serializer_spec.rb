require 'spec_helper'

describe Program::Assessment::TrueOrFalseQuestionSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Program::Assessment::TrueOrFalseQuestion
end
