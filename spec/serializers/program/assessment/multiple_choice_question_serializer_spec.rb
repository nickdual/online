require 'spec_helper'

describe Program::Assessment::MultipleChoiceQuestionSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Program::Assessment::MultipleChoiceQuestion

  it "includes the question's answers" do
    expect(described_class._associations.keys).to include(:answers)
  end
end
