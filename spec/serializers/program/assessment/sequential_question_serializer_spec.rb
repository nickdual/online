require 'spec_helper'

describe Program::Assessment::SequentialQuestionSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Program::Assessment::SequentialQuestion

  it "includes the question's answer items" do
    expect(described_class._associations.keys).to include(:answer_items)
  end
end
