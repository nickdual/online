require 'spec_helper'

describe LearningRecordSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", LearningRecord
end
