require 'spec_helper'

describe GroupSerializer do
  it_behaves_like "serializer delegating its attributes to a model class", Group
end
