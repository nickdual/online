#= require 'slider_with_input'

describe "$(form).slider_with_input", ->
  $fixture = null
  $slider_with_input = null
  $slider = null
  $input = null

  beforeEach ->
    $slider = $('<div data-SliderWithInput-Slider />')
    $input = $('<input type="text" />')

    $slider_with_input = $('<div data-SliderWithInput />')
      .append($slider)
      .append($input)

    $fixture = $("<div id='fixture' />")
    $fixture.append($slider_with_input)

    $('body').append($fixture)

  afterEach ->
    $fixture.remove()

  describe "()", ->
    it "sets the slider's widget value to the input's value", ->
      $input.val(50)
      $slider_with_input.slider_with_input()
      expect($slider_with_input.slider_with_input('slider').slider('value')).toEqual(50)
    
    it "updates the slider's input when the slider's jquery widget is changed", ->
      $slider_with_input.slider_with_input()


      expect($slider_with_input.slider_with_input('input').val()).toEqual('')

      $slider_with_input.slider_with_input('slider').slider('value', 50)
      $slider_with_input.slider_with_input('slider').trigger('change')

      expect($slider_with_input.slider_with_input('input').val()).toEqual('50')
