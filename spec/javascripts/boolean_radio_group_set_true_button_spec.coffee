#= require 'boolean_radio_group_set_true_button'

describe "$(true_button).boolean_radio_group_set_true_button", ->
  $fixture = null
  $non_set_button = null
  $true_button = null
  $false_button = null
  $other_true_button = null
  $other_false_button = null

  beforeEach ->
    $non_set_button =
      $('<input type="radio" value="false" />')
    $true_button =
      $('<input type="radio" value="true" data-boolean-radio-group-set="correct" name="[0][correct]" />')
    $false_button =
      $('<input type="radio" value="false" data-boolean-radio-group-set="correct" name="[0][correct]" />')
    $other_true_button =
      $('<input type="radio" value="true" data-boolean-radio-group-set="correct" name="[1][correct]" />')
    $other_false_button =
      $('<input type="radio" value="false" data-boolean-radio-group-set="correct" name="[1][correct]" />')

    $true_button.boolean_radio_group_set_true_button()

    $fixture = $("<div id='fixture' />")
      .append($non_set_button)
      .append($true_button)
      .append($false_button)
      .append($other_true_button)
      .append($other_false_button)

    $('body').append($fixture)

  afterEach ->
    $fixture.remove()

  describe "()", ->
    it "makes the group set refresh when changed", ->
      $true_button.prop('checked', true).trigger('change')

      expect($false_button.is(':checked')).toBeFalsy()
      expect($other_true_button.is(':checked')).toBeFalsy()
      expect($other_false_button.is(':checked')).toBeTruthy()

  describe "('refresh_group_set')", ->
    it "checks the false buttons of the other groups", ->
      $true_button.prop('checked', true)
      $true_button.boolean_radio_group_set_true_button('refresh_group_set')

      expect($false_button.is(':checked')).toBeFalsy()
      expect($other_true_button.is(':checked')).toBeFalsy()
      expect($other_false_button.is(':checked')).toBeTruthy()
      
  describe "('other_false_buttons')", ->
    it "includes only false buttons from the group set excluding the button's group", ->
      other_false_buttons = $true_button.boolean_radio_group_set_true_button('other_false_buttons')
      expect(other_false_buttons.size()).toEqual(1)
      expect(other_false_buttons.get()).toEqual($other_false_button.get())

  describe "('group_set_name')", ->
    it "is the boolean-radio-group-set data of the button", ->
      expect($true_button.boolean_radio_group_set_true_button('group_set_name'))
        .toEqual('correct')
    
  describe "('group_name')", ->
    it "is the name of the radio group which the button belongs to", ->
      expect($true_button.boolean_radio_group_set_true_button('group_name'))
        .toEqual('[0][correct]')
      
      
  

