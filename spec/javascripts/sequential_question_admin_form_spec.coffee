#= require 'sequential_question_admin_form'

describe "$(form).sequential_question_admin_form", ->
  build_answer_item = (attributes) ->
    $result = $('<div />')
    $correct_position_input = $('<input role="SequentialQuestionAdminForm.CorrectPositionInput" />')
    $correct_position_input.val(attributes['correct_position'])
    $result.append($correct_position_input)
    $result

  $fixture = null
  $form = null
  $answer_toggler = null
  $text_answer = null
  $image_answer = null

  beforeEach ->
    $answer_toggler = $('<input role="SequentialQuestionAdminForm.AnswerToggler" type="checkbox" />')
    $text_answer = $('<div role="SequentialQuestionAdminForm.TextAnswer">Text</div>')
    $image_answer = $('<div role="SequentialQuestionAdminForm.ImageAnswer">Image</div>')

    $form = $('<form />')
      .append($answer_toggler)
      .append($text_answer)
      .append($image_answer)

    $form.sequential_question_admin_form()

    $fixture = $("<div id='fixture' />")
    $fixture.append($form)

    $('body').append($fixture)

  afterEach ->
    $fixture.remove()

  describe "()", ->
    it "makes the form refresh when the answer toggler is clicked", ->
      expect($form.sequential_question_admin_form('answer_toggler').is(':checked')).toBeFalsy()

      $form.sequential_question_admin_form('answer_toggler').trigger('click')

      expect($form.sequential_question_admin_form('answer_toggler').is(':checked')).toBeTruthy()

      expect($form.sequential_question_admin_form('text_answers').filter(':visible').size()).toEqual(0)
      expect($form.sequential_question_admin_form('image_answers').filter(':visible').size()).toEqual(1)

  describe "('answer_toggler')", ->
    it "is the element with the role SequentialQuestionAdminForm.AnswerToggler", ->
      expect($form.sequential_question_admin_form('answer_toggler')[0]).toEqual($answer_toggler[0])

  describe "('text_answers')", ->
    it "should be the elements with the role SequentialQuestionAdminForm.TextAnswer", ->
      expect($form.sequential_question_admin_form('text_answers')[0]).toEqual($text_answer[0])

  describe "('image_answers')", ->
    it "should be the elements with the role SequentialQuestionAdminForm.ImageAnswer", ->
      expect($form.sequential_question_admin_form('image_answers')[0]).toEqual($image_answer[0])

  describe "('refresh')", ->
    it "shows text answers if the form must not display image answers", ->
      $answer_toggler.attr('checked', false)
      $form.sequential_question_admin_form('refresh')
      expect($form.sequential_question_admin_form('text_answers').filter(':visible').size()).toEqual(1)

    it "hides text answers if the form must display image answers", ->
      $answer_toggler.attr('checked', true)
      $form.sequential_question_admin_form('refresh')
      expect($form.sequential_question_admin_form('text_answers').filter(':visible').size()).toEqual(0)

    it "shows image answers if the form must display image answers", ->
      $answer_toggler.attr('checked', true)
      $form.sequential_question_admin_form('refresh')
      expect($form.sequential_question_admin_form('image_answers').filter(':visible').size()).toEqual(1)

    it "hides image answers if the form must not display image answers", ->
      $answer_toggler.attr('checked', false)
      $form.sequential_question_admin_form('refresh')
      expect($form.sequential_question_admin_form('image_answers').filter(':visible').size()).toEqual(0)

  describe "('must_display_image_answers')", ->
    it "is true if the answer toggler is checked", ->
      $answer_toggler.attr('checked', true)
      expect($form.sequential_question_admin_form('must_display_image_answers')).toBeTruthy()

    it "is false if the answer toggler is not checked", ->
      $answer_toggler.attr('checked', false)
      expect($form.sequential_question_admin_form('must_display_image_answers')).toBeFalsy()

  describe "('autoincrement_correct_position', answer_item)", ->
    $answer_item = null
    $correct_position_input = null

    beforeEach ->
      $answer_item = build_answer_item(correct_position: null)
    
    describe "where the form does not have answer items", ->
      it "should set correct position of the answer item to 1", ->
        $form.sequential_question_admin_form('autoincrement_correct_position', $answer_item)
        expect($answer_item.find('[role="SequentialQuestionAdminForm.CorrectPositionInput"]').val()).toEqual('1')
    
    describe "where the form has answer items", ->
      beforeEach ->
        $existing_answer_item = build_answer_item(correct_position: 1)
        $form.append($existing_answer_item)
        
      it "should set correct position of the answer item to the max correct position of the form + 1", ->
        $form.sequential_question_admin_form('autoincrement_correct_position', $answer_item)
        expect($answer_item.find('[role="SequentialQuestionAdminForm.CorrectPositionInput"]').val()).toEqual('2')
        
  describe "('max_correct_position')", ->
    describe "where the form does not have answer items", ->
      it "is negative infinity", ->
        expect($form.sequential_question_admin_form('max_correct_position')).toEqual(-Infinity)
        
    describe "where the form has answer items", ->
      beforeEach ->
        $form.append(build_answer_item(correct_position: 1))
        $form.append(build_answer_item(correct_position: 2))
        
      it "is the max correct position value of the answer items", ->
        expect($form.sequential_question_admin_form('max_correct_position')).toEqual(2)
        
    describe "where the form has answer items with non-numeric correct positions", ->
      beforeEach ->
        $form.append(build_answer_item(correct_position: 1))
        $form.append(build_answer_item(correct_position: null))

      it "excludes those non-numeric correct positions", ->
        expect($form.sequential_question_admin_form('max_correct_position')).toEqual(1)
  









