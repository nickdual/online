require 'spec_helper'

describe ExceptionWithDefaultMessage do
  module SomeModule
    class ShouldHaveNoMoreThanOneIceCreamPerWeek < ExceptionWithDefaultMessage
    end
  end

  describe "message" do
    it "should be the supplied message if given" do
      exception = SomeModule::ShouldHaveNoMoreThanOneIceCreamPerWeek.new('You ate 2 ice creams!')
      exception.message.should == 'You ate 2 ice creams!'
    end

    it "should be the default message if there is no supplied message" do
      exception = SomeModule::ShouldHaveNoMoreThanOneIceCreamPerWeek.new(:number_of_ice_creams => 3)
      exception.message.should == "Should have no more than one ice cream per week. Got {:number_of_ice_creams=>3}"
    end
  end
end

