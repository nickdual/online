require 'spec_helper'

describe InitializerWithAttributesHash do
  before do
    class Person < Struct.new(:first_name, :last_name)
      include InitializerWithAttributesHash
    end
  end

  after do
    Object.send :remove_const, :Person
  end

  describe "#initialize" do
    it "should allow the object to be constructed with incomplete attributes" do
      person = Person.new(first_name: 'George')
      person.first_name.should == 'George'
      person.last_name.should be_nil
    end

    it "should allow attributes to be passed in any order" do
      person = Person.new(last_name: 'Mendoza', first_name: 'George')
      person.first_name.should == 'George'
      person.last_name.should == 'Mendoza'
    end

    it "should raise an error if an non-existing attribute is passed" do
      lambda { Person.new(age: 32) }.should raise_error
    end
  end
end
