module InitializerWithAttributesHash
  def initialize(values = {})
    assign_attributes(values || {})
  end

  def assign_attributes(values)
    values.each do |k, v|
      send("#{k}=", v)
    end
  end
end
