class AdminResponder < ActionController::Responder
  include Responders::FlashResponder
  include Responders::HttpCacheResponder
end
