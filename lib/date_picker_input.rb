class DatePickerInput < Formtastic::Inputs::StringInput

  def input_html_options
    if object.send(input_name).blank?
      super.merge(:value => object.send(input_name))
    else
      super.merge(:value => object.send(input_name).strftime("%d/%m/%Y"))
    end
  end

  def wrapper_html_options
    super.merge(:class => "date")
  end

end