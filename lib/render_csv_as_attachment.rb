class RenderCsvAsAttachment
  def initialize(app)
    @app = app
  end

  def call(env)
    status, headers, response = @app.call(env)
    headers['Content-Disposition'] = 'attachment;' if headers["Content-Type"] && headers["Content-Type"].include?("text/csv")
    [status, headers, response]
  end
end