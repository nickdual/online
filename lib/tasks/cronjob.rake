namespace :cronjob do
  desc 'Activates learning plans for users based on LearningPlan#active_at'
  task :activate_learning_plans => :environment do
    LearningPlan.activate_plans
  end

  desc 'Reminds users of over due learning plans'
  task :overdue_learning_plans => :environment do
    LearningPlan.overdue_reminders
  end

  desc 'Reminds users of learning plans due soon'
  task :upcoming_learning_plans => :environment do
    [7, 3].each { |int| LearningPlan.due_in_x(int) }
  end
  
  desc 'Resets old tokens for services'
  task :reset_activation_tokens => :environment do
    Service.reset_old_tokens!
  end

  desc "Updated coordinators and group coordinators on user activities"
  task :update_coordinators_on_progress => :environment do
    Coordinator.coordinator_weekly_report
    Coordinator.group_coordinator_weekly_report
  end
end