namespace :cache do
  desc 'Clears and warms up the cache'
  task :reset => :environment do
    start_time = Time.now.to_i

    unless Rails.cache.is_a?(ActiveSupport::Cache::FileStore)
      puts "Clearing #{Rails.cache.stats.first.last["curr_items"]} records..."
    end

    Rails.cache.clear
    puts "Cache cleared, now setting cache..."

    [Coordinator,
    Employment,
    GroupJobCategory,
    GroupProgramAssessment,
    LearningRecord,
    LearningResource,
    Program,
    ServiceJobCategory,
    ServiceProgramAssessment,
    Service,
    User,
    Video,
    EmploymentLearningPlan,
    LearningPlan].each do |constant|
      constant.send(:all).each do |record|
        serializer = record.active_model_serializer.new record
        # This wil cache the JSON and the hash it's generated from
        serializer.to_json
      end

      puts "Reset #{constant}"
    end

    puts "Cache warmed up, time taken: #{Time.now.to_i - start_time} seconds"
  end
end