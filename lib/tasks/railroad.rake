namespace :railroad do
  desc "Generate basic model png in doc/models.png"
  task :generate do
    `bin/railroad -M | dot -Tpng > doc/models.png`
    puts "Generated doc/models.png"
  end
end