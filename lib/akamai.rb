require "net/http"

module Akamai
  class << self
    def pull_all_videos
      Video.where("description != ?", "original video").each do |video|
        Net::HTTP.start("streaming.acctv.co", 80) do |http|
          response = http.head("/#{video.file_uid}")
          puts "Video ##{video.id}: #{response['Cache-Control']}"
        end
      end
    end
  end
end