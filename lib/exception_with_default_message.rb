class ExceptionWithDefaultMessage < Exception
  attr_reader :options
  def initialize(options={})
    @options = options
    message = options.is_a?(String) ?
      options :
      "#{self.class.to_s.demodulize.underscore.humanize}. Got #{options.inspect}"
    super(message)
  end
end
