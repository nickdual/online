class BreadcrumbBuilder < BreadcrumbsOnRails::Breadcrumbs::Builder
  def render
    if @elements.any?
      @elements.last.options.merge! class: 'current'
      @elements.collect{|element| render_element element}.join('').html_safe
    end
  end

  def render_element(element)
    @context.link_to compute_name(element), compute_path(element), element.options
  end
end