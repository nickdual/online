#https://github.com/xaviershay/rspec-fire

#Using with ActiveRecord

#ActiveRecord methods defined implicitly from database columns are not
#detected. A workaround is to explicitly define the methods you are mocking:

#class User < ActiveRecord::Base
  ## Explicit column definitions for rspec-fire
  #def name; super; end
  #def email; super; end
#end

module HasExplicitColumnDefinitionsForRspecFire
  def has_explicit_column_definitions_for_rspec_fire
    # So we can run migrations from fresh
    if ActiveRecord::Base.connection.table_exists? self.table_name
      self.column_names.each do |column_name|
        define_method column_name do |*args|
          super(*args)
        end

        define_method "#{column_name}=" do |*args|
          super(*args)
        end
      end
    end
  end
end
