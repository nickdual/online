@javascript @backbone @create_service
Feature: Password reset
  In order to change my password
  As a user
  I want to be able to reset my password

  Scenario: I can reset my password
    Given I am logged in as a user
    Then I can reset my password on my account

  Scenario: I am reminded to set my password
    Given I am logged in as a user
    Then I should be prompted to change my password