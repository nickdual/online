@create_service @javascript @backbone
Feature: Learning records
  In order users to see records of completed learnings
  As a user
  I want to be able to see learning records

  Scenario: I can see my learning records
    Given I have existing learning records
    Then I should be able to view my learning records