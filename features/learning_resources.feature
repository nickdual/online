@create_service @backbone @javascript
Feature: Learning resource
  In order to test users on if they have understood the video
  As a user
  I want to download related documents and learning resources from a video

  Scenario: I can download a learning resource (and it's counted by the system)
    Given I have an existing learning resource available
    Then I should be able to download it