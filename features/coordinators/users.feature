@create_service @javascript @backbone
Feature: Coordinators
  In order to allow users to manage themselves, rather than relying on admin for everything
  As a coordinator
  I want to maintain other users within my service

  Background:
    Given I am logged in as a coordinator

  Scenario: I can view users
    Then I can view users

  Scenario: I can add a user to a service
    Then I can add a user to a service
    And I can remove a user from a service

  Scenario: I can add an existing user to a service
    Then I can add an existing user to a service

  Scenario: I can update a user
    Given I can add a user to a service
    Then I should be able to update a user