@create_service @javascript @backbone
Feature: Job titles
  In order to localize the job titles for my service
  As a (service) coordinator
  I want maintain job titles
  
  Background:
    Given I am logged in as a coordinator

  Scenario: I can edit existing job titles
    When I create a new job title
    Then I edit the first job title
    Then I should be able to see it in my employment settings
    When I remove the first job title

  Scenario: I can create new job titles
    When I create a new job title
    Then I should be able to see it in my employment settings