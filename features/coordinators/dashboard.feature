@create_service @javascript @backbone
Feature: Dashboard
  In order to keep up with the activity in the system
  As a coordinator
  I want overview the users and preform basic tasks

  Background:
    Given I am logged in as a coordinator

  Scenario: I can reset the activation token for the facility
    When I reset the service activation token
    Then I should see a new activation token