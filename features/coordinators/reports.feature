@wip @create_service @backbone @javascript
Feature: Reports
  In order to use ACC information in 3rd party software
  As a coordinator
  I want export information

  Background:
    Given I am logged in as a coordinator

  Scenario: I can view graphs
    Then I can view graphical information about the service

  Scenario: I can export everything about a service in a single file
    Given I export a spreadsheet of the service "Something"
    Then I should see a detailed view of the service
