@create_service @javascript @backbone
Feature: Searching
  In order to find users and for non-standard situations
  As a coordinator
  I want search through users based on a number of attributes

  Background:
    Given I am logged in as a coordinator
    And I am viewing the search form

  Scenario: I can search through users based on their basic details (including role and job title)
    Given I have an existing employment with name: "James Potter", job_title: "wizard"
    When I search by the role "Coordinator"
    Then I should see the matched user
    When I search by the job_title "wizard"
    Then I should see the matched user
  
  Scenario: I can search through users based on programs they have (or haven't) seen
    Given I have an existing employment who has viewed the program: "Fire Safety"
    When I search for users who have seen the program: "Fire Safety"
    Then I should see the matched user
    When I search for users who have not seen the program: "Fire Safety"
    Then I should not see any matched users

  Scenario: I can search through users based on learning plans they have (or haven't) completed
    Given I have an existing employment who has completed the learning plan: "OHS"
    When I search for users who completed the learning plan: "OHS"
    Then I should see the matched user
    When I search for users who have not completed the learning plan: "OHS"
    Then I should not see any matched users
    
  Scenario: I can search through users based on program assessments they have (or haven't) completed
    Given I have an existing employment who has completed the learning assessment: "Fire Safety"
    When I search for users who have been assessed for the program: "Fire Safety"
    Then I should see the matched user
    When I search for users who have not been assessed for the program: "Fire Safety"
    Then I should not see any matched users