Then /^I can import users with a csv file$/ do
  @service = Service.make!
  reset_mailer
  visit new_admin_service_user_import_path(@service)
  attach_file("CSV file", "spec/fixtures/users-complete.csv")
  click_button "Import Users"

  page.should have_content("Successfully received csv file, we'll email #{@admin.email} when it's complete")
  unread_emails_for(@admin.email).size.should == 1
  unread_emails_for(@admin.email).first.subject.should == "Completed user import for #{@service}"
end