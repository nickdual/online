Then /^I should automatically join the service$/ do
  @user.employments.count.should_not == 0
  @user.employments.last.service.should == Service.first
end

Given /^there is an existing user with email "([^"]*)"$/ do |email|
  user_attributes = {
    email: email,
    password: 123456,
    password_confirmation: 123456,
    first_name: "Harry",
    last_name: "Potter"
  }

  @user = User.create!(user_attributes) unless User.exists?(:email => email)
end

Given /^there is another user working for the service "([^"]*)"$/ do |service_name|
  @user = User.create!(:email => "ron@icdesign.com",
                      :password => "123456",
                      :password_confirmation => "123456",
                      :first_name => "Ron",
                      :last_name => "Weasley")

  @user.employments.create!(:service => Service.find_by_name(service_name))
end

Given /^I've invited a user/ do
  steps <<-CUCUMBER
    Given a service exists with name: "Carrington", license number: "11111111"
    When I follow "Carrington"
    And I follow "Add User"
    And I fill in the following:
      | First name | Harry |
      | Last name | Potter |
      | Email | harry@icdesign.com.au |
    And I press "Create User"
    Then I should see "User was successfully invited."
    And "harry@icdesign.com.au" should receive an email with subject "[ACC Online] You've joined Carrington"
  CUCUMBER
end

Given /^I am logged out$/ do
  click_link "Sign out" if all("a.sign_out").count > 0
end

Given /^I am logged in as a user/ do
  step 'there is an existing user with email "harry@icdesign.com.au"' unless User.where(email: "harry@icdesign.com.au").any?
  @service = Service.make!
  @service.employments.create!(user: @user)

  visit "/"
  fill_in "Email", with: "harry@icdesign.com.au"
  fill_in "Password", with: "123456"
  click_button "Sign In"
end

Given /^I have joined a service via the activation token$/ do
  step "I am logged in as a coordinator"
end

Given /^I have promoted a user to a coordinator/ do
  steps <<-CUCUMBER
    And I am on the admin page
    When I follow "Users"
    And I follow "Harry Potter"
    Then I should see "staff"
    When I follow "Modify Employment"
    And I choose "coordinator"
    And I press "Update Employment"
    Then I should see "Employment was successfully updated."
    And I should see "coordinator"
  CUCUMBER
end


Given /^I am logged in as a group coordinator$/ do
  step 'there is an existing user with email "harry@icdesign.com.au"'
  step 'I am a coordinator of the group "Bacon"'
  step "I am logged in as a user"
end

Given /^I am logged in as a coordinator$/ do
  step 'there is an existing user with email "harry@icdesign.com.au"'
  step 'I am a coordinator of the service "Milk"'
  step "I am logged in as a user"
end

Given /^there is a coordinator and user employed at service Something/ do
  steps <<-CUCUMBER
    Given I have joined a service via the activation token
    And I am on the admin page
    When I follow "Users"
    And I follow "Harry Potter"
    And I follow "Modify Employment"
    And I choose "coordinator"
    And I press "Update Employment"
  CUCUMBER
end

def visit_settings
  click_link "Settings"
end

Then /^I can reset my password on my account$/ do
  reset_mailer
  click_link "Log Out"

  sleep(0.5)
  click_link "Forgot Password"
  fill_in "Email", with: @user.email
  click_button "Send me reset password instructions"

  page.should have_content("You will receive an email with instructions about how to reset your password in a few minutes.")
  unread_emails_for(@user.email).size.should == 1

  open_email(@user.email)
  visit_in_email("click here")
  fill_in "Password", with: "123456"
  fill_in "Password confirmation", with: "123456"
  click_button "Set Password and Sign in"

  sleep(0.5)
  page.should have_content("Welcome, #{@user.name}")
  @user.reload.activation_code_set_at.should == nil
end

Then /^I should be prompted to change my password$/ do
  @user.update_attributes(activation_code_set_at: Time.zone.now)
  visit root_path

  find('a[role="change-password"]').click
  sleep(0.5)

  fill_in "password", with: "123456"
  fill_in "password_confirmation", with: "123456"
  click_button "Save"

  page.should have_content("Successfully updated profile")

  find('.noty_message').click if find('.noty_message')
  click_link "Home"
  page.should_not have_content("Your password has been set by an activation code, we recommend changing your password")

  @user.reload.activation_code_set_at.should == nil
end