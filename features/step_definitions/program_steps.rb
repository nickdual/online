def visit_first_program
  click_link "Programs"
  click_link @program.title
end

Then /^I should be able to watch a program and receive a learning record$/ do
  visit_first_program
  find("span.sublimevideo-View:last").click
  sleep(6)
  page.should have_content("Successfully created learning record")
end

Given /^I have an existing program with name: "([^"]*)"$/ do |name|
  unless Program.find_by_title(name)
    steps <<-CUCUMBER
      When I follow "Programs"
      And I follow "New Program"
      And I fill in the following:
        | Title | #{name} |
        | Code | 12345 |
    CUCUMBER

    if Capybara.current_driver.to_s == "selenium"
      page.execute_script('jQuery("#program_description_editor").html("testing")')
    else
      fill_in "program_description", :with => "testing"
    end

    steps <<-CUCUMBER
      And I press "Create Program"
      Then I should see "Program was successfully created"
    CUCUMBER
  end
end

Given /^I have an existing intro program with name: "([^"]*)"$/ do |name|
  steps <<-CUCUMBER
    When I follow "Programs"
    And I follow "New Program"
    And I fill in the following:
      | Title | #{name} |
      | Code | 123456 |
      | Description | Something else |
    And I select "introduction" from "Classification"
    And I press "Create Program"
    Then I should see "Program was successfully created"
  CUCUMBER
end

Given /^all programs have been processed$/ do
  Program.all.each do |program| 
    program.update_attribute(:processing, false)
    program.create_thumb(:image => File.open("#{Rails.root}/spec/fixtures/cat.jpg"))
    program.videos.create!(:file => File.open("#{Rails.root}/public/meow.webm"), :description => "browser_webm", :zencoder_id => 11)
  end
end

Given /^I have an existing intro and closer for my program$/ do
  intro = Program.make!(:introduction, :processing => false)
  intro.thumb.create!(:image => File.open("#{Rails.root}/spec/fixtures/cat.jpg"))
  intro.videos.create!(:file => File.open("#{Rails.root}/public/meow.webm"), :description => "browser_webm", :zencoder_id => 11)

  closer = Program.make!(:closer, :processing => false)
  closer.create_thumb(:image => File.open("#{Rails.root}/spec/fixtures/cat.jpg"))
  closer.videos.create!(:file => File.open("#{Rails.root}/public/meow.webm"), :description => "browser_webm", :zencoder_id => 11)
  Video.any_instance.stub(:url).and_return("/meow.webm")

  ServiceProgram.first.program_lists.create!(:program => intro)
  ServiceProgram.first.program_lists.create!(:program => closer)
end

Given /^I have received and processed files from Zencoder$/ do
  steps <<-CUCUMBER
    Given I have an existing program with name: "Something"
    When I follow "Something"
    Then I should see "We're still processing this program - we'll send you an email when it's finished"
    When I send mock zencoder updates
    Then "thomas@icdesign.com.au" should receive an email with subject "A new program has been processed"
    Given I am on the admin page
    When I follow "Programs"
    And I follow "Something"
    Then I should see "This program has been processed by Zencoder"
  CUCUMBER
end

Given /^there is a program for the service$/ do
  @program_category = ProgramCategory.create!(name: "Something")
  @program = Program.make!(:processed, title: "Dementia: Meaningful Activities", category: @program_category)
  @program.videos.create!(description: "browser_webm_sd", file: File.open("#{Rails.root}/public/meow.webm"))
  Video.any_instance.stub(:url).and_return("/meow.webm")
  @package = Package.make!(programs: [@program])
  Service.all.each { |service| service.update_attributes(packages: [@package]) }
end

Then /^I should be able to see the required and completed assessment status$/ do
  assessment = {essentials_required: false, extension_required: true, evidence_required: true, evidence_completed: false, evidence_overdue: true}
  User.any_instance.stub(:assessments).and_return(assessment)

  visit_first_program
  find("#assessmentStatus li:nth-child(1)").should have_css(".badge-default")
  find("#assessmentStatus li:nth-child(2)").should have_css(".badge-pending")
  find("#assessmentStatus li:nth-child(3)").should have_css(".badge-overdue")
end