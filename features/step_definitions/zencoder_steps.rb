When /^I send mock zencoder updates$/ do
  output = {:output => {"state"=>"finished", "label"=>"browser_webm", "url"=>"http://accprogramstaging.s3.amazonaws.com/2011/09/07/07/32/09/595/meow.mp4", "id"=>7287488}, "job"=>{"test"=>true, "state"=>"finished", "id"=>6424425}}
  10.times { page.driver.post "/zencoder/#{Program.first.id}", output }
end