def visit_coordinator_dashboard
  # Waiting for the notification to disappear
  sleep(0.5)

  if all(".noty_message").count > 0
    find('.noty_message').click
    sleep(0.5)
  end

  click_link "Coordinator"

  sleep(1)

  if User.first.coordinators.any?
    find("#groupslist li").click
  else
    find("#serviceslist li").click
  end
end

Given /^I have an existing coordinator "([^"]*)"$/ do |arg1|
  steps <<-CUCUMBER
    When I follow "New Coordinator"
    And I fill in the following:
      | First name | Harry |
      | Last name | Potter |
      | Email | harry@icdesign.com.au |
    And I press "Create Coordinator"
    Then I should see "Coordinator was successfully invited."
  CUCUMBER
end

Given /^I am a coordinator of the service "([^"]*)"$/ do |service_name|
  @service = Service.make!(name: service_name)
  @service.employments.create!(user: User.find_by_email('harry@icdesign.com.au'), role: "coordinator")
end

Given /^I am a coordinator of the group "([^"]*)"$/ do |group_name|
  @group = Group.create!(:name => group_name)
  @service = Service.make!(:group => @group)
  @group.coordinators.create!(:user => User.find_by_email('harry@icdesign.com.au'))
end

Given /^I export a spreadsheet of the service "([^"]*)"$/ do |service_name|
  visit_coordinator_dashboard
  click_link "Reports"
  click_link "Export all service information as an excel spreadsheet"
end

Then /^I should see a detailed view of the service$/ do
  page.should have_content("Export all service information as an excel spreadsheet")
end

When /^I export a spreadsheet of my services for my group$/ do
  visit_coordinator_dashboard
  click_link "Reports"
  click_link "Export all group information (including services) as an excel spreadsheet"
end

Then /^I should be able to mark off a learning record$/ do
  visit_coordinator_dashboard
  find('a[role="view"]').click
  find('a[role="user_assessments"]').click

  find('a[role="assess"]').click
  page.should have_content("Successfully assessed learning record")
end

Then /^I should see a detailed view of the services$/ do
  # TODO: Find a way to test a file download prompt with JS
  page.should have_content("Export all group information (including services) as an excel spreadsheet")
end