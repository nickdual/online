Given /^I have an existing learning resource available$/ do
  step "I am logged in as a user"
  step "there is a program for the service"

  @learning_resource = LearningResource.make!(program: @program)
end

Then /^I should be able to download it$/ do
  visit_first_program
  click_link @learning_resource.name

  sleep(2)
  LearningResourceDownload.count.should == 1
end