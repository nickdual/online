Given /^I create a service with name: "([^"]*)"(?:, license number: "([^"]*)")?$/ do |name, activation_token|
  steps <<-CUCUMBER
    And I follow "New Service"
    And I fill in the following:
      | Name             | #{name}         |
      | Number of beds   | 20              |
      | Number of hours  | 200             |
      | Street address 1 | Suite 1         |
      | Street address 2 | 1 Awesome Alley |
      | Suburb           | Someville       |
      | Postcode         | 2000            |
    And I select "Australia" from "Country"
    And I select "New South Wales" from "State"
    And I add "Package" from "Packages" multiselect
    And I press "Create Service"
    Then I should see "Service was successfully created"
  CUCUMBER

  if activation_token.present?
    Service.first.update_attribute(:activation_token, activation_token)
  end
end

Given /^I have an existing service with name: "([^"]*)" under a group$/ do |name|
  steps <<-CUCUMBER
    Given I have an existing group
    Given I have an existing package with name: "Package"
    When I follow "Groups"
    And I follow "Best Group"
    And I create a service with name: "#{name}"
  CUCUMBER
end

Given /^a service exists with name: "([^"]*)"(?:, license number: "([^"]*)")?$/ do |name, activation_token|
  unless Package.exists?
    Given 'I have an existing package with name: "Package"'
  end

  When 'I follow "Services"'
  And %Q(I create a service with name: "#{name}", license number: "#{activation_token}")
end

When /^I reset the service activation token$/ do
  visit_coordinator_dashboard
  find('a[role="reset-token"]').click
  page.should have_content("Successfully reset activation token")
end

Then /^I should see a new activation token$/ do
  service =  Service.where(name: "Milk").first
  page.should have_content(service.activation_token)
end

def wait_and_close
  sleep(1)
  find('a#fancybox-close').click
  sleep(1)
end

Then /^I can view graphical information about the service$/ do
  visit_coordinator_dashboard
  click_link "Reports"
  sleep(0.5)

  click_link "Learning Records created in the past 24 hours"
  wait_and_close

  click_link "Learning Records created in the past 7 days"
  wait_and_close

  click_link "Learning Records created in the past 30 days"
  wait_and_close

  click_link "Learning Plans completed in the past 30 days"
  wait_and_close

  click_link "Learning Plans pending vs. overdue"
  wait_and_close
end

Given /^there is an existing service$/ do
  @service = Service.make!
end