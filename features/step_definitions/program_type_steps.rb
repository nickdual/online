Given /^a program type exists with name: "([^"]*)"$/ do |name|
  ProgramType.create(name: name)
end