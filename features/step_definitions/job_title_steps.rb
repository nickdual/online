def visit_job_title
  visit_coordinator_dashboard
  click_link "Jobs"
end

# Coordinator related
When /^I edit the first job title$/ do
  visit_job_title
  find('a[role="edit"]').click

  within 'form[role="update_job"]' do
    fill_in "name", with: "Toilet Cleaner"
    click_button "Update"
  end

  page.should have_content("Successfully updated job title")
  @job_title = Group::JobTitle.last
end

When /^I remove the first job title$/ do
  visit_job_title
  sleep(0.5)

  find('a[role="remove-job"]').click
  page.should have_content("Successfully removed job title")
end

When /^I create a new job title$/ do
  visit_job_title
  click_link "New Job Title"

  within '#create_job' do
    fill_in 'name', with: "Toilet Cleaner"
    click_button "Add"
  end

  page.should have_content("Toilet Cleaner")
  page.should have_content("Successfully created job title")
  Group::JobTitle.count > 1 ? (@job_title = Group::JobTitle.last) : (@job_title = Service::JobTitle.last)
end

Then /^I should be able to see it in my employment settings$/ do
  visit_settings

  sleep(0.5)
  find('a[role="edit"]').click
  page.should have_content(@job_title.to_s)
end