# TODO: Refactor to take care of the multiple employments a little neater
Given /^I have an existing employment with name: "([^"]*)", job_title: "([^"]*)"$/ do |name, title|
  @created_user = User.make!(name: name)
  job = Service::JobTitle.create!(name: title, service: Service.first)

  Employment.make!(user: @created_user, service: Service.first, job_title_id: job.id, role: "coordinator")
  Employment.make!(user: @created_user, service: Service.last, job_title_id: job.id, role: "coordinator")
end

Given /^I have an existing employment with name: "([^"]*)"$/ do |name|
  @created_user = User.make!(name: name)
  @employment1 = Employment.make!(user: @created_user, service: Service.first)
  @employment2 = Employment.make!(user: @created_user, service: Service.last)
end

Given /^I have an existing employment who has viewed the program: "([^"]*)"$/ do |title|
  step "I have an existing employment with name: \"James Potter\""
  @program = Program.make!(title: title, processing: false)
  @package = Package.make!(programs: [@program], service_ids: Service.all.map(&:id))
  LearningRecord.make!(program: @program, employment: @employment1)
  LearningRecord.make!(program: @program, employment: @employment2)
end

Given /^I have an existing employment who has completed the learning plan: "([^"]*)"$/ do |name|
  step "I have an existing employment with name: \"James Potter\""
  program = Program.make!
  LearningPlan.make!(name: name, service: Service.first, programs: [program], employments: [@employment1], active_at: 2.days.ago)
  LearningPlan.make!(name: name, service: Service.last, programs: [program], employments: [@employment2], active_at: 2.days.ago)

  EmploymentLearningPlan.all.each { |e| e.complete! }
end

Given /^I have an existing employment who has completed the learning assessment: "([^"]*)"$/ do |arg1|
  step "I have an existing employment who has viewed the program: \"Fire Safety\""

  Employment.all.each do |e|
    ProgramAssessment.create!(program: Program.first, employment: e, coordinator: User.first)
  end
end

Then /^I can view users$/ do
  step "I can add a user to a service"
  visit_coordinator_dashboard
  click_link "Users"

  all("#usersList > li").count.should_not == 0
end

Then /^I can add a user to a service$/ do
  visit_coordinator_dashboard
  click_link "Users"
  click_link "new_user"

  fill_in "first_name", with: "Thomas"
  fill_in "last_name", with: "Sinclair"
  fill_in "email", with: "thomas@icdesign.com.au"
  click_button "Add"

  page.should have_content("Successfully added user")
  unread_emails_for("thomas@icdesign.com.au").size.should == 1
  unread_emails_for("thomas@icdesign.com.au").first.subject.should == "Your Account has been Created for Milk"
  Employment.active.count.should == 3
end

Then /^I can add an existing user to a service$/ do
  user = User.make!
  reset_mailer

  visit_coordinator_dashboard
  click_link "Users"
  click_link "new_user"

  fill_in "first_name", with: user.first_name
  fill_in "last_name", with: user.last_name
  fill_in "email", with: user.email
  click_button "Add"

  page.should have_content("Successfully added user")
  unread_emails_for(user.email).size.should == 1
  unread_emails_for(user.email).first.subject.should == "Your Account is now Active with Milk"
end

Then /^I can remove a user from a service$/ do
  visit_coordinator_dashboard
  click_link "Users"
  find('a[role="remove"]').click

  page.driver.browser.switch_to.alert.accept
  page.should have_content("Successfully removed user from service")
  Employment.active.count.should == 2
end


Then /^I should be able to update a user$/ do
  click_link "new_user"
  find('a[role="view"]').click
  click_link "Edit"

  select "Coordinator", from: "role"
  fill_in "employee_number", with: "123456"
  click_button "Save"
  page.should have_content("Successfully updated user")
end

Given /^the user has watched a program$/ do
  @program = Program.make!(title: "Fire Safety", processing: false)
  @package = Package.make!(programs: [@program], service_ids: Service.all.map(&:id))
  LearningRecord.make!(program: @program, employment: Employment.first)
  LearningRecord.make!(program: @program, employment: Employment.last)
end