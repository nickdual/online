Given /^I am viewing the search form$/ do
  visit_coordinator_dashboard
  click_link "Reports"
  find('[role="search"]').click
end

When /^I search for "([^"]*)"$/ do |search_term|
  fill_in "user_first_name_or_user_last_name_or_user_email_cont", with: search_term
  click_button "Search"
end

When /^I search by the role "([^"]*)"$/ do |role|
  step "I am viewing the search form"
  select(role, from: "role_eq")
  click_button "Search"
end

When /^I search by the job_title "([^"]*)"$/ do |job_title|
  step "I am viewing the search form"
  select(job_title, from: "job_title_id_eq")
  click_button "Search"
end

When /^I search for users who completed the learning plan: "([^"]*)"$/ do |name|
  step "I am viewing the search form"
  select(name, from: "completed_employment_learning_plans_learning_plan_id_eq")
  click_button "Search"
end

When /^I search for users who have not completed the learning plan: "([^"]*)"$/ do |name|
  step "I am viewing the search form"
  select(name, from: "completed_employment_learning_plans_learning_plan_id_not_eq")
  click_button "Search"
end

When /^I search for users who have seen the program: "([^"]*)"$/ do |title|
  step "I am viewing the search form"
  select(title, from: "user_learning_records_program_id_eq")
  click_button "Search"
end

When /^I search for users who have not seen the program: "([^"]*)"$/ do |title|
  step "I am viewing the search form"
  select(title, from: "user_learning_records_program_id_not_eq")
  click_button "Search"
end

When /^I search for users who have been assessed for the program: "([^"]*)"$/ do |title|
  step "I am viewing the search form"
  select(title, from: "program_assessments_program_id_eq")
  click_button "Search"
end

When /^I search for users who have not been assessed for the program: "([^"]*)"$/ do |title|
  step "I am viewing the search form"
  select(title, from: "program_assessments_program_id_not_eq")
  click_button "Search"
end

# Results
Then /^I should see the matched user$/ do
  within "#searchuserslist" do
    page.should have_content(@created_user.name)
  end
end

Then /^I should not see any matched users$/ do
  within "#searchuserslist" do
    page.should_not have_content(@created_user.name)
  end
end