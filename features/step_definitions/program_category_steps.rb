Given /^a program category exists with name: "([^"]*)"$/ do |name|
  steps <<-CUCUMBER
    When I follow "Program Categories"
    And I follow "New Program Category"
    And I fill in the following:
      | Name | #{name} |
    And I press "Create Program category"
    Then I should see "Program category was successfully created."
  CUCUMBER
end

