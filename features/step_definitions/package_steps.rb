Given /^I have an existing package with name: "([^"]*)"$/ do |name|
  steps <<-CUCUMBER
    Given I have an existing program with name: "Something"
    And all programs have been processed
    When I follow "Packages"
    And I follow "New Package"
    And I fill in the following:
      | Name | #{name} |
  CUCUMBER

  if Capybara.current_driver.to_s == "selenium"
    And 'I drag "the first program from the old box" into "the current box"'
  else
    And 'I check "Something"'
  end

  steps <<-CUCUMBER
    And I press "Create Package"
    Then I should see "Package was successfully created"
  CUCUMBER
end

When /^I drag "([^"]*)" into "([^"]*)"$/ do |object, destination|
  drag_object = find(selector_for(object))
  drop_object = find(selector_for(destination))
  drag_object.drag_to(drop_object)
end

Given /^I have an existing package with a introduction assigned to the package program$/ do
  steps <<-CUCUMBER
    Given I have an existing package with name: "Package"
    And I have an existing intro program with name: "Intro"
    When I follow "Packages"
    And I follow "Package"
    And I follow "Edit" within package programs list
    And I drag "the first program from the intro box" into "the current box"
    And I press "Update Package Program"
    Then I should see "Package program was successfully updated"
    And I should see "1" within package programs list
  CUCUMBER
end