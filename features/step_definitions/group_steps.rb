Given /^I have an existing group$/ do
  steps <<-CUCUMBER
    When I follow "Groups"
    And I follow "New Group"
    And I fill in the following:
      | Name | Best Group | 
      | Postcode | 2000 |
      | Suburb | Sydney |
    And I select "Australia" from "Country"
    And I select "New South Wales" from "State"
  CUCUMBER

  if Package.exists?
    And 'I add "Package" from "Packages" multiselect'
  end
    
  steps <<-CUCUMBER
    And I press "Create Group"
    Then I should see "Group was successfully created."
  CUCUMBER
end

Then /^I can view graphical information about the group$/ do
  visit_coordinator_dashboard
  click_link "Reports"
  sleep(1)

  click_link "Number of Users by Service"
  wait_and_close

  click_link "Learning Records created in the past 24 hours"
  wait_and_close

  click_link "Learning Records created in the past 30 days"
  wait_and_close

  click_link "Learning Plans pending vs. overdue"
  wait_and_close

  click_link "Learning Plans completed in the last 30 days"
  wait_and_close
end

Then /^I should be able to leave a group$/ do
  visit_settings

  within "#groupslist" do
    find('[role="leave"]').click
  end

  page.driver.browser.switch_to.alert.accept
  page.should have_content("Successfully left #{Group.first.name}")
  Coordinator.count.should == 0
end