Given /^I have existing learning records$/ do
  step "I am logged in as a user"
  program = Program.make!(:processed)
  Employment.first.learning_records.create!(program: program)
end

Then /^I should be able to view my learning records$/ do
  sleep(0.5)

  click_link "My Learning"
  click_button "Records"

  sleep(0.5)
  all("#learningrecords li.record").count.should == 1
end