module HtmlSelectorsHelpers
  # Maps a name to a selector. Used primarily by the
  #
  #   When /^(.+) within (.+)$/ do |step, scope|
  #
  # step definitions in web_steps.rb
  #
  def selector_for(locator)
    case locator

    when "the page"
      "html > body"
    when "the first row"
      "body table tr:nth-child(2)"
    when "the second row in the list"
      "body dl dd:nth-child(2)"
    when "the form"
      "body form.simple_form"
    when "the sidebar"
      "body aside"
    when "employments"
      "body table.employments"
    when "the current box"
      "body ul#selected_programs"
    when "the first program from the old box"
      "body ul#notselected_programs li:first"
    when "the old box"
      "body ul#notselected_programs"
    when "the first program from the current box"
      "body ul#selected_programs li:first"
    when "the first program from the intro box"
      "body ul#introductions li:first"
    when "the introductions box"
      "body ul#introductions"
    when "package programs list"
      "body table.package_programs"
    when "the program playlists"
      "body table tr:nth-child(1) td:nth-child(3)"
    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #  when /^the (notice|error|info) flash$/
    #    ".flash.#{$1}"

    # You can also return an array to use a different selector
    # type, like:
    #
    #  when /the header/
    #    [:xpath, "//header"]

    # This allows you to provide a quoted selector as the scope
    # for "within" steps as was previously the default for the
    # web steps:
    when /^"(.+)"$/
      $1

    else
      raise "Can't find mapping from \"#{locator}\" to a selector.\n" +
        "Now, go and add a mapping in #{__FILE__}"
    end
  end
end

World(HtmlSelectorsHelpers)
