@mocked @create_service @javascript @backbone
Feature: Programs
  In order to gain knowledge on aged care practices
  As a user
  I want to view programs assigned to me

  Background:
    Given I am logged in as a user
    And there is a program for the service

  Scenario: I can view a program assigned to me
    Then I should be able to watch a program and receive a learning record

  Scenario: I can view the assessment status for a program
    Then I should be able to see the required and completed assessment status