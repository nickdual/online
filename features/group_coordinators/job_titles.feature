@create_service @javascript @backbone
Feature: Job titles
  In order to localize the job titles for my group
  As a group coordinator
  I want maintain job titles
  
  Background:
    Given I am logged in as a group coordinator

  Scenario: I can edit existing job titles
    When I create a new job title
    Then I edit the first job title
    And I remove the first job title
