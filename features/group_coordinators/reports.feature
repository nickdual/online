@create_service @backbone @javascript
Feature: Reports
  In order to use ACC data in a 3rd party
  As a group coordinator
  I want export acc data via spreadsheets

  Background:
    Given I am logged in as a group coordinator

  Scenario: I can view graphs
    Then I can view graphical information about the group

  # To be fixed in #55687666
  @wip
  Scenario: I can export everything a group's services in a single file
    When I export a spreadsheet of my services for my group
    Then I should see a detailed view of the services
