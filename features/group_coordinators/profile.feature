@backbone @javascript @create_service
Feature: Profile
  In order to allow group coordinators to control higher level settings
  As a group coordinator
  I want leave a group

  Scenario: A group coordinator can leave a group
    Given I am logged in as a group coordinator
    Then I should be able to leave a group