# Cronjobs for app

## Daily, 9pm UTC
rake cronjob:overdue_learning_plans
rake cronjob:upcoming_learning_plans
rake cronjob:reset_activation_tokens
rake cronjob:activate_learning_plans

## Daily, 2:30 UTC (but only on Friday)
rake cronjob:update_coordinators_on_progress