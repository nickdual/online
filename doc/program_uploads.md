# Program Uploads cheatsheets

## Create a new program with a video
    program_src = "PROGRAM_SRC_IN_S3"
    Program.create!(title: program_src, code: program_src, description: "Automated video uploaded by console.", build_video_from_s3: program_src, created_by_id: 2)

## Update an existing program with a new video
    program_irc = "PROGRAM_SRC_IN_S3"
    program_slug = "PROGRAM_SLUG"
    p = Program.find_using_slug(program_slug)

    # Hides the current program from users
    p.update_attributes(processing: true)

    # Make sure the old original video doesn't delete the new upload
    p.videos.where(description: "original video").first.file_uid == program_src
    # If this returns true:
    p.videos.where(description: "original video").first.update_attributes(file_uid: "foo")

    p.videos.each { |v| v.destroy }
    p.update_attributes(build_video_from_s3: program_src)
    p.process_original_video