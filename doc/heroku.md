# Heroku cheatsheets

We're currently rolling Heroku for our four servers:
* [online.acctv.co](http://online.acctv.co) (production)
* [demo.acctv.co](demo.acctv.co) (demo server for sales guys)
* [staging.acctv.co](staging.acctv.co) (staging server for acceptance testing)
* [wireframes.acctv.co](wireframes.acctv.co) (wireframes server for front end acceptance testing)

Online and demo should be running the same codebase at all times, both demo and online are in production environment whilst staging and testing have it's own environment.

### Git pushing
Used for accessing various heroku instances:

    git remote add production git@heroku.com:acc-online.git
    git remote add demo git@heroku.com:acc-online-demo.git
    git remote add staging git@heroku.com:acc-online-staging.git

### Create a new backup

    heroku pgbackups:capture --remote production

### Copy production db to staging

    heroku pg:reset HEROKU_POSTGRESQL_SILVER_URL --remote staging
    heroku pgbackups:restore DATABASE `heroku pgbackups:url --remote production` --remote staging

### Copy production db to local
Assuming you're in the root directory:

    wget `heroku pgbackups:url --remote production` -O latest.dump
    rake db:drop # Drops local database, if postgres complains about a connection still you may need to manually kill that pid
    rake db:create # Creates an empty database
    pg_restore -cOd acc-online_development latest.dump
