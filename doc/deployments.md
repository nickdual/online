# Deployments

## Before you deploy to production

* Make sure CI all passes
* Make sure CI all passes
* Push to staging and ensure stakeholders have approved the latest changes

## Asset_sync and you
We're using https://github.com/rumblelabs/asset_sync for our images, this renders and uploads our assets to s3 bucket to serve directly from our cdn.

  rake assets:precompile

Each time you deploy - it's only going to upload the changes.
Make sure ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_ACCESS_KEY'], ENV['FOG_DIRECTORY'] AND ENV['FOG_PROVIDER'] are defined for the s3 bucket.

## Final staging checks

Ui checks:
* User can successfully login
* User can successfully create a learning record (if on demo)
* User can successfully update profile
* Coordinator can successfully create a group session (with auto-complete)
* User can refresh the page on a few different pages with success

## Other important notes

If any database migrations:
* Make sure they don't damage any data / we're aware of what they do
* Force backup the database
* Use the maintenance page and verify data before enabling again