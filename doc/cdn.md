# Amazon s3 buckets, Akamai and application assets

### There are two main forms of assets in ACC Online:
* Videos for programs
* Images used for services / groups

## Videos
Videos are the core functionality in our app, users look at videos to complete programs, we send the original raws to Zencoder and from there add 5 - 6 videos to our s3 bucket.
Stored in Dragonfly using pretty timestamps.

## Images
As of this writing, these assets are only for services and groups, stored in Dragonfly using pretty timestamps. Any resizing / editing of these images is done on the Heroku side with Imagemagick and stored under Rack::Cache.

## S3 storage
All assets are stored on s3 on acc_production and acc_staging (for each of the environments), demo serves production so it has access to a copy of the production programs.

## Akamai CDN
There are two CDNs for the application:
* cdn.acctv.co
Used to server static assets such as js / css / images, this is simply a proxy to the web app, think of it as adding an extra layer of caching to our application for these files.

* streaming.acctv.co
Used for serving our videos directly from the s3 bucket (rather than routing through the Heroku application) - generally once our videos are imported from Zencoder we keep them the same forever so they are pretty static.