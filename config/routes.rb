AccOnline::Application.routes.draw do
  devise_for :admins

  devise_for :users, controllers: {
    sessions:       "users/sessions",
    passwords:      "users/passwords",
    registrations:  "users/registrations"
  }

  match "/admin", :to => "admin/dashboard#index"
  namespace :admin do
    resources :admins, :program_categories
    resource :profile

    resources :packages do
      resources :package_programs
    end

    resources :services do
      member do
        put "disable"
        put "enable"
        put "reset_token"
      end

      resources :job_titles, :controller => "services/job_titles"
      resources :programs, :controller => "services/programs"
      resources :users, :controller => "services/users", :only => [:index]
      resources :user_imports

      resources :questionable, controller: "services/questionable"
      resources :learning_plans, :controller => "services/learning_plans", :only => [:index, :show]
      resources :employments do
        member do
          put "end"
        end
      end
    end

    resources :users do
      resources :employments
      resources :learning_plans, only: [:index, :show], controller: 'users/employment_learning_plans'
      resources :activities, controller: "users/activities", only: [:index]
      resource :passwords do
        member do
          put "reset"
        end
      end
    end

    resources :programs do
      resources :assessments, controller: "programs/assessments", as: :program_assessments

      resources :learning_resources do
        member do
          get :download
        end
      end
    end

    resources :program_assessment_sections do
      resources :fill_gap_questions,
        controller: "programs/assessments/sections/fill_gap_questions",
        as: :program_assessment_fill_gap_questions

      resources :true_or_false_questions,
        controller: "programs/assessments/sections/true_or_false_questions",
        as: :program_assessment_true_or_false_questions

      resources :multiple_choice_questions,
        controller: "programs/assessments/sections/multiple_choice_questions",
        as: :program_assessment_multiple_choice_questions

      resources :sequential_questions,
        controller: "programs/assessments/sections/sequential_questions",
        as: :program_assessment_sequential_questions

      resources :matching_task_questions,
        controller: "programs/assessments/sections/matching_task_questions",
        as: :program_assessment_matching_task_questions
    end

    resources :groups do
      resources :services
      resources :job_titles, controller: "groups/job_titles"
      resources :users, :controller => "groups/users"
      resources :coordinators, :controller => "groups/coordinators"
      resources :programs, :controller => "groups/programs"

      member do
        put "disable"
        put "enable"
      end
    end

    resources :style_guides, only: ['show']
  end

  namespace :api do
    resources :groups do
      resources :coordinators, :controller => "groups/coordinators"
      resources :job_titles, :controller => "groups/job_titles"
      resources :services
      resources :charts, controller: "groups/charts"

      resources :program_assessments, controller: "groups/program_assessments" do
        collection do
          post "update_all"
        end
      end
    end

    resources :role do
      resources :services
    end

    resources :services do
      member do
        put "reset_token"
        put "export_services_information"
        put "export_groups_services_information"
      end

      resources :program_assessments, controller: "services/program_assessments" do
        collection do
          post "update_all"
        end
      end

      resources :charts, controller: "services/charts"
      resources :employments, :controller => "services/employments" do
        collection do
          get "advanced"
          post "advanced"
        end
      end

      resources :job_titles, :controller => "services/job_titles"
      resources :users, :programs
      resources :learning_plans, :controller => "services/learning_plans"
      resources :user_imports, :controller => "services/user_imports"
      resources :group_sessions, :controller => "services/group_sessions"

    end

    resources :employment_learning_plans do
      resources :programs_pending, controller: "employment_learning_plans/programs_pending"
      resources :programs_completed, controller: "employment_learning_plans/programs_completed"
    end

    resources :employments do
      resources :learning_plans, :controller => "services/employment_learning_plans"
      resources :activities, controller: "employments/activities"
      resources :program_assessments, controller: "employments/program_assessments"
    end

    resources :programs do
      resources :assessments, controller: "programs/assessments"
      resources :learning_records, :controller => "programs/learning_records"
      resources :learning_resources, :controller => "programs/learning_resources"
      resources :related, controller: "programs/related"
      resources :videos, controller: "programs/videos"
    end

    resources :program_assessments do
      resources :sections, controller: "programs/assessments/sections"
      resources :results, controller: "programs/assessments/results"
    end

    resources :program_assessment_sections do
      resources :questions, controller: "programs/assessments/sections/questions"
    end

    resources :learning_plans do
      resources :programs, controller: "learning_plans/programs"
      resources :employments, controller: "learning_plans/employments"
      resources :employment_learning_plans, controller: "learning_plans/employment_learning_plans"
    end

    resource :profile do
      resource :password, :controller => "profiles/password"
      resources :learning_plans, :controller => "profiles/employment_learning_plans"
      resources :coordinators, controller: "profiles/coordinators"
      resources :activities, controller: "profiles/activities"
      resources :assessments, controller: "profiles/assessments"

      resources :employments, :controller => "profiles/employments" do
        member do
          put "finish"
        end
      end
    end
  end

  namespace :webhooks do
    # For testing the connection when configuring in Mandrill
    get "mandrill_event_bulk_update", to: "mandrill_event_bulk_updates#create"

    resource :mandrill_event_bulk_update, only: [:create]

    get "/zendesk/authorize", to: "zendesk#authorize"
    post "/zencoder/:id", :to => "zencoder#update"
  end

  # Backbone views
  get "/dashboard", to: "welcome#index"
  get "/programs", to: "welcome#index"
  get "/programs/*views", to: "welcome#index"
  get "/my-learning/*views", to: "welcome#index"
  get "/my-learning", to: "welcome#index"
  get "/coordinator", to: "welcome#index"
  get "/coordinator/*views", to: "welcome#index"
  get "/settings", to: "welcome#index"

  get "mailers/:id", :to => "mailers#index"
  get "mailers", :to => "mailers#index"

  # Wireframes
  namespace :admin do
    get "wireframes/:id", to: "wireframes#index"
    get "wireframes", to: "wireframes#index"
  end

  # TODO: Merge the two wireframes directories together
  get "wf/:id", :to => "wf#index"
  get "wf", :to => "wf#index"

  get "wireframes/:id", :to => "wireframes#index"
  get "wireframes", :to => "wireframes#index"

  root :to => "welcome#index"

  mount JasmineRails::Engine => "/specs" if defined?(JasmineRails)
end
