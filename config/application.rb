require File.expand_path('../boot', __FILE__)

require 'rails/all'

if defined?(Bundler)
  # If you precompile assets before deploying to production, use this line
  Bundler.require *Rails.groups(:assets => %w(development test))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

module AccOnline
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += %W(#{config.root}/lib)

    # Only load the plugins named here, in the order given (default is alphabetical).
    # :all can be used as a placeholder for all plugins not explicitly named.
    # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

    # Activate observers that should always be running.
    # config.active_record.observers = :cacher, :garbage_collector, :forum_observer

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Sydney'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:confirm_password, :password]

    config.middleware.insert 0, 'Rack::Cache', {
      :verbose     => true,
      :metastore   => URI.encode("file:#{Rails.root}/tmp/dragonfly/cache/meta"),
      :entitystore => URI.encode("file:#{Rails.root}/tmp/dragonfly/cache/body")
    } unless ["staging", "production"].include?(Rails.env)
    
    config.middleware.insert_after 'Rack::Cache', 'Dragonfly::Middleware', :assets

    # Use SQL instead of Active Record's schema dumper when creating the database.
    # This is necessary if your schema can't be completely dumped by the schema dumper,
    # like if you have constraints or database-specific column types
    # config.active_record.schema_format = :sql

    # Enforce whitelist mode for mass assignment.
    # This will create an empty whitelist of attributes available for mass-assignment for all models
    # in your app. As such, your models will need to explicitly whitelist or blacklist accessible
    # parameters by using an attr_accessible or attr_protected declaration.
    # config.active_record.whitelist_attributes = true

    config.to_prepare do
      Devise::Mailer.layout "notifier" # email.haml or email.erb
    end

    config.middleware.use "RenderCsvAsAttachment"

    # Enable the asset pipeline
    config.assets.enabled = true
    config.assets.initialize_on_precompile = false

    config.assets.paths << "#{Rails.root}/app/assets/fonts"
    config.assets.paths << "#{Rails.root}/app/assets/templates"
    config.assets.paths << "#{Rails.root}/vendor/admin_assets/images"
    config.assets.paths << "#{Rails.root}/vendor/assets/images"
    config.assets.paths << "#{Rails.root}/vendor/admin_assets/javascripts"
    config.assets.paths << "#{Rails.root}/vendor/admin_assets/stylesheets"
    config.assets.paths << File.join(Rails.root,'vendor/assets/images/jquery-ui/images/')

    # Version of your assets, change this if you want to expire all your assets
    config.assets.version = '1.1'

    config.assets.precompile = [ Proc.new { |path|
       !File.extname(path).in?(['.js', '.css', ''])
    }, /(?:\/|\\|\A)application\.(css|js)$/ ]

    config.assets.precompile += ["public.css", "public.js"]
    config.assets.precompile += ["libraries.js"]
    config.assets.precompile += ["admin.js", "admin.css", "email.css"]
    config.assets.precompile += ["admin_wireframes.js", 'admin_wireframes.css']
    config.assets.precompile += ["wf.css", "wf.js"]
    config.assets.precompile += ["wireframes.js"]

    # Logging for unicorn
    # config.logger = Logger.new(STDOUT)
  end
end