# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
#
every 1.day, :at => '1:00 am' do
  runner "Service.reset_old_tokens"
  rake "db2fog:backup:clean"
end

every 1.day, :at => '1:10 am' do
  runner "LearningPlan.overdue_reminders"
end

every 1.day, :at => '1:20 am' do
  runner "LearningPlan.upcoming_reminders"
end

every 1.hour do
  rake "thinking_sphinx:index RAILS_ENV=production"
end

every :sunday, at: "9pm" do
  rake "cronjob:update_coordinators_on_progress"
end