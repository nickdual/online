# Textacular and ransack both add #search method to ActiveRecord::Base. Prioritize
# ransack because we can still use Textacular.basic_search for searches
require 'textacular'

# Somehow, the call to textacular/rails isn't doing its thing.
ActiveRecord::Base.extend(Textacular)

require 'ransack'
