class StagingMailInterceptor
  def self.delivering_email(message)
    message.subject = "#{message.to} #{message.subject}"
    message.to = "gricker@acctv.co"
  end
end

if Rails.env.staging?
  Mail.register_interceptor(StagingMailInterceptor)
end