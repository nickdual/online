require 'dragonfly'

image = Dragonfly[:assets]
image.configure_with(:imagemagick)
image.configure_with(:rails)
image.define_macro(ActiveRecord::Base, :asset_accessor)

unless %w(development test cucumber).include? Rails.env
  image.datastore = Dragonfly::DataStorage::S3DataStore.new

  image.datastore.configure do |c|
    c.bucket_name       = ENV['FOG_DIRECTORY']
    c.access_key_id     = ENV['AWS_ACCESS_KEY_ID']
    c.secret_access_key = ENV['AWS_SECRET_ACCESS_KEY']
  end

  image.cache_duration = 3600*24*365*3

  image.configure do |c|
    # Override the .url method...
    c.define_url do |app, job, opts|
      thumb = DragonflyThumb.find_by_job(job.serialize)
      # If (fetch 'some_uid' then resize to '40x40') has been stored already, give the datastore's remote url ...
      if thumb
        app.datastore.url_for(thumb.uid)
      # ...otherwise give the local Dragonfly server url
      else
        app.server.url_for(job)
      end
    end

    # Before serving from the local Dragonfly server...
    c.server.before_serve do |job, env|
      # ...store the thumbnail in the datastore...
      uid = job.store

      # ...keep track of its uid so next time we can serve directly from the datastore
      DragonflyThumb.create!(uid: uid, job: job.serialize)
    end
  end
end

# Removing Rack::Cache notices thanks to https://github.com/markevans/dragonfly/issues/159
if Rails.env.test?
  Rails.application.middleware.delete Rack::Cache
  Rails.application.middleware.insert 0, Rack::Cache, {
    :verbose     => false,
    :metastore   => URI.encode("file:#{Rails.root}/tmp/dragonfly/cache/meta"), # URI encoded in case of spaces
    :entitystore => URI.encode("file:#{Rails.root}/tmp/dragonfly/cache/body")
  }
end
