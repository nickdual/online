ActiveRecord::Base.include_root_in_json = false

ActiveSupport.on_load(:active_model_serializers) do
  ActiveModel::ArraySerializer.root = false
end