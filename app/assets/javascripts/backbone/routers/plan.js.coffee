class PlanRouter extends Backbone.Router
  routes :
    'coordinator/services/:serviceId/learning-plans/:id/edit'     : 'editPlan'
    'coordinator/services/:serviceId/learning-plans/:id/users'    : 'showPlanUsers'
    'coordinator/services/:serviceId/learning-plans/:id/programs' : 'showPlanPrograms'

  initialize : =>
    @.bind "all", (route, router) =>
      @_trackPageview()
      window.scrollTo(0, 0)
      $(".tooltip").remove()

  _trackPageview: ->
    url = Backbone.history.getFragment()
    _gaq.push(['_trackPageview', "/#{url}"])

  editPlan : (serviceId, planId) ->
    @requirePlan serviceId, planId,
      =>
        new ACC.Views.SidebarPlanUsers().render(@plan)

        view = new ACC.Views.ServicePlanEdit(@service, @plan)
        $("#serviceWrapper").html(view.render().el)
      => (@editPlan(serviceId, planId))

  showPlanUsers : (serviceId, planId) ->
    @requirePlan serviceId, planId,
      =>
        new ACC.Views.SidebarPlanUsers().render(@plan)

        view = new ACC.Views.ServicePlan(@service, @plan)
        $("#serviceWrapper").html(view.render().el)
        view.showUsers()

      => (@showPlanUsers(serviceId, planId))

  showPlanPrograms : (serviceId, planId) ->
    @requirePlan serviceId, planId,
      =>
        new ACC.Views.SidebarPlanPrograms().render(@plan)

        view = new ACC.Views.ServicePlan(@service, @plan)
        $("#serviceWrapper").html(view.render().el)
        view.showPrograms()

      => (@showPlanPrograms(serviceId, planId))

  requirePlan : (serviceId, planId, success, failure) ->
    @requireLogin ->
      if ACC.Services.length
        ACC.Views.Navigation.highlightTab(".coordinator")
        @service = ACC.Services.get(serviceId)
        @service.Programs.fetch()
        @service.Employments.fetch()
        @service.JobTitles.fetch()

        if @service.LearningPlans.length
          @plan = @service.LearningPlans.get(planId)

          view = new ACC.Views.Service(@service)
          $("#application").html(view.render().el)
          view.highlightTab("coordinator/services/#{serviceId}/learning-plans")

          @plan.EmploymentLearningPlans.fetch
            url: "/api/learning_plans/#{planId}/employment_learning_plans"
          @plan.Programs.fetch
            url: "/api/learning_plans/#{planId}/programs"
          success.call(@)
        else
          @service.LearningPlans.fetch
            success: => (failure.call(@))
      else
        ACC.Services.fetch
          success: => (failure.call(@))

  requireLogin : (callback) =>
    $("#application > div").hide()
    callback.call(@)

ACC.Routers.ServicePlan = new PlanRouter