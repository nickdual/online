class CoordinatorRouter extends Backbone.Router
  routes :
    'coordinator'                             : 'coordinator'
    'coordinator/groups/:id'                  : 'showGroup'
    'coordinator/groups/:id/job-titles'       : 'showGroupJobs'
    'coordinator/groups/:id/reports'          : 'showGroupReports'
    'coordinator/groups/:id/coordinators/new' : 'showGroupCoordinatorNew'
    'coordinator/groups/:id/coordinators'     : 'showGroupCoordinators'

  initialize : =>
    @.bind "all", (route, router) =>
      @_trackPageview()
      window.scrollTo(0, 0)
      $(".tooltip").remove()

  _trackPageview: ->
    url = Backbone.history.getFragment()
    _gaq.push(['_trackPageview', "/#{url}"])

  coordinator : ->
    @requireLogin -> 
      ACC.Groups.fetch()
      ACC.Services.fetch()
      new ACC.Views.SidebarCoordinator().render()

      view = new ACC.Views.Coordinator
      $("#application").html(view.render().el)

  showGroup : (id) ->
    # TODO: Deprecate this at some point in a release or two
    @showGroupCoordinators(id) # Redirecting to the new location

  showGroupJobs : (id) ->
    @requireGroup id,
      =>
        @group.JobTitles.fetch()
        new ACC.Views.SidebarGroupJobCategories().render(@group)
        @serviceView.highlightTab("/coordinator/groups/#{id}/job-titles")

        view = new ACC.Views.GroupJobTitles(@group)
        $("#groupWrapper").html(view.render().el)
      => (@showGroup(id))

  showGroupReports : (id) ->
    @requireGroup id,
      =>
        @group.Charts.fetch()
        new ACC.Views.SidebarGroupCharts().render()
        @serviceView.highlightTab("/coordinator/groups/#{id}/reports")

        view = new ACC.Views.GroupReports(@group)
        $("#groupWrapper").html(view.render().el)
      => (@showGroupReports(id))

  showGroupCoordinators : (id) ->
    @requireGroup id,
      =>
        @group.Coordinators.fetch()
        new ACC.Views.Sidebar.Groups.CoordinatorsIndex({model: @group}).render()
        @serviceView.highlightTab("/coordinator/groups/#{id}/coordinators")

        view = new ACC.Views.Group.CoordinatorsIndex({model: @group})
        $("#groupWrapper").html(view.render().el)
      => (@showGroupCoordinators(id))

  showGroupCoordinatorNew : (id) ->
    @requireGroup id,
      =>
        new ACC.Views.Sidebar.Groups.CoordinatorsNew({model: @group}).render()
        @serviceView.highlightTab("/coordinator/groups/#{id}/coordinators")

        view = new ACC.Views.Group.CoordinatorsNew({model: @group})
        $("#groupWrapper").html(view.render().el)
      => (@showGroupCoordinatorNew(id))

  requireGroup : (id, success, failure) ->
    @requireLogin =>
      if ACC.Groups.length
        @group = ACC.Groups.get(id)

        @serviceView = new ACC.Views.Group.Show({model: @group})
        $("#application").html(@serviceView.render().el)

        ACC.Views.Navigation.highlightTab(".coordinator")
        success.call(@)
      else
        ACC.Groups.fetch
          success: => (failure.call(@))

  requireLogin : (callback) =>
    $("#application > div").hide()
    ACC.Views.Navigation.highlightTab(".coordinator")
    callback.call(@)

ACC.Routers.Coordinator = new CoordinatorRouter