class ServiceRouter extends Backbone.Router
  routes :
    'coordinator/services/:id'                          : 'showService'
    'coordinator/services/:id/reports'                  : 'showServiceCharts'
    'coordinator/services/:id/reports/search'           : 'showServiceSearch'
    'coordinator/services/:id/job-titles'               : 'showServiceJobs'
    'coordinator/services/:id/group-sessions'           : 'showServiceSessions'
    'coordinator/services/:id/learning-plans/new'       : 'showServicePlanNew'
    'coordinator/services/:id/learning-plans'           : 'showServicePlans'

  initialize : =>
    @.bind "all", (route, router) =>
      @_trackPageview()
      window.scrollTo(0, 0)
      $(".tooltip").remove()

  _trackPageview: ->
    url = Backbone.history.getFragment()
    _gaq.push(['_trackPageview', "/#{url}"])

  showService : (id) ->
    @requireService id,
      =>
        new ACC.Views.SidebarService().render(@service)

        view = new ACC.Views.ServiceUsers(@service)
        $("#serviceWrapper").html(view.render().el)

        @service.Employments.fetch()
        @service.JobTitles.fetch()
      => (@showService(id))

  showServiceJobs : (id) ->
    @requireService id,
      =>
        new ACC.Views.SidebarServiceJobCategories().render(@service)
        @service.JobTitles.fetch()

        view = new ACC.Views.ServiceJobs(@service)
        $("#serviceWrapper").html(view.render().el)
      => (@showServiceJobs(id))
    @highlightTab()

  showServiceSessions : (id) ->
    @requireService id,
      =>
        @service.Programs.fetch()
        @service.Employments.fetch()
        @service.JobTitles.fetch()
        new ACC.Views.SidebarServiceSessions().render(@service)

        view = new ACC.Views.ServiceSessions(@service)
        $("#serviceWrapper").html(view.render().el)
      => (@showServiceSessions(id))
    @highlightTab()

  showServiceCharts : (id) ->
    @requireService id,
      =>
        new ACC.Views.SidebarServiceCharts().render()
        @service.Charts.fetch()

        view = new ACC.Views.ServiceCharts(@service)
        $("#serviceWrapper").html(view.render().el)
      => (@showServiceCharts(id))
    @highlightTab()

  showServiceSearch : (id) ->
    @requireService id,
      =>
        new ACC.Views.SidebarServiceSearch().render()
        @service.JobTitles.fetch()
        @service.LearningPlans.fetch()
        @service.Programs.fetch()

        view = new ACC.Views.ServiceSearch(@service)
        $("#serviceWrapper").html(view.render().el)
      => (@showServiceSearch(id))

  showServicePlans : (id) ->
    @requireService id,
      => 
        new ACC.Views.SidebarServicePlans().render(@service)
        @service.LearningPlans.fetch()

        view = new ACC.Views.ServicePlans(@service)
        $("#serviceWrapper").html(view.render().el)
      => (@showServicePlans(id))
    @highlightTab()

  showServicePlanNew : (id) ->
    @requireService id,
      =>
        new ACC.Views.SidebarServicePlans().render(@service)
        @service.Employments.fetch()
        @service.JobTitles.fetch()

        view = new ACC.Views.ServicePlanNew({model: @service})
        $("#serviceWrapper").html(view.render().el)
      => (@showServicePlanNew(id))

  loadAlltheCollections : (service) =>
    service.Programs.fetch()

  requireService : (id, success, failure) ->
    @requireLogin =>
      if ACC.Services.length
        @service = ACC.Services.get(id)
        @loadAlltheCollections(@service)

        view = new ACC.Views.Service(@service)
        $("#application").html(view.render().el)

        ACC.Views.Navigation.highlightTab(".coordinator")
        success.call(@)
      else
        ACC.Services.fetch
          success: => (failure.call(@))

  requireLogin : (callback) =>
    $("#application > div").hide()
    callback.call(@)

  highlightTab:() ->
    href = Backbone.history.getFragment()
    tab = $("#mainheader").find('.btn.btn-3d.active')
    if tab.length > 0
      tab.removeClass('active')
      $("#mainheader").find("a[href='/#{href}']").addClass('active')

ACC.Routers.Service = new ServiceRouter