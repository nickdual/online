class DashboardRouter extends Backbone.Router
  routes :
    ''                      : 'home'
    'settings'              : 'settings'
    'my-learning'           : 'learningPlans'
    'my-learning/plans'     : 'learningPlans'
    'my-learning/plan/:id'  : 'showLearningPlan'
    'my-learning/records'   : 'learningRecords'
    'programs'              : 'programs'

  initialize : =>
    @.bind "all", (route, router) =>
      @_trackPageview()
      window.scrollTo(0, 0)
      $(".tooltip").remove()

  _trackPageview: ->
    url = Backbone.history.getFragment()
    _gaq.push(['_trackPageview', "/#{url}"])

  home : ->
    @requireLogin ->
      ACC.IncompletedLearningPlans.fetch()
      ACC.RecentLearningRecords.fetch()
      ACC.Views.Navigation.highlightTab(".home")

      new ACC.Views.SidebarDashboard().render()

      view = new ACC.Views.Dashboard
      $("#application").html(view.render().el)

      # Making it feel fast to users who sign in
      @loadAlltheCollections()

  programs : ->
    @requireLogin ->
      unless ACC.Programs.length
        ACC.Programs.fetch()

      ACC.Views.Navigation.highlightTab(".programs")
      new ACC.Views.SidebarPrograms().render()

      view = new ACC.Views.Programs
      $("#application").html(view.render().el)

  learningPlans : ->
    @requireLogin ->
      unless ACC.EmploymentLearningPlans.length
        ACC.EmploymentLearningPlans.fetch()
      unless ACC.LearningRecords.length
        ACC.LearningRecords.fetch()

      new ACC.Views.SidebarLearningPlans().render()
      ACC.Views.Navigation.highlightTab(".my-learning")

      view = new ACC.Views.LearningPlans
      $("#application").html(view.render().el)

  learningRecords : ->
    @requireLogin ->
      unless ACC.LearningRecords.length
        ACC.LearningRecords.fetch()
      new ACC.Views.SidebarLearningRecords().render()

      ACC.Views.Navigation.highlightTab(".my-learning")
      view = new ACC.Views.LearningRecords
      $("#application").html(view.render().el)

  showLearningPlan : (id) ->
    @requireLogin ->
      if ACC.EmploymentLearningPlans.length
        plan = ACC.EmploymentLearningPlans.get(id)
        plan.ProgramsPending.fetch()
        plan.ProgramsCompleted.fetch()
        new ACC.Views.SidebarLearningPlan({model: plan}).render()

        ACC.Views.Navigation.highlightTab(".my-learning")
        view = new ACC.Views.LearningPlan(plan)
        ACC.Programs.fetch()
        $("#application").html(view.render().el)
      else
        ACC.EmploymentLearningPlans.fetch
          success: =>
            @showLearningPlan(id)

  settings : ->
    @requireLogin ->
      ACC.User.Employments.fetch()
      ACC.User.Coordinators.fetch()
      new ACC.Views.SidebarSettings().render()
      ACC.Views.Navigation.highlightTab(".home")

      view = new ACC.Views.Settings
      $("#application").html(view.render().el)

  loadAlltheCollections : ->
    ACC.Programs.lazyFetch()
    ACC.User.Employments.lazyFetch()
    ACC.User.Coordinators.fetch()
    ACC.LearningRecords.lazyFetch()
    ACC.EmploymentLearningPlans.lazyFetch()

  requireLogin : (callback) =>
    $("#application > div").hide()
    callback.call(@)

ACC.Routers.Dashboard = new DashboardRouter