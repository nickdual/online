class ProgramRouter extends Backbone.Router
  routes :
    'programs/:id/essential-assessments/new'  : 'startAssessment'
    'programs/:id'                            : 'showProgram'

  initialize : =>
    @.bind "all", (route, router) =>
      @_trackPageview()
      window.scrollTo(0, 0)
      $(".tooltip").remove()

  _trackPageview: ->
    url = Backbone.history.getFragment()
    _gaq.push(['_trackPageview', "/#{url}"])

  showProgram : (id) =>
    @requireProgram id,
      =>
        @program.Videos.fetch()
        @program.Related.fetch()
        @program.LearningResources.fetch()
        @program.CoordinatorLearningResources.fetch()
        @program.ProgramAssessment.fetch()

        new ACC.Views.SidebarProgram().render(@program)

        view = new ACC.Views.Program({model: @program})
        $("#application").html(view.render().el)
      => (@showProgram(id))

  startAssessment : (id) =>
    @requireProgram id,
      =>
        new ACC.Views.SidebarAssessment().render()

        @program.EssentialsAssessment.fetch
          success: =>
            @assessment = @program.EssentialsAssessment.first()
            view = new ACC.Views.EssentialsAssessment.Show({program: @program, model: @assessment})
            $("#application").html(view.render().el)

            unless @program.Videos.length
              @program.Videos.fetch()
            @assessment.Sections.fetch()
      => (@startAssessment(id))

  requireProgram : (id, success, failure) =>
    if ACC.Programs.length
      @program = ACC.Programs.get(id)

      ACC.Views.Navigation.highlightTab(".programs")
      success.call(@)
    else
      ACC.Programs.fetch
        success: => (failure.call(@))

ACC.Routers.Program = new ProgramRouter