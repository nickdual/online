class UserRouter extends Backbone.Router
  routes :
    'coordinator/services/:serviceID/users/:userId'           : 'showUser'
    'coordinator/services/:serviceID/users/:userId/edit'      : 'editUser'
    'coordinator/services/:serviceID/users/:userId/records'   : 'showUserRecords'
    'coordinator/services/:serviceID/users/:userId/plans/:id' : 'showUserPlan'

  initialize : =>
    @.bind "all", (route, router) =>
      @_trackPageview()
      window.scrollTo(0, 0)
      $(".tooltip").remove()

  _trackPageview: ->
    url = Backbone.history.getFragment()
    _gaq.push(['_trackPageview', "/#{url}"])

  showUser : (serviceId, userId) ->
    @requireUser serviceId, userId,
      =>
        @user.EmploymentLearningPlans.fetch()
        new ACC.Views.SidebarUserPlans().render(@user)

        view = new ACC.Views.ServiceUser({service: @service, model: @user})
        $("#serviceWrapper").html(view.render().el)
        view.showPlans(@plan)
      => (@showUser(serviceId, userId))

  editUser : (serviceId, userId) =>
    @requireUser serviceId, userId,
      =>
        new ACC.Views.SidebarServiceUserEdit({model: @user}).render()

        view = new ACC.Views.Service.UserEdit({serviceView: @serviceView, service: @service, model: @user})
        $("#serviceWrapper").html(view.render().el)

      => (@editUser(serviceId, userId))

  showUserRecords : (serviceId, userId) ->
    @requireUser serviceId, userId,
      =>
        @user.LearningRecords.fetch()
        new ACC.Views.SidebarUserRecords().render(@user)

        view = new ACC.Views.ServiceUser({service: @service, model: @user})
        $("#serviceWrapper").html(view.render().el)
        view.showRecords()
      => (@showUserRecords(serviceId, userId))

  # TODO: Find out if this is still needed
  showUserAssessments : (serviceId, userId) ->
    @requireUser serviceId, userId,
      =>
        @user.LearningAssessments.fetch()
        new ACC.Views.SidebarUserAssessments().render(@user)
        ACC.Views.ServiceUser.render(@service, @user)
        ACC.Views.ServiceUser.showAssessments()
      => (@showUserAssessments(serviceId, userId))

  showUserPlan : (serviceId, userId, planId) ->
    @requireUser serviceId, userId,
      =>
        if @user.EmploymentLearningPlans.length
          @plan = @user.EmploymentLearningPlans.get(planId)
          new ACC.Views.SidebarUserPlan().render(@plan)

          @plan.ProgramsPending.fetch()
          @plan.ProgramsCompleted.fetch()

          view = new ACC.Views.ServiceUser({service: @service, model: @user})
          $("#serviceWrapper").html(view.render().el)
          view.showPlan(@plan)
        else
          @user.EmploymentLearningPlans.fetch
            success: =>
              @showUserPlan(serviceId, userId, planId)
      => (@showUserPlan(serviceId, userId, planId))

  # TODO: Put this into one group router
  requireUser : (serviceId, userId, success, failure) =>
    @requireLogin =>
      if ACC.Services.length
        @service = ACC.Services.get(serviceId)

        # Job Titles are required
        unless @service.JobTitles.length or @service.JobTitles.fetched?
          @service.JobTitles.fetch
            success: => 
              failure.call(@)
              @service.JobTitles.fetched = true

        if @service.Employments.length
          @user = @service.Employments.get(userId)

          @serviceView = new ACC.Views.Service(@service)
          $("#application").html(@serviceView.render().el)
          @serviceView.highlightTab("coordinator/services/#{serviceId}")

          success.call(@)
        else
          @service.Employments.fetch
            success: => (failure.call(@))
      else
        ACC.Services.fetch
          success: => (failure.call(@))

  requireLogin : (callback) =>
    $("#application > div").hide()
    callback.call(@)

ACC.Routers.ServiceUser = new UserRouter