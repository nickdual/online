class ACC.Views.Profile extends Backbone.View
  template  : JST['settings/_profile']

  events :
    'submit form' : 'updateProfile'

  initialize: =>
    ACC.User.Employments.on 'reset', @addAll
    ACC.User.on 'change', @render, @

  render: =>
    $(@el).html(@template(ACC.User.toJSON()))
    @loadFromProfile()
    @

  loadFromProfile : =>
    form = $(@el).find('form')
    form.find('[name="gender"]').val( ACC.User.get('gender') )
    form.find('[name="timezone"]').val( ACC.User.get('time_zone') )

    form.find('[name="dob"]').datepicker({
      format: "dd/mm/yyyy"
    })

  updateProfile : (event) ->
    event.preventDefault()
    form = $(@el).find('form')
    form.find(".error").removeClass("error")
    form.find(".help-inline").remove()

    ACC.User.save
      first_name        : form.find('input[name="first_name"]').val()
      last_name         : form.find('input[name="last_name"]').val()
      email             : form.find('input[name="email"]').val()
      formatted_born_at : form.find('input[name="dob"]').val()
      gender            : form.find('select[name="gender"]').val()
      time_zone         : form.find('select[name="timezone"]').val()
      password    : form.find('input[name="password"]').val()
      password_confirmation: form.find('input[name="password_confirmation"]').val()
      user:
        first_name  : form.find('input[name="first_name"]').val()
        last_name   : form.find('input[name="last_name"]').val()
        email       : form.find('input[name="email"]').val()
        born_at     : form.find('input[name="dob"]').val()
        gender      : form.find('select[name="gender"]').val()
        time_zone   : form.find('select[name="timezone"]').val()
        password    : form.find('input[name="password"]').val()
        password_confirmation: form.find('input[name="password_confirmation"]').val()
      {success: (user) =>
        SuccessNotification.text("Successfully updated profile")
        ACC.User.fetch()

        if form.find('input[name="password"]').val().length
          ACC.User.set({activation_code_set_at: null})

        form.find('input[name="password"]').val("")
        form.find('input[name="password_confirmation"]').val("")
      error: (user, errors) ->
        _.each errors, (error, name) ->
          form.find("[name=#{name}]").parent().parent().addClass("error")
          form.find("[name=#{name}]").parent().append("<span class=\"help-inline\">#{error}</span>")

        ErrorNotification.text("Failed to update profile")}