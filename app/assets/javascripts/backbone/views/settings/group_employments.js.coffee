class ACC.Views.GroupEmployments extends Backbone.View
  template      : JST['settings/groups/_index']
  groupTemplate : JST['settings/groups/_show']

  events :
    'click #groupslist a[role="leave"]' : "leave"

  initialize: =>
    ACC.User.Coordinators.on 'reset', @addAll

  render : =>
    $(@el).html(@template())
    @

  addAll: =>
    $(@el).find("#groupslist").empty()

    if ACC.User.Coordinators.length
      ACC.User.Coordinators.each (coordinator) => @addOne(coordinator)
    else
      $(@el).hide()

  addOne: (coordinator) =>
    template = $(@groupTemplate(coordinator.toJSON()))
    $(@el).find("#groupslist").append(template)

  leave: (event) ->
    event.preventDefault()
    id = $(event.currentTarget).data('id')
    coordinator = ACC.User.Coordinators.get(id)
    confirm_dialog = confirm("Are you sure you want to terminate your coordinator status at #{coordinator.get('group_name')}?")

    if confirm_dialog is true
      coordinator.destroy
        url: "/api/profile/coordinators/#{coordinator.id}"
        success: (coordinators) =>
          SuccessNotification.text("Successfully left #{coordinator.get('group_name')}")
          @addAll()
          ACC.Groups.fetch()
          ACC.Services.fetch()
          ACC.User.fetch()
        error: =>
          ErrorNotification.text("Failed to leave #{coordinator.get('group_name')}")