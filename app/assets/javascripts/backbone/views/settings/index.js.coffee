class ACC.Views.Settings extends Backbone.View
  template  : JST['settings/index']

  render : =>
    $(@el).html(@template())
    @setupViews()
    @

  setupViews : =>
    view = new ACC.Views.Profile
    $(@el).find("#profile").html(view.render().el)

    view = new ACC.Views.Employments
    $(@el).find("#employments").html(view.render().el)

    view = new ACC.Views.GroupEmployments
    $(@el).find("#groups").html(view.render().el)