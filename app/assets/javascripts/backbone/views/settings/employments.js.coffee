class ACC.Views.Employments extends Backbone.View
  template:           JST['settings/employments/_index']
  employmentTemplate: JST['settings/employments/_show']
  # TODO: Move the single employments to a new view

  events :
    'click #new_employment'                   : 'toggleCreateEmployment'
    'click #create_employment a'              : 'toggleCreateEmployment'
    'submit #create_employment form'          : 'createEmployment'
    'click #employmentslist a[role="leave"]'  : 'leaveEmployment'
    'click #show_employments a[role="edit"]'  : 'toggleEditEmployment'
    'click #show_employments a[role="cancel"]': 'toggleEditEmployment'
    'submit #employmentslist form'            : 'updateEmployment'

  initialize: =>
    ACC.User.Employments.on 'reset', @addAll

  render : =>
    $(@el).html(@template())
    @addAll() if ACC.User.Employments.length
    @

  addAll: =>
    $(@el).find("#employmentslist").empty()

    if ACC.User.Employments.length
      ACC.User.Employments.each (employment) => @addOne(employment)
    else
      $(@el).append("<p>You have no employments</p>")

  addOne: (employment) ->
    template = $(@employmentTemplate(employment.toJSON()))
    job_template = template.find('[name="job_title_id"]')
    $(@el).find("#employmentslist").append(template)


    employment.JobTitles.fetch
      success: =>
        job_template.empty()
        employment.JobTitles.each (job) =>
          option = "<option value=\"#{job.id}\">#{job.get('name')}</option>"
          job_template.append(option)

        job_template.append("<option value=\"\"></option>")
        job_template.val(employment.get('job_title_id'))

  createEmployment: (event) ->
    event.preventDefault()
    form = $(@el).find('form')
    activation_token = form.find('input[name="activation_token"]').val()

    ACC.User.Employments.create
      activation_token: activation_token
      employment:
        activation_token: activation_token
      {url: "/api/profile/employments"
      success: (employment) =>
        SuccessNotification.text("Successfully joined employment")
        ACC.User.Services.fetch
          success: =>
            @addAll()
            @toggleCreateEmployment()
            ACC.User.fetch()
      error: (employment, errors) =>
        if typeof errors is "string"
          ErrorNotification.text(errors)
        else
          ErrorNotification.text("Could not match activation token")}

  leaveEmployment : (event) ->
    employment_id = $(event.currentTarget).attr("data-id")
    employment = ACC.User.Employments.get(employment_id)
    confirm_dialog = confirm("Are you sure you want to terminate your employment at this #{employment.get('service_name')}?")

    if confirm_dialog is true
      employment.destroy
        url: "/api/profile/employments/#{employment.id}"
        success: (employment) =>
          SuccessNotification.text("Successfully left service")
          ACC.Groups.fetch()
          ACC.Services.fetch()
          ACC.User.fetch()
          @addAll()
        error: ->
          ErrorNotification.text("Failed to leave service")

  updateEmployment : (event) ->
    event.preventDefault()
    form = $(event.currentTarget)
    id = form.parent().attr("data-id")
    employment = ACC.User.Employments.get(id)

    employment.save
      employee_number : form.find('input[name="employee_number"]').val()
      job_title_id : form.find('select[name="job_title_id"]').val()
      job_title_name: form.find('select[name="job_title_id"] :selected').html()
      {url: "/api/profile/employments/#{id}"
      success: (employment) =>
        ACC.Groups.fetch()
        ACC.Services.fetch()
        ACC.User.fetch()
        SuccessNotification.text("Successfully updated employment")
        @addAll()
      error: ->
        ErrorNotification.text("Failed to update employment")}

  toggleEditEmployment : (event) =>
    event.preventDefault() if event
    id = $(event.currentTarget).data('id')
    $(@el).find("li[data-id=#{id}]").toggle()

  toggleCreateEmployment : (event) ->
    event.preventDefault() if event
    $(@el).find("#create_employment,#show_employments").toggle()