class ACC.Views.LearningResource extends Backbone.View
  template: JST['programs/_learning_resource']
  tagName : "li"
  events  :
    'click a' : 'downloadResource'

  initialize : (learningResource) =>
    @learningResource = learningResource

  render : =>
    $(@el).html(@template(@learningResource.toJSON()))
    @

  downloadResource : (event) =>
    event.preventDefault()
    url = $(event.currentTarget).attr("href")
    window.location.href = url
    ACC.LearningRecords.fetch()
    ACC.RecentLearningRecords.fetch()
    ACC.Routers.Dashboard.navigate("programs/#{@program.id}", true)
    ACC.User.fetch()