class ACC.Views.LearningResourcesIndex extends Backbone.View

  initialize : (program) =>
    @program = program
    @program.LearningResources.on 'reset', @addAll

  addAll: =>
    @program.LearningResources.each (resource) =>
      view = new ACC.Views.LearningResource(resource)
      $(@el).append(view.render().el)

    if @program.LearningResources.length is 0
      $(@el).html("There are no learning resources for this program")