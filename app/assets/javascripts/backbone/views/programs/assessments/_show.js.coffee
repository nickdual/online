class ACC.Views.ProgramAssessment extends Backbone.View
  template: JST['programs/_assessment']

  initialize : =>
    @model.ProgramAssessment.on 'reset', @addProgramAssessments

  render : =>
    $(@el).html("<p>Loading Program Assessments</p>")
    @

  addProgramAssessments : =>
    $(@el).html(@template(@model.ProgramAssessment.toJSON()[0]))