class ACC.Views.Programs extends Backbone.View
  template: JST['programs/index']

  initialize : ->
    ACC.Programs.on 'reset',  @addAll

  render : ->
    $(@el).html(@template())
    @addAll() if ACC.Programs.length
    @

  addOne : (program) ->
    view = new ACC.Views.SharedProgram(program)
    $(@el).find('#programslist').append(view.render().el)

  addAll : =>
    $(@el).find('#programslist').empty()

    if ACC.Programs.length
      ACC.Programs.each (program) => @addOne(program)
      $(@el).find('input.searchfield').quicksearch($(@el).find('#programslist > li'))
    else
      $(@el).find('#programslist').append("<p>You have no programs to view.</p>")