class ACC.Views.SharedProgram extends Backbone.View
  template  : JST['programs/_program']
  tagName   : "li"
  className : "plan program span4"

  events :
    'click a' : 'goToProgram'

  # TODO: Make this set model instead
  initialize : (program) =>
    @program = program

  render : =>
    $(@el).html(@template(@program.toJSON()))
    @

  goToProgram : (event) ->
    event.preventDefault()
    url = $(event.currentTarget).attr("href")
    ACC.Routers.Dashboard.navigate url, true