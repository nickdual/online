class ACC.Views.RelatedIndex extends Backbone.View
  initialize : (program) =>
    @program = program
    @program.Related.on 'reset', @addAll

  addAll: =>
    @program.Related.each (program) =>
      view = new ACC.Views.SharedProgram(program)
      $(@el).append(view.render().el)