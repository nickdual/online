class ACC.Views.CoordinatorLearningResources extends Backbone.View

  initialize : (program) =>
    @program = program
    @program.CoordinatorLearningResources.on 'reset', @addAll

  addAll: =>
    @program.CoordinatorLearningResources.each (resource) =>
      view = new ACC.Views.LearningResource(resource)
      $(@el).append(view.render().el)

    if @program.CoordinatorLearningResources.length is 0
      $(@el).html("There are no coordinator learning resources for this program")