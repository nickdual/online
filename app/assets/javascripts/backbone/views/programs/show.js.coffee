class ACC.Views.Program extends Backbone.View
  template: JST['programs/show']

  events :
    'click [role=start-assessment]' : 'goToAssessmentStart'

  render : =>
    $(@el).html(@template(@model.toJSON()))
    @loadViews()
    @

  loadViews: =>
    view = new ACC.Views.VideosIndex({model: @model})
    $(@el).find("#videoWrapper").html(view.render().el)

    learningResources = new ACC.Views.LearningResourcesIndex(@model)
    $(@el).find('#learningResources').html(learningResources.render().el)

    coordinatorLearningResourcesView = new ACC.Views.CoordinatorLearningResources(@model)
    $(@el).find('#coordinatorLearningResources').html(coordinatorLearningResourcesView.render().el)

    relatedView = new ACC.Views.RelatedIndex(@model)
    $(@el).find('#relatedPrograms').html(relatedView.render().el)

    programAssessmentView = new ACC.Views.ProgramAssessment({model: @model})
    $(@el).find("#assessmentStatus").html(programAssessmentView.render().el)

  goToAssessmentStart : (event) =>
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.ServiceUser.navigate href, true