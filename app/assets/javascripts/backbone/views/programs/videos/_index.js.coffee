class ACC.Views.VideosIndex extends Backbone.View
  tagName: 'video'

  initialize : =>
    @model.Videos.on 'reset', @addAll
    @addAll() if @model.Videos.length

  attributes : =>
    "id"                    : 'video'
    "preload"               : "none"
    "poster"                : @model.get('image_url')
    "data-fullmode-enable"  : 'true'
    "data-autoresize"       : 'fit'
    "data-uid"              : "program-#{@model.get('id')}"
    "data-title"            : "#{@model.get('title')}"
    "data-on-end"           : 'stop'
    "width"                 : @model.get('video_width')
    "height"                : @model.get('video_height')

  addAll: =>
    @model.Videos.each (video) =>
      view = new ACC.Views.Video({model: video})
      $(@el).append(view.render().el)

    _.delay(@startVideo, 100)

  startVideo: =>
    sublime.ready =>
      sublime.unprepare()
      sublime.prepare 'video', (player) =>
        console.log("#{player} is now loaded.")
        player.on 'end', =>
          @createLearningRecord()

  createLearningRecord : =>
    # TODO: Move these attributes to be set at the model level
    ACC.LearningRecords.create
      program_id: @model.id
      title: @model.get('title')
      downloaded: false
      assessed: false
      thumb_60_60_url: @model.get('thumb_60_60_url')
      viewed_at: Date()
      learning_record:
        program_id: @model.id
      {url: "/api/programs/#{@model.id}/learning_records"
      success: (learning_record) =>
        SuccessNotification.text("Successfully created learning record")
      error: =>
        alert("Learning record failed to create")
        ErrorNotification.text("Error adding learning record to user")}