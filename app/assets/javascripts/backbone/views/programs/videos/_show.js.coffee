class ACC.Views.Video extends Backbone.View
  tagName: 'source'

  attributes: =>
    'src': @.model.get('url')
    "data-quality": @hd()

  hd: =>
    if @model.get('hd') is true
      'hd'