class ACC.Views.EssentialsAssessment.SectionSummary extends Backbone.View
  className : "row-fluid"
  template  : JST['programs/essentials_assessments/correct_summary']

  events :
    'click a[role=nextSection]' : 'goToNextSection'

  initialize : (attributes) =>
    @baseView = attributes.baseView

  render : =>
    $(@el).html(@template(@model.toJSON()))
    @

  goToNextSection : (event) =>
    event.preventDefault()
    @baseView.nextSection()