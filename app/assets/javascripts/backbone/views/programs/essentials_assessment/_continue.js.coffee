class ACC.Views.EssentialsAssessment._Continue extends Backbone.View
  submitTemplate    : JST['programs/essentials_assessments/continue/_submit']
  continueTemplate  : JST['programs/essentials_assessments/continue/_continue']

  render : (submit) =>
    $('html, body').animate({ scrollTop: 0 }, 'fast');
    if submit
      $(@el).html(@submitTemplate(@model.toJSON()))
      $(@el).find("input").tooltip()
    else
      $(@el).html(@continueTemplate(@model.toJSON()))

    @

  enable : =>
    $(@el).find("input").removeClass("disabled")
    $(@el).find("input").attr({"data-original-title": null})

  enabled : =>
    $(@el).find("input.disabled").length is 0