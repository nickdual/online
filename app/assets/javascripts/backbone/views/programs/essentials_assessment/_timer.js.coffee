class ACC.Views.EssentialsAssessment._Timer extends Backbone.View
  template : JST['programs/essentials_assessments/_timer']

  initialize : (attributes) =>
    @baseView = attributes.baseView

  render : =>
    $(@el).html(@template(timeAllowed: @formatTime()))
    @

  setTimer : =>
    @startingTime = moment.duration(@model.get('time_allowed'), "minutes")
    @elapsed_seconds = 0
    @timer = $.timer => (@updateTimer())
    @timer.set({time: 1000})

  start : =>
    @started = true
    @timer.play()

  stop : =>
    @timer.pause()

  updateTimer : =>
    @elapsed_seconds = @elapsed_seconds + 1
    @startingTime = moment.duration((@startingTime._milliseconds - 1000), "milliseconds")
    $(@el).find('span').html(@formatTime(@startingTime))

    if @startingTime._milliseconds is 0
      @baseView.showResults()
      @stop()

  currentTime : =>
    $(@el).find('span').html()

  elapsedMinutes : =>
    Math.ceil(@elapsed_seconds/60)

  pad : (string) =>
    if string.length < 2 then "0#{string}" else string

  formatTime : =>
    if @startingTime
      "#{@pad(@startingTime.hours().toString())}:#{@pad(@startingTime.minutes().toString())}:#{@pad(@startingTime.seconds().toString())}"
    else
      @setTimer()
      @formatTime()