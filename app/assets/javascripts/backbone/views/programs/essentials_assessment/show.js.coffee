class ACC.Views.EssentialsAssessment.Show extends Backbone.View
  template  : JST['programs/essentials_assessments/show']

  events :
    'click a[role=next]' : 'nextSection'

  initialize : (attributes) =>
    @program = attributes.program

  render : =>
    @model.set({program_description: @program.get('description')})

    $(@el).html(@template(@model.toJSON()))
    @loadVideo()
    @loadProgressBar()
    @loadTimer()
    @

  loadVideo : =>
    view = new ACC.Views.VideosIndex({model: @program})
    $(@el).find("#videoWrapper").html(view.render().el)

  loadProgressBar : =>
    @progressBar = new ACC.Views.EssentialsAssessment._Progress({model: @model})
    $(@el).find("#progressBar").html(@progressBar.render().el)

  loadTimer : =>
    @timer = new ACC.Views.EssentialsAssessment._Timer({model: @model, baseView: @})
    $(@el).find("#timer").html(@timer.render().el)

  pauseTimer : =>
    @timer.stop()

  startTimer : =>
    @timer.start()

  nextSection : (event) =>
    if event
      event.preventDefault()
    if @timer.started then (@startTimer()) else @timer.start()

    if @model.Sections.unAnswered().length > 0
      section = @model.Sections.unAnswered()[0]
      section.Questions.fetch({reset: true})
      switch section.get('question_type')
        when 'True/False'
          view = new ACC.Views.EssentialsAssessment.TrueFalse({model: section, baseView: @})
        when 'Multiple Choice'
          view = new ACC.Views.EssentialsAssessment.MultipleChoice({model: section, baseView: @})
        when 'Sequencing'
          view = new ACC.Views.EssentialsAssessment.Sequence({model: section, baseView: @})
        when 'Fill Gap'
          view = new ACC.Views.EssentialsAssessment.FillGap({model: section, baseView: @})
        when 'Matching Task'
          view = new ACC.Views.EssentialsAssessment.MatchingTask({model: section, baseView: @})

      $(@el).find("#questionsWrapper").html(view.render().el)
    else
      @showResults()

  showResults : =>
    # TODO: When a result is updated, reset the sections and stop the timer
    @pauseTimer()
    @model.set({percentageCorrect: @model.percentageCorrect(), timeTaken: @timer.elapsedMinutes()})
    @model.postResults()

    if @model.passed()
      view = new ACC.Views.EssentialsAssessment._success({model: @model})
    else
      view = new ACC.Views.EssentialsAssessment._unSuccessful({model: @model, baseView: @})

    $(@el).find("#questionsWrapper").html(view.render().el)