class ACC.Views.EssentialsAssessment.Sequence extends Backbone.View
  className       : "row-fluid"
  template        : JST['programs/essentials_assessments/sequence/index']
  exampleTemplate : JST['programs/essentials_assessments/sequence/_example_questions']

  events :
    'submit form' : 'showResults'

  initialize : (attributes) =>
    @baseView = attributes.baseView
    @model.Questions.on 'reset', @addAll
    @model.Questions.on 'change', @enableSubmit

  render : =>
    $(@el).html(@template(@model.toJSON()))
    @loadExamples()
    @loadSubmitButton()
    @

  loadExamples : =>
    $(@el).find("#exampleQuestions").html(@exampleTemplate())

  loadSubmitButton : =>
    @submitButton = new ACC.Views.EssentialsAssessment._Continue({model: @model})
    $(@el).find("form").append(@submitButton.render(true).el)

  addAll : =>
    $(@el).find("#questions").html("")

    @model.Questions.each (question) =>
      view = new ACC.Views.EssentialsAssessment.SequenceQuestion({model: question})
      $(@el).find("#questions").append(view.render().el)

  enableSubmit : =>
    if @model.Questions.answered().length is @model.Questions.length
      @submitButton.enable()

  showResults: (event) =>
    event.preventDefault()
    @baseView.pauseTimer()

    if @submitButton.enabled()
      $(@el).find("#exampleQuestions").html("")
      $(@el).find("#questions").html("")
      @model.set({completed: true})

      if @model.Questions.inCorrect().length
        _.each @model.Questions.inCorrect(), (question) =>
          view = new ACC.Views.EssentialsAssessment.SequenceAnswer({model: question})
          $(@el).find("#questions").append(view.render().el)

        @submitButton.remove()
      else
        @showSummary()

      submitButton = new ACC.Views.EssentialsAssessment._Continue({model: @model})
      $(@el).append(submitButton.render().el)

  showSummary: =>
    view = new ACC.Views.EssentialsAssessment.SectionSummary({model: @model, baseView: @baseView})
    $(@el).html(view.render().el)