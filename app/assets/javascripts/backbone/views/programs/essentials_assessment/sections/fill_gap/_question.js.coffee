class ACC.Views.EssentialsAssessment.FillGapQuestion extends Backbone.View
  textTemplate  : JST['programs/essentials_assessments/fill_gap/_question']
  imageTemplate : JST['programs/essentials_assessments/fill_gap/_question_with_image']
  className     : "question"

  events :
    'drop' : 'markQuestion'

  render : =>
    if @model.get('reference_image_url') is null
      $(@el).html(@textTemplate(@model.toJSON()))
    else
      $(@el).html(@imageTemplate(@model.toJSON()))
      $(@el).addClass("image-question")

    @model.generateTooltip(@el)

    @

  markQuestion : (event, ui) =>
    # TODO: Make this happen in the model
    userAnswer  = $(ui.draggable).data('answer')
    correctAnswer = @model.answerInString()
    correct     = userAnswer.toString() is correctAnswer.toString()
    @model.set({userAnswer: userAnswer, correctAnswer: correctAnswer, correct: correct})