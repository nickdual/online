class ACC.Views.EssentialsAssessment.SequenceAnswer extends Backbone.View
  textTemplate  : JST['programs/essentials_assessments/sequence/_answer']
  imageTemplate : JST['programs/essentials_assessments/sequence/_answer_with_image']
  className     : "question sequence feedback incorrect"

  render : =>
    if @model.get('answer_items')[0].reference_image_url is null
      $(@el).html(@textTemplate(@model.toJSON()))
    else
      $(@el).html(@imageTemplate(@model.toJSON()))
      $(@el).addClass("image-question")

    @