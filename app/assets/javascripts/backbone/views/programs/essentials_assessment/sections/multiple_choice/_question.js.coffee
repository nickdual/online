class ACC.Views.EssentialsAssessment.MultipleChoiceQuestion extends Backbone.View
  textTemplate  : JST['programs/essentials_assessments/multiple_choice/_question']
  imageTemplate : JST['programs/essentials_assessments/multiple_choice/_question_with_image']
  className     : "question"

  events :
    'click input' : 'markQuestion'

  render : =>
    if @model.get('reference_image_url') is null
      $(@el).html(@textTemplate(@model.toJSON()))
    else
      $(@el).html(@imageTemplate(@model.toJSON()))
      $(@el).addClass("image-question")

    @model.generateTooltip(@el)

    @

  markQuestion : (event) =>
    userAnswer  = $(event.currentTarget).val()
    correct     = userAnswer.toString() is @model.answerInString()
    @model.set({userAnswer: parseInt(userAnswer), correct: correct})