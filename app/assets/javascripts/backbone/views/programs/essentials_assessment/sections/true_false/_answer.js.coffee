class ACC.Views.EssentialsAssessment.TrueFalseAnswer extends Backbone.View
  textTemplate  : JST['programs/essentials_assessments/true_false/_answer']
  imageTemplate : JST['programs/essentials_assessments/true_false/_answer_with_image']
  className     : "question feedback incorrect"

  render : =>
    if @model.get('reference_image_url') is null
      $(@el).html(@textTemplate(@model.toJSON()))
    else
      $(@el).html(@imageTemplate(@model.toJSON()))
      $(@el).addClass("image-question")

    @