class ACC.Views.EssentialsAssessment.MatchingTaskAnswer extends Backbone.View
  textTemplate  : JST['programs/essentials_assessments/matching_task/_answer']
  className     : "question matching_task feedback incorrect"

  render : =>
    $(@el).html(@textTemplate(@model.toJSON()))

    @