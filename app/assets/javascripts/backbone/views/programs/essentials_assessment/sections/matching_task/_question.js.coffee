class ACC.Views.EssentialsAssessment.MatchingTaskQuestion extends Backbone.View
  textTemplate: JST['programs/essentials_assessments/matching_task/_question']
  className: "question matching_task"

  render: =>
    $(@el).html(@textTemplate(@model.toJSON()))

    $(@el).find(".sortable").sortable()
    $(@el).find(".sortable").on('sortstop', @markQuestion)

    @model.generateTooltip(@el)

    @

  sortableAnswers: =>
    $(@el).find(".sortable").sortable("toArray", {attribute: "data-answer-id"})

  sortablePosition : =>
    $(@el).find(".sortable").sortable("toArray", {attribute: "data-answer-position"})

  markQuestion: (event) =>
    userAnswer = @sortableAnswers()
    correctAnswer = _.map(@model.get('randomised_items'), (item) -> item.answer)
    @model.set({userAnswer: userAnswer})
    @model.set({sortablePosition: @sortablePosition()})

    correct = "#{correctAnswer}" is "#{userAnswer}"
    @model.set({correct: correct})
