class ACC.Views.EssentialsAssessment.FillGapAnswer extends Backbone.View
  textTemplate  : JST['programs/essentials_assessments/fill_gap/_answer']
  className     : "question feedback incorrect"

  render : =>
    $(@el).html(@textTemplate(@model.toJSON()))
    @