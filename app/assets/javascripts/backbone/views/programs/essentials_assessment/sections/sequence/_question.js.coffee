class ACC.Views.EssentialsAssessment.SequenceQuestion extends Backbone.View
  textTemplate  : JST['programs/essentials_assessments/sequence/_question']
  imageTemplate : JST['programs/essentials_assessments/sequence/_question_with_image']
  className     : "question sequence"

  render : =>
    if @model.get('answer_items')[0].reference_image_url is null
      $(@el).html(@textTemplate(@model.toJSON()))
    else
      $(@el).html(@imageTemplate(@model.toJSON()))
      $(@el).addClass("image-question")

    sortfix = ->
      $('#questions').find(".sortable").sortable()
    setTimeout sortfix, 100

    $(@el).find(".sortable").on('sortstop', @markQuestion)

    @model.generateTooltip(@el)

    @

  sortableAnswers : =>
    $(@el).find(".sortable").sortable("toArray", {attribute: "data-answer-id"})

  markQuestion : (event) =>
    userAnswer = @sortableAnswers()
    @model.set({userAnswer: userAnswer})

    correct = userAnswer.length == @model.answerInString().length and _.all( _.zip(userAnswer, @model.answerInString()), ([x,y]) -> x is y )
    @model.set({correct: correct})

    # TODO: Find out why this isn't being set in the template
    @model.set({yourAnswerOrder: @model.yourAnswerOrder(), correctAnswerOrder: @model.correctAnswerOrder()})