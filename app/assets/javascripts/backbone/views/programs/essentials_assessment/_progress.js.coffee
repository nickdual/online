class ACC.Views.EssentialsAssessment._Progress extends Backbone.View
  template  : '<div class="bar" style="width: 0%;"></div>'
  className : "progress progress-striped active"

  initialize : =>
    @model.Sections.on 'change', @updateProgress

  render : =>
    $(@el).html(@template)
    @

  updateProgress : =>
    width = @model.currentProgress()
    $(@el).find("div").attr({style: "width: #{width}%;"})