class ACC.Views.EssentialsAssessment._success extends Backbone.View
  template: JST['programs/essentials_assessments/_success']

  events :
    'click a[role=returnToProgram]' : 'goToProgram'

  render : =>
    $(@el).html(@template(@model.toJSON()))
    @

  goToProgram : (event) =>
    event.preventDefault()
    url = $(event.currentTarget).attr("href")
    ACC.Routers.Dashboard.navigate url, true