class ACC.Views.EssentialsAssessment._unSuccessful extends Backbone.View
  template: JST['programs/essentials_assessments/_unsuccessful']

  events :
    'click a[role=returnToProgram]' : 'goToProgram'
    'click a[role=tryAgain]'        : 'tryAgain'

  initialize : (attributes) =>
    @baseView = attributes.baseView

  render : =>
    $(@el).html(@template(@model.toJSON()))
    @

  goToProgram : (event) =>
    event.preventDefault()
    url = $(event.currentTarget).attr("href")
    ACC.Routers.Dashboard.navigate url, true

  tryAgain : (event) =>
    event.preventDefault()
    @model.Sections.fetch({reset: true})
    @baseView.render()