class NavigationView extends Backbone.View
  el        : '#nav'

  events  :
    'click .nav:first a'  : 'goTo'
    'click a#settingsbtn' : 'goToSettings'

  render : =>
    $(@el).html(@template(ACC.User.toJSON()))

  goTo : (event) ->
    event.preventDefault()
    href = $(event.target).attr("href")
    ACC.Routers.Dashboard.navigate href, true

  goToSettings : (event) ->
    event.preventDefault()
    ACC.Routers.Dashboard.navigate "/settings", true

  highlightTab : (tab) ->
    $(@el).find("a").removeClass("current")
    $(tab).addClass("current")

window.NavigationView = NavigationView