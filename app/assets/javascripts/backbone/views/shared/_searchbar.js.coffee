class SearchbarView extends Backbone.View
  el: 'body'

  events :
    'keypress input[type="search"]' : 'preventEvent'
    'click input[role="search"]'    : 'preventEvent'

  preventEvent : (event) =>
    if event.keyCode is 13 or event.keyCode is undefined
      event.preventDefault()

window.SearchbarView = SearchbarView