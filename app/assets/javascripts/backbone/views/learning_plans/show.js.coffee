class ACC.Views.LearningPlan extends Backbone.View
  pendingList     : '#pendingprogramslist'
  completedList   : '#completedprogramslist'
  template        : JST['learning_plans/show']
  programTemplate : JST['learning_plans/_program']

  events :
    'click .vidcontainer a': 'goToProgram'

  # TODO: Change to a model
  initialize : (plan) =>
    @plan = plan
    @plan.ProgramsPending.on 'reset', @addAllProgramsPending
    @plan.ProgramsCompleted.on 'reset', @addAllProgramsCompleted

  render : =>
    $(@el).html(@template(@plan.toJSON()))
    @

  addOneCompleted : (program) ->
    view = $(@programTemplate(program.toJSON()))
    $(@el).find(@completedList).append(view)

  addOnePending : (program) ->
    view = $(@programTemplate(program.toJSON()))
    $(@el).find(@pendingList).append(view)

  addAllProgramsCompleted : =>
    $(@el).find(@completedList).empty()

    if @plan.ProgramsCompleted.length
      @plan.ProgramsCompleted.each (program) =>
        @addOneCompleted(program)
    else
      $(@el).find(@completedList).append("<p>You have no programs completed.</p>")

  addAllProgramsPending : =>
    $(@el).find(@pendingList).empty()

    if @plan.ProgramsPending.length
      @plan.ProgramsPending.each (program) =>
        @addOnePending(program)
    else
      $(@el).find(@pendingList).append("<p>You have no programs pending.</p>")

  goToProgram : (event) ->
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Dashboard.navigate href, true