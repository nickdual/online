class ACC.Views.LearningPlanSummary extends Backbone.View
  template: JST['learning_plans/_learning_plan']
  className: 'employment_learning_plans span4'

  events :
    'click .actionable': 'goToLearningPlan'

  render : =>
    $(@el).html(@template(@model.toJSON()))
    @

  goToLearningPlan : (event) =>
    event.preventDefault()
    ACC.Routers.Dashboard.navigate "my-learning/plan/#{@model.get('id')}", true