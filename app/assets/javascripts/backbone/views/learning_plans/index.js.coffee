class ACC.Views.LearningPlans extends Backbone.View
  template  : JST['learning_plans/index']
  overdue_list: '#learningplanoverduelist'
  active_list: '#learningplanactivelist'
  completed_list: '#learningplancompletedlist'
  learningPlanTemplate: JST['learning_plans/_learning_plan']

  events :
    'click button[role="learning-records"]' : 'goToLearningRecords'
    'click .employment_learning_plans'      : 'goToLearningPlan'

  initialize : ->
    ACC.EmploymentLearningPlans.on 'reset', @addAll

  render : ->
    $(@el).html(@template())
    @addAll() if ACC.EmploymentLearningPlans.length
    @

  emptyLists: ->
    $(@el).find(@overdue_list).empty()
    $(@el).find(@active_list).empty()
    $(@el).find(@completed_list).empty()

  attach404Messages: ->
    overdue = ACC.EmploymentLearningPlans.filter (plan) ->
      plan.get('status') is 'overdue'

    if overdue.length is 0
      $(@el).find(@overdue_list).append("<p>You have no overdue learning plans.</p>")

    active = ACC.EmploymentLearningPlans.filter (plan) ->
      plan.get('status') is 'active'

    if active.length is 0
      $(@el).find(@active_list).append("<p>You have no active learning plans.</p>")

    completed = ACC.EmploymentLearningPlans.filter (plan) ->
      plan.get('status') is 'completed'

    if completed.length is 0
      $(@el).find(@completed_list).append("<p>You have no completed learning plans.</p>")

  addOne : (learning_plan) ->
    view = new ACC.Views.LearningPlanSummary({model: learning_plan})

    switch learning_plan.get('status')
      when 'overdue'
      
        $(@el).find(@overdue_list).append(view.render().el)
      when 'active'
        $(@el).find(@active_list).append(view.render().el)
      else
        $(@el).find(@completed_list).append(view.render().el)

  addAll : =>
    @emptyLists()
    ACC.EmploymentLearningPlans.each (plan) => @addOne(plan)
    $(@el).find('input.searchfield').quicksearch($(@el).find('#learningplanoverduelist > div, #learningplanactivelist > div, #learningplancompletedlist > div'))
    $(@el).find('.timeago').timeago()
    $(@el).find('.bartip').tooltip()
    @attach404Messages()
    @padPlans()

  padPlans : ->
    $(@el).find(@overdue_list).find("> div:nth-child(3n+1)").addClass("unpad")
    $(@el).find(@active_list).find("> div:nth-child(3n+1)").addClass("unpad")
    $(@el).find(@completed_list).find("> div:nth-child(3n+1)").addClass("unpad")

  goToLearningRecords : (event) ->
    event.preventDefault()
    ACC.Routers.Dashboard.navigate "my-learning/records", true

  goToLearningPlan : (event) ->
    event.preventDefault()
    plan_id = $(event.currentTarget).data('id')
    ACC.Routers.Dashboard.navigate "my-learning/plan/#{plan_id}", true