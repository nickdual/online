class ACC.Views.Dashboard extends Backbone.View
  template        : JST['dashboard/index']
  planTemplate    : JST['learning_plans/_learning_plan']
  recordTemplate  : JST['learning_records/_learning_record']

  events :
    'click .wrapper.dashboard header a' : "goToX"
    'click a[role=change-password]'     : "goToSettings"

  initialize : =>
    ACC.IncompletedLearningPlans.on 'reset', @addAllPlans, @
    ACC.RecentLearningRecords.on    'reset', @addAllRecords, @
    ACC.User.on 'change', @render, @

  render : =>
    $(@el).html(@template(ACC.User.toJSON()))
    @

  addAllRecords : ->
    $(@el).find('#summary-learning-records').empty()
    if ACC.RecentLearningRecords.length
      ACC.RecentLearningRecords.each (record) => @addOneRecord(record)
      # TODO: Scope by a better @el rather than .dashboard rubbish
      $(@el).find('#summary-learning-records .timeago').timeago()
    else
      $(@el).find('#summary-learning-records').append("<p>You have no learning records</p>")

  addOneRecord : (record) ->
    li = $(@recordTemplate(record.toJSON()))
    $(@el).find('#summary-learning-records').append(li)

  addAllPlans : ->
    $(@el).find('#learningplans .row-fluid').empty()

    if ACC.IncompletedLearningPlans.length
      ACC.IncompletedLearningPlans.each (plan) =>
        view = new ACC.Views.LearningPlanSummary({model: plan})
        $(@el).find('#learningplans .row-fluid').append(view.render().el)

      $(@el).find('#learningplans .timeago').timeago()
      $(@el).find('.bartip').tooltip()
    else
      $(@el).find('#learningplans .row-fluid').append("<p>There are currently no learning plans outstanding</p>")

  goToLearningPlan : (event) ->
    event.preventDefault()
    plan_id = $(event.currentTarget).data('id')
    ACC.Routers.Dashboard.navigate "my-learning/plan/#{plan_id}", true

  goToSettings : (event) =>
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Dashboard.navigate href, true

  goToX : (event) ->
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Dashboard.navigate href, true