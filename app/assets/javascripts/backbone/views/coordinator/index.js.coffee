class ACC.Views.Coordinator extends Backbone.View
  template        : JST['coordinator/index']
  groupTemplate   : JST['groups/_group']
  serviceTemplate : JST['services/_service']

  initialize : ->
    ACC.Groups.on   'reset', @addAllGroups, @
    ACC.Groups.on   'add',   @addGroup, @
    ACC.Services.on 'reset', @addAllServices, @
    ACC.Services.on 'add',   @addService,  @

  render : ->
    $(@el).html(@template())
    @

  addAllGroups : ->
    $(@el).find('#groupslist').empty()
    if ACC.Groups.length
      ACC.Groups.each (group) => @addGroup(group)
    else
      $(@el).find('#groupslist').append("<p>You are not the group coordinator of any groups</p>")

  addGroup : (group) =>
    li = $(@groupTemplate(group.toJSON()))
    li.click => @showGroup(group)
    $(@el).find('#groupslist').append(li)
    @

  addAllServices : ->
    $(@el).find('#serviceslist').empty()
    if ACC.Services.length
      ACC.Services.each (service) => @addService(service)
      $(@el).find('input.searchfield').quicksearch($(@el).find($(@el).find('#serviceslist > li')))
    else
      $(@el).find('#serviceslist').append("<p>You are not a coordinator of any services</p>")

  addService : (service) ->
    li = $(@serviceTemplate(service.toJSON()))
    li.click => @showService(service)
    $(@el).find('#serviceslist').append(li)
    @

  showGroup : (group) ->
    ACC.Routers.Coordinator.navigate "coordinator/groups/#{group.id}", true

  showService : (service) ->
    ACC.Routers.Service.navigate "coordinator/services/#{service.id}", true