class ACC.Views.ServiceSearch extends Backbone.View
  list            : '#searchuserslist'
  template        : JST['services/reports/search']
  resultsTemplate : JST['services/reports/results']
  userTemplate    : JST['services/users/_user']

  events :
    'click a[role="search"]'                  : 'goToSearch'
    'submit #search-users'                    : 'searchEmployments'
    'click #searchuserslist a[role="view"]'   : 'goToUser'

  # TODO: Change to a model
  initialize : (service) =>
    @service = service
    @service.JobTitles.on     'reset', @addAllJobs, @
    @service.LearningPlans.on     'reset', @addAllLearningPlans, @
    @service.Programs.on          'reset', @addAllPrograms, @
    @service.SearchEmployments.on 'reset', @addAllResults, @

  render : =>
    $(@el).html(@template(@service.toJSON()))
    @

  addAllLearningPlans : ->
    $(@el).find('select[name="completed_employment_learning_plans_learning_plan_id_eq"]').empty()
    $(@el).find('select[name="completed_employment_learning_plans_learning_plan_id_not_eq"]').empty()

    option = '<option value=""></option>'
    $(@el).find('select[name="completed_employment_learning_plans_learning_plan_id_eq"]').append(option)
    $(@el).find('select[name="completed_employment_learning_plans_learning_plan_id_not_eq"]').append(option)

    @service.LearningPlans.each (plan) =>
      option = "<option value=\"#{plan.id}\">#{plan.get('name')}</option>"
      $(@el).find('select[name="completed_employment_learning_plans_learning_plan_id_eq"]').append(option)
      $(@el).find('select[name="completed_employment_learning_plans_learning_plan_id_not_eq"]').append(option)

  addAllPrograms : ->
    $(@el).find('select[name="user_learning_records_program_id_eq"]').empty()
    $(@el).find('select[name="user_learning_records_program_id_not_eq"]').empty()
    $(@el).find('select[name="program_assessments_program_id_eq"]').empty()
    $(@el).find('select[name="program_assessments_program_id_not_eq"]').empty()

    option = "<option value=\"\"></option>"
    $(@el).find('select[name="user_learning_records_program_id_eq"]').append(option)
    $(@el).find('select[name="user_learning_records_program_id_not_eq"]').append(option)
    $(@el).find('select[name="program_assessments_program_id_eq"]').append(option)
    $(@el).find('select[name="program_assessments_program_id_not_eq"]').append(option)

    @service.Programs.each (program) =>
      option = "<option value=\"#{program.id}\">#{program.get('title')}</option>"
      $(@el).find('select[name="user_learning_records_program_id_eq"]').append(option)
      $(@el).find('select[name="user_learning_records_program_id_not_eq"]').append(option)
      $(@el).find('select[name="program_assessments_program_id_eq"]').append(option)
      $(@el).find('select[name="program_assessments_program_id_not_eq"]').append(option)

  addAllJobs : ->
    $(@el).find('select[name="job_title_id_eq"]').append('<option value=""></option>')
    @service.JobTitles.each (job) =>
      option = "<option value=\"#{job.id}\">#{job.get('name')}</option>"
      $(@el).find('select[name="job_title_id_eq"]').append(option)

  addAllResults : ->
    $(@el).find(@list).empty()

    if @service.SearchEmployments.length
      @service.SearchEmployments.each (user) =>
        user.attributes.search = true
        view = $(@userTemplate(user.toJSON()))
        $(@el).find(@list).append(view)
    else
      $(@el).find(@list).append("<p>Returned 0 results</p>")

  searchEmployments : (event) ->
    event.preventDefault() if event
    form = $(@el).find('form#search-users')
    role_eq = form.find('select[name="role_eq"]').val()
    job_title_id_eq = form.find('select[name="job_title_id_eq"]').val()
    user_learning_records_program_id_eq = form.find('select[name="user_learning_records_program_id_eq"]').val()
    user_learning_records_program_id_does_not_exist = form.find('select[name="user_learning_records_program_id_not_eq"]').val()
    completed_employment_learning_plans_learning_plan_id_eq = form.find('select[name="completed_employment_learning_plans_learning_plan_id_eq"]').val()
    completed_employment_learning_plans_learning_plan_id_does_not_exist = form.find('select[name="completed_employment_learning_plans_learning_plan_id_not_eq"]').val()
    program_assessments_program_id_eq = form.find('select[name="program_assessments_program_id_eq"]').val()
    program_assessments_program_id_does_not_exist = form.find('select[name="program_assessments_program_id_not_eq"]').val()
    $(@el).html(@resultsTemplate(@service.toJSON()))

    @service.SearchEmployments.fetch
      type: "POST"
      data:
        q:
          role_eq: role_eq
          job_title_id_eq: job_title_id_eq
          user_learning_records_program_id_eq: user_learning_records_program_id_eq
          user_learning_records_program_id_does_not_exist: user_learning_records_program_id_does_not_exist
          completed_employment_learning_plans_learning_plan_id_eq: completed_employment_learning_plans_learning_plan_id_eq
          completed_employment_learning_plans_learning_plan_id_does_not_exist: completed_employment_learning_plans_learning_plan_id_does_not_exist
          program_assessments_program_id_eq: program_assessments_program_id_eq
          program_assessments_program_id_does_not_exist: program_assessments_program_id_does_not_exist
      success: (employments) =>
        SuccessNotification.text("Successfully retrieved #{employments.length} users")
      error: =>
        ErrorNotification.text("Failed to search for employments")

  goToUser : (event) ->
    event.preventDefault()
    id = $(event.currentTarget).data('id')
    ACC.Routers.ServiceUser.navigate "coordinator/services/#{@service.id}/users/#{id}", true

  goToSearch : (event) =>
    event.preventDefault()
    $(@el).find("#serviceWrapper").html(@template(@service.toJSON())) if @service