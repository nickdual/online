class ACC.Views.ServiceCharts extends Backbone.View
  template  : JST['services/reports/index']

  events :
    'click [role=send-service-information]' : 'mailServiceInformation'

  # TODO: Change to a model
  initialize : (service) =>
    @service = service
    @service.Charts.on 'reset', @addAll, @

  render : =>
    $(@el).html(@template(@service.toJSON()))
    @

  addAll : ->
    @service.Charts.each (chart) =>
      if chart.get('type') is "pie"
        Chart.pie(chart.get('name'), chart.get('data'))
      else if chart.get('type') is "column"
        Chart.column(chart.get('name'), chart.get('data'))

    $(@el).find("#charts li").toggle()
    $(@el).find("#charts a").fancybox({
      overlayOpacity: 0.75,
      overlayColor: '#000',
      autoDimensions: false,
      width: 670,
      height: 420
    })

  mailServiceInformation : (event) =>
    event.preventDefault()
    $(event.currentTarget).parent().html("The report will be emailed to you shortly")
    href = $(event.currentTarget).attr("href")
    serviceId = $(event.currentTarget).attr("data-service-id")
    console.log(href)
    $.ajax
      url  : href,
      type: 'PUT',
      data: {serviceId : serviceId}
      success   : (response) =>
        $(event.currentTarget).parent().html(response)
