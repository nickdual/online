class ACC.Views.ServiceSessions extends Backbone.View
  template      : JST['services/group_sessions/index']
  errorTemplate : '<div class="alert alert-error">ERROR: <%= error %></div>'

  events:
    'submit #create_session'                        : 'createSession'
    'change input[name=create_program_record]'      : 'toggleMediums'
    'change input[name=create_assessment_records]'  : 'toggleCriteria'

  # TODO: Change to a model
  initialize : (service) =>
    @service = service
    @service.LearningPlans.on 'reset', @addAll, @

  render : =>
    $(@el).html(@template(@service.toJSON()))

    ACC.Views.ServiceSessionPrograms = new ServiceSessionProgramsView
      el: $(@el).find("#create_session")
      service: @service

    ACC.Views.ServiceSessionUsers = new ServiceSessionUsersView
      el: $(@el).find("#create_session")
      service: @service

    $(@el).find('input[name="viewed_at_date"]').datepicker({
      format: "dd/mm/yyyy"
    })

    if (!navigator.userAgent.match(/(iPod|iPhone|iPad)/i))
      $(@el).find('input[name="viewed_at_time"]').timePicker({
        startTime: "12:00am",
        show24Hours: false
      })

    @

  createSession : (event) ->
    event.preventDefault()
    form = $(@el).find('#create_session')
    form.find(".alert-error").remove()

    @service.GroupSessions.createSession form,
      =>
        @render()
        SuccessNotification.text("Successfully created group session")

        @service.Employments.fetch()
        @service.Programs.fetch()
      (model, errors) =>
        errorMsg = _.template(@errorTemplate, {error: "There were errors while trying to create a group session. Please check issues below."})
        $(@el).find('.error-header div').html(errorMsg)

        _.each errors, (error, name) =>
          errorMsg = _.template(@errorTemplate, {error: error})
          form.find("[name=#{name}]").parent().append(errorMsg)

        ErrorNotification.text("Failed to create session")

  toggleMediums: (event) ->
    $(@el).find('input[name=medium]').parent().toggleClass("muted")

    if $(@el).find('input[name=medium]').is(':disabled')
      $(@el).find('input[name=medium]').attr({disabled: false})
    else
      $(@el).find('input[name=medium]').attr({disabled: true}).attr({checked: false})

  toggleCriteria: (event) ->
    _.each ['essentials', 'extension', 'evidence'], (criteria) ->
      checkBox = $("input[name=#{criteria}]")
      checkBox.parent().toggleClass("muted")

      if checkBox.is(':disabled')
        checkBox.attr({disabled: false})
      else
        checkBox.attr({disabled: true}).attr({checked: false})