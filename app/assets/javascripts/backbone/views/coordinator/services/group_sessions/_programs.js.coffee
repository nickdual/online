class ProgramsView extends Backbone.View
  autocompleteInput : 'input[name=programs-autocomplete]'
  programResults    : '#programs-results'
  programTemplate   : '<li class="item" data-id="<%= id %>"><%= title %></li>'

  events :
    'click #programs-results li.item:not(.active)': 'selectProgram'
    "keyup input[name=programs-autocomplete]"     : 'autocomplete'

  initialize: (attributes) ->
    @service = attributes.service
    @service.Programs.on 'reset', @clearResults

  autocomplete : (event) =>
    switch event.keyCode
      when 8 # Backspace
        search = $(@el).find(@autocompleteInput).val()

        if search is ""
          @clearResults()
        else
          @search()
      else
        @search()

  clearResults : =>
    $(@el).find(@programResults).html("<li>No results</li>")

  search : =>
    input = $(@el).find(@autocompleteInput).val()
    $(@el).find(@programResults).empty()

    @service.Programs.each (model) =>
      attributes = model.get('to_label')
      matcher = new RegExp(@escapeRegex(input), 'i');
      matches = @iterator(attributes, matcher)
      if matches
        @addResult(model)
    @SearchResults

  addResult : (program) =>
    option = _.template(@programTemplate, program.toJSON())
    $(@el).find(@programResults).append(option)

  escapeRegex: (value) ->
    value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")

  iterator : (attributes, matcher) ->
    matcher.test(attributes)

  selectProgram : (event) ->
    event.preventDefault()
    $(@el).find(@programResults).find('li').removeClass("selected")
    $(event.currentTarget).toggleClass("selected")
    programTitle = $(event.currentTarget).html()
    $(@el).find('.selected-program-title').html(programTitle)

window.ServiceSessionProgramsView = ProgramsView