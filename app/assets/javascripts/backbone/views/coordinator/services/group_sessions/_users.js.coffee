class UsersView extends Backbone.View
  autocompleteInput : 'input[name=users-autocomplete]'
  userResults       : '#users-form-results'
  usersChosen       : '#users-form-chosen'
  userTemplate      : '<li class="item user" data-id="<% if (typeof(employment_name) != "undefined") { %><%= employment_id %><% } else { %><%= id %><% } %>"><span class="icon">U</span> <% if (typeof(employment_name) != "undefined") { %><%= employment_name %><% } else { %><%= name %><% } %></li>'
  jobTemplate       : '<li class="item role" data-id="<%= id %>" title="<%= name %>"><span class="icon">G</span> Job Title: <%= truncated_name %><span class="label label-info pull-right"><%= users_count %> User<% if (users_count > 1) { %>s<% } %></span></li>'
  noJobTemplate     : '<li class="item role" data-id="null"><span class="icon">G</span> No Job Title Assigned<span class="label label-info pull-right"><%= users_count %> User<% if (users_count > 1) { %>s<% } %></span></li>'

  events :
    'click .add'                                    : 'addUsers'
    'click .remove'                                 : 'removeUsers'
    'click #users-form-results li.item:not(.active)': 'selectUser'
    'click #users-form-chosen li.item:not(.active)' : 'selectUser'
    'keyup input[name=users-autocomplete]'          : 'autocomplete'

  initialize: (attributes) ->
    @service = attributes.service
    @service.Employments.on 'reset', @clearResults

  autocomplete : (event) ->
    switch event.keyCode
      when 8
        search = $(@el).find(@autocompleteInput).val()

        if search is ""
          @clearResults()
        else
          @search()
      when 13
        # We're hitting the return button, don't submit anything
        event.preventDefault()
      else
        @search()

  addUserResult : (employment) =>
    @findOrInsertJobTitle(employment.get('job_title_id'))

    option = _.template(@userTemplate, employment.toJSON())
    $(@el).find(@userResults).find("[data-id=#{employment.get('job_title_id')}]").after(option)

    # If the user is already selected, mark it as active
    if _.indexOf(@selected_ids(), employment.get('id')) >= 0
      employment = $(@el).find(@userResults).find("[data-id=#{employment.get('id')}]")
      employment.addClass("active")

  findOrInsertJobTitle : (job_title_id) ->
    job = @service.JobTitles.get(job_title_id)

    # Don't insert the job title twice
    unless $(@el).find(@userResults).find("[data-id=#{job_title_id}]").length
      if job
        job.attributes.users_count = @service.Employments.filter((employment) => employment.get('job_title_id') is job_title_id).length
        option = _.template(@jobTemplate, job.toJSON())
      else
        job = {users_count: @service.Employments.filter((employment) => employment.get('job_title_id') is null).length}
        option = _.template(@noJobTemplate, job)

      $(@el).find(@userResults).append(option)
      $(@el).find("#{@userResults} li").tooltip()

  addSelectedUser : (employment) ->
    # Don't want to add duplicates
    unless $(@el).find(@usersChosen).find("[data-id=#{employment.get('id')}]").length
      option = _.template(@userTemplate, employment.toJSON())
      $(@el).find(@usersChosen).append(option)

  clearResults : =>
    $(@el).find(@userResults).html("<li>No results</li>")

  search : =>
    input = $(@el).find(@autocompleteInput).val()
    $(@el).find(@userResults).empty()

    @service.Employments.each (model) =>
      attributes = model.get('label')
      matcher = new RegExp(@escapeRegex(input), 'i');
      matches = @iterator(attributes, matcher)
      if matches
        @addUserResult(model)

  escapeRegex: (value) ->
    value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")

  iterator : (attributes, matcher) ->
    matcher.test(attributes)

  selected_ids : ->
    _.map($(@el).find(@usersChosen).find("li"), (li) => $(li).data('id'))

  selectUser : (event) ->
    event.preventDefault()
    $(event.currentTarget).toggleClass("selected")

  addUsers : (event) ->
    event.preventDefault()
    $(@el).find(@usersChosen).find("li:not(.item)").remove()

    _.each $(@el).find(@userResults).find(".role.selected"), (job) =>
      job_id = $(job).data("id")
      $(@el).find(@userResults).find(".role[data-id=#{job_id}]").remove()

      users = @service.Employments.filter (user) =>
        user.get('job_title_id') is job_id

      _.each users, (user) =>
        @addSelectedUser(user)
        $(@el).find(@userResults).find(".user[data-id=#{user.get('id')}]").remove()

    _.each $(@el).find(@userResults).find(".user.selected"), (user) =>
      user_id = $(user).data("id")
      $(user).remove()
      user = @service.Employments.get(user_id)
      @addSelectedUser(user)

  removeUsers : (event) ->
    event.preventDefault()
    $(@el).find(@usersChosen).find(".selected").remove()
    @noUsersAdded() unless $(@el).find(@usersChosen).find("li").length

  noUsersAdded : ->
    $(@el).find(@usersChosen).append("<li>No Users Added</li>")

window.ServiceSessionUsersView = UsersView