class ACC.Views.ServiceUser extends Backbone.View
  template: JST['services/users/show']

  events :
    'click #user_nav a'       : 'goToX'
    'click [role=edit]'       : 'goToX'
    'click a[role="remove"]'  : 'remove'

  initialize : (attributes) =>
    @service  = attributes.service

  render : =>
    $(@el).html(@template(@model.toJSON()))
    $(@el).find('.bartip').tooltip()
    @

  showPlan: (plan) ->
    view = new ACC.Views.ServiceUserPlan(@model, plan)
    $(@el).find("#user_details").html(view.render().el)

  showPlans: ->
    view = new ACC.Views.ServiceUserPlans(@service, @model)
    $(@el).find("#user_details").html(view.render().el)

  showRecords: =>
    view = new ACC.Views.ServiceUserRecords(@service, @model)
    $(@el).find("#user_details").html(view.render().el)

  goToX : (event) ->
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.ServiceUser.navigate href, true

  remove : (event) ->
    event.preventDefault()
    confirm_dialog = confirm("Are you sure you want to terminate #{@model.get('name')}'s employment from #{@model.get('service_name')}")

    if confirm_dialog is true
      @model.destroy
        success: (user) =>
          SuccessNotification.text("Successfully removed user from service")
          ACC.Routers.ServiceUser.navigate "/coordinator/services/#{@model.get('service_id')}", true
        error: =>
          ErrorNotification.text("Failed to remove use from service")