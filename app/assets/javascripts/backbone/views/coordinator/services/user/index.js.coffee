class ACC.Views.ServiceUsers extends Backbone.View
  list          : "#usersList"
  template      : JST['services/users/index']

  events :
    'click #new_user'       : 'toggleNewUsers'
    'click #create_user a'  : 'toggleNewUsers'
    'submit #create_user'   : 'createUser'
    'click a[role="cancel"]': 'hideEdit' # This is for the edit view we load over everything

  # TODO: Change to a model
  initialize : (service) =>
    @service = service
    @service.Employments.on 'reset', @addAll, @
    @service.Employments.on 'sync', @addAll, @

  render : =>
    $(@el).html(@template(@service.toJSON()))
    @

  addAll : =>
    $(@el).find(@list).empty()

    @service.Employments.each (user) =>
      view = new ACC.Views.Service._user({model: user, parentView: @})
      $(@el).find(@list).append(view.render().el)

    $(@el).find('input.searchfield').quicksearch($(@el).find(@list).find("> li"))

  # TODO: Proper garbage collection on this edit form
  hideEdit : (event) =>
    event.preventDefault()
    @addAll()

  toggleNewUsers : (event) ->
    event.preventDefault()
    $(@el).find("#search,.users,#create_user").toggle()

  createUser : (event) ->
    event.preventDefault()
    form = $(@el).find('#create_user')
    form.find(".help-inline").remove()
    form.find(".control-group").removeClass("error")

    # TODO: Put this into the model
    @service.Employments.create
      first_name: form.find('input[name="first_name"]').val()
      last_name:  form.find('input[name="last_name"]').val()
      email:      form.find('input[name="email"]').val()
      role:       form.find('select[name="role"]').val()
      service_id: @service.get('id')
      user:
        first_name:       form.find('input[name="first_name"]').val()
        last_name:        form.find('input[name="last_name"]').val()
        email:            form.find('input[name="email"]').val()
        role:             form.find('select[name="role"]').val()
        activation_token: @service.get('activation_token')
      {success: (user) =>
        SuccessNotification.text("Successfully added user")
        form.find('input[name="first_name"]').val("")
        form.find('input[name="last_name"]').val("")
        form.find('input[name="email"]').val("")
        form.find('select[name="role"]').val("staff")

        user.EmploymentLearningPlans.url  = "/api/employments/#{user.get('id')}/learning_plans"
        user.LearningRecords.url          = "/api/employments/#{user.get('id')}/activities"
        user.JobTitles.url                = "/api/services/#{user.get('service_id')}/job_titles"
        user.ProgramAssessment.url        = "/api/employments/#{user.get('id')}/program_assessments"
      error: (employment, errors) =>
        _.each errors, (error, name) ->
          form.find("[name=#{name}]").parent().parent().addClass("error")
          form.find("[name=#{name}]").parent().append("<span class=\"help-inline\">#{error}</span>")
          ErrorNotification.text("Failed to add user")}