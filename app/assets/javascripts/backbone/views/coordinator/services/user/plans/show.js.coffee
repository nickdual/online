class ACC.Views.ServiceUserPlan extends Backbone.View
  menu                  : "#user_nav"
  pendingList           : '#pendingprogramslist'
  completedList         : '#completedprogramslist'
  template              : JST['services/users/plans/show']
  programTemplate       : JST['services/users/plans/_program']

  events :
    'click a[role="view-plan"]'                   : 'goToPlan'
    'click #pendingprogramslist a[role="access"]' : 'accessUser'
    'click .vidcontainer a'                       : 'goToProgram'

  initialize : (user, plan) =>
    @user = user
    @plan = plan

  render : =>
    $(@el).html(@template(@plan.toJSON()))

    $(@menu).find("li").removeClass("active")
    $(@menu).find('a[role="plans"]').parent().addClass("active")

    @plan.ProgramsPending.on 'reset', @addAllProgramsPending
    @plan.ProgramsCompleted.on 'reset', @addAllCompletedPrograms
    @

  addOnePending : (program) ->
    if @plan.get('days_till_active') > 0
      program.attributes.inactive_plan = true
    view = $(@programTemplate(program.toJSON()))
    $(@el).find(@pendingList).append(view)

  addOneCompleted : (program) ->
    view = $(@programTemplate(program.toJSON()))
    $(@el).find(@completedList).append(view)

  addAllCompletedPrograms : =>
    $(@el).find(@completedList).empty()

    if @plan.ProgramsCompleted.length
      @plan.ProgramsCompleted.each (program) =>
        @addOneCompleted(program)
    else
      $(@el).find(@completedList).append("<p>#{@user.get('name')} has no programs completed for this learning plan.</p>")

  addAllProgramsPending : =>
    $(@el).find(@pendingList).empty()

    if @plan.ProgramsPending.length
      @plan.ProgramsPending.each (program) =>
        @addOnePending(program)
    else
      $(@el).find(@pendingList).append("<p>#{@user.get('name')} has no pending programs for this learning plan.</p>")

  goToProgram : (event) ->
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Dashboard.navigate href, true

  accessUser : (event) ->
    event.preventDefault()
    programId       = $(event.currentTarget).data("id")
    assessmentType  = $(event.currentTarget).data("assessment-type")

    # TODO: Move into model
    @user.ProgramAssessment.create
      program_id:       programId
      employment_id:    @user.id
      assessment_type:  assessmentType
      coordinator_id:   ACC.User.get('id')
      {success: =>
        SuccessNotification.text("Successfully assessed program")
        @plan.ProgramsPending.fetch()
        @plan.ProgramsCompleted.fetch()
        ACC.LearningRecords.fetch()
        ACC.RecentLearningRecords.fetch()
        ACC.IncompletedLearningPlans.fetch()
        ACC.EmploymentLearningPlans.fetch()
        ACC.User.fetch()
      error: (assessment, error) =>
        ErrorNotification.text("Failed to assess program")}

  goToPlan : (event) ->
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Service.navigate href, true