class ACC.Views.Service.UserEdit extends Backbone.View
  className : "box records unstyled"

  events :
    'click [role=cancel]': 'goToX'

  initialize : (attributes) =>
    @serviceView  = attributes.serviceView
    @service      = @serviceView.service

  render : =>
    @editForm = new ACC.Views.Service.UsersQuickEdit({model: @model, service: @service})
    $(@el).html(@editForm.render().el)
    @

  goToX : (event) ->
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.ServiceUser.navigate href, true