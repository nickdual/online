class ACC.Views.ServiceUserRecords extends Backbone.View
  menu                  : "#user_nav"
  list                  : 'ul#recordslist'
  template              : JST['services/users/_records']
  learningRecordTemplate: JST['learning_records/_learning_record']

  initialize : (service, user) =>
    @service  = service
    @user     = user
    @user.LearningRecords.on 'reset', @addAll

  render : ->
    $(@el).html(@template(@user.toJSON()))
    $(@menu).find("li").removeClass("active")
    $(@menu).find('a[role="records"]').parent().addClass("active")
    @

  addOne : (learning_record) ->
    view = $(@learningRecordTemplate(learning_record.toJSON()))
    $(@el).find(@list).append(view)

  addAll : =>
    $(@el).find(@list).empty()

    if @user.LearningRecords.length
      @user.LearningRecords.each (record) => @addOne(record)
      $(@el).find('input.searchfield').quicksearch($(@el).find('ul#recordslist > li'))
      $(@el).find(@list).find('.timeago').timeago()
    else
      $(@el).find(@list).append("<p>This user has no learning records.</p>")