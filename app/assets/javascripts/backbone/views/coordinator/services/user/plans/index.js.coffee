class ACC.Views.ServiceUserPlans extends Backbone.View
  menu                  : "#user_nav"
  list                  : '#planslist'
  template              : JST['services/users/plans/index']
  learningPlanTemplate  : JST['services/users/plans/_plan']

  events :
    'click #planslist > li[data-context="user"]'     : 'goToPlan'

  initialize : (service, user) =>
    @service  = service
    @user     = user
    @user.EmploymentLearningPlans.on 'reset', @addAll

  render : ->
    $(@el).html(@template(@user.toJSON()))
    $(@menu).find("li").removeClass("active")
    $(@menu).find('a[role="plans"]').parent().addClass("active")
    @

  addOne : (plan) ->
    # TODO: Bump this into a model
    plan.attributes.limited_programs = _.filter(plan.attributes.programs_remaining, (element, index) -> (true if index < 3))

    view = $(@learningPlanTemplate(plan.toJSON()))
    $(@el).find(@list).append(view)

  addAll : =>
    $(@el).find(@list).empty()

    if @user.EmploymentLearningPlans.length
      @user.EmploymentLearningPlans.each (record) => @addOne(record)
      $(@el).find('input.searchfield').quicksearch($(@el).find("ul#{@list} > li"))
      $(@el).find(@list).find('.timeago').timeago()
      $(@el).find(@list).find("> li:nth-child(3n+1)").addClass("unpad")
    else
      $(@el).find(@list).append("<p>This user has no learning plans.</p>")

  goToPlan : (event) ->
    event.preventDefault() if event
    id = $(event.currentTarget).data('id')
    ACC.Routers.ServiceUser.navigate "coordinator/services/#{@user.get('service_id')}/users/#{@user.id}/plans/#{id}", true