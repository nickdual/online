class ACC.Views.Service.UsersQuickEdit extends Backbone.View
  template      : JST['services/users/_edit_form']
  errorTemplate : '<div class="alert alert-error">ERROR: <%= error %></div>'

  events :
    'submit form'           : 'updateUser'
    'click [role=settings]' : 'goToX'

  initialize : (attributes) =>
    @serviceView  = attributes.serviceView
    @service      = attributes.service
    @service.JobTitles.on 'reset', @loadJobTitles

  render: =>
    $(@el).html(@template(@model.toJSON()))
    @selectRole()
    @loadEmailField()

    if @service.JobTitles.length > 0
      @loadJobTitles()

    @

  loadJobTitles : =>
    $(@el).find('[name=job_title_id]').empty()

    blankOption = "<option value=\"\"></option>"
    $(@el).find('[name=job_title_id]').append(blankOption)

    @service.JobTitles.each (job_title) =>
      option = "<option value=\"#{job_title.get('id')}\">#{job_title.get('name')}</option>"
      $(@el).find('[name=job_title_id]').append(option)

    _.defer =>
      $(@el).find('[name=job_title_id]').val(@model.get('job_title_id'))

  loadEmailField : =>
    if @model.get('resent_welcome_email') is false
      if @model.get('last_email_status') is 'bounced' || @model.get('last_email_status') is 'soft-bounced'
        $(@el).find('[name=email]').parent().parent().addClass("error")

  selectRole : =>
    $(@el).find('[name=role]').val(@model.get('role'))

  updateUser: (event) ->
    event.preventDefault()
    form = $(@el).find('form')
    form.find(".alert-error").remove()

    # TODO: User toJSON to remove this duplication
    @model.save
      employee_number : form.find('input[name="employee_number"]').val()
      role            : form.find('select[name="role"]').val()
      job_title_name  : form.find('select[name="job_title_id"] :selected').html()
      job_title_id    : form.find('select[name="job_title_id"]').val()
      email           : form.find('input[name="email"]').val()
      employment:
        employee_number : form.find('input[name="employee_number"]').val()
        role            : form.find('select[name="role"]').val()
        job_title_name  : form.find('select[name="job_title_id"] :selected').html()
        job_title_id    : form.find('select[name="job_title_id"]').val()
        email           : form.find('input[name="email"]').val()
      {url: "/api/services/#{@service.id}/employments/#{@model.id}"
      success: (employment) =>
        SuccessNotification.text("Successfully updated user")

        if @serviceView is undefined
          ACC.Routers.ServiceUser.navigate "/coordinator/services/#{@model.get('service_id')}/users/#{@model.get('id')}", true
        else
          @serviceView.addAll()

        # TODO: Change to be hooks at the model level
        @model.EmploymentLearningPlans.fetch()
        @model.LearningRecords.fetch()
      error: (user, errors) =>
        # TODO: Make this happening in the model
        emailErrors = $.parseJSON(errors.responseText).errors["user.email"]

        if emailErrors.length > 0
          _.each emailErrors, (error) =>
            errorMsg = _.template(@errorTemplate, {error: error})
            form.find("[name=email]").parent().append(errorMsg)
            form.find("[name=email]").parent().parent().addClass("error")

        ErrorNotification.text("Error updating user details")}

  goToX : (event) ->
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.ServiceUser.navigate href, true