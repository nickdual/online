class ACC.Views.Service._user extends Backbone.View
  tagName   : "li"
  className : "record record-split"
  template  : JST['services/users/_user']

  events:
    'click a[role="remove"]'  : 'remove'
    'click a[role="view"]'    : 'goToUser'
    'click a[role="edit"]'    : 'showEdit'

  initialize : (attributes) =>
    @parentView = attributes.parentView
    @service    = @parentView.service
    @model.on 'change', @render

  render : =>
    @model.set(recentlyResetWelcomeEmail: @model.recentlyResetWelcomeEmail())
    $(@el).html(@template(@model.toJSON()))
    $(@el).find('.bartip').tooltip()
    @

  remove : (event) ->
    event.preventDefault()
    confirm_dialog = confirm("Are you sure you want to terminate #{@model.get('name')}'s employment from #{@model.get('service_name')}")

    if confirm_dialog is true
      @model.destroy
        url: "/api/services/#{@model.get('service_id')}/employments/#{@model.get('id')}" # TODO: Remove the need for this
        success: (user) =>
          SuccessNotification.text("Successfully removed user from service")
          $(@el).remove()
        error: =>
          ErrorNotification.text("Failed to remove use from service")

  goToUser : (event) ->
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.ServiceUser.navigate href, true

  showEdit : (event) =>
    event.preventDefault()
    @editForm = new ACC.Views.Service.UsersQuickEdit({model: @model, serviceView: @parentView, service: @service})
    $(@parentView.el).find(@parentView.list).html(@editForm.render().el)