class ACC.Views.Service extends Backbone.View
  template  : JST["services/show"]

  events :
    'click .services-nav.btn-group a'      : 'goToPage'
    'click a[role="index"]'   : 'goToCharts'
    'click a[role="search"]'  : 'goToSearch'

  # TODO: Change to a model
  initialize : (service) =>
    @service = service

  render : =>
    $(@el).html(@template(@service.toJSON()))
    @

  highlightTab : (href) =>
    $(@el).find(".btn-group a").removeClass("active")
    $(@el).find(".btn-group a[href='/#{href}']").addClass("active")

  goToPage : (event) =>
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Service.navigate href, true

  goToCharts : (event) =>
    event.preventDefault()
    ACC.Routers.ServiceUser.navigate "coordinator/services/#{@service.id}/reports", true

  goToSearch : (event) =>
    event.preventDefault()
    ACC.Routers.ServiceUser.navigate "coordinator/services/#{@service.id}/reports/search", true