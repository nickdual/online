class ACC.Views.ServicePlanUsers extends Backbone.View
  menu              : "#plan_nav"
  list              : "#userslist"
  template          : JST['services/plans/_users']
  userTemplate      : JST['services/plans/_user']

  events :
    'click #userslist a[role="remove"]'           : 'removeUser'
    'click a[role="view-user-plan"]'              : 'goToUser'

  # TODO: Change to a model
  initialize : (service, plan) =>
    @service = service
    @plan = plan
    @plan.EmploymentLearningPlans.on 'reset', @addAll, @

  render : =>
    $(@el).html(@template(@plan.toJSON()))
    $(@menu).find("li").removeClass("active")
    $(@menu).find("a[role=\"plan_users\"]").parent().addClass("active")
    @

  addOne : (user) ->
    view = $(@userTemplate(user.toJSON()))
    $(@el).find(@list).append(view)

  addAll : =>
    $(@el).find(@list).empty()

    if @plan.EmploymentLearningPlans.length
      @plan.EmploymentLearningPlans.each (record) => @addOne(record)
    else
      $(@el).find(@list).append("<p>This plan has no users assigned to it.</p>")

  removeUser : (event) ->
    event.preventDefault()
    id = $(event.currentTarget).data('id')
    user = @plan.EmploymentLearningPlans.get(id)
    confirm_dialog = confirm("Are you sure you want to remove #{user.get('employment_name')}?")

    if confirm_dialog is true
      user.destroy
        url: "/api/learning_plans/#{@plan.id}/employment_learning_plans/#{id}"
        success: (employment) =>
          SuccessNotification.text("Successfully removed user")
          @addAll()

          @plan.fetch
            url: "/api/services/#{@service.id}/learning_plans/#{@plan.id}"
        error: ->
          ErrorNotification.text("Failed to remove user")

  goToUser : (event) ->
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.ServiceUser.navigate href, true