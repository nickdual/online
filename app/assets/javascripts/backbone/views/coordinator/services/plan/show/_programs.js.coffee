class ACC.Views.ServicePlanPrograms extends Backbone.View
  menu    : "#plan_nav"
  list    : "#programsList"
  template: JST['services/plans/_programs']

  # TODO: Change to a model
  initialize : (service, plan) =>
    @service = service
    @plan = plan
    @plan.Programs.on 'reset', @addAll

  render : ->
    $(@el).html(@template(@plan.toJSON()))
    $(@menu).find("li").removeClass("active")
    $(@menu).find("a[role=\"plan_programs\"]").parent().addClass("active")
    @

  addOne : (program) ->
    view = new ACC.Views.SharedProgram(program)
    $(@el).find(@list).append(view.render().el)

  addAll : =>
    $(@el).find(@list).empty()

    if @plan.Programs.length
      @plan.Programs.each (record) => @addOne(record)
    else
      $(@el).find(@list).append("<p>This plan has no programs assigned to it.</p>")