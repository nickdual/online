class ACC.Views.ServicePlanNew extends Backbone.View
  template      : JST['services/plans/new']
  formTemplate  : JST['services/plans/_form']

  events :
    'click  a[role=cancel]'             : 'goToPlans'
    'submit form'                       : 'createPlan'
    'click  .assessment-criteria input' : 'checkBoxes'

  render : =>
    $(@el).html(@template(@model.toJSON()))
    $(@el).append(@formTemplate({service_id: @model.id}))

    ACC.Views.ServicePlanFormPrograms = new ServicePlanProgramsFormView({model: @model})
    $(@el).find('#programsearch').html(ACC.Views.ServicePlanFormPrograms.render().el)

    ACC.Views.ServicePlanFormUsers    = new ServicePlanUsersFormView({model: @model})
    $(@el).find('#usersearch').html(ACC.Views.ServicePlanFormUsers.render().el)


    $(@el).find('[name="active_at"]').datepicker({dateFormat: "dd/mm/yy"})

    @

  createPlan : (event) =>
    event.preventDefault()

    name            = $(@el).find('input[name="name"]').val()
    days_till_due   = $(@el).find('input[name="days_till_due"]').val()
    active_at       = $(@el).find('input[name="active_at"]').val()
    description     = $(@el).find('textarea[name="description"]').val()
    program_ids     = ACC.Views.ServicePlanFormPrograms.selectedCriteria()
    employment_ids  = ACC.Views.ServicePlanFormUsers.selected_ids()

    $(@el).find(".help-inline").remove()

    @model.LearningPlans.create
      name              : name
      days_till_due     : days_till_due
      description       : description
      active_at         : active_at
      learning_plan_programs_attributes: program_ids
      employment_ids    : employment_ids
      learning_plan :
        name              : name
        days_till_due     : days_till_due
        description       : description
        active_at         : active_at
        learning_plan_programs_attributes       : program_ids
        employment_ids    : employment_ids
      programs_count    : []
      employments_count : []
      {url: "/api/services/#{@model.id}/learning_plans"
      success: (plan) =>
        SuccessNotification.text("Successfully created learning plan")
        ACC.Routers.Service.navigate "coordinator/services/#{@model.id}/learning-plans/#{plan.id}/users", true
      error: (plan, errors) =>
        _.each errors, (error, name) =>
          $(@el).find("[name=#{name}]").parent().parent().addClass("error")
          $(@el).find("[name=#{name}]").parent().append("<span class=\"help-inline\">#{error}</span>")

        ErrorNotification.text("Failed to create learning plan")
      }

  goToPlans : (event) =>
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Service.navigate href, true

  checkBoxes : (event) =>
    target = $(event.currentTarget).data("target")
    checkboxes = $(@el).find("#programCriteria tbody").find(target).find("input:visible")

    if $(event.currentTarget).attr("checked") is 'checked'
      checkboxes.attr({"checked": true})
    else
      checkboxes.attr({"checked": false})