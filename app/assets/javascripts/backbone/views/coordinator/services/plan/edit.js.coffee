class ACC.Views.ServicePlanEdit extends Backbone.View
  template      : JST['services/plans/_form']
  programsChosen    : '#programs-form-chosen'
  programTemplate   : '<li class="item" data-id="<%= id %>"><%= title %></li>'

  events :
    'click a[role=cancel]' : 'goToPlanUsers'
    'submit form'          : 'updatePlan'

  # TODO: Change to a model
  initialize : (service, plan) =>
    @service = service
    @plan = plan
    @plan.EmploymentLearningPlans.on 'reset', @addAllUsers, @

  render : =>
    $(@el).html(@template(@plan.toJSON()))

    _.defer =>
      ACC.Views.ServicePlanFormUsers    = new ServicePlanUsersFormView({model: @service})
      $(@el).find('#usersearch').html(ACC.Views.ServicePlanFormUsers.render().el)

    $(@el).find('[name="active_at"]').datepicker({ dateFormat: "dd/mm/yy" })

    @

  updatePlan : (event) =>
    event.preventDefault()
    $(@el).find(".help-inline").remove()

    # LearningPlan updates only allow name, description and user changes.
    @plan.save
      name          : $(@el).find('[name="name"]').val()
      description   : $(@el).find('[name="description"]').val()
      learning_plan:
        name          : $(@el).find('[name="name"]').val()
        description   : $(@el).find('[name="description"]').val()
        employment_ids: ACC.Views.ServicePlanFormUsers.selected_ids()
      {url: "/api/services/#{@service.id}/learning_plans/#{@plan.id}"
      success: (plan) =>
        SuccessNotification.text("Successfully updated learning plan")
        @goToPlanUsers()
      error: (plan, errors) =>
        _.each errors, (error, name) ->
          form.find("[name=#{name}]").parent().parent().addClass("error")
          form.find("[name=#{name}]").parent().append("<span class=\"help-inline\">#{error}</span>")

        ErrorNotification.text("There was an error updating this learning plan")
      }

  addAllUsers: =>
    $(@el).find('#users-form-chosen').html("")

    unless @plan.EmploymentLearningPlans.length
      ACC.Views.ServicePlanFormUsers.noUsersAdded()
    @plan.EmploymentLearningPlans.each (employment) =>
      ACC.Views.ServicePlanFormUsers.addSelectedUser(employment)

  goToPlanUsers : (event) ->
    event.preventDefault() if event
    ACC.Routers.ServiceUser.navigate "coordinator/services/#{@service.id}/learning-plans/#{@plan.id}/users", true