# This view is used in the main view for editing / creating a learning plan
class ProgramsView extends Backbone.View
  autocompleteInput       : 'input.searchfield'
  programResults          : '#programs-form-results'
  programsChosen          : '#programs-form-chosen'
  programCriteria         : '#programCriteria tbody'
  template                : JST['services/plans/form/_programs']
  programTemplate         : '<li class="item" data-id="<%= id %>"><%= title %></li>'
  programCriteriaTemplate : JST['services/plans/form/_assessment']

  events :
    'click a.add'                 : 'addPrograms'
    'click a.remove'              : 'removePrograms'
    'click li.item:not(.active)'  : 'selectProgram'
    'keyup input.searchfield'     : 'autocomplete'
    'submit form'                 : 'preventFormSubmission'

  render : =>
    $(@el).html(@template(@model))
    @

  autocomplete : (event) =>
    switch event.keyCode
      when 8 # Backspace
        search = $(@el).find(@autocompleteInput).val()

        if search is ""
          @clearResults()
        else
          @search()
      else
        @search()

  addResult : (program) =>
    option = _.template(@programTemplate, program.toJSON())
    $(@el).find(@programResults).append(option)

    # If the program is already selected, mark it as active
    if _.indexOf(@selected_ids(), program.get('id')) >= 0
      program = $(@el).find(@programResults).find("[data-id=#{program.get('id')}]")
      program.addClass("active")

  addSelected : (program) ->
    template = _.template(@programTemplate, program.toJSON())
    $(@el).find(@programsChosen).append(template)

    template = @programCriteriaTemplate(program.toJSON())
    $(@el).find(@programCriteria).append(template)

  clearResults : ->
    $(@el).find(@programResults).html("<li>No results</li>")

  search : =>
    input = $(@el).find(@autocompleteInput).val()
    $(@el).find(@programResults).empty()

    @model.Programs.each (model) =>
      attributes = model.get('to_label')
      matcher = new RegExp(@escapeRegex(input), 'i');
      matches = @iterator(attributes, matcher)
      if matches
        @addResult(model)
    @SearchResults

  escapeRegex: (value) ->
    value.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&")

  iterator : (attributes, matcher) ->
    matcher.test(attributes)

  selectedCriteria : =>
    _.map $(@el).find(@programCriteria).find("tr.assessment"), (tr) =>
      program_id: $(tr).data("id")
      essentials: true
      extension:  $(tr).find('input[name=extension]').attr("checked") is "checked"
      evidence:   $(tr).find('input[name=evidence]').attr("checked") is "checked"

  selected_ids : ->
    _.map($(@el).find(@programsChosen).find("li"), (li) => $(li).data('id'))

  selectProgram : (event) ->
    event.preventDefault()
    $(event.currentTarget).toggleClass("selected")

  removePrograms : (event) ->
    event.preventDefault()

    # TODO: Move programs into their own view
    selectedsIds = _.map($(@el).find(@programsChosen).find("li.selected"), (li) => $(li).data('id'))
    _.each selectedsIds, (programId) =>
      $(@el).find(@programCriteria).find("tr[data-id=#{programId}]").remove()

    $(@el).find(@programsChosen).find(".selected").remove()
    @noProgramsAdded() unless $(@el).find(@programsChosen).find("li").length

  addPrograms : (event) ->
    event.preventDefault()
    $(@el).find(@programsChosen).find("li:not(.item)").remove()

    _.each $(@el).find(@programResults).find(".selected"), (program) =>
      program_id = $(program).data("id")
      $(program).remove()
      program = @model.Programs.get(program_id)
      @addSelected(program)

  noProgramsAdded : ->
    $(@el).find(@programsChosen).append("<li>No Programs Added</li>")

  preventFormSubmission : (event) ->
    event.preventDefault()

window.ServicePlanProgramsFormView = ProgramsView