class ACC.Views.ServicePlan extends Backbone.View
  form              : '#update_plan'
  template          : JST['services/plans/show']
  detailsTemplate   : JST['services/plans/_details']
  formTemplate      : JST['services/plans/_form']
  programsChosen    : '#programs-form-chosen'
  programTemplate   : '<li class="item" data-id="<%= id %>"><%= title %></li>'

  events :
    'click a#delete_plan'           : 'removePlan'
    'click a.edit_plan'             : 'goToPlanEdit'
    'click a[role="plan_users"]'    : 'goToPlanUsers'
    'click a[role="plan_programs"]' : 'goToPlanPrograms'

  # TODO: Change to a model
  initialize : (service, plan) =>
    @service = service
    @plan = plan
    @plan.on 'change', @loadDetails, @

  render : =>
    $(@el).html(@template(@plan.toJSON()))
    @loadDetails()
    $(@el).find(".timeago").timeago()
    @

  loadDetails : ->
    $(@el).find("#show_plan").html(@detailsTemplate(@plan.toJSON()))

  removePlan : (event) ->
    event.preventDefault()
    confirm_dialog = confirm("Are you sure you want to remove this learning plan?")

    if confirm_dialog is true
      @plan.destroy
        url: "/api/services/#{@service.id}/learning_plans/#{@plan.id}"
        success: =>
          ACC.Routers.Dashboard.navigate "coordinator/services/#{@service.id}/learning-plans", true
          SuccessNotification.text("Successfully removed learning plan")
        error: =>
          ErrorNotification.text("There was an error removing this learning plan")

  showUsers: =>
    view = new ACC.Views.ServicePlanUsers(@service, @plan)
    $(@el).find("#plan_details").html(view.render().el)

  showPrograms: ->
    view = new ACC.Views.ServicePlanPrograms(@service, @plan)
    $(@el).find("#plan_details").html(view.render().el)

  goToPlanEdit : (event) ->
    event.preventDefault()
    ACC.Routers.ServiceUser.navigate "coordinator/services/#{@service.id}/learning-plans/#{@plan.id}/edit", true

  goToPlanPrograms : (event) ->
    event.preventDefault() if event
    ACC.Routers.ServiceUser.navigate "coordinator/services/#{@service.id}/learning-plans/#{@plan.id}/programs", true

  goToPlanUsers : (event) ->
    event.preventDefault() if event
    ACC.Routers.ServiceUser.navigate "coordinator/services/#{@service.id}/learning-plans/#{@plan.id}/users", true