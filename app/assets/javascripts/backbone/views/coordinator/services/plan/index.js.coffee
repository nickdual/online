class ACC.Views.ServicePlans extends Backbone.View
  list          : "#planslist"
  template      : JST['services/plans/index']
  planTemplate  : JST['services/plans/_plan']
  formTemplate  : JST['services/plans/_form']

  events :
    'click a[role=new]'                         : 'goToNewPlan'
    'click #planslist > li[data-context="plan"]': 'goToPlan'

  # TODO: Change to a model
  initialize : (service) =>
    @service = service
    @service.LearningPlans.on 'reset', @addAll, @

  render : =>
    $(@el).html(@template(@service.toJSON()))
    @

  addOne : (plan) ->
    view = $(@planTemplate(plan.toJSON()))
    $(@el).find(@list).append(view)

    unless plan.get("active") is true
      $(@el).find(@list).find("[data-id=#{plan.get('id')}]").find(".plan").addClass("completed")

  addAll : =>
    $(@el).find(@list).empty()

    if @service.LearningPlans.length
      @service.LearningPlans.each (plan) => @addOne(plan)
      $(@el).find('#quicksearch input.searchfield').quicksearch($(@el).find("#{@list} > li"))
      $(@el).find(@list).find("> li:nth-child(3n+1)").addClass("unpad")
      $(@el).find(@list).find(".timeago").timeago()
      $(@el).find('.progress-plan .bar').tooltip()
    else
      $(@el).find(@list).append("<p>There are no learning plans for this service</p>")

  goToNewPlan : (event) ->
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Service.navigate href, true

  goToPlan : (event, plan_id) =>
    if event
      event.preventDefault()
      plan_id = $(event.currentTarget).data('id')

    ACC.Routers.Service.navigate "coordinator/services/#{@service.id}/learning-plans/#{plan_id}/users", true