class ACC.Views.Group.Show extends Backbone.View
  template  : JST["groups/show"]

  events :
    'click #nav a': 'goToPage'

  render : =>
    $(@el).html(@template(@model.toJSON()))
    @

  highlightTab : (href) =>
    $(@el).find("#nav a").removeClass("active")
    $(@el).find("#nav a[href='#{href}']").addClass("active")

  goToPage : (event) =>
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Service.navigate href, true