class ACC.Views.GroupReports extends Backbone.View
  template  : JST['groups/reports/index']

  events :
    'click [role=send-group-information]' : 'mailGroupsInformation'

  initialize : (group) =>
    @group = group
    @group.Charts.on 'reset', @addAll, @

  render : =>
    $(@el).html(@template(@group.toJSON()))
    @

  addAll : ->
    @group.Charts.each (chart) =>
      if chart.get('type') is "pie"
        Chart.pie(chart.get('name'), chart.get('data'))
      else if chart.get('type') is "column"
        Chart.column(chart.get('name'), chart.get('data'))

    $(@el).find("#charts li").toggle()
    $(@el).find("#charts a").fancybox({
      overlayOpacity: 0.75,
      overlayColor: '#000',
      autoDimensions: false,
      width: 670,
      height: 420
    })

  mailGroupsInformation : (event) =>
    event.preventDefault()
    $(event.currentTarget).parent().html("The report will be emailed to you shortly")
    href = $(event.currentTarget).attr("href")
    groupId = $(event.currentTarget).attr("data-group-id")
    $.ajax
      url  : href,
      type: 'PUT',
      data: {groupId : groupId}

