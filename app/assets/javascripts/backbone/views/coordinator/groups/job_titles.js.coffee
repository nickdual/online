class ACC.Views.GroupJobTitles extends Backbone.View
  list      : "#recordslist"
  template  : JST['groups/jobs/index']
  jobTemplate: JST['groups/jobs/_job']

  events :
    'click #new_job'                      : 'toggleNewJobs'
    'click #create_job a.cancel'          : 'toggleNewJobs'
    'click #recordslist a[role="remove-job"]' : 'removeJob'
    'click #recordslist a[role="edit"]'   : 'toggleJobEdit'
    'click #recordslist a[role="cancel"]' : 'toggleJobEdit'
    'submit [role="update_job"]'          : 'updateJob'
    'submit #create_job'                  : 'createJob'

  # TODO: use model
  initialize: (group) =>
    @group = group
    @group.JobTitles.on 'reset', @addAll, @

  render : =>
    $(@el).html(@template(@group.toJSON()))
    @

  addOne : (job) ->
    view = $(@jobTemplate(job.toJSON()))
    $(@el).find(@list).append(view)

  addAll : =>
    $(@el).find(@list).empty()

    if @group.JobTitles.length
      @group.JobTitles.each (job) => @addOne(job)
      $(@el).find('input.searchfield').quicksearch($(@el).find("#{@list} > li:not(.edit)"))
      $(@el).find(".edit").hide()
    else
      $(@el).find(@list).append("<p>There are no job titles for this group</p>")

  removeJob : (event) ->
    event.preventDefault()
    id = $(event.currentTarget).data('id')
    job = @group.JobTitles.get(id)
    job.destroy
      url: "/api/groups/#{@group.id}/job_titles/#{id}"
      success: =>
        SuccessNotification.text("Successfully removed job title")
        $(@el).find("li[data-id=#{id}]").remove()
        ACC.User.Services.fetch()
      error: ->
        ErrorNotification.text("Failed to remove job title")

  toggleJobEdit : (event) ->
    event.preventDefault() if event
    id = $(event.currentTarget).data('id')
    $(@el).find("li[data-id=#{id}]").toggle()

  createJob : (event) ->
    event.preventDefault()
    form = $(@el).find('#create_job')
    name = form.find('input[name="name"]').val()

    @group.JobTitles.create
      name: name
      job_title:
        name: name
      {url: "/api/groups/#{@group.id}/job_titles"
      success: (job) =>
        @addAll()
        @toggleNewJobs()
        ACC.User.Employments.fetch()
        SuccessNotification.text("Successfully created job title")
        ACC.User.Services.fetch()
        form.find('input[name="name"]').val("")
        form.find('select[name="parent_id"]').val("")
      error: =>
        ErrorNotification.text("Failed to create job title")}

  updateJob : (event) ->
    event.preventDefault()
    form = $(event.currentTarget)
    id = $(event.currentTarget).parent().prev().attr("data-id")
    name = form.find('input[name="name"]').val()

    job = @group.JobTitles.get(id)
    job.save
      name: name
      job_title:
        name: name
      {url: "/api/groups/#{@group.id}/job_titles/#{id}"
      success: (job) =>
        ACC.User.Employments.fetch()
        SuccessNotification.text("Successfully updated job title")
        @addAll()
        ACC.User.Services.fetch()
      error: ->
        ErrorNotification.text("Failed to update job title")}

  toggleNewJobs : (event) ->
    event.preventDefault() if event
    $(@el).find("#create_job, #jobs").toggle()