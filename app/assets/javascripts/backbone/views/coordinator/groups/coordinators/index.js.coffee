class ACC.Views.Group.CoordinatorsIndex extends Backbone.View
  list      : '#coordinatorList'
  template  : JST['groups/coordinators/index']

  events :
    'click a[role=new]' : 'goToX'

  initialize: =>
    @model.Coordinators.on 'reset', @addAll, @

  render : =>
    $(@el).html(@template(@model.toJSON()))
    @

  addAll : =>
    $(@el).find(@list).empty()

    @model.Coordinators.each (coordinator) =>
      view = new ACC.Views.Group.CoordinatorShow({model: coordinator})
      $(@el).find(@list).append(view.render().el)

  goToX : (event) =>
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Service.navigate href, true