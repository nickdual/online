class ACC.Views.Group.CoordinatorShow extends Backbone.View
  tagName   : "li"
  className : "record record-split"
  template  : JST['groups/coordinators/_coordinator']

  events :
    'click a[role=remove]' : 'remove'

  render : =>
    $(@el).html(@template(@model.toJSON()))
    $(@el).find('.bartip').tooltip({container: @el})
    @

  remove : (event) =>
    event.preventDefault()
    confirmMessage = "Are you sure you want to remove this coordinator?"

    if confirm(confirmMessage)
      @model.destroy
        success: =>
          SuccessNotification.text("Successfully removed coordinator")
          $(@el).remove()