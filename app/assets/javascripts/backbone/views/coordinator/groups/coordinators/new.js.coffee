class ACC.Views.Group.CoordinatorsNew extends Backbone.View
  template: JST['groups/coordinators/new']

  events :
    'click a[role=cancel]'  : 'goToX'
    'submit form'           : 'createCoordinator'

  render : =>
    $(@el).html(@template(@model.toJSON()))
    @

  createCoordinator : (event) =>
    event.preventDefault()
    data = $(@el).find('form')
    data.find(".help-inline").remove()
    data.find(".control-group").removeClass("error")

    @model.Coordinators.addCoordinator data,
      =>
        SuccessNotification.text("Successfully added coordinator")
        ACC.Routers.Service.navigate "coordinator/groups/#{@model.id}", true
      (coordinator) =>
        _.each $.parseJSON(coordinator.responseText).errors, (error, name) =>
          $(@el).find("[name=#{name}]").parent().parent().addClass("error")
          $(@el).find("[name=#{name}]").parent().append("<span class=\"help-inline\">#{error}</span>")

          if name is "user_id"
            $(@el).find("[name=email]").parent().parent().addClass("error")
            $(@el).find("[name=email]").parent().append("<span class=\"help-inline\">#{error}</span>")

        ErrorNotification.text("Failed to add coordinator")


  goToX : (event) =>
    event.preventDefault()
    href = $(event.currentTarget).attr("href")
    ACC.Routers.Service.navigate href, true