class ACC.Views.LearningRecords extends Backbone.View
  list: '#learningrecords ul:first'
  template : JST["learning_records/index"]
  learningRecordTemplate: JST['learning_records/_learning_record']

  events :
    'click button[role="learning-plans"]' : 'goToLearningPlans'

  initialize : ->
    ACC.LearningRecords.on 'reset', @addAll

  render : =>
    $(@el).html(@template())
    @addAll() if ACC.LearningRecords.length
    @

  addOne : (learning_record) ->
    view = $(@learningRecordTemplate(learning_record.toJSON()))
    $(@el).find(@list).append(view)

  addAll : =>
    $(@el).find(@list).empty()

    if ACC.LearningRecords.length
      ACC.LearningRecords.each (record) => @addOne(record)
      $(@el).find('input.searchfield').quicksearch($(@el).find("#{@list} > li"))
      $(@el).find(@list).find('.timeago').timeago()
    else
      $(@el).find(@list).append("<p>You have no learning records to view.</p>")

  goToLearningPlans : (event) ->
    event.preventDefault()
    ACC.Routers.Dashboard.navigate "my-learning/plans", true