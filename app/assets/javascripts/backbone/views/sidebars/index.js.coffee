class SidebarView extends Backbone.View
  el      : '#sidebar-parent'

  events :
    'click a#helpbtn' : 'loadZendeskTicket'

  initialize: ->
    $(document).ready ->
      Zenbox.init
        dropboxID:   "20079286"
        url:         "https://support.acctv.co"
        tabID:       "support"
        hide_tab: true
        requester_name: ACC.User.get('name')
        requester_email: ACC.User.get('email')

  loadZendeskTicket : (event) ->
    event.preventDefault()
    Zenbox.show()

window.SidebarIndexView = SidebarView