class ACC.Views.SidebarLearningPlan extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/my_learning/learning_plans/show']

  render : =>
    $(@el).html(@template(@model.toJSON()))