class ACC.Views.SidebarLearningPlans extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/my_learning/learning_plans/index']

  render : ->
    $(@el).html(@template())