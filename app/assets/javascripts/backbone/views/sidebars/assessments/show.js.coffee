class ACC.Views.SidebarAssessment extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/assessments/show']

  render : ->
    $(@el).html(@template())