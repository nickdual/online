class ACC.Views.SidebarProgram extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/programs/show']

  render : ->
    $(@el).html(@template())