class ACC.Views.SidebarPrograms extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/programs/index']

  render : ->
    $(@el).html(@template())