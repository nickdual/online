class ACC.Views.SidebarSettings extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/settings']

  render : ->
    $(@el).html(@template())