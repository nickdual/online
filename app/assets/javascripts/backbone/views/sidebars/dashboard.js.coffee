class ACC.Views.SidebarDashboard extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/dashboard']

  render : ->
    $(@el).html(@template())