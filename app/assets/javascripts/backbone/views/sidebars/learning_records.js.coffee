class ACC.Views.SidebarLearningRecords extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/my_learning/learning_records']

  render : ->
    $(@el).html(@template())