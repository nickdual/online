class ACC.Views.SidebarGroupCharts extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/groups/charts']

  render : ->
    $(@el).html(@template())