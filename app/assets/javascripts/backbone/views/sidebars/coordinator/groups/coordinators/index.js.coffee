class ACC.Views.Sidebar.Groups.CoordinatorsIndex extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/groups/coordinators/index']

  render : ->
    $(@el).html(@template(@model.toJSON()))