class ACC.Views.Sidebar.Groups.CoordinatorsNew extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/groups/coordinators/new']

  render : ->
    $(@el).html(@template(@model.toJSON()))