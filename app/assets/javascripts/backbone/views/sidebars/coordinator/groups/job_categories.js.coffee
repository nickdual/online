class ACC.Views.SidebarGroupJobCategories extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/groups/job_categories']

  render : (group) ->
    $(@el).html(@template(group.toJSON()))