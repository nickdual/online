class ACC.Views.SidebarUserRecords extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/users/learning_records']

  render : (user) ->
    $(@el).html(@template(user.toJSON()))