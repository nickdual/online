class ACC.Views.SidebarServiceJobCategories extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/job_categories']

  render : (service) ->
    $(@el).html(@template(service.toJSON()))