class ACC.Views.SidebarServiceUserEdit extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/users/edit']

  render : =>
    $(@el).html(@template(@model.toJSON()))