class ACC.Views.SidebarServicePlans extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/learning_plans/index']

  render : (service) ->
    $(@el).html(@template(service.toJSON()))