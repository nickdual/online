class ACC.Views.SidebarPlanUsers extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/learning_plans/users']

  render : (plan) ->
    $(@el).html(@template(plan.toJSON()))