class UserAssessmentsView extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/users/assessments']

  render : (user) ->
    $(@el).html(@template(user.toJSON()))

window.SidebarUserAssessmentsView = UserAssessmentsView