class ACC.Views.SidebarServiceSearch extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/reports/search']

  render : ->
    $(@el).html(@template())