class ACC.Views.SidebarService extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/index']

  events :
    'click a[role="reset-token"]' : 'resetActivationToken'

  render : (service) ->
    @service = service
    $(@el).html(@template(@service.toJSON()))

  resetActivationToken : (event) ->
    event.preventDefault()
    $(@el).find('[role="reset-token"]').html("resetting token...")

    $.ajax
      url       : "/api/services/#{@service.id}/reset_token.json"
      type      : 'PUT'

      success   : (service) =>
        SuccessNotification.text("Successfully reset activation token")
        $(@el).find('[role="reset-token"]').html("reset token")
        @service.set({activation_token: service.service.activation_token}, {silent: true})
        $(@el).find('[role="activation-token"]').html(@service.get('activation_token'))
      error     : =>
        ErrorNotification.text("Failed to reset activation token")