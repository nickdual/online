class ACC.Views.SidebarUserPlans extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/users/learning_plans/index']

  render : (user) ->
    $(@el).html(@template(user.toJSON()))