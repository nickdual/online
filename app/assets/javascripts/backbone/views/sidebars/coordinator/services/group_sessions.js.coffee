class ACC.Views.SidebarServiceSessions extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/group_sessions']

  render : (service) ->
    $(@el).html(@template(service.toJSON()))