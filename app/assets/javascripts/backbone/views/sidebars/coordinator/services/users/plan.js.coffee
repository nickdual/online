class ACC.Views.SidebarUserPlan extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/users/learning_plans/show']

  render : (plan) ->
    $(@el).html(@template(plan.toJSON()))