class ACC.Views.SidebarServiceCharts extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/reports/charts']

  render : ->
    $(@el).html(@template())