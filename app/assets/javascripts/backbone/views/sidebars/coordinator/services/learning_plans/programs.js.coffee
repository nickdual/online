class ACC.Views.SidebarPlanPrograms extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/learning_plans/programs']

  render : (plan) ->
    $(@el).html(@template(plan.toJSON()))