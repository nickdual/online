class ACC.Views.SidebarServiceProgramAssessments extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/services/program_assessments']

  render : (service) ->
    $(@el).html(@template(service.toJSON()))