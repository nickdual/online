class ACC.Views.SidebarCoordinator extends Backbone.View
  el        : "#sidebar section"
  template  : JST['sidebars/coordinator/index']

  render : ->
    $(@el).html(@template())