class LearningRecord extends Backbone.Model
  url: "/api/profile/activities/:id"

  initialize : =>
    @.on 'sync', (learningRecord) =>
      ACC.RecentLearningRecords.fetch()
      ACC.IncompletedLearningPlans.fetch()
      ACC.LearningRecords.fetch()
      ACC.EmploymentLearningPlans.fetch()
      ACC.User.fetch()

class LearningRecordsCollection extends Backbone.Collection
  model : LearningRecord
  url   : "/api/profile/activities"

  lazyFetch: ->
    unless @.length
      @.fetch(add: true)

ACC.LearningRecords = new LearningRecordsCollection
window.LearningRecordsCollection = LearningRecordsCollection