class LearningResource extends Backbone.Model
class LearningResourcesCollection extends Backbone.Collection
  model : LearningResource
  url   : "/api/programs/:id/learning_resources"

# TODO: Look at merging into CoordinatorLearningResources
ACC.LearningResources = new LearningResourcesCollection
window.LearningResourcesCollection = LearningResourcesCollection