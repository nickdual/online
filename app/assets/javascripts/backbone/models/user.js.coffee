class User extends Backbone.Model
  url: "/api/profile"

  initialize : ->
    @EmploymentLearningPlans      = new EmploymentLearningPlansCollection
    @EmploymentLearningPlans.url  = "/api/profile/learning_plans"

    @Employments      = new EmploymentsCollection
    @Employments.url  = "/api/profile/employments"

    @Coordinators     = new CoordinatorsCollection
    @Coordinators.url = "/api/profile/coordinators"

    @Services     = new ServicesCollection
    @Services.url = "/api/services"

    @programs =
      pending : ACC.Programs
      viewed  : ACC.Programs

    @on 'change', (me, what) =>
      if me.get('programs')
        if me.get('programs').pending
          @programs.pending.reset me.get('programs').pending
        if me.get('programs').viewed
          @programs.viewed.reset me.get('programs').viewed

  _.extend(Backbone.Model.prototype, Backbone.Validation.mixin)

  validation:
    first_name:
      required: true
    last_name:
      required: true
    email:
      required: true
      pattern: 'email'
    password:
      required: false
      minLength: 6
    password_confirmation:
      equalTo: 'password'

  signUp : (first_name, last_name, email, activation_token, quiet = false, onSuccess = null, onFail = null) ->
    $.ajax
      url       : '/users.json'
      type      : 'POST'
      data      : { user: { first_name: first_name, last_name: last_name, email: email, self_registered: "1", employments_attributes: { "0": { activation_token: activation_token } } } }

      success   : (user) =>
        @set user.user
        $.cookie 'signed-in', @get('id'), {path: "/"}
        @trigger 'sign-in'
        if _.isFunction(onSuccess) then onSuccess(@)
      error     : =>
        @trigger 'failed-sign-in' unless quiet
        if _.isFunction(onFail) then onFail(@)
    @

  signIn : (email, password, onSuccess = null, onFail = null) ->
    $.ajax
      url       : '/users/sign_in.json'
      type      : 'POST'
      data      : { user: { email : email, password : password } }

      success   : (user) =>
        @set user.user
        $.cookie 'signed-in', @get('id'), {path: "/"}
        @trigger 'sign-in'
        if _.isFunction(onSuccess) then onSuccess(@)
      error     : =>
        @trigger 'failed-sign-in'
        if _.isFunction(onFail) then onFail(@)
    @

  forgotPassword: (email, quiet = false, onSuccess = null, onFail = null) ->
    $.ajax
      url       : '/users/password.json'
      dataType  : 'json'
      type      : 'POST'
      data      : { user: { email : email } }

      success   : (user) =>
        if _.isFunction(onSuccess) then onSuccess(@)
      error     : =>
        if _.isFunction(onFail) then onFail(@)
    @

  setPassword: (token, password, password_confirmation, onSuccess = null, onFail = null) ->
    $.ajax
      url       : "/users/password.json"
      type      : 'PUT'
      data      : { user: { reset_password_token : token, password : password, password_confirmation: password_confirmation } }

      success   : (user) =>
        @set user.user
        $.cookie 'signed-in', @get('id'), {path: "/"}
        @trigger 'sign-in'
        if _.isFunction(onSuccess) then onSuccess(@)
      error     : =>
        if _.isFunction(onFail) then onFail(@)
    @

  isSignedIn : ->
    $.cookie('signed-in', {path: "/"}) != null

  loadPrograms : (programs) ->
    @programs.viewed.reset  programs.viewed   if programs.viewed
    @programs.pending.reset programs.pending  if programs.pending

ACC.User = new User