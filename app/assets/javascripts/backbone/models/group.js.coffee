class Group extends Backbone.Model
  url: "/api/groups/:id"

  initialize: ->
    @JobTitles      = new GroupJobTitlesCollection
    @JobTitles.url  = "/api/groups/#{@.id}/job_titles"

    @Charts     = new GroupChartsCollection
    @Charts.url = "/api/groups/#{@.id}/charts"

    @ProgramAssessments     = new ProgramAssessmentsCollection
    @ProgramAssessments.url = "/api/groups/#{@.id}/program_assessments"

    @Coordinators     = new CoordinatorsCollection
    @Coordinators.url = "/api/groups/#{@.id}/coordinators"
    @Coordinators.on 'reset', @setCoordinatorPermissions

  setCoordinatorPermissions: =>
    @Coordinators.each (coordinator) =>
      if ACC.User.get('id').toString() is coordinator.get('user_id').toString()
        coordinator.set('currentUser', true)

class GroupsCollection extends Backbone.Collection
  model : Group
  url   : "/api/groups"

ACC.Groups = new GroupsCollection