class Employment extends Backbone.Model
  initialize: ->
    @EmploymentLearningPlans      = new EmploymentLearningPlansCollection
    @EmploymentLearningPlans.url  = "/api/employments/#{@.get('id')}/learning_plans"

    @LearningRecords              = new LearningRecordsCollection
    @LearningRecords.url          = "/api/employments/#{@.get('id')}/activities"

    @JobTitles                    = new ServiceJobTitlesCollection
    @JobTitles.url                = "/api/services/#{@.get('service_id')}/job_titles"

    @ProgramAssessment            = new ProgramAssessmentsCollection
    @ProgramAssessment.url        = "/api/employments/#{@.get('id')}/program_assessments"


  _.extend(Backbone.Model.prototype, Backbone.Validation.mixin)

  recentlyResetWelcomeEmail : =>
    unless @.get('welcome_email_resent_at') is null
      resetAt = moment(@.get('welcome_email_resent_at'))
      currentTime = moment()
      (currentTime - resetAt) < 50000

  validation:
    first_name:
      required: true
    last_name:
      required: true
    role:
      required: true
    email:
      required: true
      pattern: 'email'
    email: (value, column, employment) =>
      service = ACC.Services.get(employment.service_id)

      if service
        if service.Employments.where({email: value}).length > 1
          return "Already employed at service"

class EmploymentsCollection extends Backbone.Collection
  model : Employment
  url: "/api/services/:service_id/employments"

  lazyFetch: ->
    unless @.length
      @.fetch(add: true)

ACC.Employments = new EmploymentsCollection
window.EmploymentsCollection = EmploymentsCollection