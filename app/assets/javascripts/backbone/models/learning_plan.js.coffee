class LearningPlan extends Backbone.Model
  url: "/api/services/:service_id/learning_plans"
  initialize: ->
    @Employments = new EmploymentsCollection
    @Employments.url = "/api/learning_plans/#{@id}/employments"

    @EmploymentLearningPlans      = new EmploymentLearningPlansCollection
    @EmploymentLearningPlans.url  = "/api/learning_plans/#{@id}/employment_learning_plans"

    @Programs = new ProgramsCollection
    @Programs.url = "/api/learning_plans/#{@id}/programs"

  _.extend(Backbone.Model.prototype, Backbone.Validation.mixin)

  validation:
    name:
      required: true
    days_till_due: [
      required: true
      msg: "Days till due is required"
    ,
      min: 1
      msg: "Must be at least one day"
    ]
    active_at:
      required: true

class LearningPlansCollection extends Backbone.Collection
  model : LearningPlan
  url   : "/api/services/:service_id/learning_plans"

  lazyFetch: ->
    unless @.length
      @.fetch(add: true)

ACC.LearningPlans = new LearningPlansCollection
window.LearningPlansCollection = LearningPlansCollection