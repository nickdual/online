class EssentialAssessment extends Backbone.Model
  initialize : =>
    @Sections      = new ACC.Sections
    @Sections.url  = "/api/program_assessments/#{@id}/sections"

    @Results      = new EssentialAssessmentResultCollection
    @Results.url  = "/api/program_assessments/#{@id}/results"

  currentProgress : =>
    (@.Sections.answered().length / @.Sections.length) * 100

  questions : =>
    collection = _.map @Sections.models, (section) =>
      _.map section.Questions.models, (question) =>
        question

    _.flatten collection

  totalCorrect : =>
    collection = _.filter @questions(), (question) =>
      question.get('correct') is true

    collection.length

  totalQuestions : =>
    @questions().length

  percentageCorrect : =>
    parseInt((@totalCorrect() / @totalQuestions()) * 100)

  passed : =>
    @percentageCorrect() >= @.get('pass_mark')

  questionResults : =>
    _.map @questions(), (question) =>
      question_id:  question.id
      question_type: question.get('questionType')
      correct:      question.get('correct')

  postResults : =>
    @Results.create
      assessment_id: @.get('id')
      passed:        @passed()
      time_taken:    @.get('timeTaken')
      questions_attributes: @questionResults()
      {wait: true}

class EssentialsAssessmentCollection extends Backbone.Collection
  model : EssentialAssessment

window.EssentialsAssessmentCollection = EssentialsAssessmentCollection