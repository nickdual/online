class Section extends Backbone.Model
  defaults:
    'completed': false

  initialize: =>
    @Questions      = new ACC.Questions
    @Questions.url  = "/api/program_assessment_sections/#{@id}/questions"
    @Questions.on 'reset', @setQuestionAttributes

  randomizeQuestionPool : =>
    pool_size = @Questions.size()
    required_size = @get('minimum_number_of_questions')
    to_delete = pool_size - required_size
    target = @Questions
    target.models = _.shuffle(target.models)
    if to_delete > 0
      _(to_delete).times ->
        target.remove(target.at(_.random(target.size()-1), 1));

  setQuestionAttributes : =>
    @randomizeQuestionPool()
    @Questions.each (question, index) =>
      question.set({questionType: @.get('question_type')})
      question.set({incrementingId: (index + 1)})

    if @.get('question_type') is "Sequencing"
      @Questions.each (question) =>
        _.each question.get('randomised_answer_items'), (answer, index) =>
          answer.incrementingId = @incrementingId(index)
  incrementingId : (key) =>
    ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"][key]

class SectionsCollection extends Backbone.Collection
  model : Section

  unAnswered : =>
    @.where({completed: false})

  answered : =>
    @.where({completed: true})

ACC.Sections = SectionsCollection