class EssentialAssessmentResult extends Backbone.Model
  toJSON: -> "results" : super()

  initialize : =>
    @.on 'sync', @updateRecords

  updateRecords : =>
    ACC.EmploymentLearningPlans.fetch()
    ACC.LearningRecords.fetch()
    ACC.IncompletedLearningPlans.fetch()
    ACC.RecentLearningRecords.fetch()

class EssentialAssessmentResultCollection extends Backbone.Collection
  model : EssentialAssessmentResult

window.EssentialAssessmentResultCollection = EssentialAssessmentResultCollection