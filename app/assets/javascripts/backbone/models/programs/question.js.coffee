class Question extends Backbone.Model
  answerInString : =>
    switch @.get('questionType')
      when "True/False"
        @.get('answer').toString()
      when "Sequencing"
        _.map @correctPosition(), (answer) =>
          answer.id.toString()
      when "Multiple Choice"
        correct = _.select @.get("answers"), (answer) =>
          answer.correct is true
        correct[0].id.toString()
      when "Fill Gap"
        correct = _.select @.get("answers"), (answer) =>
          answer.correct is true
        correct[0].answer


  # View changes for Sequential
  # ["Gather the wood", "Wait till Winter", "Start a fire"]
  yourAnswerOrder : =>
    if @.get('userAnswer')
      _.map @.get('userAnswer'), (answerInteger) =>
        _.find @.get('randomised_answer_items'), (answer) =>
          answer.id.toString() is answerInteger.toString()
    else
      console.log("No user answer provided")

  # [2, 3, 1]
  correctPosition : =>
    _.sortBy @.get('randomised_answer_items'), (answer) =>
      parseInt(answer.correct_position)

  # [{text: "Wait till Winter", reference_image_url: "Derp.jpg"}]
  correctAnswerOrder : =>
    _.map @correctPosition(), (answer) =>
      {text: answer.text, reference_image_url: answer.reference_image_url, incrementingId: answer.incrementingId}

  generateTooltip : (placeholder) =>
    $(placeholder).find('h2').
      prepend( "<div class='tooltip-container'><div class='bartip' data-original-title=' #{@.get('question_id')}'></div></div>").
      find('.bartip').tooltip()

class QuestionsCollection extends Backbone.Collection
  model : Question

  inCorrect : =>
    @.where({correct: false})

  answered : =>
    @.filter (question) =>
      question.get('userAnswer')

ACC.Questions = QuestionsCollection