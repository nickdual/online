class Coordinator extends Backbone.Model
  toJSON: -> "coordinator" : super()
  _.extend(Backbone.Model.prototype, Backbone.Validation.mixin)

  validation:
    first_name:
      required: true
    last_name:
      required: true
    email:
      required: true
      pattern: 'email'

class CoordinatorsCollection extends Backbone.Collection
  model : Coordinator

  addCoordinator : (data, success, failure) =>
    firstName = $(data).find('[name=first_name]').val()
    lastName  = $(data).find('[name=last_name]').val()
    email     = $(data).find('[name=email]').val()

    @.create
      first_name: firstName
      last_name:  lastName
      email:      email
      {success: => (success.call(@))
      error: (coordinator, errors) =>
        failure.call(coordinator, errors)}

window.CoordinatorsCollection = CoordinatorsCollection