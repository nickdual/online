class GroupChart extends Backbone.Model
class GroupChartsCollection extends Backbone.Collection
  model : GroupChart
  url   : "/api/groups/:group_id/charts"

  lazyFetch: ->
    unless @.length
      @.fetch(add: true)

# TODO: Merge into services chart colection
ACC.GroupCharts = new GroupChartsCollection
window.GroupChartsCollection = GroupChartsCollection