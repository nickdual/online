# Move this into a sub folder
class GroupJobTitle extends Backbone.Model
class GroupJobTitlesCollection extends Backbone.Collection
  model : GroupJobTitle

  lazyFetch: ->
    unless @.length
      @.fetch(add: true)

ACC.GroupJobTitles = new GroupJobTitlesCollection
window.GroupJobTitlesCollection = GroupJobTitlesCollection