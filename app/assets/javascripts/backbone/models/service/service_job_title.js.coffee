class ServiceJobTitle extends Backbone.Model
class ServiceJobTitlesCollection extends Backbone.Collection
  model : ServiceJobTitle
  url   : "/api/services/:service_id/job_title"

  lazyFetch: ->
    unless @.length
      @.fetch(add: true)

# TODO: Merge into Groups collection
ACC.ServiceJobTitles = new ServiceJobTitlesCollection
window.ServiceJobTitlesCollection = ServiceJobTitlesCollection