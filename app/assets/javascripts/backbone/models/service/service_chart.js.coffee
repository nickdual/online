class ServiceChart extends Backbone.Model
class ServiceChartsCollection extends Backbone.Collection
  model : ServiceChart
  url   : "/api/services/:service_id/charts"

  lazyFetch: ->
    unless @.length
      @.fetch(add: true)

ACC.ServiceCharts = new ServiceChartsCollection