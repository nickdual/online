class IncompletedLearningPlan extends Backbone.Model
class IncompletedLearningPlansCollection extends Backbone.Collection
  model : IncompletedLearningPlan
  url   : "/api/profile/learning_plans?limit=3&active=true"

ACC.IncompletedLearningPlans = new IncompletedLearningPlansCollection