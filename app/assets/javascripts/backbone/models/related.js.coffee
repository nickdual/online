class Related extends Backbone.Model
class RelatedCollection extends Backbone.Collection
  model : Related
  url: "/api/programs/:program_id/related"

# TODO: Look at merging related into programs
window.RelatedCollection = RelatedCollection
ACC.ProgramsRelated = new RelatedCollection