class EmploymentLearningPlan extends Backbone.Model
  url: "/api/profile/learning_plans"

  initialize: =>
    @ProgramsPending = new ProgramsPendingCollection
    @ProgramsPending.url = "/api/employment_learning_plans/#{@id}/programs_pending"
    @ProgramsPending.service_id = @.get('service_id')
    @ProgramsPending.learning_plan_id = @.get('learning_plan_id')

    @ProgramsCompleted = new ProgramsCompletedCollection
    @ProgramsCompleted.url = "/api/employment_learning_plans/#{@id}/programs_completed"
    @ProgramsCompleted.service_id = @.get('service_id')
    @ProgramsCompleted.learning_plan_id = @.get('learning_plan_id')

    @.set('limited_programs', @limitedPrograms())

  limitedPrograms : =>
    _.filter(@.get('programs_remaining'), (element, index) -> (true if index < 3))

class EmploymentLearningPlansCollection extends Backbone.Collection
  model         : EmploymentLearningPlan
  url   : "/api/profile/learning_plans"

  lazyFetch: ->
    unless @.length
      @.fetch(add: true)

ACC.EmploymentLearningPlans = new EmploymentLearningPlansCollection
window.EmploymentLearningPlansCollection = EmploymentLearningPlansCollection