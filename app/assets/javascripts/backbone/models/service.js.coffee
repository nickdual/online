class Service extends Backbone.Model
  url: "/api/services/:id"
  initialize: ->
    @Employments      = new EmploymentsCollection
    @Employments.url  = "/api/services/#{@.id}/employments"
    @Employments.on 'reset', @setEmploymentPermissions

    @SearchEmployments      = new SearchEmploymentsCollection
    @SearchEmployments.url  = "/api/services/#{@.id}/employments/advanced"

    @LearningPlans      = new LearningPlansCollection
    @LearningPlans.url  = "/api/services/#{@.id}/learning_plans"

    @GroupSessions      = new GroupSessionsCollection
    @GroupSessions.url  = "/api/services/#{@.id}/group_sessions"

    @Programs     = new ProgramsCollection
    @Programs.url = "/api/services/#{@.id}/programs"

    @JobTitles      = new ServiceJobTitlesCollection
    @JobTitles.url  = "/api/services/#{@.id}/job_titles"

    @Charts     = new GroupChartsCollection
    @Charts.url = "/api/services/#{@.id}/charts"

    @ProgramAssessments     = new ProgramAssessmentsCollection
    @ProgramAssessments.url = "/api/services/#{@.id}/program_assessments"

  setEmploymentPermissions: =>
    @Employments.each (employment) =>
      if ACC.User.get('id').toString() is employment.get('user_id').toString()
        employment.set('currentUser', true)

class ServicesCollection extends Backbone.Collection
  model         : Service
  url           : "/api/role/coordinator/services"

ACC.Services = new ServicesCollection
window.ServicesCollection = ServicesCollection