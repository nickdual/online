class Program extends Backbone.Model
  url: "/api/programs/:id"
  initialize: ->
    @Related      =   new RelatedCollection
    @Related.url  = "/api/programs/#{@id}/related"
    # Learning Resources
    @LearningResources      = new LearningResourcesCollection
    @LearningResources.url  = "/api/programs/#{@id}/learning_resources"
    @CoordinatorLearningResources     = new CoordinatorLearningResourcesCollection
    @CoordinatorLearningResources.url = "/api/programs/#{@id}/learning_resources?coordinator=true"

    # Program assessments are in the context of the current user
    @ProgramAssessment      = new ProgramAssessmentsCollection
    @ProgramAssessment.url  = "/api/profile/assessments/#{@id}"
    @ProgramAssessment.on 'reset', @setAssessmentAttributes

    @EssentialsAssessment   = new EssentialsAssessmentCollection
    @EssentialsAssessment.url = "/api/programs/#{@id}/assessments"
    @EssentialsAssessment.on 'reset', @setEssentialsAttributes

    # Videos
    @Videos     = new VideosCollection
    @Videos.url = "/api/programs/#{@id}/videos"
  setAssessmentAttributes: =>
    @ProgramAssessment.each (assessment) =>
      assessment.set('program_id', @.get('id'))
      assessment.set('hasOnlineAssessment', @hasOnlineAssessment())
      assessment.set('hasActiveOnlineAssessment', @hasActiveOnlineAssessment())

  setEssentialsAttributes: =>
    @EssentialsAssessment.each (assessment) =>
      assessment.set('title', @.get('title'))
      assessment.set('program_id', @.get('id'))

  hasActiveOnlineAssessment : =>
    console.log("@.get('active_assessments_count')" + @.get('active_assessments_count'))

    @.get('active_assessments_count') > 0

  hasOnlineAssessment : =>
    @.get('assessments_count') > 0

class ProgramsCollection extends Backbone.Collection
  model : Program
  url   : "/api/programs"

  lazyFetch: ->
    unless @.length
      @.fetch(add: true)

ACC.Programs = new ProgramsCollection
window.ProgramsCollection = ProgramsCollection
