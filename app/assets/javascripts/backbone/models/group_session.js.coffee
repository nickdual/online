class GroupSession extends Backbone.Model
  toJSON: -> "group_session": super()

  initialize : =>
    @.on 'sync', (session) =>
      ACC.IncompletedLearningPlans.fetch()
      ACC.EmploymentLearningPlans.fetch()
      ACC.RecentLearningRecords.fetch()
      ACC.LearningRecords.fetch()

  _.extend(Backbone.Model.prototype, Backbone.Validation.mixin)

  validation:
    program_and_assessment_check: (errors, field, model) =>
      if model.create_program_record is false and model.create_assessment_records is false
        return "You must choose to add a record for a PROGRAM, an ASSESSMENT, or BOTH"
      else if model.create_program_record is true and model.medium is undefined
        return "You must choose A MEDIUM"
      else if model.create_assessment_records is true and model.essentials is false and model.extension is false and model.evidence is false
        return "You must choose AT LEAST ONE ASSESSMENT TYPE"
    employment_ids:
      required: true,
      msg: 'You must choose AT LEAST ONE USER'
    program_id:
      required: true
      msg: 'You must select a program'
    viewed_at_date: (errors, field, model) =>
      date = moment("#{model.viewed_at_date} #{model.viewed_at_time}", "DD/MM/YYYY h:mA");
  
      if date._isValid is false
        return "Date must be present"
      else if date > moment()
        return "You must enter a date and time in the past"
    viewed_at_time:
      required: true

  labels:
    program_id: 'Program'
    employment_ids: 'Users'
    viewed_at_date: 'Date'
    viewed_at_time: 'Time'

class GroupSessionsCollection extends Backbone.Collection
  model : GroupSession

  # For validation handling
  employment_ids: (ids) ->
    if ids.length > 0
      return ids
    else
      return null

  createSession: (form, onSuccess, onFailure) =>
    program_id      = form.find("ul#programs-results li.selected").data('id')
    medium          = form.find('input[name="medium"]:checked').val()
    viewed_at_date  = form.find('input[name="viewed_at_date"]').val()
    viewed_at_time  = form.find('select[name="viewed_at_time"]').val()
    essentials      = form.find('input[name="essentials"]').is(":checked")
    extension       = form.find('input[name="extension"]').is(":checked")
    evidence        = form.find('input[name="evidence"]').is(":checked")
    employment_ids  = _.map(form.find("ul#users-form-chosen li"), (li) => $(li).data('id'))

    created_by_id   = ACC.User.get('id')

    # TODO: Find a way to not send these attributes
    create_program_record     = form.find('input[name="create_program_record"]').is(":checked")
    create_assessment_records = form.find('input[name="create_assessment_records"]').is(":checked")

    @.create
      program_id      : program_id
      medium          : medium
      viewed_at_date  : viewed_at_date
      viewed_at_time  : viewed_at_time
      essentials      : essentials
      extension       : extension
      evidence        : evidence
      employment_ids  : @employment_ids(employment_ids)
      created_by_id   : created_by_id
      create_program_record     : form.find('input[name="create_program_record"]').is(":checked")
      create_assessment_records : form.find('input[name="create_assessment_records"]').is(":checked")
      program_and_assessment_check: true
      {success: =>
        onSuccess(@)
      error: (session, errors) =>
        onFailure(session, errors)}

ACC.GroupSessions = new GroupSessionsCollection
window.GroupSessionsCollection = GroupSessionsCollection