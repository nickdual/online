class CoordinatorLearningResource extends Backbone.Model
class CoordinatorLearningResourcesCollection extends Backbone.Collection
  model : CoordinatorLearningResource
  url   : "/api/programs/:id/learning_resources?coordinator=true"

ACC.CoordinatorLearningResources = new CoordinatorLearningResourcesCollection
window.CoordinatorLearningResourcesCollection = CoordinatorLearningResourcesCollection