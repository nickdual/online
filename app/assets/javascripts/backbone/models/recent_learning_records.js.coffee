# TODO: Look at merging this into LearningRecord class
class RecentLearningRecord extends Backbone.Model
class RecentLearningRecordsCollection extends Backbone.Collection
  model : RecentLearningRecord
  url   : "/api/profile/activities?limit=3"

ACC.RecentLearningRecords = new RecentLearningRecordsCollection