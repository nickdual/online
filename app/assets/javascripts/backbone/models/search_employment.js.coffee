class SearchEmployment extends Backbone.Model
class SearchEmploymentsCollection extends Backbone.Collection
  model : SearchEmployment
  url: "/api/services/:service_id/employments/advanced"

# TODO: Merge into ACC.Employments
ACC.SearchEmployments = new SearchEmploymentsCollection
window.SearchEmploymentsCollection = SearchEmploymentsCollection