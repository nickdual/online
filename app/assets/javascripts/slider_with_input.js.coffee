SliderWithInput = {
  _init: ->
    @.slider().slider
      change: =>
        @.input().val(@.slider().slider('value'))
      value: @.input().val()
  
  input: ->
    @.element.find('input')

  slider: ->
    @.element.find('[data-SliderWithInput-Slider]')

}

$.widget("ui.slider_with_input", SliderWithInput);