//= require highcharts/highcharts.src.js
//= require highcharts/modules/exporting.js

//  Setting colours based on ACC themes
Highcharts.setOptions({
    colors: ['#66CC33', '#009900', '#0099FF', '#0033FF', '#FF9933', '#FF3333']
});

var Chart = {
  column: function(id, options, chartOptions) {
    var columnChartOptions = jQuery.extend(true, {
      chart: {
        renderTo: id,
        defaultSeriesType: 'column'
      },
      title: {
        text: options.title
      },
      xAxis: {
        categories: options.categories
      },
      yAxis: {
        min: 0,
        title: {
          text: options.yAxisTitle
        }
      },
      legend: {
        align: 'left',
        x: 80,
        verticalAlign: 'top',
        y: 40,
        floating: true,
        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
      },
      tooltip: {
        formatter: function() {
          return '<b>'+ this.x +'</b><br/>'+
            this.series.name +': '+ this.y +'<br/>'+
            'Total: '+ this.point.stackTotal;
        }
      },
      plotOptions: {
        column: {
          stacking: 'normal',
          dataLabels: {
            enabled: true,
            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
          }
        }
      },
      series: options.series
    }, chartOptions);

    if (Chart.labelsDoNotFit(options.categories)) {
      jQuery.extend(true, columnChartOptions, Chart.verticalxAxisLabelsOptions);
    }

    return new Highcharts.Chart(columnChartOptions);
  },

  pie: function(id, options) {
    new Highcharts.Chart({
      chart: {
        renderTo: id,
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        defaultSeriesType: 'pie'
      },
      title: {
        text: options.title
      },
      tooltip: {
        formatter: function() {
          return '<b>'+ this.point.name +'</b>: '+ this.point.y;
        }
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            color: Highcharts.theme && Highcharts.theme.textColor || '#000000',
            connectorColor: Highcharts.theme && Highcharts.theme.textColor || '#000000',
            formatter: function() {
              return this.point.name +': '+ this.point.y;
            }
          }
        }
      },
      series: options.series
    });
  },


  // Consider these below private.
  labelsDoNotFit: function(labels) {
    for (var i = 0; i < labels.length; i++) {
      if (labels[i].length > 2) {
        return true;
      }
    }
    return false;
  },

  verticalxAxisLabelsOptions: {
    xAxis: {
      labels: {
        align: 'right',
        rotation: -90
      }
    }
  }
};
