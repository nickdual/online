#= require underscore

#= require jquery
#= require jquery_ujs
#= require jquery.quicksearch.js
#= require jquery-ui-1.10.3.custom
#= require jquery-ui-slider.min.js
#= require boolean_radio_group_set_true_button
#= require cocoon
#= require image_preview
#= require sequential_question_admin_form
#= require slider_with_input

# Load all jquery UI widgets before bootstrap plugins.
# Bootstrap plugins have higher priority.
#= require admin-unicorn-bootstrap

$ ->
  init_widgets = ->
    $('[data-SliderWithInput]').slider_with_input()
    $('input[type=radio][data-boolean-radio-group-set][value=true]').boolean_radio_group_set_true_button()
    
  # Chosen Styles
  $('select').chosen()
  $('.radio').button()
  $('input[type=checkbox],input[type=radio],input[type=file]').not('[data-not-uniform]').uniform()
  $('input.quicksearch').quicksearch('table.quicksearch-target tbody tr')

  $("input[role=toggleImageHelper]").live "change", (event) =>
    $(event.currentTarget).closest(".control-group").find("[role=imageHelper]").toggle()

  $('form[role=SequentialQuestionAdminForm]').sequential_question_admin_form()

  $('[data-ImagePreview]').image_preview()

  $('input[type=radio][data-boolean-radio-group-set][value=true]').boolean_radio_group_set_true_button()

  init_widgets()

  $('body').on 'cocoon:after-insert', (event, inserted_item)->
    init_widgets()
