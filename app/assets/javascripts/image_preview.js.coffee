ImagePreview =
  _init: ->
    if @.supported()
      @.image_input().on 'change', (event)=>
        @.refresh(event.target.files[0])

      @.delete_action().on 'click', (event)=>
        @.refresh(null)
        event.preventDefault()

  delete_action: ->
    @.element.find('[data-ImagePreview-DeleteAction]')

  file_reader: ->
    result = new FileReader()
    result.onload = (onload_event) =>
      @.preview().attr('src', onload_event.target.result)
    result

  image_input: ->
    @.element.find('[data-ImagePreview-ImageInput]')

  preview: ->
    @.element.find('[data-ImagePreview-Preview]')

  refresh: (file)->
    if file && file.type.match('image.*')
      @.file_reader().readAsDataURL(file)
      @.remove_image_input().prop('checked', false)
    else
      @.preview().attr('src', 'http://placehold.it/539x302')
      @.remove_image_input().prop('checked', true)

    undefined

  remove_image_input: ->
    @.element.find('[data-ImagePreview-RemoveImageInput]')
    
  supported: ->
    window.File && window.FileReader && window.FileList && window.Blob

$.widget("ui.image_preview", ImagePreview);
