#= require 'jquery'
#= require 'jquery-ui-1.10.3.custom'
#= require underscore

BooleanRadioGroupSet =
  TrueButton:
    _init: ->
      @.element.on 'change', =>
        @.refresh_group_set()

    group_name: ->
      @.element.attr('name')

    group_set_name: ->
      @.element.data('boolean-radio-group-set')

    refresh_group_set: ->
      @.other_false_buttons().prop('checked', true)

    other_false_buttons: ->
      $('input[type=radio][data-boolean-radio-group-set=' + @.group_set_name() + ']')      
        .filter('[value=false]')
        .not('[name="' + @.group_name() + '"]')

$.widget("ui.boolean_radio_group_set_true_button", BooleanRadioGroupSet.TrueButton);
