# We're loading all of our libraries in a different request / file, checkout libraries.js.coffee
#= require backbone/config
#= require_tree ./../templates
#= require_tree ./backbone/models
#= require_tree ./backbone/views
#= require_tree ./backbone/routers

$(document).ready ->
  ACC.Views.Navigation    = new NavigationView
  ACC.Views.Searchbar     = new SearchbarView
  ACC.Views.SidebarIndex  = new SidebarIndexView

  Backbone.history.start(pushState : true)
  sublimevideo.load()