#= require jquery
#= require jquery-ui-1.10.3.custom.js

#= require bootstrap
#= require bootstrap-datepicker

$ ->
  $(".sortable").sortable()
  $(".sortable").disableSelection()

$ ->
  $(".draggable").draggable revert: "invalid"
  $(".droppable").droppable
    activeClass: "ui-state-hover"
    hoverClass: "ui-state-active"
