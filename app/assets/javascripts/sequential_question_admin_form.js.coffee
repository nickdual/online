#= require 'jquery'
#= require 'jquery-ui-1.10.3.custom'
#= require underscore

SequentialQuestionAdminForm =
  _init: ->
    @.refresh()

    @.answer_toggler().on 'change', =>
      @.refresh()

    @.answer_items().on 'cocoon:after-insert', (event, answer_item) =>
      @.refresh()
      @.autoincrement_correct_position(answer_item)

  answer_items: ->
    @.element.find('[role="SequentialQuestionAdminForm.AnswerItems"]')

  answer_toggler: ->
    @.element.find('[role="SequentialQuestionAdminForm.AnswerToggler"]')

  autoincrement_correct_position: (answer_item)->
    answer_item.find('[role="SequentialQuestionAdminForm.CorrectPositionInput"]').val(_([@.max_correct_position(), 0]).max() + 1)
    undefined

  image_answers: ->
    @.element.find('[role="SequentialQuestionAdminForm.ImageAnswer"]')

  max_correct_position: ->
    _(@.element.find('[role="SequentialQuestionAdminForm.CorrectPositionInput"]'))
      .chain()
      .map((e) ->  parseInt($(e).val()))
      .compact()
      .max()
      .value()

  must_display_image_answers: ->
    @.answer_toggler().is(':checked')

  refresh: ->
    @.image_answers().toggle(@.must_display_image_answers())
    @.text_answers().toggle(! @.must_display_image_answers())
    undefined

  text_answers: ->
    @.element.find('[role="SequentialQuestionAdminForm.TextAnswer"]')


$.widget("ui.sequential_question_admin_form", SequentialQuestionAdminForm);
