#= require jquery
#= require jquery_ujs
#= require jquery.quicksearch.js
#= require admin-unicorn-bootstrap
#= require cocoon
#= require jquery-ui-1.10.3.custom.js
#= require image_preview

$ ->
  $(".sortable").sortable placeholder: "ui-state-highlight"
  $(".sortable").disableSelection()

  $('[data-ImagePreview]').image_preview()

$ ->
  # Chosen Styles
  $("select").chosen()
  $(".radio").button()
  $("input[type=checkbox],input[type=radio],input[type=file]").uniform()
  $("input.quicksearch").quicksearch "table.quicksearch-target tbody tr"

  $ ->
  $(".draggable").draggable revert: "invalid"
  $(".droppable").droppable
    activeClass: "ui-state-hover"
    hoverClass: "ui-state-active"
