class Employment < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  belongs_to :user, touch: true
  belongs_to :service, touch: true

  has_many :employment_learning_plans, :dependent => :destroy
  has_many :completed_employment_learning_plans, :dependent => :destroy, :class_name => "EmploymentLearningPlan", :include => "employment_learning_plans", :conditions => "employment_learning_plans.status = 'completed'"
  has_many :learning_plans, :through => :employment_learning_plans
  has_many :learning_resource_downloads, :dependent => :destroy
  has_many :groups, :through => :service
  has_many :learning_records, :dependent => :destroy
  has_many :program_assessments, :dependent => :destroy
  has_many :programs, :through => :service
  has_many :learning_resources, :through => :programs

  has_and_belongs_to_many :group_sessions
  before_create :set_started_at

  attr_accessor :activation_token, :job_title_name

  before_save :set_service_from_activation_token, :if => :find_service_by_token
  before_save :remove_learning_plans, :if => proc { |attrs| attrs.status == "inactive" }
  before_validation :set_service_id, :set_role

  validates :service_id, presence: true
  validate :ensure_only_one_current_employment

  validates_date :started_at, :allow_blank => true
  validates_date :ended_at, :allow_blank => true

  delegate :name, :first_name, :last_name, :email, :formatted_born_at, :gender, :sign_in_count, :current_sign_in_at, :to => :user
  delegate :email_verified_at, :last_email_status, :last_email_bounce_description, :welcome_email_resent_at, :to => :user
  delegate :group, to: :service, prefix: true
  alias_attribute :to_s, :label

  accepts_nested_attributes_for :user
  accepts_nested_attributes_for :program_assessments,
                                :reject_if => proc { |attrs| attrs['selected'] != "true" }

  scope :order_by_name, joins(:user).order("users.first_name,users.last_name")
  scope :order_by_service, joins(:service).order("services.name")
  scope :active, where("ended_at IS NULL")
  scope :inactive, where("ended_at IS NOT NULL")
  scope :not_group_coordinator, where("role != ?", "group coordinator")
  scope :coordinator, where("role = ?", "coordinator")
  scope :non_staff, where("role != ?", "staff")

  def label
    if job_title_name
      [user.to_s, " <i>(", job_title_name, ")</i>"].join("").html_safe
    else
      user.to_s
    end
  end

  def email=(email)
    self.user.email = email
  end

  def active?
    status == "active"
  end

  def status
    ended_at ? "inactive" : "active"
  end

  def find_service_by_token
    Service.find_by_activation_token(activation_token)
  end

  def finish!
    update_attributes(:ended_at => Time.zone.now)
  end

  def formatted_started_at
    started_at.to_s(:aus) unless started_at.blank?
  end

  def formatted_ended_at
    ended_at.to_s(:aus) unless ended_at.blank?
  end

  def learning_record_count
    learning_records.count
  end

  def pending_learning_plan_programs
    employment_learning_plans.upcoming.flat_map(&:programs_pending).uniq(&:program_id)
  end

  def overdue_learning_plan_programs
    employment_learning_plans.overdue.flat_map(&:programs_pending).uniq(&:program_id)
  end

  def learning_record_programs
    learning_records.reorder("program_id").select("DISTINCT ON (program_id) *").map(&:program).sort_by!(&:title)
  end

  # Polymorphic association based on the service, #winning
  def job_title
    if self.job_title_id
      self.service.group_id.present? ? Group::JobTitle.find(self.job_title_id) : Service::JobTitle.find(self.job_title_id)
    end
  end
  
  def job_title_name
    begin
      job_title.try(:name)
    rescue ActiveRecord::RecordNotFound
      nil
    end
  end

  def service_name
    service.name
  end

  def job_titles
    if service.group_id.present?
      Group::JobTitle.where(group_id: service.group_id).default_order
    else
      Service::JobTitle.where(service_id: service_id).default_order
    end
  end

  def self.role_options
    ["staff", "coordinator"]
  end

  private

    def remove_learning_plans
      employment_learning_plans.each { |e| e.destroy }
    end

    def service_id_by_activation_token
      Service.where(activation_token: self.activation_token).first.try(:id)
    end

    def existing_employments
      Employment.where(service_id: self.service_id).where(user_id: self.user_id).active
    end

    def ensure_only_one_current_employment
      if self.new_record? && existing_employments.any?
        errors.add(:user_id, "is already employed at service")
      end
    end

    def set_service_id
      unless service_id
        self.service_id = service_id_by_activation_token
      end
    end

    def set_service_from_activation_token
      self.service = find_service_by_token
    end

    def set_started_at
      self.started_at = Time.zone.now if started_at.blank?
    end

    def set_role
      self.role ||= "staff"
    end
end
