module LearningPlanReminder
  extend ActiveSupport::Concern
  module ClassMethods
    def activate_plans
      today = Time.zone.now

      EmploymentLearningPlan.where(active_at: [today.beginning_of_day..today.end_of_day]).each do |plan|
        plan.active!
      end
    end

    def overdue_reminders
      yesterday = Time.zone.now.yesterday

      EmploymentLearningPlan.where(due_at: [yesterday.beginning_of_day..yesterday.end_of_day]).active.each do |plan|
        plan.overdue!
      end
    end

    def due_in_x(days)
      due_at = Time.zone.now.advance(days: days)

      EmploymentLearningPlan.where(due_at: [due_at.beginning_of_day..due_at.end_of_day]).upcoming.each do |plan|
        LearningPlanNotifier.due_in_x(plan, days).deliver
      end
    end
  end
end