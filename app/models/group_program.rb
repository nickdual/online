class GroupProgram < ActiveRecord::Base

  self.primary_keys = [:group_id, :program_id]
  belongs_to :package
  belongs_to :group
  belongs_to :program

  has_many :programs, :through => :program_lists
  has_many :program_lists, :class_name => "GroupProgramList", :foreign_key => [:group_id, :group_program_id], :primary_key => [:group_id, :program_id]

  delegate :code, :title, :updated_by, :updated_at, :to => :program, :prefix => "program"

  scope :default_order, includes(:program).order("programs.title, programs.code")
  accepts_nested_attributes_for :program_lists,
                                :reject_if => proc { |attrs| attrs['selected'] != "1" },
                                :allow_destroy => true

  def program_ids
    program_lists.reject(&:new_record?).map(&:program_id)
  end

end
