class Employment::ExistingUser < Employment
  after_create :welcome_user_to_service

  def active_model_serializer
    EmploymentSerializer
  end

  private

    def welcome_user_to_service
      UserNotifier.user_has_joined_service(self).deliver
    end

end