class Video < ActiveRecord::Base

  include ProgramSettings # To create zencoder settings
  belongs_to :program

  asset_accessor :file
  delegate :remote_url, :to => :file
  after_create :mark_video_as_processed, :if => :program_complete?
  before_save :set_codec, :set_order_id
  before_create :set_dimensions

  validates_uniqueness_of :description, :scope => :program_id

  default_scope order(:order_id)
  scope :default_order, order(:order_id)
  scope :zencoder_processed, where("zencoder_id IS NOT null")
  scope :hd, where(description: ["browser_webm_hd", "browser_h264_hd"])
  scope :sd, where(description: ["browser_h264_sd", "browser_webm_sd"])
  scope :mobile, where(description: "playlist")
  scope :not_original, where("description != ?", "original video")

  def url
    if ENV['STREAMING_URL'].blank?
      file.url
    else
      ENV['STREAMING_URL'] + self.file_uid
    end
  end
  
  def hd
    ["browser_webm_hd", "browser_h264_hd"].include?(self.description)
  end

  def hd_video
    case description
    when "browser_h264_sd"
      program.videos.where(description: "browser_h264_hd").first
    when "browser_webm_sd"
      program.videos.where(description: "browser_webm_hd").first
    end
  end
  
  def program_complete?
    program.videos.zencoder_processed.count == 10
  end
  
  def set_dimensions
    unless zencoder_id.blank?
      zencoder = Zencoder::Output.details(zencoder_id).body
      self.width, self.height = zencoder["width"], zencoder["height"]
    end
  end
  
  private
    
    def mark_video_as_processed
      self.program.mark_as_processed!
      self.program.update_attributes(
        video_width:  program.videos.sd.first.try(:width),
        video_height: program.videos.sd.first.try(:height))
    end

end
