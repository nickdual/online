class Service::JobTitle < ActiveRecord::Base

  belongs_to :service

  validates :service_id, presence: true
  validates :name, presence: true

  before_destroy :clear_job_titles
  scope :default_order, order(:name)
  alias_attribute :to_s, :name

  def truncated_name
    name.length > 21 ? (name[0..21] + "...") : name
  end

  def employments
    Employment.includes(:user).where(job_title_id: self.id).where(service_id: self.service_id)
  end

  private

    def clear_job_titles
      employments.each { |e| e.update_attribute(:job_title_id, nil) }
    end

end
