class ProgramType < ActiveRecord::Base

  has_many :programs
  validates :name, presence: true
  alias_attribute :to_s, :name

end
