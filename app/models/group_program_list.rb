class GroupProgramList < ActiveRecord::Base

  belongs_to :program
  belongs_to :group_program, :foreign_key => [:group_id, :group_program_id], :primary_key => [:group_id, :program_id]

  attr_accessor :selected
  before_save :set_position
  after_save :update_service_lists
  before_destroy :remove_from_service_lists
  
  scope :default_order, order(:position)
  
  delegate :fixed_width, :fixed_height, :thumb, :videos, :title, :to => :program
  
  def service_ids
    Service.where(group_id: group_id).map(&:id)
  end

  private

    def set_position
      last_program = group_program.program_lists.joins(:program).where("programs.classification = ?", program.classification).last

      if last_program
        self.position = (last_program.position + 1)
      else
        self.position = 1
      end
    end

    def update_service_lists
      ServiceProgram.where(service_id: service_ids).where(program_id: group_program.program_id).each do |service_program|
        existing_programs = service_program.program_lists.where(:program_id => self.program_id)
        service_program.program_lists.create!(:program_id => self.program_id, :position => self.position) unless existing_programs.any?
      end
    end
    
    def remove_from_service_lists
      ServiceProgram.where(service_id: service_ids).where(program_id: group_program.program_id).each do |service_program|
        service_program.program_lists.where(:program_id => self.program_id).each { |p| p.destroy }
      end
    end
end
