class User::Activity
  attr_accessor :activities

  def initialize(user)
    @user       = user
    @activities = []
    learning_records
    group_session_records
    assessments
    order_activities
  end

  def order_activities
    @activities.sort_by { |a| a[:viewed_at] }.reverse
  end

  def date_ordinalized(date)
    day_of_month  = date.strftime("%-d")
    month         = date.strftime("%b")
    "#{ActiveSupport::Inflector.ordinalize(day_of_month)} #{month}"
  end

  def group_session_records
    @user.learning_records.includes(:program).group_sessions.group_by_viewed_at.each do |learning_record|
      attributes = {
        activity_type:      "Viewed Program",
        title:              learning_record.title,
        medium:             "Viewed via #{learning_record.medium}",
        record_created_by:  learning_record.group_session.created_by.to_s,
        viewed_at:          learning_record.viewed_at,
        viewed_date:        date_ordinalized(learning_record.viewed_at),
        viewed_time:        learning_record.viewed_at.strftime("%l:%M%P"),
        thumb_60_60_url:    learning_record.thumb_60_60_url,
        program_url:        Rails.application.routes.url_helpers.admin_program_path(learning_record.program)
      }

      @activities << attributes
    end
  end

  def learning_records
    @user.learning_records.includes(:program).where(group_session_id: nil).default_order.group_by_viewed_at.each do |learning_record|
      attributes = {
        activity_type:    "Viewed Program",
        title:            learning_record.title,
        medium:           "Viewed #{learning_record.medium}",
        viewed_at:        learning_record.viewed_at,
        viewed_date:      date_ordinalized(learning_record.viewed_at),
        viewed_time:      learning_record.viewed_at.strftime("%l:%M%P"),
        thumb_60_60_url:  learning_record.thumb_60_60_url,
        program_url:      Rails.application.routes.url_helpers.admin_program_path(learning_record.program)
      }

      @activities << attributes
    end
  end

  def assessments
    @user.program_assessments.group_by_assessed_at.includes(:program).each do |assessment|
      attributes = {
        activity_type:      "#{assessment.assessment_type} Assessment Completed",
        title:              assessment.title,
        medium:             "Assessed Online",
        record_created_by:  assessment.coordinator.try(:name) || @user.name,
        assessment_type:    assessment.assessment_type,
        viewed_at:          assessment.assessed_at,
        viewed_date:        date_ordinalized(assessment.assessed_at),
        viewed_time:        assessment.assessed_at.strftime("%l:%M%P"),
        thumb_60_60_url:    assessment.program.thumb_60_60_url,
        program_url:        Rails.application.routes.url_helpers.admin_program_path(assessment.program)
      }

      @activities << attributes
    end
  end
end
