# This is creating an employment / existing user / whatever off the bat
class User::Questionable < User
  attr_accessor :contact, :attributes

  def initialize(attributes)
    @attributes = attributes
  end

  def save
    if existing_user
      @contact = Employment::ExistingUser.new(employment_attributes)
    else
      @contact = User::Invited.new(user_attributes)
    end

    contact.save
  end

  def errors
    @contact.errors
  end

  def employment
    if @contact.is_a?(Employment::ExistingUser)
      @contact
    elsif @contact
      @contact.employments.first
    end
  end

  def user
    @contact.is_a?(Employment::ExistingUser) ? @contact.user : @contact
  end

  private

    def employment_attributes
      {
        activation_token: @attributes[:activation_token],
        employee_number:  @attributes[:employee_number],
        service_id:       @attributes[:service_id],
        user_id:          existing_user.id,
        job_title_id:     @attributes[:job_title_id],
        role:             @attributes[:role]
      }
    end

    def user_attributes
      {
        first_name:       @attributes[:first_name],
        last_name:        @attributes[:last_name],
        email:            @attributes[:email],
        gender:           @attributes[:gender],
        activation_token: @attributes[:activation_token],
        employee_number:  @attributes[:employee_number],
        job_title_id:     @attributes[:job_title_id],
        role:             @attributes[:role]
      }
    end

    def existing_user
      User.where(email: user_attributes[:email]).first
    end
end
