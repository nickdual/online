module User::Stats
  extend ActiveSupport::Concern

  def plans_overdue_count
    employment_learning_plans.overdue.count
  end

  def assessments_completed_count
    program_assessments.count
  end

  def programs_viewed_count
    # AR doesn't include discint in .count, so we're forcing into an account
    learning_records.group_by_viewed_at.all.count
  end

  def programs_awaiting_completion_count
    employment_learning_plans.not_completed.collect { |plan| plan.programs_pending.flat_map(&:id) }.uniq.count
  end

  def active_employments_count
    employments.active.count
  end

  def learning_plan_progress
    if employment_learning_plans.not_pending.count.zero?
      0
    else
      ((employment_learning_plans.completed.count.to_f / employment_learning_plans.not_pending.count.to_f) * 100).to_i
    end
  end
end