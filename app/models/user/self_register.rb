class User::SelfRegister < User
  # Employment attributes
  attr_accessor :activation_token, :job_title_id, :employee_number, :role

  validates :activation_token, presence: true, allow_blank: true

  before_validation :set_password
  before_create :build_employment, :set_activation_code_set_at
  after_create :welcome_user

  def active_model_serializer
    UserSerializer
  end

  def employment
    self.employments.first
  end

  private

    def welcome_user
      UserNotifier.welcome_self_registered_user(self, self.employment.service).deliver
    end

    def employment_attributes
      {
        role:             role || "staff",
        activation_token: activation_token,
        employee_number:  employee_number,
        job_title_id:     job_title_id
      }
    end

    def build_employment
      employments.build(employment_attributes)
    end

    def valid_token
      Service.where(activation_token: activation_token).any?
    end

    def ensure_valid_activation_token
      valid_token ? valid_token : errors.add(:activation_token, "must be valid")
    end

    def set_activation_code_set_at
      self.activation_code_set_at = Time.zone.now
    end

    def set_password
      if ensure_valid_activation_token
        self.password, self.password_confirmation = activation_token
      end
    end
end
