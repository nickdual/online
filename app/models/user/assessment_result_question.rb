class User::AssessmentResultQuestion < ActiveRecord::Base
  belongs_to :assessment_result, class_name: "User::AssessmentResult", foreign_key: "assessment_result_id"

  attr_accessible :assessment_result_id, :correct, :question_id, :question_type

  validates :question_id, presence: true

  def self.correct
    where(correct: true)
  end

  def self.incorrect
    where(correct: false)
  end
end
