class User::AssessmentResult < ActiveRecord::Base
  belongs_to :user
  belongs_to :assessment, class_name: "Program::Assessment"

  has_many :questions, class_name: "User::AssessmentResultQuestion", foreign_key: "assessment_result_id"

  attr_accessible :assessment_id, :user_id, :passed, :time_taken, :questions_attributes

  validates :time_taken, presence: true

  before_save :create_program_assessments
  accepts_nested_attributes_for :questions

  def self.failed
    where(passed: false)
  end

  def self.passed
    where(passed: true)
  end

  def failed?
    ! passed?
  end

  private

    def create_program_assessments
      if passed
        self.user.create_program_assessment!(assessment.program_id, "Essentials")
      end
    end

end
