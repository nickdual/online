class User::Invited < User::SelfRegister

  private

    def welcome_user
      UserNotifier.welcome_invited_user(self, self.employments.first).deliver
    end

end