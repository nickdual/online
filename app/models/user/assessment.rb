require 'ostruct'

class User::Assessment
  def initialize(user, program)
    @user     = user
    @program  = program
  end

  def assessments
    attributes = {
      essentials_required:  essentials_required,
      evidence_required:    evidence_required,
      extension_required:   extension_required,
      essentials_completed: essentials_completed,
      evidence_completed:   evidence_completed,
      extension_completed:  extension_completed,
      essentials_overdue:   essentials_overdue,
      evidence_overdue:     evidence_overdue,
      extension_overdue:    extension_overdue,
      essentials_failed:    essentials_failed
    }
  end

  def learning_plan_ids
    LearningPlanProgram.where(program_id: @program.id).map(&:learning_plan_id)
  end

  def learning_plan_programs
    @learning_plan_programs ||= @user.employment_learning_plans.not_pending.where(learning_plan_id: learning_plan_ids)
         .includes(:service, :user, :employment, :learning_records, :program_assessments, :employment_learning_plan_programs)
         .flat_map(&:programs_all).select { |p| p.program_id == @program.id }
  end

  def learning_assessments
    @learning_assessments ||= @user.program_assessments.where(program_id: @program.id)
  end

  def essentials_failed
    @program.essentials_assessment.failed_by_user?(@user)
  end

  def essentials_required
    learning_plan_programs.map(&:require_essentials).include?(true)
  end

  def evidence_required
    learning_plan_programs.map(&:require_evidence).include?(true)
  end

  def extension_required
    learning_plan_programs.map(&:require_extension).include?(true)
  end

  def essentials_overdue
    learning_plan_programs.map(&:overdue_essentials).include?(true)
  end

  def evidence_overdue
    learning_plan_programs.map(&:overdue_evidence).include?(true)
  end

  def extension_overdue
    learning_plan_programs.map(&:overdue_extension).include?(true)
  end

  def essentials_completed
    required_essentials = learning_plan_programs.select { |p| p.require_essentials == true }
    completed_essentials = learning_plan_programs.select { |p| p.completed_essentials == true }

    # Completed all required essentials
    required_essentials.count <= completed_essentials.count && !required_essentials.count.zero?
  end

  def evidence_completed
    required_evidence = learning_plan_programs.select { |p| p.require_evidence == true }
    completed_evidence = learning_plan_programs.select { |p| p.completed_evidence == true }

    # Completed all required evidence
    required_evidence.count <= completed_evidence.count && !required_evidence.count.zero?
  end

  def extension_completed
    required_extension = learning_plan_programs.select { |p| p.require_extension == true }
    completed_extension = learning_plan_programs.select { |p| p.completed_extension == true }

    # Completed all required extension
    required_extension.count <= completed_extension.count && !required_extension.count.zero?
  end
end
