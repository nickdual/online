class Admin < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  belongs_to :created_by, :class_name => "Admin"
  belongs_to :updated_by, :class_name => "Admin"
  has_many :created_admins, :class_name => "Admin", :foreign_key => "created_by_id"
  has_many :updated_admins, :class_name => "Admin", :foreign_key => "updated_by_id"
  has_many :disabled_services, :class_name => "Service", :foreign_key => "disabled_by_id"
  has_many :created_services, :class_name => "Service", :foreign_key => "created_by_id"
  has_many :updated_services, :class_name => "Service", :foreign_key => "updated_by_id"
  has_many :activated_assessments, :class_name => "Program::Assessment", :foreign_key => "activated_by_id"
  has_many :disabled_groups, :class_name => "Group", :foreign_key => "disabled_by_id"
  has_many :created_groups, :class_name => "Group", :foreign_key => "created_by_id"
  has_many :updated_groups, :class_name => "Group", :foreign_key => "updated_by_id"

  has_many :created_learning_resources, :class_name => "LearningResource", :foreign_key => "created_by_id"
  has_many :updated_learning_resources, :class_name => "LearningResource", :foreign_key => "updated_by_id"
  has_many :created_program_categories, :class_name => "ProgramCategory", :foreign_key => "created_by_id"
  has_many :updated_program_categories, :class_name => "ProgramCategory", :foreign_key => "updated_by_id"
  has_many :created_packages, :class_name => "Package", :foreign_key => "created_by_id"
  has_many :updated_packages, :class_name => "Package", :foreign_key => "updated_by_id"
  has_many :created_users, :class_name => "User", :foreign_key => "created_by_id"
  has_many :updated_users, :class_name => "User", :foreign_key => "updated_by_id"

  belongs_to :created_programs, :class_name => "Program", :foreign_key => :created_by_id
  belongs_to :updated_programs, :class_name => "Program", :foreign_key => :updated_by_id

  validates_presence_of :first_name, :last_name
  validates_inclusion_of :role, :allow_nil => true, :in => lambda { |obj| Admin.role_options }

  alias_attribute :to_s, :name

  is_sluggable :name

  scope :default_order, order("first_name, last_name, email")
  after_create :invite_admin_and_reset_password!
  before_save :set_role

  def name
    [first_name, last_name].join(" ")
  end

  def name=(name)
    name = name.split(" ")
    self.first_name = name.first
    self.last_name = name.last
  end

  def self.role_options
    ["administrator", "moderator", "viewer"]
  end

  def name_changed?
    first_name_changed? or last_name_changed?
  end

  private

  def set_role
    self.role = "viewer" unless role
  end

  def invite_admin_and_reset_password!
    generate_reset_password_token!
    UserNotifier.delay.invite_admin(self)
  end

end
