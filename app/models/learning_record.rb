class LearningRecord < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  belongs_to :program
  belongs_to :employment
  belongs_to :group_session
  has_one :user, :through => :employment
  validates_presence_of :program_id, :employment_id

  delegate :title, :thumb_200_200_url, :thumb_60_60_url, :to => :program
  before_save :set_viewed_at
  after_save :update_learning_plans

  scope :default_order, order("viewed_at DESC")
  scope :group_by_medium, select(:medium).group(:medium).reorder(:medium)
  scope :group_by_viewed_at, select("DISTINCT ON (date_trunc('minute', viewed_at), program_id) learning_records.*").reorder("date_trunc('minute', viewed_at) DESC")
  scope :group_sessions, where("group_session_id IS NOT NULL")

  def downloaded
    learning_resource_ids = program.learning_resources.map(&:id)
    employment.learning_resource_downloads.where(learning_resource_id: learning_resource_ids).any?
  end

  private

    def set_viewed_at
      self.viewed_at = Time.zone.now unless viewed_at
    end

    def learning_plan_ids
      employment.employment_learning_plans
        .not_completed.joins(:learning_plan_programs)
        .where("learning_plan_programs.program_id = ?", self.program_id).pluck("employment_learning_plans.id")
    end

    def update_learning_plans
      # We're touching the plan to reset the cache
      EmploymentLearningPlan.where(id: learning_plan_ids).each do |plan|
        plan.touch; plan.set_status!;
        plan.learning_plan.touch
      end
    end
end
