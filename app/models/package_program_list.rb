class PackageProgramList < ActiveRecord::Base
  
  belongs_to :package_program
  belongs_to :program
  
  attr_accessor :selected
  delegate :title, :to => :program
  before_save :set_position
  after_save :update_group_lists, :update_service_lists
  before_destroy :remove_from_group_lists, :remove_from_service_lists
  
  scope :default_order, order(:position)
  
  delegate :fixed_width, :fixed_height, :thumb, :videos, :title, :to => :program
  
  private
  
    def set_position
      last_program = package_program.program_lists.joins(:program).where("programs.classification = ?", program.classification).last

      if last_program
        self.position = (last_program.position + 1)
      else
        self.position = 1
      end
    end
    
    def update_group_lists
      GroupProgram.where(program_id: package_program.program_id, package_id: package_program.package_id).each do |group_program|
        existing_programs = group_program.program_lists.where(:program_id => self.program_id)

        program_list_attributes = {
          :group_id => group_program.group_id,
          :group_program_id => group_program.program_id,
          :program_id => self.program_id,
          :position => self.position
        }

        group_program.program_lists.create!(program_list_attributes) unless existing_programs.any?
      end
    end
    
    def remove_from_group_lists
      GroupProgram.where(program_id: package_program.program_id, package_id: package_program.package_id).each do |group_program|
        group_program.program_lists.where(:program_id => self.program_id).each { |p| p.destroy }
      end
    end
    
    def update_service_lists
      ServiceProgram.where(program_id: package_program.program_id, group_id: nil, package_id: package_program.package_id).where(group_id: nil).each do |service_program|
        existing_programs = service_program.program_lists.where(:program_id => self.program_id)

        program_list_attributes = {
          :service_id => service_program.service_id,
          :service_program_id => service_program.program_id,
          :program_id => self.program_id,
          :position => self.position
        }

        service_program.program_lists.create!(program_list_attributes) unless existing_programs.any?
      end
    end
    
    def remove_from_service_lists
      ServiceProgram.where(program_id: package_program.program_id, group_id: nil, package_id: package_program.package_id).each do |service_program|
        service_program.program_lists.where(:program_id => self.program_id).each { |p| p.destroy }
      end
    end
    
end
