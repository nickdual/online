class Coordinator < ActiveRecord::Base

  # Including CoordinatorReminder so we get Sidekiq#delay easily
  include CoordinatorReminder

  belongs_to :user
  belongs_to :group

  delegate :name, :current_sign_in_at, :to => :user
  delegate :name, :to => :group, :prefix => "group"

  before_create :assign_user_to_services
  before_destroy :remove_user_to_services

  validates_uniqueness_of :user_id, :scope => :group_id

  scope :default_order, joins(:user).order("users.first_name, users.last_name")

  def save_with_user
    if self.valid? && user.valid?
      user.save && self.save
    else
      user.errors.each do |attribute, message|
        self.errors.add attribute, message
      end
      false
    end
  end

  private

    def assign_user_to_services
      group.services.each do |service|
        existing_employment = service.employments.where(user_id: user_id).active.first

        if existing_employment
          existing_employment.update_attributes(:role => "group coordinator")
        else
          service.employments.create(:user => user, :role => "group coordinator")
        end
      end
    end

    def remove_user_to_services
      service_ids = group.services.map(&:id)

      Employment.where("user_id = ? AND service_id in (?)", user_id, service_ids).each do |employment|
        employment.finish!
      end
    end

end
