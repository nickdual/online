class UserImport::User
  # As we need to set items on the original object
  attr_accessor :user_import
  attr_accessor :questionable, :user_attributes

  delegate :user, :employment, :to => :questionable

  def initialize(attributes, user_import)
    @user_import      = user_import
    @user_attributes  = attributes
    @questionable     = ::User::Questionable.new(@user_attributes)
  end

  def import
    questionable.save ? successful_import : unsuccessful_import
  end

  def check_job_title
    if employment && employment.job_title_id.blank? && !@user_attributes[:job_title].blank?
      unsuccessful_job_title
    end
  end

  private

    def successful_import
      user_import.successful_invited_users << user
    end

    def unsuccessful_import
      if user.errors.any?
        user_import.unsuccessful_users << user
      else
        user_import.unsuccessful_users << employment
      end
    end

    def unsuccessful_job_title
      user.job_title = @user_attributes[:job_title]
      user_import.failed_job_titles << user
    end

end
