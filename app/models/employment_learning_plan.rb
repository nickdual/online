class EmploymentLearningPlan < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  belongs_to :learning_plan, touch: true
  belongs_to :employment

  include EmploymentLearningPlanStats

  has_many :employment_learning_plan_programs, :primary_key => :learning_plan_id, :foreign_key => :learning_plan_id
  has_many :learning_plan_programs, :through => :learning_plan
  has_many :programs, :through => :learning_plan_programs
  has_many :program_assessments, :through => :employment
  has_many :learning_records, :through => :employment
  has_one :user, :through => :employment
  has_one :service, :through => :learning_plan

  validates_uniqueness_of :employment_id, :scope => :learning_plan_id

  before_create :set_active_at, :set_due_at

  delegate :to_s, :name, :description, :service_id, :to => :learning_plan
  delegate :first_name, :to => :employment
  delegate :service_name, :to => :learning_plan

  delegate :name, :to => :user, :prefix => true

  ["active", "overdue", "pending", "completed"].each do |status|
    scope status, where(status: status)
  end

  scope :upcoming, where(status: ["overdue", "active"])
  scope :not_completed, where("status != ?", "completed")
  scope :not_pending, where("status != ?", "pending")

  state_machine :status, :initial => :pending do
    event :pending do
      transition :active    => :pending
      transition :completed => :pending
      transition :overdue   => :pending
    end

    after_transition :on => :active, :do => :notify_users_of_created_learning_plan!
    before_transition :on => :active, :do => :activate_now!
    event :active do
      transition :completed => :active # For when we add programs to a learning plan
      transition :pending   => :active
      transition :overdue   => :active # Someone updates the days till due
    end

    before_transition :on => :complete, :do => :set_completed_at
    event :complete do
      transition :pending => :completed
      transition :active  => :completed
      transition :overdue => :completed
    end

    before_transition :on => :overdue, :do => :notify_of_overdue!
    event :overdue do
      transition :pending   => :overdue
      transition :active    => :overdue
      transition :completed => :overdue
    end
  end

  # TODO: Make days_till_due not dependent on cached views
  def cache_key
    "#{self.id}-#{self.updated_at.utc.to_s(:number)}-#{Date.today.strftime("%Y%m%d")}"
  end

  def status_id
    case status
    when "overdue"
      1
    when "active"
      2
    when "pending"
      3
    when "completed"
      4
    end
  end

  def formatted_due_at
    due_at.to_s(:aus) unless due_at.blank?
  end

  def formatted_completed_at
    completed_at.to_s(:aus) unless completed_at.blank?
  end

  def formatted_active_at
    active_at.to_s(:aus) unless active_at.blank?
  end

  def days_till_active
    distance = (self.active_at.beginning_of_day.to_i - Time.zone.now.beginning_of_day.to_i) / 60 / 60 / 24.0
    distance > 0 ? distance.to_i.abs : distance.abs.ceil
  end

  def days_remaining
    distance = (self.due_at.beginning_of_day.to_i - Time.zone.now.beginning_of_day.to_i) / 60 / 60 / 24.0
    distance > 0 ? distance.to_i.abs : distance.abs.ceil
  end
  def check_status
    distance = (self.due_at.beginning_of_day.to_i - Time.zone.now.beginning_of_day.to_i) / 60 / 60 / 24.0
    distance > 0 ? 1 : 0
  end

  def employment_name
    employment.name
  end

  def loaded_program_assessments
    program_assessments.select { |assessment| assessment.assessed_at > active_at.beginning_of_day }
  end

  def loaded_learning_records
    learning_records.select { |record| record.viewed_at > active_at.beginning_of_day }
  end

  def service
    Service.find(learning_plan.service_id) if learning_plan.service_id
  end

  def set_status!
    set_status; save!
  end

  def set_due_at!
    set_due_at; save!
  end

  private

    def notify_users_of_created_learning_plan!
      LearningPlanNotifier.new_plan(self.id).deliver
    end

    def notify_of_overdue!
      LearningPlanNotifier.overdue_plan(self).deliver
    end

    def set_completed_at
      self.completed_at = Time.zone.now
    end

    # We're force activating the plan, may as well start it from today.
    def activate_now!
      self.active_at = Time.zone.now.beginning_of_day
      set_due_at
    end

    def set_active_at
      # If the active date is in the future, set for this time
      if learning_plan.reload.active_at > Time.zone.now
        self.active_at = learning_plan.reload.active_at
      else
        # Else set it for tomorrow
        self.active_at = Time.zone.now.tomorrow.beginning_of_day
      end
    end

    def set_due_at
      # Assigned from when the learning plan becomes active
      self.due_at = self.active_at.advance(days: learning_plan.reload.days_till_due)
    end

    def set_status
      # We only about updating status if the plan is already active
      if (self.active? || self.overdue?) && self.programs_pending_count.zero?
        self.complete!
      end
    end

end
