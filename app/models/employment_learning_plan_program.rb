class EmploymentLearningPlanProgram < LearningPlanProgram
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  attr_accessor :employment, :employment_learning_plan

  alias_attribute :require_essentials, :essentials
  alias_attribute :require_extension, :extension
  alias_attribute :require_evidence, :evidence

  delegate :status, :to => :employment_learning_plan

  def viewed_program
    employment_learning_plan.loaded_learning_records.select { |p| p.program_id == program_id }.last.try(:viewed_at)
  end

  def viewed_at_time
    viewed_program.strftime("%l:%M%P") if viewed_program
  end

  def viewed_at_date
    viewed_program.to_s(:aus) if viewed_program
  end

  def viewed_at
    viewed_program
  end

  def assessments
    employment_learning_plan.loaded_program_assessments.
      select { |assessment| assessment.program_id == program_id }.map(&:assessment_type).to_set
  end

  # TODO: This is being generated twice, stop that
  def completed_essentials
    assessments.include?("Essentials")
  end

  def completed_evidence
    assessments.include?("Evidence")
  end

  def completed_extension
    assessments.include?("Extension")
  end

  def overdue_evidence
    employment_learning_plan.overdue? && require_evidence.present? && !completed_evidence
  end

  def overdue_extension
    employment_learning_plan.overdue? && require_extension.present? && !completed_extension
  end

  def overdue_essentials
    employment_learning_plan.overdue? && require_essentials.present? && !completed_essentials
  end

  def is_essential_assessment_active
    program.assessments.where(assessment_type: 'Essentials').first.active?
  end

  def is_essential_assessment_unsuccessful
    employment.user.assessments(program)[:essentials_failed]
  end


  class << self
    def generate_completions(learning_plan_programs)
      learning_plan_programs.each do |plan_program|
        plan_program.employment               = @employment
        plan_program.employment_learning_plan = @plan
      end
    end

    def pending(plan)
      @plan = plan
      @employment = plan.employment

      # TODO: Make this one line
      if completed_ids.any?
        generate_completions @plan.employment_learning_plan_programs.select { |program|
          !completed_ids.include?(program.program_id) }
      else
        generate_completions @plan.employment_learning_plan_programs.select { |program|
          program_ids.include?(program.id) }
      end
    end

    def completed(plan)
      @plan = plan
      @employment = plan.employment

      # TODO: Make this one line
      if completed_ids.any?
        generate_completions @plan.employment_learning_plan_programs.select { |program|
          completed_ids.include?(program.program_id) }
      else
        []
      end
    end

    def program_ids
      @plan.employment_learning_plan_programs.map(&:id)
    end

    def learning_record_ids
      program_ids = @plan.loaded_learning_records.map(&:program_id).uniq
      @plan.employment_learning_plan_programs.select { |program| program_ids.include?(program.program_id) }.map(&:program_id)
    end

    def assessed_program_ids
      assessed = @plan.employment_learning_plan_programs
        .select { |program| program_ids.include?(program.id) }.select do |plan_program|
        assessments = @plan.loaded_program_assessments
          .select { |assessment| assessment.program_id == plan_program.program_id }
          .select { |assessment| plan_program.criteria.include?(assessment.assessment_type) }

        if assessments.any?
          results = assessments.map(&:assessment_type).to_set
          results == plan_program.criteria
        end
      end

      unassessable_ids = @plan.employment_learning_plan_programs.select { |program|
        !program.require_essentials && !program.require_extension && !program.require_evidence }.map(&:program_id)

      assessed.map(&:program_id) + unassessable_ids
    end

    # A program is marked as completed based on...
    def completed_ids
      learning_record_ids & assessed_program_ids
    end
  end
end
