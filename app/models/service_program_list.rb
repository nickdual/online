class ServiceProgramList < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  belongs_to :program
  belongs_to :service_program, :foreign_key => [:service_id, :service_program_id], :primary_key => [:service_id, :program_id]

  attr_accessor :selected
  before_save :set_position

  validates_presence_of :service_id, :service_program_id, :program_id

  scope :default_order, order(:position)
  scope :introductions, joins(:program).where("programs.classification = ?", "introduction")
  scope :closers, joins(:program).where("programs.classification = ?", "closer")

  delegate :thumb, :videos, :fixed_width, :fixed_height, :title, :to => :program

  private

    def set_position
      last_program = ServiceProgramList.where(:service_id => self.service_id, :service_program_id => self.service_program_id).joins(:program).where("programs.classification = ?", program.classification).last

      if last_program
        self.position = (last_program.position + 1)
      else
        self.position = 1
      end
    end

end
