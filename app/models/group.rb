class Group < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  has_many :services, :dependent => :destroy
  has_many :employment_learning_plans, :through => :services
  has_many :service_users, :through => :services, :as => :users

  has_many :group_programs, :class_name => "GroupProgram"
  has_many :programs, :through => :group_programs
  has_many :job_titles, class_name: "Group::JobTitle", :dependent => :destroy
  has_and_belongs_to_many :packages

  has_many :coordinators, :dependent => :destroy
  has_many :users, :through => :coordinators
  has_many :employments, :through => :services
  has_many :learning_plans, :through => :services

  belongs_to :disabled_by, :class_name => "Admin"
  belongs_to :created_by, :class_name => "Admin"
  belongs_to :updated_by, :class_name => "Admin"

  validates_presence_of :name
  validates_uniqueness_of :name

  alias_attribute :to_s, :name
  alias_attribute :disabled?, :disabled_at

  is_sluggable :name
  scope :default_order, order(:name)
  scope :active, where(disabled_at: nil)

  after_save :update_group_program_playlists

  def disable!(admin)
    self.update_attributes(:disabled_by_id => admin.id, :disabled_at => Time.zone.now)

    self.services.each do |service|
      service.disable!(admin)
    end
  end

  def enable!
    self.update_attributes(:disabled_by_id => nil,
                           :disabled_at => nil)
    self.services.each do |service|
      service.enable!
    end
  end

  def status
    if self.disabled_by_id
      return "disabled by " + self.disabled_by.name + " (" + self.disabled_at.to_s(:short) + ")"
    else
      return "active"
    end
  end

  def address
    @address = country
  end

  def package_names
    self.packages.map(&:name).join(", ")
  end

  def employment_users
    service_ids = services.active.pluck(:id)
    user_ids = Employment.where(service_id: service_ids).active.pluck(:user_id)
    User.where(id: user_ids)
  end

  def services_count
    services.active.count
  end

  def employments_count
    service_ids = services.active.pluck(:id)
    Employment.where(service_id: service_ids).active.pluck(:user_id).uniq.count
  end

  def programs_viewed_this_week
    # TODO: This count could have issues with counting users employed at multiple services
    services.active.map(&:programs_viewed_this_week).flatten
  end

  private

    def update_group_program_playlists
      self.group_programs.each do |group_program|
        PackageProgram.where(:package_id => group_program.package_id).each do |package_program|
          package_program.program_lists.each { |p| p.update_attributes(:updated_at => Time.now) }
        end
      end
    end
end
