class EmploymentAbility
  include CanCan::Ability

  def initialize(user)
    can :manage, Employment do |employment|
      user_roles = user.employment_by_service(employment.service_id).map(&:role)
      user_roles.include?("coordinator") || user_roles.include?("group coordinator")
    end

    can :manage, Service do |service|
      user_roles = user.employment_by_service(service.id).map(&:role)
      user_roles.include?("coordinator") || user_roles.include?("group coordinator")
    end

    cannot :manage, Employment do |employment|
      user_roles = user.employment_by_service(employment.service_id).map(&:role)
      !user_roles.include?("coordinator") && !user_roles.include?("group coordinator")
    end

    cannot :manage, Service do |service|
      user_roles = user.employment_by_service(service.id).map(&:role)
      !user_roles.include?("coordinator") && !user_roles.include?("group coordinator")
    end
    
    can :read_coordinator_resources, Program do |program|
      service_id = user.user_programs.where(:program_id => program.id).first.service_id
      user_roles =  Employment.where(:service_id => service_id).where(:user_id => user.id).map(&:role)
      user_roles.include?("coordinator") || user_roles.include?("group coordinator")
    end
    
    can :read, LearningResource do |learning_resource|
      user.program_ids.include?(learning_resource.program_id)
    end
  end

end
