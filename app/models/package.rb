class Package < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  belongs_to :created_by, :class_name => "Admin", :foreign_key => :created_by_id
  belongs_to :updated_by, :class_name => "Admin", :foreign_key => :updated_by_id
  has_and_belongs_to_many :groups
  has_and_belongs_to_many :services

  has_many :program_lists, :through => :package_programs
  has_many :package_programs
  has_many :programs, :through => :package_programs, :order => "package_programs.position ASC"

  alias_attribute :to_s, :name
  accepts_nested_attributes_for :package_programs,
                                :reject_if => proc { |attrs| attrs['selected'] != "1" },
                                :allow_destroy => true

  validates_presence_of :name

  scope :default_order, order(:name)
  is_sluggable :name

  def program_ids
    package_programs.reject(&:new_record?).map(&:program_id)
  end

  def program_list_ids
    program_lists.map(&:id)
  end
end
