class ProgramAssessment < ActiveRecord::Base
  belongs_to :learning_record
  belongs_to :employment
  belongs_to :program
  belongs_to :coordinator, class_name: "User"

  has_one :user, :through => :employment
  before_save :set_assessed_at
  after_save :update_learning_plans

  validates :employment_id, presence: true
  validates :program_id, presence: true

  delegate :title, to: :program

  scope :default_order, includes(:program).order('programs.title', 'programs.code')
  scope :group_by_assessed_at, select("DISTINCT ON (date_trunc('minute', assessed_at), program_id) program_assessments.*").reorder("date_trunc('minute', assessed_at) DESC")

  private

  # TODO: This could probably have some sort of shared class with LearningRecord
  def learning_plan_ids
    employment.employment_learning_plans
      .not_completed.joins(:learning_plan_programs)
      .where("learning_plan_programs.program_id = ?", self.program_id).pluck("employment_learning_plans.id")
  end

  def update_learning_plans
    # We're touching the plan to reset the cache
    EmploymentLearningPlan.where(id: learning_plan_ids).each do |plan|
      plan.touch; plan.set_status!;
      plan.learning_plan.touch
    end
  end

  def set_assessed_at
    self.assessed_at = Time.zone.now unless assessed_at
  end
end
