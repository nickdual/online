class Mailchimp
  MC_LIST_ID = "8e59984495"

  def self.subscribe_coordinator_to_mailchimp(coordinator_id)
    coordinator = Coordinator.find(coordinator_id)

    attributes = {
      id: MC_LIST_ID,
      email_address: coordinator.user.email,
      merge_vars: {
        groupings: [
          { name: "Group Preferences", groups: "Group Coordinators" }
        ],
        fname: coordinator.user.first_name,
        lname: coordinator.user.last_name
      },
      double_optin: false,
      update_existing: true,
      replace_interests: true
    }

    Gibbon.listSubscribe(attributes)
  end

  def self.subscribe_employment_to_mailchimp(employment_id)
    employment = Employment.find(employment_id)

    attributes = {
      id: MC_LIST_ID,
      email_address: employment.user.email,
      merge_vars: {
        groupings: [
          { name: "Group Preferences", groups: group_lists(employment.user.roles) }
        ],
        fname: employment.user.first_name,
        lname: employment.user.last_name
      },
      double_optin: false,
      update_existing: true,
      replace_interests: true
    }

    Gibbon.listSubscribe(attributes)
  end

  def self.group_lists(roles)
    lists = []
    lists << "Service Coordinators" if roles.include?("coordinator")
    lists << "Users" if roles.include?("staff")
    lists.join(",")
  end

  # Not ready for prod yet
  def self.unsubscribe
    attributes = {
      id: MC_LIST_ID,
      email_address: self.user.email,
      send_goodbye: false
    }

    Gibbon.listUnsubscribe(attributes)
  end
end