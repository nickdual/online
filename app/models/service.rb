require 'ostruct'
class Service < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  include ServiceZendesk
  belongs_to :group
  belongs_to :disabled_by, :class_name => "Admin"
  belongs_to :created_by, :class_name => "Admin"
  belongs_to :updated_by, :class_name => "Admin"
  has_and_belongs_to_many :packages

  has_many :program_categories, :through => :programs, :source => :category
  has_many :programs, :through => :service_programs, uniq: true
  has_many :service_programs, :select => 'DISTINCT ON (program_id, service_id) *'
  has_many :subscriptions, :class_name => "ServiceSubscription", :select => 'DISTINCT ON (package_id, service_id) *'

  has_many :job_titles, class_name: "Service::JobTitle", :dependent => :destroy
  has_many :learning_plans, :dependent => :destroy
  has_many :employments, :dependent => :destroy
  has_many :group_sessions, :dependent => :destroy

  has_many :employment_learning_plans, :through => :employments
  has_many :users, :through => :employments
  has_many :staff, :through => :employments, :source => :user, :include => :employments, :conditions => { "employments.role" => "staff" }
  has_many :coordinators, :through => :employments, :source => :user, :include => :employments, :conditions => { "employments.role" => "coordinator" }
  has_many :learning_records, :through => :employments

  before_create :generate_token, :assign_group_coordinators
  after_save :update_service_program_playlists

  scope :default_order, order(:name)
  scope :active, where(disabled_at: nil)
  scope :tokens_reset_30_days_ago, lambda { where("token_generated_at < ?", 30.days.ago.end_of_day) }

  validates_presence_of :name, :country

  alias_attribute :to_s, :name
  alias_attribute :disabled?, :disabled_at
  alias_attribute :updated?, :updated_at

  is_sluggable :name

  def independent?
    !self.group_id.present?
  end

  def disable!(admin)
    self.update_attributes(:disabled_by_id => admin.id,
                           :disabled_at => Time.zone.now)

    self.employments.active.each { |e| e.update_attribute(:ended_at, Time.zone.now) }
  end

  def enable!
    self.update_attributes(:disabled_by_id => nil,
                           :disabled_at => nil)

    set_coordinators_to_active
  end

  def reset_token
    generate_token
    save
  end

  def email_is_not_employed?(email)
    self.employments.joins(:user).where("users.email = ?", email).active.any?
  end

  def active_users
    user_ids = employments.active.map(&:user_id)
    User.where(id: user_ids)
  end

  def import_users(csv_file, admin_id)
    user_import = UserImport.new(csv_file: csv_file)
    user_import.service_id = self.id
    user_import.admin_id = admin_id
    user_import.save
  end

  def self.reset_old_tokens!
    Service.where("token_generated_at < ?", 30.days.ago.end_of_day).each do |f|
      f.reset_token
    end
  end

  def status
    if self.disabled_by_id
      return "disabled by " + self.disabled_by.name + " (" + self.disabled_at.to_s(:short) + ")"
    else
      return "active"
    end
  end

  def address
    country || ""
  end

  def programs_count
    programs.map(&:id).uniq.count
  end

  def employments_count
    employments.active.count
  end

  def plans_count
    learning_plans.count
  end

  def programs_viewed_this_week
    employment_ids = employments.pluck(:id)
    LearningRecord.where(employment_id: employment_ids).where("created_at > ?", 7.days.ago)
  end

  private

    def set_coordinators_to_active
      if group && group.coordinators.any?
        coordinator_ids = group.coordinators.map(&:user_id).join(", ")
        employments.where("user_id IN (?)", coordinator_ids).each { |e| e.update_attribute(:ended_at, nil) }
      end
    end

    def generate_token
      self.activation_token = SecureRandom.hex(3)
      self.token_generated_at = Time.zone.now
    end

    def assign_group_coordinators
      if group && group.coordinators.any?
        group.coordinators.each do |coordinator|
          self.employments.build(:user => coordinator.user, :role => "group coordinator")
        end
      end
    end

    def update_service_program_playlists
      if group.present?
        program_ids = self.group.group_programs.map(&:program_id)
        GroupProgramList.where(group_program_id: program_ids).each { |p| p.update_attributes(:updated_at => Time.now) }
      end

      self.service_programs.each do |service_program|
        PackageProgram.where(:package_id => service_program.package_id).each do |package_program|
          package_program.program_lists.each { |p| p.update_attributes(:updated_at => Time.now) }
        end
      end
    end
end
