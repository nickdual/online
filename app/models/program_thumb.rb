class ProgramThumb < ActiveRecord::Base
  # If you need to upload thumbs from s3 themselves you probably want something like this:
  # new_url = "http://s3.amazonaws.com/acc_assets_production/new%20codecs/#{thumb.image_uid.gsub(" ", "%20")}"
  # thumb.update_attributes(image_url: new_url)
  
  belongs_to :program
  asset_accessor :image
  delegate :thumb, :remote_url, :url, :to => :image

  before_save :destroy_image?

  def url
    image.url
  end

  def thumb_60_60_url
    image.thumb("60x60#").url
  end

  def thumb_200_200_url
    image.thumb("200x200#").url
  end

  def image_delete
    @image_delete ||= "0"
  end

  def image_delete=(value)
    @image_delete = value
  end

  private
    
    def destroy_image?
      self.image = nil if @image_delete == "1" && !image.changed?
    end
    
end
