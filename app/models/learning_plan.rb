class LearningPlan < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  # Including CoordinatorReminder so we get Sidekiq#delay easily
  include LearningPlanReminder
  belongs_to :service, touch: true

  has_many :learning_plan_programs
  has_many :programs, :through => :learning_plan_programs

  has_many :employment_learning_plans, :dependent => :destroy
  has_many :employments, :through => :employment_learning_plans
  has_many :users, :through => :employments

  alias_attribute :to_s, :name
  validates_presence_of :name, :days_till_due
  validates_numericality_of :days_till_due, min: 1
  validates_date :active_at, :on => :create

  accepts_nested_attributes_for :learning_plan_programs

  scope :for_service, lambda { |service_id| where(service_id: service_id) }
  scope :default_order, order(:name)

  after_save :update_employment_learning_plans
  is_sluggable :slug_name

  def cache_key
    "#{self.id}-#{self.updated_at.utc.to_s(:number)}-#{Date.today.strftime("%Y%m%d")}"
  end

  def slug_name
    [service.try(:name), name].join(" ")
  end

  def slug_name_changed?
    service.try(:name_changed?) || name_changed?
  end

  def status
    active ? "Active" : "Inactive"
  end

  def active
    Time.zone.now.to_date >= self.active_at.to_date
  end

  def formatted_active_at
    active_at.to_s(:aus)
  end

  def programs_count
    programs.count
  end

  def service_name
    service.name
  end

  def employments_count
    employment_learning_plans.reload.count
  end

  def percentage_overdue
    if overdue_count.zero?
      0
    else
      ((overdue_count.to_f / employment_learning_plans.count.to_f) * 100).to_i
    end
  end

  def overdue_count
    employment_learning_plans.overdue.count
  end

  def percentage_active
    if active_count.zero?
      0
    else
      ((active_count.to_f / employment_learning_plans.count.to_f) * 100).to_i
    end
  end

  def active_count
    employment_learning_plans.active.count
  end

  def percentage_pending
    if pending_count.zero?
      0
    else
      ((pending_count.to_f / employment_learning_plans.count.to_f) * 100).to_i
    end
  end

  def pending_count
    employment_learning_plans.pending.count
  end

  def percentage_completed
    if completed_count.zero?
      0
    else
      ((completed_count.to_f / employment_learning_plans.count.to_f) * 100).to_i
    end
  end

  def completed_count
    employment_learning_plans.completed.count
  end

  def days_till_active
    (self.active_at.to_date - Time.zone.now.to_date).to_i
  end

  def update_employment_learning_plans
    employment_learning_plans.pending.each { |e| e.set_due_at! }
  end

end
