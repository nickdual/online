class LearningResourceDownload < ActiveRecord::Base

  has_one :user, :through => :employment
  belongs_to :employment
  belongs_to :learning_resource

  delegate :name, :program, :to => :learning_resource

end
