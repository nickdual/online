# Not to be confused with the Group class, this is a LearningRecord for a collection of Employments
class GroupSession < ActiveRecord::Base

  belongs_to :created_by, class_name: "User"
  belongs_to :service
  belongs_to :program

  has_and_belongs_to_many :programs
  has_and_belongs_to_many :employments
  has_many :users, :through => :employments
  has_many :learning_records

  validates_datetime :viewed_at
  validates :created_by_id, presence: true
  validates :program_id, presence: true

  attr_accessor :viewed_at_date, :viewed_at_time

  # TODO: Remove these attributes
  attr_accessor :create_program_record, :create_assessment_records, :program_and_assessment_check

  before_validation :make_viewed_at
  after_create :create_learning_records, :create_assessment_records

  def viewed_at_date
    return @viewed_at_date if @viewed_at_date.present?
    return @viewed_at if @viewed_at.present?
    return Time.zone.now.strftime("%d/%m/%Y")
  end

  def viewed_at_time
    return @viewed_at_time.to_time.strftime("%l:%M%P") if @viewed_at_time.present?
    return @viewed_at.to_time.strftime("%l:%M%P") if @viewed_at.present?
    return "10:00am"
  end

  def viewed_at_date=(new_date)
    @viewed_at_date = self.string_to_datetime(new_date, "d/m/yyyy")
  end

  def viewed_at_time=(new_time)
    if self.string_to_datetime(new_time, "h:nn_ampm")
      @viewed_at_time = self.string_to_datetime(new_time, "h:nn_ampm")
    else
      # To account for people who do not add AM / PM
      @viewed_at_time = self.string_to_datetime(new_time, "h:nn_")
    end
  end

  private

    def create_learning_records
      if medium
        employments.each do |employment|
          employment.user.create_learning_record!(program, medium, viewed_at, id)
        end
      end
    end

    def create_assessment_records
      employments.each do |employment|
        [:essentials, :extension, :evidence].each do |assessment_type|
          if self.send(assessment_type)
            attributes = {
              assessment_type:  assessment_type.capitalize,
              program:          program,
              coordinator_id:   self.created_by_id,
              group_session_id: self.id,
              assessed_at:      viewed_at
            }

            employment.program_assessments.create!(attributes)
          end
        end
      end
    end

    protected

    def string_to_datetime(value, format)
      Timeliness.parse(value, format: format, zone: Time.zone)
    end

    def make_viewed_at
      if @viewed_at_date.present? && @viewed_at_time.present?
        self.viewed_at = Timeliness.parse("#{@viewed_at_date.strftime("%d/%m/%Y")} #{@viewed_at_time.strftime("%I:%M%P")}", format: "d/m/yyyy h:nn_ampm", zone: Time.zone)
      end
    end
end
