require 'zendesk'
module ServiceZendesk
  extend ActiveSupport::Concern

  included do
    before_create :create_zendesk_organisation
    before_save :update_zendesk_organisation
    before_destroy :destroy_zendesk_organisation
  end
  
  def setup_zendesk_connection
    @zendesk_config = YAML.load_file "#{Rails.root}/spec/zendesk_config.yml" unless ENV["ZENDESK_URL"]
    @zendesk = Zendesk::Client.new do |config|
      config.account = ENV["ZENDESK_URL"] || @zendesk_config["url"]
      config.basic_auth (ENV["ZENDESK_USERNAME"] || @zendesk_config["username"]), (ENV["ZENDESK_PASSWORD"] || @zendesk_config["password"])
    end
  end

  def create_zendesk_organisation
    setup_zendesk_connection
    organization = @zendesk.organizations.create({name: self.name})
    self.zendesk_id = organization.id
  end
  
  def update_zendesk_organisation
    if name_changed? && !self.new_record?
      setup_zendesk_connection
      organization = @zendesk.organizations(self.zendesk_id)
      organization.update({name: self.name})
    end
  end

  def destroy_zendesk_organisation
    if Rails.env.production? || Rails.env.test?
      begin
        setup_zendesk_connection
        organization = @zendesk.organizations(self.zendesk_id)
        organization.delete
      rescue MultiJson::DecodeError
        # Something weird is happening here with deletes, still works either way~
      end
    end
  end
end