class AdminAbility
  include CanCan::Ability

  def initialize(admin)
    if admin.role == "viewer"
      can :read, :all
    elsif admin.role == "moderator"
      can [:read, :create, :update], :all
      cannot :manage, [Admin]
    elsif admin.role == "administrator"
      can :manage, :all
    end
  end
end
