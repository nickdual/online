class ProgramCategory < ActiveRecord::Base

  has_many :programs, :foreign_key => :category_id
  has_many :services, :through => :programs
  has_many :employments, :through => :services

  validates_presence_of :name
  validates_uniqueness_of :name, :allow_nil => true, :scope => :ancestry

  alias_attribute :to_s, :name

  belongs_to :created_by, :class_name => "Admin"
  belongs_to :updated_by, :class_name => "Admin"
  before_save :set_parent_category_name

  is_sluggable :name
  has_ancestry

  scope :default_order, order(:name)

  def child_programs
    Program.where(category_id: self.child_ids)
  end

  def all_programs_count
    Program.where(category_id: descendant_ids << id).count
  end

  private

    def set_parent_category_name
      self.parent_category_name = parent.name if self.parent
    end

end
