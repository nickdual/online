class DragonflyThumb < ActiveRecord::Base
  attr_accessible :job, :uid

  validate :job, presence: true
  validate :uid, presence: true
end
