class PackageProgram < ActiveRecord::Base
  
  belongs_to :package
  belongs_to :program
  
  has_many :program_lists, :class_name => "PackageProgramList"
  has_many :programs, :through => :program_lists
  
  delegate :title, :code, :to_s, :to => :program
  acts_as_list :scope => :package_id
  attr_accessor :selected

  accepts_nested_attributes_for :program_lists,
                                :reject_if => proc { |attrs| attrs['selected'] != "1" },
                                :allow_destroy => true

  scope :default_order, joins(:program).order(:title, :code)

  def playlist_program_ids
    program_lists.map(&:program_id)
  end

end
