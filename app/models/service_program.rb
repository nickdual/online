class ServiceProgram < ActiveRecord::Base

  self.primary_keys = [:service_id, :program_id]
  belongs_to :group
  belongs_to :service
  belongs_to :program
  belongs_to :package

  has_many :programs, :through => :program_lists

  has_many :program_lists, :class_name => "ServiceProgramList", :foreign_key => [:service_id, :service_program_id], :primary_key => [:service_id, :program_id]

  delegate :fixed_width, :fixed_height, :code, :title, :updated_by, :updated_at, :to => :program, :prefix => "program"
  scope :default_order, includes(:program).order("programs.title")

  accepts_nested_attributes_for :program_lists,
                                :reject_if => proc { |attrs| attrs['selected'] != "1" },
                                :allow_destroy => true

  def program_ids
    program_lists.reject(&:new_record?).map(&:program_id)
  end

end
