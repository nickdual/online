module EmploymentLearningPlanStats
  extend ActiveSupport::Concern

  included do
    after_create :precache_stats
  end

  def programs_viewed_pending_ids
    Rails.cache.fetch([self, "/programs_viewed_pending_ids"]) do
      programs_all.select { |p| !p.viewed_program.present? && !self.overdue? }.map(&:id)
    end
  end

  def programs_viewed_completed_ids
    Rails.cache.fetch([self, "/programs_viewed_completed_ids"]) do
      programs_all.select { |p| p.viewed_program.present? }.map(&:id)
    end
  end

  def programs_viewed_overdue_ids
    Rails.cache.fetch([self, "/programs_viewed_overdue_ids"]) do
      programs_all.select { |p| !p.viewed_program.present? && self.overdue? }.map(&:id)
    end
  end

  def program_assessments_completed_ids
    Rails.cache.fetch([self, "/program_assessments_completed_ids"]) do
      programs_all.select { |p| p.completed_essentials.present? && p.require_essentials.present?  }.map(&:id) +
      programs_all.select { |p| p.completed_extension.present? && p.require_extension.present? }.map(&:id) +
      programs_all.select { |p| p.completed_evidence.present? && p.require_evidence.present? }.map(&:id).uniq
    end
  end

  def program_assessments_completed_objects
      programs_all.select { |p| p.completed_essentials.present? && p.require_essentials.present?  } +
      programs_all.select { |p| p.completed_extension.present? && p.require_extension.present? } +
      programs_all.select { |p| p.completed_evidence.present? && p.require_evidence.present? }.uniq
  end

  def program_assessments_pending_ids
    Rails.cache.fetch([self, "/program_assessments_pending_ids"]) do
      programs_all.select { |p| !p.completed_essentials.present? &&
        !p.overdue_essentials.present? && p.essentials.present? }.map(&:id) +
      programs_all.select { |p| !p.completed_extension.present? &&
        !p.overdue_extension.present? && p.extension.present? }.map(&:id) +
      programs_all.select { |p| !p.completed_evidence.present? &&
        !p.overdue_evidence.present? && p.evidence.present? }.map(&:id).uniq
    end
  end

  def program_assessments_overdue_ids
    Rails.cache.fetch([self, "/program_assessments_overdue_ids"]) do
      programs_all.select { |p| p.overdue_essentials.present? }.map(&:id) +
      programs_all.select { |p| p.overdue_extension.present? }.map(&:id) +
      programs_all.select { |p| p.overdue_evidence.present? }.map(&:id).uniq
    end
  end

  def assessment_not_required_count
    Rails.cache.fetch([self, "/assessment_not_required_count"]) do
      employment_learning_plan_programs.select { |program| !program.essentials.present? }.size +
      employment_learning_plan_programs.select { |program| !program.extension.present? }.size +
      employment_learning_plan_programs.select { |program| !program.evidence.present? }.size
    end
  end

  # TODO: This is going to need some serious server-side caching
  def programs_count
    employment_learning_plan_programs.size
  end

  def programs_pending
    EmploymentLearningPlanProgram.pending(self)
  end
  
  def programs_completed
    EmploymentLearningPlanProgram.completed(self)
  end

  def programs_all
    programs_pending + programs_completed
  end

  # Counters relying on caches in the database
  def programs_pending_count
    if self.overdue?
      ([programs_viewed_overdue_ids + program_assessments_overdue_ids].flatten.uniq).count
    else
      ([programs_viewed_pending_ids + program_assessments_pending_ids].flatten.uniq).count
    end
  end

  def programs_completed_count
    EmploymentLearningPlanProgram.completed(self).count
  end

  def assessment_total_count
    employment_learning_plan_programs.size * 3.0
  end

  def assessment_required_count
    (assessment_total_count - assessment_not_required_count).to_i
  end

  def viewed_count
    {completed: programs_viewed_completed_ids.count,
      pending:  programs_viewed_pending_ids.count,
      overdue:  programs_viewed_overdue_ids.count}
  end

  def assessment_count
    {not_required: assessment_not_required_count,
      completed:  program_assessments_completed_ids.count,
      pending:    program_assessments_pending_ids.count,
      overdue:    program_assessments_overdue_ids.count}
  end

  def viewed_percentage
    {completed: ((viewed_count[:completed] / programs_count.to_f) * 100).to_i,
      pending:  ((viewed_count[:pending] / programs_count.to_f) * 100).to_i,
      overdue:  ((viewed_count[:overdue] / programs_count.to_f) * 100).to_i}
  end

  def assessment_percentage
    {not_required:  ((assessment_count[:not_required] / assessment_total_count.to_f) * 100).to_i,
      completed:    ((assessment_count[:completed] / assessment_total_count.to_f) * 100).to_i,
      pending:      ((assessment_count[:pending] / assessment_total_count.to_f) * 100).to_i,
      overdue:      ((assessment_count[:overdue] / assessment_total_count.to_f) * 100).to_i}
  end

  private

    def precache_stats
      learning_plan_program_ids = learning_plan.learning_plan_programs.map(&:id)

      Rails.cache.write([self, "/programs_viewed_pending_ids"], learning_plan_program_ids)
      Rails.cache.write([self, "/programs_viewed_completed_ids"], [])
      Rails.cache.write([self, "/programs_viewed_overdue_ids"], [])
      Rails.cache.write([self, "/program_assessments_completed_ids"], [])
      Rails.cache.write([self, "/program_assessments_pending_ids"], learning_plan_program_ids)
      Rails.cache.write([self, "/program_assessments_overdue_ids"], [])
      Rails.cache.write([self, "/assessment_not_required_count"], 0)
    end
end