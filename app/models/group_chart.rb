module GroupChart
  class << self
    def number_of_users_by_service(group)
      services_count = group.services.collect { |f| [f.to_s, f.employments.active.count] }
      {
        title: 'Number of Users by Service',
        series: [{
          name: 'Number of users',
          data: services_count
        }]
      }
    end

    def learning_records_created_in_24_hours(group)
      hours = (-23..0).collect { |hours_ago| Time.zone.now.advance(hours: hours_ago).strftime("%l%P") }
      service_ids = group.services.map(&:id)
      employment_ids = Employment.where(service_id: service_ids).map(&:id)

      counts = (-23..0).collect do |hours_ago|
        hour = Time.zone.now.advance(hours: hours_ago)
        begining_of_hour = hour.change(:min => 0, :sec => 0)
        end_of_hour = hour.change(:min => 59, :sec => 59)
        [LearningRecord.where(employment_id: employment_ids).where(viewed_at: [begining_of_hour..end_of_hour]).count].sum
      end

      series = [{ name: "programs", data: counts }]

      { title: 'Learning Records created in the past 24 hours',
        yAxisTitle: 'Number of Programs',
        categories: hours,
        series: series }
    end

    def learning_records_past_30_days_by_service(group)
      services = group.services.default_order.map(&:name)

      counts = group.services.default_order.collect do |service|
        service.learning_records.where(viewed_at: [30.days.ago..Time.zone.now]).count
      end

      series = [{ name: "programs", data: counts }]

      { title: 'Learning Records created in the past 30 days',
        yAxisTitle: 'Number of Programs',
        categories: services,
        series: series }
    end

    def learning_plans_completed_over_30_days(group)
        services = group.services.default_order.map(&:name)

        counts = group.services.default_order.collect do |service|
          service.employment_learning_plans.completed.where(updated_at: [30.days.ago..Time.zone.now]).count
        end

        series = [{ name: "learning plans", data: counts }]

        { title: 'Learning Plans completed in the past 30 days',
          yAxisTitle: 'Number of Learning Plans completed',
          categories: services,
          series: series }
    end

    def learning_plan_pending_vs_overdue(group)
      service_ids = group.services.map(&:id)
      overdue_counts = Employment.where(service_id: service_ids).flat_map { |e| e.overdue_learning_plan_programs.count }.sum
      pending_counts = Employment.where(service_id: service_ids).flat_map { |e| e.pending_learning_plan_programs.count}.sum

      learning_plan_summaries = []
      learning_plan_summaries << ["Overdue", overdue_counts]
      learning_plan_summaries << ["Pending Completion", pending_counts]

      { title: 'Learning Plans pending vs. overdue',
        series: [{
          name: 'Number of programs',
          data: learning_plan_summaries }]
        }
    end
  end
end