class Group::JobTitle < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  belongs_to :group

  validates :group_id, presence: true
  validates :name, presence: true

  before_destroy :clear_job_titles
  scope :default_order, order(:name)
  alias_attribute :to_s, :name

  def truncated_name
    name.length > 21 ? (name[0..18] + "...") : name
  end

  def employments
    service_ids = Service.where(group_id: self.group_id)
    Employment.includes(:user).where(job_title_id: self.id).where(service_id: service_ids)
  end

  private

  def clear_job_titles
    employments.each { |e| e.update_attribute(:job_title_id, nil) }
  end

end
