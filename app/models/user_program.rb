class UserProgram < ActiveRecord::Base
  
  belongs_to :program
  belongs_to :subscription
  belongs_to :user
  belongs_to :service
  
  has_many :program_lists, :through => :service
  delegate :to_s, :to => :program
  
end
