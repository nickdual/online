module ProgramSettings
  extend ActiveSupport::Concern

  def create_zencoder_job
    @s3_bucket    = ENV['FOG_DIRECTORY']
    @postback_url = ENV['ZENCODER_POSTBACK']
    @base_url     = "s3://#{@s3_bucket}/#{generate_uid}"

    @outputs = outputs.each do |output|
      output[:public]         = true
      output[:notifications]  = create_notifications
      output[:watermarks]     = create_watermark
      output[:base_url]       = @base_url
      output[:deinterlace]    = "on"
    end

    # Another helper for when manually triggering videos
    # self.video.remote_url
    Zencoder::Job.create(input: "https://s3.amazonaws.com/acc_assets_production/#{self.video.file_uid}", outputs: @outputs, test: !Rails.env.production?)
  end

  def outputs
    array = [to_hd_h264, to_hd_webm] # HD outputs
    array << [to_sd_h264, to_sd_webm] # SD outputs
    array << [to_mobile_sd, to_mobile_hd] # Mobile HD and SD (used in streaming)
    array << [segmented_sd_audio, segmented_sd_video, segmented_hd, streams] # Streaming for iOS
    array.flatten
  end

  def generate_uid # Thanks Dragonfly gem
    "#{Time.now.strftime '%Y/%m/%d/%H/%M/%S'}/#{rand(1000)}/"
  end

  private

    def to_hd_h264 # H264 Browser(HD)
      { label: "browser_h264_hd",
        format: "mp4",
        max_video_bitrate: 1600,
        h264_profile: "high"
      }
    end

    def to_hd_webm # WebM Browser(HD)
      { label: "browser_webm_hd",
        format: "webm",
        max_video_bitrate: 1600
      }
    end

    def to_sd_h264 # H264 BROWSER(SD)
      { label: "browser_h264_sd",
        format: "mp4",
        height: "360",
        max_video_bitrate: 400,
        aspect_mode: "preserve"
      }
    end

    def to_sd_webm # WEBM BROWSER(SD)
      { label: "browser_webm_sd",
        format: "webm",
        height: "360",
        max_video_bitrate: 400,
        aspect_mode: "preserve"
      }
    end

    def to_mobile_sd # Mobile(SD)
      # Based on the output in https://app.zencoder.com/docs/guides/encoding-settings/transmuxing-outputs-for-streaming
      { label: "modile_sd",
        format: "mp4",
        video_bitrate: 200,
        decoder_bitrate_cap: 300,
        decoder_buffer_size: 1200,
        audio_sample_rate: 44100,
        height: "288",
        h264_reference_frames: 1,
        forced_keyframe_rate: "0.1",
        audio_bitrate: 56,
        decimate: 2 }
    end

    def to_mobile_hd # Mobile(HD)
      # Again, based on the output in https://app.zencoder.com/docs/guides/encoding-settings/transmuxing-outputs-for-streaming
      { label: "modile_hd",
        format: "mp4",
        video_bitrate: 1000,
        decoder_bitrate_cap: 1500,
        decoder_buffer_size: 6000,
        audio_sample_rate: 44100,
        height: "432",
        h264_reference_frames: "auto",
        h264_profile: "main",
        forced_keyframe_rate: "0.1",
        audio_bitrate: 56 }
    end

    def segmented_sd_audio
      { label: "hls-audio-only",
        source: "modile_sd", # references to_mobile_sd label
        segment_video_snapshots: "true",
        copy_audio: "true",
        skip_video: "true",
        filename: "hls-audio-only.m3u8",
        type: "segmented",
        format: "aac" }
    end

    def segmented_sd_video
      { label: "hls-low",
        source: "modile_sd", # references to_mobile_sd label
        format: "ts",
        copy_audio: "true",
        copy_video: "true",
        filename: "hls-low.m3u8",
        type: "segmented" }
    end

    def segmented_hd
      { label: "hls-high",
        source: "modile_hd", # references to_mobile_hd label
        format: "ts",
        copy_audio: "true",
        copy_video: "true",
        filename: "hls-high.m3u8",
        type: "segmented" }
    end

    def streams
      { label: "playlist",
        filename: "playlist.m3u8",
        type: "playlist",
        streams: [
        { path: "hls-low.m3u8",
          bandwidth: 256 },
        { path: "hls-audio-only.m3u8",
          bandwidth: 56 },
        { path: "hls-high.m3u8",
          bandwidth: 1056 }],
      }
    end

    def create_notifications
      { format: "json",
        url: "#{@postback_url}/#{self.id}.json" }
    end

    def create_watermark
      [{ y: "-5%", :x => "-5%",
         url: "s3://#{@s3_bucket}/watermark.png" }]
    end

    # For getting the loading order_id of programs in the <video> tag
    def set_order_id
      case description
      when "browser_h264_sd"
        self.order_id = 1
      when "browser_h264_hd"
        self.order_id = 2
      when "browser_webm_sd"
        self.order_id = 3
      when "browser_webm_hd"
        self.order_id = 4
      when "browser_ogg_sd"
        self.order_id = 5
      when "browser_ogg_hd"
        self.order_id = 6
      when "playlist"
        self.order_id = 7
      when "hls-audio-only"
        self.order_id = 8
      when "hls-low"
        self.order_id = 9
      when "hls-high"
        self.order_id = 10
      when "mobile_h264"
        self.order_id = 11
      when "modile_sd"
        self.order_id = 12
      when "modile_hd"
        self.order_id = 13
      end
    end

    def set_codec
      case description
      when "browser_webm_hd"
        self.codec = "video/webm; codecs='vp8, vorbis'"
      when "mobile_h264"
        self.codec = "video/mp4; codecs='avc1.42E01E, mp4a.40.2'"
      when "browser_h264_hd"
        self.codec = "video/mp4; codecs='avc1.64001E, mp4a.40.2'"
      when "browser_h264_sd"
        self.codec = "video/mp4"
      when "browser_webm_sd"
        self.codec = "video/webm"
      when "playlist"
        self.codec = "application/x-mpegURL"
      else
        self.codec = "application/x-mpegURL"
      end
    end
end
