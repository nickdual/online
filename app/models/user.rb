class User < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  include User::Stats

  has_explicit_column_definitions_for_rspec_fire

  belongs_to :created_by, :class_name => "Admin"
  belongs_to :updated_by, :class_name => "Admin"

  has_many :coordinators, :dependent => :destroy
  has_many :groups, :through => :coordinators

  has_many :employments, :dependent => :destroy
  has_many :services, :through => :employments, :include => "employments", :conditions => "employments.ended_at IS NULL"
  has_many :previous_services, :through => :employments, :include => "employments", :conditions => "employments.ended_at IS NOT NULL"
  has_many :user_programs, :class_name => "UserProgram"
  has_many :programs, :through => :user_programs, :uniq => true
  has_many :program_lists, :through => :services
  has_many :learning_resource_downloads, :through => :employments
  has_many :learning_resources, :through => :services, :source => :programs

  has_many :employment_learning_plans, :through => :employments, :conditions => "employments.ended_at IS NULL"
  has_many :learning_plans, :through => :employment_learning_plans

  has_many :learning_records, :through => :employments
  has_many :program_assessments, :through => :employments
  has_many :assessment_results, class_name: "User::AssessmentResult"

  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessor :job_title

  validates_presence_of :first_name, :last_name
  validate :ensure_gender_valid

  alias_attribute :to_s, :name

  before_save :set_time_zone, :expire_all_employments_cache
  before_update :update_welcome_email_resent_at, :if => :email_changed?
  after_update :welcome_user, :if => :email_changed?
  after_save :expire_employments_cache

  accepts_nested_attributes_for :employments
  is_sluggable :name
  scope :default_order, order(:first_name, :last_name)
  scope :not_logged_in_for_30_days, lambda { where("current_sign_in_at < ? OR current_sign_in_at IS NULL", 30.days.ago) }

  def password_required?
    !persisted? || !password.nil? || !password_confirmation.nil?
  end

  def name
    [first_name, last_name].join(" ")
  end

  def name=(name)
    name = name.split(" ")
    self.first_name = name.first
    self.last_name = name.last
  end

  def formatted_born_at
    born_at.to_s(:aus) unless born_at.blank?
  end

  def employment_by_service(service_id)
    employments.where("service_id = ?", service_id)
  end

  def name_changed?
    first_name_changed? or last_name_changed?
  end

  def existing_email?
    if new_record? && self.errors.any?
      if errors.messages[:email]
        errors.messages[:email].include?("has already been taken")
      end
    end
  end

  def roles
    employments.active.map(&:role)
  end

  def is_coordinator
    roles.include?("coordinator") || roles.include?("group coordinator")
  end

  def existing_user
    User.where(email: email).first if existing_email?
  end

  def zendesk_organization
    employments.first.try(:service).try(:name)
  end

  def zendesk_tags
    employments.collect { |e| [(e.service.to_s.gsub(" ", "_")), (e.role.gsub(" ", "_"))].join(" ") }.join(" ")
  end

  def create_program_assessment!(program_id, assessment_type)
    self.employments.active.each do |employment|
      attributes = {
        program_id: program_id,
        assessment_type: assessment_type
      }

      employment.program_assessments.create!(attributes)
    end
  end

  def create_learning_record!(program, medium = "Online", viewed_at = Time.zone.now, group_session_id = nil)
    self.employments.active.each do |employment|
      attributes = {
        program: program,
        medium: medium,
        viewed_at: viewed_at,
        group_session_id: group_session_id
      }

      employment.learning_records.create!(attributes)
    end
  end

  def activities
    User::Activity.new(self).order_activities
  end

  def assessments(program)
    User::Assessment.new(self, program).assessments
  end

  def resent_welcome_email
    welcome_email_resent_at && 5.minutes.ago < welcome_email_resent_at
  end

  class << self
    # This method should only be run under the context of an existing service
    def find_or_build_user(attributes, service)
      existing_user = User.where(email: attributes[:email]).first

      if existing_user
        employment = existing_user.employments.create!(role: attributes[:employments_attributes][0][:role], service: service)
      else
        attributes[:password], attributes[:password_confirmation] = service.activation_token
        existing_user = User.create!(attributes)
        employment = existing_user.employments.first
      end

      employment
    end
  end

  private

    def expire_all_employments_cache
      employments.each { |e| e.touch }
    end

    def set_time_zone
      self.time_zone = nil if time_zone == ""
    end

    def ensure_gender_valid
      self.gender = gender.to_s.downcase

      case gender
      when "m"
        self.gender = "male"
      when "f"
        self.gender =  "female"
      end

      errors.add(:gender, "not a valid gender type") unless [nil, "", "male", "female"].include?(gender)
    end

    def expire_employments_cache
      employments.active.each { |e| e.touch }
    end

    def update_welcome_email_resent_at
      self.welcome_email_resent_at = Time.zone.now
      self.password, self.password_confirmation = self.employments.first.service.activation_token
    end

    def welcome_user
      UserNotifier.welcome_invited_user(self, self.employments.first).deliver
    end

end
