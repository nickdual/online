require 'set'

class LearningPlanProgram < ActiveRecord::Base

  belongs_to :learning_plan
  belongs_to :program

  has_one :service, :through => :learning_plan
  delegate :to_s, :to => :program

  after_save :update_employment_learning_records
  # before_create :set_assessment_requirements
  validate :learning_plan_id, presence: true
  validate :program_id, presence: true

  delegate :code, :title, :thumb_200_200_url, :duration, to: :program
  attr_accessor :completed_essentials, :completed_evidence, :completed_extension

  scope :inaccessible, where("extension IS NOT TRUE").where("evidence IS NOT TRUE").where("essentials IS NOT TRUE")

  def criteria
    set = Set[]

    [:essentials, :evidence, :extension].each do |assess|
      set << assess.to_s.capitalize if self.send(assess)
    end

    set
  end

  private

    def update_employment_learning_records
      learning_plan.update_employment_learning_plans
    end

    def set_assessment_requirements
      if Service.find(learning_plan.service_id).independent?
        assessment = ServiceProgramAssessment.where(service_id: learning_plan.service_id).where(program_id: program_id).first
      else
        group_id = Service.find(learning_plan.service_id).group_id
        assessment = GroupProgramAssessment.where(group_id: group_id).where(program_id: program_id).first
      end

      if assessment.present?
        self.essentials = assessment.essentials
        self.evidence   = assessment.evidence
        self.extension  = assessment.extension
      else
        self.essentials = true
      end

      true
    end
end
