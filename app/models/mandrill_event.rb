class MandrillEvent
  include InitializerWithAttributesHash

  attr_accessor :bounce_description, :email, :occurred_at, :status

  def update_user!
    user = User.find_by_email(email)

    if user
      case status
      when 'sent'
        user.update_attributes(
          email_verified_at: self.occurred_at,
          last_email_bounce_description: nil,
          last_email_status: status)
      when 'soft-bounced'
        user.update_attributes(
          last_email_bounce_description: bounce_description,
          last_email_status: status)
      when 'bounced'
        user.update_attributes(
          email_verified_at: nil,
          last_email_bounce_description: bounce_description,
          last_email_status: status)
      else
        raise UnknownStatus.new(self)
      end
    end
  end

  class UnknownStatus < ExceptionWithDefaultMessage; end
end
