module ServiceChart
  class << self
    def learning_plan_pending_vs_overdue(service)
      overdue_counts = service.employments.flat_map { |e| e.overdue_learning_plan_programs.count }.sum
      pending_counts = service.employments.flat_map { |e| e.pending_learning_plan_programs.count}.sum

      learning_plan_summaries = []
      learning_plan_summaries << ["Overdue", overdue_counts]
      learning_plan_summaries << ["Pending Completion", pending_counts]

      { title: 'Learning Plans pending vs. overdue',
        series: [{
          name: 'Number of programs',
          data: learning_plan_summaries }]
        }
    end
    
    def learning_records_over_24_period(service)
      hours = (-23..0).collect { |hours_ago| Time.zone.now.advance(hours: hours_ago).strftime("%l%P") }

      counts = (-23..0).collect do |hours_ago|
        hour = Time.zone.now.advance(hours: hours_ago)
        begining_of_hour = hour.change(:min => 0, :sec => 0)
        end_of_hour = hour.change(:min => 59, :sec => 59)
        [service.learning_records.where(viewed_at: [begining_of_hour..end_of_hour]).count].sum
      end

      series = [{ name: "programs", data: counts }]

      { title: 'Learning Records created in the past 24 hours',
        yAxisTitle: 'Number of Programs',
        categories: hours,
        series: series }
    end

    def learning_records_past_7_days(service)
      days = (-6..0).collect { |days_ago| Time.zone.now.advance(days: days_ago).strftime("%A") }

      counts = (-6..0).collect do |days_ago|
        date = Time.zone.now.advance(days: days_ago)
        beginning_of_day = date.beginning_of_day
        end_of_day = date.end_of_day
        [service.learning_records.where(viewed_at: [beginning_of_day..end_of_day]).count].sum
      end

      series = [{ name: "programs", data: counts }]

      { title: 'Learning Records created in the past 7 days',
        yAxisTitle: 'Number of Programs',
        categories: days,
        series: series }
    end

    def learning_records_over_30_days(service)
      days = (-30..0).collect { |days_ago| Time.zone.now.advance(days: days_ago).strftime("%d %h") }

      counts = (-30..0).collect do |days_ago|
        day = Time.zone.now.advance(days: days_ago)
        beginning_of_day = day.beginning_of_day
        end_of_day = day.end_of_day
        [service.learning_records.where(viewed_at: [beginning_of_day..end_of_day]).count].sum
      end

      series = [{ name: "programs", data: counts }]

      { title: 'Learning Records created in the last 30 days',
        yAxisTitle: 'Number of Programs',
        categories: days,
        series: series }
    end

    def learning_plans_completed_over_30_days(service)
      days = (-30..0).collect { |days_ago| Time.zone.now.advance(days: days_ago).strftime("%d %h") }

      counts = (-30..0).collect do |days_ago|
        day = Time.zone.now.advance(days: days_ago)
        beginning_of_day = day.beginning_of_day
        end_of_day = day.end_of_day
        [service.employment_learning_plans.completed.where(updated_at: [beginning_of_day..end_of_day]).count].sum
      end

      series = [{ name: "learning plans", data: counts }]

      { title: 'Learning Plans completed in the last 30 days',
        yAxisTitle: 'Number of Learning Plans Completed',
        categories: days,
        series: series }
    end
  end
end