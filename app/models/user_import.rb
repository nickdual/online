require 'csv'

class UserImport
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  def initialize(attributes)
    @service_id = attributes[:service_id]
    @admin_id   = attributes[:admin_id]
    @csv_file   = attributes[:csv_file]

    # Default attributes
    @successful_invited_users = []
    @failed_job_titles        = []
    @unsuccessful_users       = []
  end

  # Basic attributes
  attr_accessor :service_id, :admin_id, :csv_file

  attr_accessor :successful_invited_users, :unsuccessful_users, :total_users,
                :total_successful_users, :failed_job_titles

  validates :service_id,  presence: true
  validates :admin_id,    presence: true
  validates :csv_file,    presence: true

  # Used by simple_form
  def persisted?
    false
  end

  def service
    Service.find(service_id)
  end

  def admin
    Admin.find(admin_id)
  end

  def save
    if self.valid?
      import_users; notify_admin
    end
  end

  def import_users
    CSV.parse(csv_file, :headers => :first_row) do |record|
      user = UserImport::User.new(user_attributes(record), self)
      user.import; user.check_job_title
    end
  end

  def total_successful_users
    successful_invited_users.count
  end

  def total_users
    (successful_invited_users + unsuccessful_users).count
  end

  private

    def user_attributes(user)
      {
        first_name:       user["first_name"],
        last_name:        user["last_name"],
        email:            user["email"],
        gender:           user["gender"],
        employee_number:  user["employee_number"],
        job_title:        user["job_title"],
        service_id:       service_id,
        activation_token: service.activation_token,
        job_title_id:     job_title_id(user["job_title"])
      }
    end

    def notify_admin
      ImportNotifier.user_import_complete(self).deliver
    end

    def job_title_id(job_title)
      # TODO: Move into the service class
      if service.group_id
        service.group.job_titles.where(name: job_title).first.try(:id)
      else
        service.job_titles.where(name: job_title).first.try(:id)
      end
    end

end
