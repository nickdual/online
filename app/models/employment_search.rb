module EmploymentSearch
  def self.results(params, service)
    @search = Employment.search(params[:q])
    @employments = @search.result(distinct: true)

    employments_table = Employment.arel_table

    unless params[:q][:user_learning_records_program_id_does_not_exist].blank?
      learning_records_table = LearningRecord.arel_table
      @employments = @employments.where(LearningRecord.where(learning_records_table[:employment_id].eq(employments_table[:id])).where(learning_records_table[:program_id].eq(params[:q][:user_learning_records_program_id_does_not_exist])).exists.not)
    end

    unless params[:q][:completed_employment_learning_plans_learning_plan_id_does_not_exist].blank?
      # Huge todo: Move this into one tidy query
      employment_learning_plans_table = EmploymentLearningPlan.arel_table
      employment_ids1 = @employments.where(EmploymentLearningPlan.where(employment_learning_plans_table[:employment_id].eq(employments_table[:id])).where(employment_learning_plans_table[:learning_plan_id].eq(params[:q][:completed_employment_learning_plans_learning_plan_id_does_not_exist])).exists.not).map(&:id)
      employment_ids2 = @employments.joins(:employment_learning_plans).where("employment_learning_plans.learning_plan_id = ?", params[:q][:completed_employment_learning_plans_learning_plan_id_does_not_exist]).where("employment_learning_plans.status != ?", "completed").map(&:id)
      employment_ids = (employment_ids1 + employment_ids2).compact.uniq
      @employments = @employments.where(id: employment_ids)
    end

    unless params[:q][:program_assessments_program_id_does_not_exist].blank?
      program_assessments_table = ProgramAssessment.arel_table
      @employments = @employments.where(ProgramAssessment.where(program_assessments_table[:employment_id].eq(employments_table[:id])).where(program_assessments_table[:program_id].eq(params[:q][:program_assessments_program_id_does_not_exist])).exists.not)
    end

    @employments.where(service_id: service.id).where("employments.ended_at IS NULL").sort_by! { |e| [e.name] }
  end
end