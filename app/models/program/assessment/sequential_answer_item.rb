class Program::Assessment::SequentialAnswerItem < ActiveRecord::Base
  self.table_name = :program_assessment_sequential_answer_items

  asset_accessor :reference_image

  belongs_to :sequential_question, class_name: 'Program::Assessment::SequentialQuestion', inverse_of: :answer_items

  delegate :upload_images?, to: :sequential_question

  validates :reference_image, presence: true, :if => :upload_images?
  validates :text, presence: true, :unless => :upload_images?

  validates :correct_position, presence: true

  def self.correct_order
    order(:correct_position)
  end

  def self.default_order
    order(:position)
  end

  def name
    if upload_images?
      [reference_image_name, ("(#{text})" if text.present?)].compact.join(' ')
    else
     text
    end
  end

  def reference_image_url
    if reference_image
      reference_image.thumb("210x120#").url
    end
  end

end
