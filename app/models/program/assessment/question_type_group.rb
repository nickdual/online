require 'securerandom'

class Program::Assessment::QuestionTypeGroup
  extend ActiveModel::Naming
  include ActiveModel::Conversion

  attr_accessor :learning_outcome, :question_class, :questions

  def initialize(attributes = {})
    attributes.each do |key, value|
      self.send "#{key}=", value
    end
  end

  def id
    @id ||= SecureRandom.uuid
  end

  def persisted?
    true
  end
end
