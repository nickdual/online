class Program::Assessment::Section < ActiveRecord::Base
  self.table_name = :program_assessment_sections

  QUESTION_CLASSES = [
    Program::Assessment::MultipleChoiceQuestion,
    Program::Assessment::SequentialQuestion,
    Program::Assessment::TrueOrFalseQuestion,
    Program::Assessment::MatchingTaskQuestion,
    Program::Assessment::FillGapQuestion ]

  belongs_to :assessment, class_name: 'Program::Assessment', touch: true, inverse_of: :sections

  has_many :true_or_false_questions, class_name: 'Program::Assessment::TrueOrFalseQuestion', order: "random()"
  has_many :matching_task_questions, class_name: 'Program::Assessment::MatchingTaskQuestion', order: "random()"
  has_many :multiple_choice_questions, class_name: 'Program::Assessment::MultipleChoiceQuestion', order: "random()"
  has_many :sequential_questions, class_name: 'Program::Assessment::SequentialQuestion', order: "random()"
  has_many :fill_gap_questions, class_name: 'Program::Assessment::FillGapQuestion', order: "random()"
  has_many :matching_task_questions, class_name: 'Program::Assessment::MatchingTaskQuestion', order: "random()"

  validates :minimum_number_of_questions, :name, :question_type, presence: true
  validates :minimum_number_of_questions, numericality: { greater_than_or_equal_to: 0, only_integer: true }

  validate :validate_minimum_number_of_questions_againts_questions_size

  scope :requires_answers, where("minimum_number_of_questions > ?", 0).order("random()")

  def self.default_order
    order(:name)
  end

  def question_ratio
    minimum_number_of_questions.to_f / questions.count
  end

  def questions
    true_or_false_questions +
      sequential_questions +
      multiple_choice_questions +
      matching_task_questions +
      fill_gap_questions
  end

  def question_class
    QUESTION_CLASSES.detect{|klass| klass.question_type == question_type }.tap do |result|
      raise QuestionTypeDoesntHaveQuestionClass.new(self) if result.nil?
    end
  end

  class QuestionTypeDoesntHaveQuestionClass < ExceptionWithDefaultMessage
  end

  private

  def validate_minimum_number_of_questions_againts_questions_size
    if self.assessment.try(:active?) && self.questions.size < self.minimum_number_of_questions
      self.errors.add :minimum_number_of_questions, :greater_than_questions_count
    end
  end
end
