class Program::Assessment::TrueOrFalseQuestion < ActiveRecord::Base
  include Program::Assessment::IsAQuestion
  self.table_name = :program_assessment_true_or_false_questions

  asset_accessor :reference_image

  validates :question, presence: true

  def self.code_name
    'TF'
  end

  def self.question_type
    "True/False"
  end

  def reference_image_url
    if reference_image
      reference_image.thumb("320x180>").url
    end
  end
end
