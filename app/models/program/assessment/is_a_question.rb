module Program::Assessment::IsAQuestion
  extend ActiveSupport::Concern

  included do
    belongs_to :learning_outcome, class_name: 'Program::Assessment::LearningOutcome', touch: true
    belongs_to :section, class_name: 'Program::Assessment::Section', touch: true

    delegate :assessment, to: :section

    validates :learning_outcome_id, presence: true

    def attempts_count
      self.user_assessment_result_questions.count
    end

    def pass_rate
      successful_attempts_count.to_f / attempts_count
    end

    def possible_learning_outcomes
      self.assessment.learning_outcomes
    end

    def question_id
      id_to_s = format("%0#{self.class.count.to_s.size}d", id)
      "#{self.class.code_name}#{id_to_s}"
    end
    alias_method :name, :question_id

    def successful_attempts_count
      self.user_assessment_result_questions.correct.count
    end

    def unsuccessful_attempts_count
      self.user_assessment_result_questions.incorrect.count
    end

    def user_assessment_result_questions
      User::AssessmentResultQuestion.where(question_id: id, question_type: self.class.question_type)      
    end
  end
end
