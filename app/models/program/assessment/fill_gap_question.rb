class Program::Assessment::FillGapQuestion < ActiveRecord::Base
  include Program::Assessment::IsAQuestion

  self.table_name = :program_assessment_fill_gap_questions

  asset_accessor :reference_image

  has_many :answers, class_name: 'Program::Assessment::FillGapAnswer'
  accepts_nested_attributes_for :answers, :reject_if => :all_blank, :allow_destroy => true

  validates :question, :section_id, presence: true

  def self.code_name
    'FG'
  end

  def self.question_type
    "Fill Gap"
  end

  def reference_image_url
    if reference_image
      reference_image.thumb("320x180>").url
    end
  end

  def randomised_answers
    answers.order("random()")
  end
end