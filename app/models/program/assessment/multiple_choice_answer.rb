class Program::Assessment::MultipleChoiceAnswer < ActiveRecord::Base
  self.table_name = :program_assessment_multiple_choice_answers

  belongs_to :question, class_name: 'Program::Assessment::MultipleChoiceQuestion', touch: true

  validates :answer, presence: true

  def self.correct
    where(correct: true)
  end

end
