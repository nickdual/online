class Program::Assessment::SequentialQuestion < ActiveRecord::Base
  include Program::Assessment::IsAQuestion

  self.table_name = :program_assessment_sequential_questions

  QUESTION_TYPES = ["Image", "Text"]

  belongs_to :section, class_name: 'Program::Assessment::Section'
  
  has_many :answer_items, class_name: 'Program::Assessment::SequentialAnswerItem', inverse_of: :sequential_question
  accepts_nested_attributes_for :answer_items, :reject_if => :all_blank, :allow_destroy => true

  alias_attribute :to_s, :question_id
  alias_attribute :name, :question_id
  validates :question, :question_type, :section_id, presence: true

  def self.code_name
    'SQ'
  end

  def self.question_type
    "Sequencing"
  end

  def add_default_answer_items
    3.times do |i|
      self.answer_items.build(correct_position: i + 1)
    end
    self
  end

  def randomised_answer_items
    answer_items.order("random()")
  end

  def upload_images
    self.question_type == 'Image'
  end
  alias_method :upload_images?, :upload_images

  def upload_images=(value)
    self.question_type = ActiveRecord::ConnectionAdapters::Column.value_to_boolean(value) ? 'Image' : 'Text'
  end
end
