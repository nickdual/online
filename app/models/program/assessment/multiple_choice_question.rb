class Program::Assessment::MultipleChoiceQuestion < ActiveRecord::Base
  include Program::Assessment::IsAQuestion

  self.table_name = :program_assessment_multiple_choice_questions

  asset_accessor :reference_image

  has_many :answers, class_name: 'Program::Assessment::MultipleChoiceAnswer'
  accepts_nested_attributes_for :answers, :reject_if => :all_blank, :allow_destroy => true

  validates :question, :section_id, presence: true

  def self.code_name
    'MC'
  end

  def self.question_type
    "Multiple Choice"
  end

  def reference_image_url
    if reference_image
      reference_image.thumb("320x180>").url
    end
  end

  def randomised_answers
    answers.order("random()")
  end
end
