class Program::Assessment::LearningOutcome < ActiveRecord::Base
  self.table_name = :program_assessment_learning_outcomes

  belongs_to :assessment, class_name: 'Program::Assessment', touch: true

  has_many :fill_gap_questions, class_name: 'Program::Assessment::FillGapQuestion'
  has_many :matching_task_questions, class_name: 'Program::Assessment::MatchingTaskQuestion'
  has_many :multiple_choice_questions, class_name: 'Program::Assessment::MultipleChoiceQuestion'
  has_many :sequential_questions, class_name: 'Program::Assessment::SequentialQuestion'
  has_many :true_or_false_questions, class_name: 'Program::Assessment::TrueOrFalseQuestion'

  validate :description, presence: true

  def question_type_groups
    questions.group_by(&:class).map { |question_class, questions|
      Program::Assessment::QuestionTypeGroup.new(
        learning_outcome: self,
        question_class: question_class,
        questions: questions) }
  end

  def questions
    fill_gap_questions +
      matching_task_questions +
      multiple_choice_questions +
      sequential_questions +
      true_or_false_questions
  end

  def percentage
    @percentage ||= rand
  end

  def to_s
    description
  end
end
