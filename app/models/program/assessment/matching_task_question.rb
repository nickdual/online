class Program::Assessment::MatchingTaskQuestion < ActiveRecord::Base
  include Program::Assessment::IsAQuestion

  self.table_name = :program_assessment_matching_task_questions

  has_many :items, class_name: 'Program::Assessment::MatchingTaskItem'
  accepts_nested_attributes_for :items, :reject_if => :all_blank, :allow_destroy => true

  def self.code_name
    'MT'
  end

  def self.question_type
    "Matching Task"
  end

  def randomised_items
    items.order("random()")
  end
end
