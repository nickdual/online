class Program::Assessment::FillGapAnswer < ActiveRecord::Base
  self.table_name = :program_assessment_fill_gap_answers

  belongs_to :question, class_name: 'Program::Assessment::FillGapQuestion', touch: true

  validates :answer, presence: true

  alias_attribute :to_s, :answer

  def self.correct
    where(correct: true)
  end

end
