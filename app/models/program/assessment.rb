class Program::Assessment < ActiveRecord::Base
  self.table_name = :online_program_assessments

  belongs_to :program, touch: true
  belongs_to :activated_by, :class_name => "Admin", :foreign_key => :activated_by_id
  has_many :learning_outcomes, class_name: 'Program::Assessment::LearningOutcome'
  has_many :sections, class_name: 'Program::Assessment::Section', inverse_of: :assessment
  has_many :user_assessment_results, class_name: 'User::AssessmentResult'

  accepts_nested_attributes_for :learning_outcomes, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :sections

  alias_attribute :to_s, :name
  validates :assessment_type, :name, :program_id, :time_allowed, :pass_mark, presence: true

  def self.active
    where(" activated_at is not null")
  end

  def self.essentials
    where(assessment_type: 'Essentials')
  end

  def active?
    activated_at.present?
  end

  def being_activated?
    changes.keys.include?('activated_at')
  end

  def add_default_sections
    Section::QUESTION_CLASSES.each do |question_class|
      unless self.sections.map(&:question_type).include?(question_class.question_type)
        self.sections.build(
          name: question_class.question_type,
          question_type: question_class.question_type,
          minimum_number_of_questions: 0)
      end
    end

    self.sections
  end

  def description
    "Ah, yes! John Quincy Adding Machine. He struck a chord with the voters when he pledged not to go on a killing spree. How much did you make me? There's one way and only one way to determine if an animal is intelligent. Dissect its brain! Ah, the 'Breakfast Club' soundtrack! I can't wait til I'm old enough to feel ways about stuff! Michelle, I don't regret this, but I both rue and lament it. Oh right. I forgot about the battle."
  end

  def answer_attempts_count
    user_assessment_results.count
  end

  def answer_successful_count
    user_assessment_results.passed.count
  end

  def answer_unsuccessful_count
    user_assessment_results.failed.count
  end

  def failed_by_user?(user)
    user_assessment_results.where(user_id: user).last.try(:failed?)
  end

  def pass_rate
    answer_successful_count.to_f / answer_attempts_count
  end

  def average_complete_time
    time_array = user_assessment_results.where("assessment_id = ? AND time_taken > 0", self.id).pluck(:time_taken)
    return 0 if time_array.empty?
    total_time = time_array.inject(:+)
    total_time / time_array.size
  end

  def status
    active? ? 'Active' : 'Inactive'
  end
end
