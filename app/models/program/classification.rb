class Program::Classification
  IDS = ["introduction", "program", "closer"]

  attr_reader :id

  def self.all
    @all ||= IDS.map{|id| new(id) }
  end

  def self.find(id)
    all.detect{|type| type.id == id}
  end

  def initialize(id)
    @id = id
  end

  def to_s
    id.titleize
  end
end