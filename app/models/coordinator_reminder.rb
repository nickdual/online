# Thanks Heroku for only daily jobs
module CoordinatorReminder
  extend ActiveSupport::Concern
  module ClassMethods
    def coordinator_weekly_report
      if Time.now.friday?
        Employment.where(role: "coordinator").active.each do |employment|
          CoordinatorNotifier.delay.coordinator_weekly_report(employment)
        end

        puts "Emails send out to #{Employment.where(role: "coordinator").active.count} coordinators"
      else
        puts "It's not Friday, cannot run task"
      end
    end

    def group_coordinator_weekly_report
      if Time.now.friday?
        Coordinator.all.each do |coordinator|
          CoordinatorNotifier.delay.group_coordinator_weekly_report(coordinator)
        end

        puts "Emails send out to #{Coordinator.count} coordinators"
      else
        puts "It's not Friday, cannot run task"
      end
    end
  end
end