class LearningResource < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  has_explicit_column_definitions_for_rspec_fire

  validates_presence_of :name
  validates_presence_of :resource

  belongs_to :created_by, :class_name => "Admin"
  belongs_to :updated_by, :class_name => "Admin"
  belongs_to :program

  has_many :assessements, :class_name => "LearningResourceAssessment"
  has_many :downloads, :class_name => "LearningResourceDownload"

  alias_attribute :to_s, :name
  delegate :file, :remote_url, :url, :to => :resource
  validate :enforce_program_classification

  is_sluggable :name
  asset_accessor :resource

  scope :default_order, order(:name)
  scope :all_staff, where("coordinators_only != true")
  scope :coordinators_only, where("coordinators_only = true")

  def create_downloads_by_user!(user)
    # Gathering a list of employments who have access to this learning resource
    @services = self.program.services.where(id: user.services.map(&:id))
    @employments = Employment.where(service_id: @services.map(&:id)).where(user_id: user.id)

    @employments.each do |employment|
      self.downloads.create!(:employment => employment)
    end
  end

  def url
    resource.url
  end

  private

    def enforce_program_classification
      errors.add(:program, "must be a program classification") unless program.classification == "program"
    end
end
