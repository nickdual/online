class Program < ActiveRecord::Base
  extend HasExplicitColumnDefinitionsForRspecFire

  include ProgramSettings # To create zencoder job

  has_explicit_column_definitions_for_rspec_fire

  belongs_to :created_by, :class_name => "Admin", :foreign_key => :created_by_id
  belongs_to :updated_by, :class_name => "Admin", :foreign_key => :updated_by_id
  belongs_to :category, :class_name => "ProgramCategory", :foreign_key => :category_id
  belongs_to :program_type

  has_many :assessments, class_name: "Program::Assessment"
  has_many :group_sessions
  has_many :learning_plan_programs, :dependent => :destroy
  has_many :learning_plans, :through => :learning_plan_programs
  has_many :service_assessments, class_name: "ServiceProgramAssessment"
  has_many :group_assessments, class_name: "GroupProgramAssessment"

  has_many :package_programs, :dependent => :destroy
  has_many :packages, :through => :package_programs
  has_many :service_programs
  has_many :services, :through => :service_programs

  has_one :original_video, :foreign_key => :program_id, :class_name => "Video", :conditions => where(:description => "original video")
  has_one :thumb, :class_name => "ProgramThumb", :dependent => :destroy

  has_many :videos, :dependent => :destroy
  has_many :not_original_videos, :class_name => "Video", :foreign_key => :program_id, :conditions => "videos.description != 'original video'"

  has_many :learning_resources, :dependent => :destroy
  has_many :user_learning_resources, :class_name => "LearningResource", :conditions => "learning_resources.coordinators_only != true"
  has_many :coordinator_learning_resources, :class_name => "LearningResource", :conditions => "learning_resources.coordinators_only = true"

  has_many :learning_records, :dependent => :destroy
  has_many :service_programs
  has_many :services, :through => :service_programs

  validate :title, presence: true
  validate :code, presence: true, uniqueness: true
  validate :classification, presence: true
  validate :category_id, presence: true

  accepts_nested_attributes_for :thumb
  is_sluggable :title

  after_create :create_essential_assessment, if: :classified_as_program?
  after_create :process_original_video, :if => :original_video

  attr_accessor :program_ids
  alias_attribute :to_s, :title
  scope :default_order, order(:title, :code)
  scope :processing, where("processing = true")
  scope :processed, where("processing != true")
  scope :closers, where(:classification => "closer")
  scope :introductions, where(:classification => "introduction")
  scope :programs, where(:classification => "program")
  scope :introductions_or_closers, where("classification = ? OR classification = ?", "introduction", "closer")

  def active_assessments_count
    assessments.active.count
  end

  def essentials_assessment
    assessments.essentials.first
  end

  def to_label
    if category_id.present?
      category.parent_category_name ? ([title, " (", category.parent_category_name, " - ", category.to_s, ")"].join) : ([title, "(", category.to_s, ")"].join)
    else
      title
    end
  end

  def video=(original_video)
    self.videos.build(:file => original_video, :description => "original video")
  end

  def video
    self.original_video
  end

  def build_video_from_s3=(file_name)
    self.videos.build(:file_uid => file_name, :file_name => file_name, :description => "original video")
  end

  def mark_as_processed!
    job = Zencoder::Job.details(self.zencoder_id).body["job"]
    self.update_attributes(:processing => false)
    ImportNotifier.delay.program_processed(self)
  end

  def self.classifications
    Program::Classification::IDS
  end

  def process_original_video
    @zencode = self.create_zencoder_job

    raise "Errors processing original video #{@zencode.errors.to_s}" if @zencode.errors.present?
    update_zencoder_id
  end

  def thumb_60_60_url
    thumb.try(:thumb_60_60_url)
  end

  def thumb_200_200_url
    thumb.try(:thumb_200_200_url)
  end

  def image_url
    thumb.try(:url)
  end

  def related
    Program.where(category_id: self.category_id).where("id != ?", self.id)
  end

  def category_name
    category.try(:to_s)
  end

  def parent_category_name
    category.try(:parent).try(:to_s)
  end

  def assessments_count
    assessments.count
  end

  def classified_as_program?
    self.classification == 'program'
  end

  def create_essential_assessment
    attributes = {
      assessment_type: 'Essentials',
      name: 'Essentials',
      time_allowed: 0 }

    assessments.create(attributes).tap do |assessment|
      assessment.add_default_sections.each do |section|
        section.save!
      end
    end
  end

  private

    def update_zencoder_id
      self.update_attribute(:zencoder_id, @zencode.body["id"])
    end

end
