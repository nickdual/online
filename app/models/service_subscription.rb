class ServiceSubscription < ActiveRecord::Base
  
  belongs_to :group
  belongs_to :service
  belongs_to :package

end
