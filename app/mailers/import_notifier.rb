class ImportNotifier < ActionMailer::Base
  # Used for genetic imports / background processing notifications
  default from: "ACC Online <online@acctv.co>"
  layout "notifier"

  def user_import_complete(import)
    @admin = import.admin
    @import = import
    mail(to: @admin.email, subject: "Completed User Import for #{@import.service}")
  end

  def program_processed(program)
    @admin = program.created_by
    @program = program
    mail(to: program.created_by.email, subject: "#{@program} has finished Encoding")
  end
end