class MailerTemplate < ActionMailer::Base
  default from: "online@acctv.co"
  layout "mailers"

  def template(template, email)
    mail to: email, subject: "Template test: #{template}" do |format|
      format.html { render "mailers/#{template}" }
    end
  end
end
