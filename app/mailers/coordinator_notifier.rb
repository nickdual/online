class CoordinatorNotifier < ActionMailer::Base
  default from: "ACC Online <online@acctv.co>"
  layout "notifier"

  def coordinator_weekly_report(employment)
    @employment = employment
    @user = @employment.user
    @service = @employment.service
    mail(to: @user.email, :subject => "Your Weekly Coordinator Report for #{@service} - #{Date.today.to_s(:long_ordinal)}")
  end

  def group_coordinator_weekly_report(group_coordinator)
    @user = group_coordinator.user
    @group = group_coordinator.group
    mail(to: @user.email, :subject => "Your Weekly Group Coordinator Report for #{@group} - #{Date.today.to_s(:long_ordinal)}")
  end

  def service_information_report(user, coordinator, attach_filename, content)
    @coordinator = coordinator
    @user = user
    attachments[attach_filename] = {:mime_type => 'application/xls', :content => content }
    mail(to: @user.email, :subject => "Excel Report")
  end
end
