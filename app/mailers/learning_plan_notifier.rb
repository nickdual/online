class LearningPlanNotifier < ActionMailer::Base
  default from: "ACC Online <online@acctv.co>"
  # We're using a different mailer here as the context is different, this is coordinators speaking to users, not acc speaking to users
  # TODO: Make this another template
  layout "notifier"

  def new_plan(employment_learning_plan_id)
    @plan = EmploymentLearningPlan.find(employment_learning_plan_id)
    @user = @plan.user
    @service = @plan.service

    mail(to: @user.email, subject: "You have a New Learning Plan: #{@plan}")
  end

  def overdue_plan(plan)
    @plan = plan
    @service = @plan.service
    @user = @plan.user

    mail(to: @user.email, subject: "Your Learning Plan #{@plan} is now Overdue")
  end

  def due_in_x(plan, days_till_due)
    @plan = plan
    @service = @plan.service
    @user = @plan.user

    mail(to: @user.email, subject: "Your Learning Plan #{@plan} is due in #{days_till_due} days")
  end
end
