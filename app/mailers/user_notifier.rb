class UserNotifier < ActionMailer::Base
  default from: "ACC Online <online@acctv.co>"
  layout "notifier"

  # Admin area
  def invite_admin(admin)
    @admin = admin
    mail(:to => admin.email, :subject => "You have been invited to become an ACC Admin")
  end

  # User account registration
  def welcome_self_registered_user(user, service)
    @user     = user
    @service  = service
    mail(to: user.email, subject: "Your Account has been Successfully Activated with #{@service}")
  end
  
  def welcome_invited_user(user, employment)
    @user     = user
    @service  = (employment.service ? employment.service : employment.find_service_by_token)
    mail(to: user.email, subject: "Your Account has been Created for #{@service}")
  end
  
  def user_has_joined_service(employment)
    @user = employment.reload.user
    @service = employment.reload.service
    mail(:to => @user.email, :subject => "Your Account is now Active with #{@service}")
  end

  # Coordinator account registration
  def existing_user_has_joined_group(coordinator)
    @coordinator = coordinator
    @user = @coordinator.user
    @group = @coordinator.group

    mail(to: @user.email, subject: "You are now a Group Coordinator for #{@group}")
  end

  def new_user_has_joined_group(coordinator, password)
    @user = coordinator.user
    @group = coordinator.group
    @password = password
    mail(to: @user.email, subject: "Your Group Coordinator Account has been Created for #{@group}")
  end
end
