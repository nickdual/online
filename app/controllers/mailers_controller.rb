class MailersController < ApplicationController
  layout "mailers"

  def index
    render(:action => params[:id])
    MailerTemplate.template(params[:id], params[:email]).deliver if params[:email]
  end
end