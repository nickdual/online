class Api::Profiles::AssessmentsController < Api::BaseController
  def show
    @program = Program.find(params[:id])
    @assessment = current_user.assessments(@program)
    respond_with(@assessment, root: false)
  end
end
