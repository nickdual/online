class Api::Profiles::EmploymentLearningPlansController < Api::BaseController
  def index
    if params[:active]
      @plans = EmploymentLearningPlan.where(employment_id: employment_ids)
        .includes(:service, :user, :employment, :learning_records, :program_assessments, :employment_learning_plan_programs)
        .not_pending.not_completed.order("due_at").limit(params[:limit] || 999)
    else
      @plans = EmploymentLearningPlan.where(employment_id: employment_ids)
        .includes(:service, :user, :employment, :learning_records, :program_assessments, :employment_learning_plan_programs)
        .not_pending.limit(params[:limit] || 999)
    end

    @plans = @plans.sort_by { |e| [e.status_id, e.due_at] }
    respond_with(@plans)
  end

  private

    def employment_ids
      current_user.employments.active.pluck(:id)
    end
end
