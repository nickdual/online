class Api::Profiles::ActivitiesController < Api::BaseController
  def index
    if params[:limit]
      @activities = current_user.activities[0, params[:limit].to_i]
    else
      @activities = current_user.activities
    end

    respond_with(@activities)
  end
end