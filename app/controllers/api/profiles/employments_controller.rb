class Api::Profiles::EmploymentsController < Api::BaseController

  def index
    @employments = current_user.employments.active.not_group_coordinator
      .includes(:service, :user).order_by_service
    respond_with(@employments, root: false)
  end

  def create
    @employment = current_user.employments.new(params[:employment])
    @employment.save
    respond_with @employment, root: false
  end

  def show
    @employment = current_user.employments.find(params[:id])
    respond_with @employment, root: false
  end

  def update
    @employment = current_user.employments.find(params[:id])
    @employment.update_attributes(params[:employment])
    respond_with @employment, root: false
  end

  def destroy
    @employment = current_user.employments.find(params[:id])
    @employment.finish!
    respond_with(@employment)
  end
end
