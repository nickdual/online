class Api::Profiles::CoordinatorsController < Api::BaseController
  def index
    @coordinators = current_user.coordinators
    respond_with(@coordinators)
  end

  def destroy
    @coordinator = current_user.coordinators.find(params[:id])
    @coordinator.destroy
    respond_with(@coordinator)
  end
end
