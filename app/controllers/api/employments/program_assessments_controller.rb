class Api::Employments::ProgramAssessmentsController < Api::BaseController
  before_filter :employment

  def create
    @program_assessment = @employment.program_assessments.new(params[:program_assessment])
    @program_assessment.save!
    respond_with(@program_assessment)
  end

  private

    def employment
      @employment = Employment.find(params[:employment_id])
    end

end