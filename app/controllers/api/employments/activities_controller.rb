class Api::Employments::ActivitiesController < Api::BaseController
  before_filter :employment

  def index
    @activities = @employment.user.activities
    respond_with(@activities)
  end

  private

    def employment
      @employment = Employment.find(params[:employment_id])
    end

end