class Api::BaseController < ApplicationController

  before_filter :authenticate_user!, :set_timezone

  self.responder = ApplicationResponder
  respond_to :json

  def current_ability
    @current_ability ||= EmploymentAbility.new(current_user)
  end

  rescue_from CanCan::AccessDenied do
    flash[:notice] = "Access denied."
    redirect_to root_path
  end

  private

    def set_timezone
      Time.zone = current_user.time_zone
    end

    def roles
      @roles = Employment.role_options
    end

    def doc_raptor_send(options = { })
      if Rails.env.test?
        render_to_string
      else
          default_options = { 
            :name             => controller_name,
            :document_type    => request.format.to_sym,
            :test             => !Rails.env.production?,
          }
          options = default_options.merge(options)
          options[:document_content] ||= render_to_string
          ext = options[:document_type].to_sym
      
          response = DocRaptor.create(options)
          if response.code == 200
            send_data response, :filename => "#{options[:name]}.#{ext}", :type => ext
          else
            render :inline => response.body, :status => response.code
          end
        end
      end
end
