class Api::EmploymentLearningPlans::ProgramsCompletedController < Api::BaseController
  before_filter :employment_learning_plan

  def index
    @completed_programs = @employment_learning_plan.programs_completed
    respond_with @completed_programs
  end

  private

    def employment_learning_plan
      @employment_learning_plan = EmploymentLearningPlan.find(params[:employment_learning_plan_id])
    end
end