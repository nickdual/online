class Api::EmploymentLearningPlans::ProgramsPendingController < Api::BaseController
  before_filter :employment_learning_plan

  def index
    @pending_programs = @employment_learning_plan.programs_pending
    respond_with @pending_programs
  end

  private

    def employment_learning_plan
      @employment_learning_plan = EmploymentLearningPlan.find(params[:employment_learning_plan_id])
    end
end