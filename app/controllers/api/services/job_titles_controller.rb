class Api::Services::JobTitlesController < Api::BaseController
  before_filter :service

  def index
    if @service.group_id
      @job_titles = @service.group.job_titles.default_order
    else
      @job_titles = @service.job_titles.default_order
    end

    respond_with(@job_titles, root: false)
  end

  def create
    @job_title = @service.job_titles.new(params[:job_title])
    @job_title.save
    respond_with(@job_title, root: false)
  end

  def show
    @job_title = @service.job_titles.find(params[:id])
    respond_with(@job_title, root: false)
  end

  def update
    @job_title = @service.job_titles.find(params[:id])
    @job_title.update_attributes(params[:job_title])
    respond_with(@job_title, root: false)
  end

  def destroy
    @job_title = @service.job_titles.find(params[:id])
    @job_title.destroy
    respond_with(@job_title)
  end

  private

  def service
    @service = Service.find_using_slug!(params[:service_id])
  end
end
