class Api::Services::ChartsController < Api::BaseController
  before_filter :service

  def index
    charts = []
    charts << { type: 'column', name: 'learning_records_in_24_hours', data: ServiceChart.learning_records_over_24_period(@service) }
    charts << { type: 'column', name: 'learning_records_in_7_days', data: ServiceChart.learning_records_past_7_days(@service) }
    charts << { type: 'column', name: 'learning_records_in_30_days', data: ServiceChart.learning_records_over_30_days(@service) }
    charts << { type: 'column', name: 'learning_plans_in_30_days', data: ServiceChart.learning_plans_completed_over_30_days(@service) }
    charts << { type: 'pie', name: 'learning_plans_pending_vs_overdue', data: ServiceChart.learning_plan_pending_vs_overdue(@service) }

    respond_with(charts) do |format|
      format.json { render json: charts.to_json }
    end
  end

  private

  def service
    @service = Service.find_using_slug!(params[:service_id])
  end
end
