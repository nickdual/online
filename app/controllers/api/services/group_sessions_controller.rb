class Api::Services::GroupSessionsController < Api::BaseController
  before_filter :service

  def create
    @group_session = @service.group_sessions.new(params[:group_session])
    @group_session.save
    respond_with(@group_session)
  end

    private

    def service
      @service = Service.find_using_slug!(params[:service_id])
    end

end