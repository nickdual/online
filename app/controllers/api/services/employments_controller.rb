class Api::Services::EmploymentsController < Api::BaseController
  before_filter :service
  before_filter :set_questionable_params, only: [:create]

  def index
    @employments = Employment.where(service_id: @service.id)
      .active.includes(:service, :user).order_by_name
    respond_with(@employments, root: false)
  end

  def advanced
    @employments = EmploymentSearch.results(params, @service)

    respond_with(@employments) do |format|
      format.json { render json: @employments, root: false }
    end
  end

  def create
    @questionable = User::Questionable.new(params[:user])

    if @questionable.save
      respond_with(@questionable.employment, root: false)
    else
      respond_with(@questionable.user)
    end
  end

  def show
    @employment = @service.employments.find(params[:id])
    respond_with(@employment, root: false)
  end

  def update
    @employment = @service.employments.find(params[:id])
    @employment.update_attributes(params[:employment])
    @employment.touch # Because updating a user and not an employment returns a cached user
    respond_with(@employment, root: false)
  end

  def destroy
    @employment = @service.employments.find(params[:id])
    @employment.update_attribute(:ended_at, Time.zone.now)
    respond_with(@employment, root: false)
  end

  private

    def service
      @service = Service.find_using_slug!(params[:service_id])
    end

    def set_questionable_params
      params[:user][:service_id] = @service.id
      params[:user][:activation_token] = @service.activation_token
    end

end
