class Api::Services::LearningPlansController < Api::BaseController
  before_filter :service

  def index
    @learning_plans = @service.learning_plans.default_order
    respond_with(@learning_plans, root: false)
  end

  def create
    @learning_plan = LearningPlan.new(params[:learning_plan])
    @learning_plan.service_id = @service.id
    @learning_plan.save
    respond_with(@learning_plan, root: false)
  end

  def show
    @learning_plan = @service.learning_plans.find(params[:id])
    respond_with(@learning_plan, root: false)
  end

  def update
    @learning_plan = @service.learning_plans.find(params[:id])
    @learning_plan.update_attributes(params[:learning_plan])
    respond_with(@learning_plan, root: false)
  end

  def destroy
    @learning_plan = @service.learning_plans.find(params[:id])
    @learning_plan.destroy
    respond_with(@learning_plan, root: false)
  end

  private

  def service
    @service = Service.find_using_slug!(params[:service_id])
  end
end
