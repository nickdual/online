class Api::ProfilesController < Api::BaseController
  def show
    respond_with(current_user, root: false)
  end

  def update
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    params[:user][:activation_code_set_at] = nil
    current_user.update_attributes(params[:user])
    sign_in current_user, :bypass => true

    respond_with(current_user, root: false)
  end
end
