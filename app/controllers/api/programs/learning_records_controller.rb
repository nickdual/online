class Api::Programs::LearningRecordsController < Api::BaseController
  before_filter :program

  def create
    @learning_record = current_user.create_learning_record!(@program)
    respond_with(@learning_record)
  end

  def show
    @learning_record = @program.learning_records.find(params[:id])
    respond_with(@learning_record)
  end

  private

    def program
      @program = Program.find_using_slug!(params[:program_id])
    end

end
