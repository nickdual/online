class Api::Programs::VideosController < Api::BaseController
  before_filter :program

  def index
    @videos = @program.videos.not_original
    respond_with(@videos)
  end

  private

    def program
      @program = Program.find_using_slug!(params[:program_id])
    end

end