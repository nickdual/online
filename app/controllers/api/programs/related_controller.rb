class Api::Programs::RelatedController < Api::BaseController

  before_filter :program

  def index
    # Applying context - we don't want to display programs the user doesn't have access to
    available_program_ids = current_user.programs.map(&:id)
    @programs = @program.related.select{ |p| available_program_ids.include?(p.id) }
    respond_with(@programs)
  end

  private

    def program
      @program = Program.find_using_slug!(params[:program_id])
    end

end
