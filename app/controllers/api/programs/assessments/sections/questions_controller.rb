class Api::Programs::Assessments::Sections::QuestionsController < Api::BaseController
  def index
    respond_with(collection)
  end

  private

  def collection
    parent.questions.shuffle
  end

  def parent
    @parent ||= Program::Assessment::Section.find(params[:program_assessment_section_id])
  end
end

