class Api::Programs::Assessments::ResultsController < Api::BaseController
  def create
    results = current_user.assessment_results.create!(params[:results])
    respond_with(results)
  end
end