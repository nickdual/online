class Api::Programs::Assessments::SectionsController < Api::BaseController
  def index
    respond_with(collection)
  end

  def show
    respond_with(collection.find(params[:id]))
  end

  private

  def collection
    parent.sections.requires_answers
  end

  def parent
    @parent ||= Program::Assessment.find(params[:program_assessment_id])
  end
end
