class Api::Programs::AssessmentsController < Api::BaseController
  def index
    respond_with(collection)
  end

  def show
    respond_with(collection.find(params[:id]))
  end

  def update
    results = current_user.assessment_results.create!(params[:results])
    respond_with(results)
  end

  private

  def collection
    parent.assessments
  end

  def parent
    @parent ||= Program.find_using_slug!(params[:program_id])
  end
end
