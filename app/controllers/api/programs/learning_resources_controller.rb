class Api::Programs::LearningResourcesController < Api::BaseController

  before_filter :program

  def index
    if params[:coordinator]
      @learning_resources = @program.learning_resources.coordinators_only.default_order
    else
      @learning_resources = @program.learning_resources.all_staff.default_order
    end

    respond_with(@learning_resources)
  end

  def show
    learning_resource = @program.learning_resources.find_using_slug!(params[:id])
    learning_resource.create_downloads_by_user!(current_user)
    send_file learning_resource.file, :filename => learning_resource.resource_name, :disposition => "attachment"
  end

  private

    def program
      @program = Program.find_using_slug!(params[:program_id])
    end

end