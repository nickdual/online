class Api::LearningPlans::ProgramsController < Api::BaseController
  before_filter :learning_plan

  def index
    @programs = @learning_plan.programs.default_order.includes(:category, :program_type, :thumb)
    respond_with(@programs, root: false)
  end

  def create
    @program = Program.find_using_slug!(params[:program_id])
    @learning_plan.programs << @program
    @learning_plan.save
    respond_with(@program, root: false)
  end

  def destroy
    @program = Program.find_using_slug!(params[:id])
    @learning_plan.programs.delete(@program)
    respond_with(@program, root: false)
  end

  private

  def learning_plan
    @learning_plan = LearningPlan.find_using_slug!(params[:learning_plan_id])
  end
end
