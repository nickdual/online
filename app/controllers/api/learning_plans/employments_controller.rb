class Api::LearningPlans::EmploymentsController < Api::BaseController
  before_filter :learning_plan

  def index
    @employments = @learning_plan.employments
    respond_with(@employments, root: false)
  end

  def destroy
    @employment_learning_plan = @learning_plan.employment_learning_plans.where('employment_id = ?', params[:id]).first
    @employment_learning_plan.destroy
    respond_with(@employment_learning_plan, root: false)
  end

  private

  def learning_plan
    @learning_plan = LearningPlan.find_using_slug!(params[:learning_plan_id])
  end
end