class Api::LearningPlans::EmploymentLearningPlansController < Api::BaseController
  before_filter :learning_plan

  def index
    @employment_learning_plans = @learning_plan.employment_learning_plans
      .includes(:service, :user, :employment, :learning_records, :program_assessments, :employment_learning_plan_programs)
    @employment_learning_plans = @employment_learning_plans.sort_by { |e| [e.status_id, e.due_at] }
    respond_with(@employment_learning_plans, root: false)
  end

  def show
    @employment_learning_plan = @learning_plan.employment_learning_plans.find(params[:id])
    respond_with(@employment_learning_plan, root: false)
  end

  def create
    @employment_learning_plan = @learning_plan.employment_learning_plans.new(params[:employment_learning_plan])
    @employment_learning_plan.save
    respond_with(@employment_learning_plan, root: false)
  end

  def destroy
    @employment_learning_plan = @learning_plan.employment_learning_plans.find(params[:id])
    @employment_learning_plan.destroy
    respond_with(@employment_learning_plan)
  end

  private

  def learning_plan
    @learning_plan = LearningPlan.find_using_slug!(params[:learning_plan_id])
  end
end
