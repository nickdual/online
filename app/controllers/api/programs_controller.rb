class Api::ProgramsController < Api::BaseController

  def index
    if params[:service_id]
      @service = Service.find_using_slug!(params[:service_id])
      program_ids = @service.service_programs.pluck(:program_id)
    else
      program_ids = current_user.user_programs.pluck(:program_id)
    end

    @programs = Program.where(id: program_ids).default_order.includes(:category, :program_type, :thumb)
    respond_with(@programs)
  end

  def show
    @program = current_user.programs.find(params[:id])
    respond_with(@program, root: false)
  end
end
