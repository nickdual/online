class Api::Groups::CoordinatorsController < Api::BaseController
  before_filter :group

  def create
    @coordinator = @group.coordinators.new(user: user)

    if @coordinator.save_with_user
      if @password
        UserNotifier.new_user_has_joined_group(@coordinator, @password).deliver
      else
        UserNotifier.existing_user_has_joined_group(@coordinator).deliver
      end
    end

    respond_with(@coordinator)
  end

  def destroy
    @coordinator = @group.coordinators.find(params[:id])
    @coordinator.destroy
    respond_with(@coordinator)
  end

  def index
    @coordinators = @group.coordinators.default_order
    respond_with(@coordinators)
  end

  private

  def group
    @group = Group.find_using_slug!(params[:group_id])
  end

  def user
    @user = User.find_or_initialize_by_email(params[:coordinator][:email])
    @user.attributes = params[:coordinator]

    if @user.new_record?
      @password = SecureRandom.hex(3)
      @user.activation_code_set_at = Time.zone.now
      @user.password, @user.password_confirmation = @password
    end

    @user
  end
end
