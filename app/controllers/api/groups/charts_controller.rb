class Api::Groups::ChartsController < Api::BaseController
  before_filter :group
  respond_to :json

  def index
    charts = []
    charts << { type: 'column', name: 'learning_records_in_24_hours', data: GroupChart.learning_records_created_in_24_hours(@group) }
    charts << { type: 'column', name: 'learning_records_in_30_days', data: GroupChart.learning_records_past_30_days_by_service(@group) }
    charts << { type: 'column', name: 'learning_plans_in_30_days', data: GroupChart.learning_plans_completed_over_30_days(@group) }
    charts << { type: 'pie', name: 'users_by_service', data: GroupChart.number_of_users_by_service(@group) }
    charts << { type: 'pie', name: 'learning_plans_pending_vs_overdue', data: GroupChart.learning_plan_pending_vs_overdue(@group) }

    respond_with(charts) do |format|
      format.json { render json: charts.to_json }
    end
  end

  private

  def group
    @group = Group.find_using_slug!(params[:group_id])
  end
end
