class Api::Groups::JobTitlesController < Api::BaseController
  before_filter :group

  def index
    @job_titles = @group.job_titles.default_order
    respond_with(@job_titles)
  end

  def create
    @job_title = @group.job_titles.new(params[:job_title])
    @job_title.save
    respond_with(@job_title, root: false)
  end

  def show
    @job_title = @group.job_titles.find(params[:id])
    respond_with(@job_title, root: false)
  end

  def update
    @job_title = @group.job_titles.find(params[:id])
    @job_title.update_attributes(params[:job_title])
    respond_with(@job_title, root: false)
  end

  def destroy
    @job_title = @group.job_titles.find(params[:id])
    @job_title.destroy
    respond_with(@job_title)
  end

  private

  def group
    @group = Group.find_using_slug!(params[:group_id])
  end
end
