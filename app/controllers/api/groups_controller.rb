class Api::GroupsController < Api::BaseController
  def index
    @groups = current_user.groups.active
    respond_with(@groups)
  end
end
