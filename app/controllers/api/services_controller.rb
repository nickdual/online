class Api::ServicesController < Api::BaseController
  before_filter :service, except: ["index"]

  def index
    if params[:group_id]
      @group = Group.find_using_slug!(params[:group_id])
      service_ids = @group.services.map(&:id)
    elsif params[:role_id]
      service_ids = current_user.employments.active.non_staff.map(&:service_id)
    else
      service_ids = current_user.employments.active.map(&:service_id)
    end

    @services = Service.where(id: service_ids).active.default_order

    respond_to do |format|
      format.xls { doc_raptor_send }
      format.json { render json: @services }
    end
  end

  def show
    respond_to do |format|
      format.xls { doc_raptor_send }
      format.html { }
    end
  end

  def export_services_information
    template = '/api/services/show.xls.haml'
    coordinator = @service.to_s
    DocraptorUserReportWorker.perform_async(Service, @service.id, template, @current_user.id, coordinator)
    render :nothing => true
  end

  def export_groups_services_information
    template = '/api/services/index.xls.haml'
    group = Group.find_using_slug!(params[:groupId])
    coordinator = group.to_s
    DocraptorUserReportWorker.perform_async(Group, group.id, template, @current_user.id, coordinator)
    render :nothing => true
  end

  def reset_token
    @service.reset_token

    respond_with(@service) do |format|
      format.json { render :json => @service }
    end
  end

  protected

    def service
      @service = Service.find_using_slug!(params[:id])
    end

    def reports
      resource

      @learning_records_past_7_days = ServiceChart.learning_records_past_7_days(@service)
      @learning_plan_pending_vs_overdue = ServiceChart.learning_plan_pending_vs_overdue(@service)
      @learning_records_over_24_period = ServiceChart.learning_records_over_24_period(@service)
      @learning_records_over_30_days = ServiceChart.learning_records_over_30_days(@service)
      @learning_plans_completed_over_30_days = ServiceChart.learning_plans_completed_over_30_days(@service)
    end

end
