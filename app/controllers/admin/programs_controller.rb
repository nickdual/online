class Admin::ProgramsController < Admin::BaseController

  inherit_resources
  defaults :route_prefix => 'admin', :finder => :find_using_slug!
  before_filter :program_categories, :packages

  # Breadcrumbs
  before_filter do
    program = Program.find_using_slug params[:id]
    add_breadcrumb 'Programs', admin_programs_path
    add_breadcrumb program.title, admin_program_path(program) if program
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def index
    if params[:search]
      @programs = Program.search(params[:search])
      flash[:notice] = "#{@programs.total_entries} record found"
    else
      @programs = Program.default_order
    end
  end

  def new
    @program = Program.new(:classification => "program")
  end

  def create
    create! { admin_program_path @program }
  end

  def show
    resource
  end

  def update
    update! { admin_program_path @program }
  end

  protected

    def create_resource(object)
      object.created_by, object.updated_by = current_admin, current_admin
      super
    end

    def update_resource(object, attributes)
      object.updated_by = current_admin
      super
    end

  private

    def program_categories
      @program_category_parents = ProgramCategory.roots.default_order
    end

    def packages
      @packages = Package.default_order
    end
end
