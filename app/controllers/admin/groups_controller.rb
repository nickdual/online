class Admin::GroupsController < Admin::BaseController

  inherit_resources
  defaults :route_prefix => 'admin'

  authorize_resource

  before_filter :countries, :packages
  before_filter :resource, :only => [:enable, :disable]

  # Breadcrumbs
  before_filter do
    group = Group.find_using_slug params[:id]
    add_breadcrumb 'Groups', admin_groups_path
    add_breadcrumb group.name, admin_group_path(group) if group
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def index
    @groups = Group.default_order
  end

  def enable
    @group.enable!
    redirect_to admin_group_path(@group), :notice => 'Successfully enabled group.'
  end

  def disable
    @group.disable!(current_admin)
    redirect_to admin_group_path(@group), :notice => 'Successfully disabled group.'
  end

  def new
    @group = Group.new(:country => "Australia")
  end

  def create
    @group = Group.new(params[:group])

    if @group.save
      redirect_to admin_group_path(@group), notice: "Group was successfully created."
    else
      render action: "new"
    end
  end

  def update
    @group = Group.find_using_slug!(params[:id])

    if @group.update_attributes(params[:group])
      redirect_to admin_group_path(@group), notice: "Group was successfully updated."
    else
      render action: "edit"
    end
  end

  protected

  def resource
    @group ||= Group.find_using_slug!(params[:id])
  end

  def create_resource(object)
    @group = object
    @group.created_by, @group.updated_by = current_admin, current_admin
    super
  end

  def update_resource(object, attributes)
    object.updated_by = current_admin
    super
  end

end
