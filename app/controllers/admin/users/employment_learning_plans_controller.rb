class Admin::Users::EmploymentLearningPlansController < Admin::BaseController

  before_filter do
    @user = User.find_using_slug! params[:user_id]
    @employment_learning_plan = EmploymentLearningPlan.includes(learning_plan: [:service, {learning_plan_programs: :program}]).find params[:id] if params[:id]
    add_breadcrumb 'Users', admin_users_path
    add_breadcrumb @user, admin_user_path(@user)
    add_breadcrumb 'Learning Plans', admin_user_learning_plans_path(@user)
    add_breadcrumb @employment_learning_plan, admin_user_learning_plan_path(@user, @employment_learning_plan) if @employment_learning_plan
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  authorize_resource :user
  authorize_resource :employment_learning_plan, through: :user

  def index
    @employment_learning_plans = @user.employment_learning_plans.includes(learning_plan: [:service, {learning_plan_programs: :program}])
  end

  def show
  end

end
