class Admin::Users::ActivitiesController < Admin::BaseController

  before_filter do
    @user = User.find_using_slug! params[:user_id]
    add_breadcrumb 'Users', admin_users_path
    add_breadcrumb @user, admin_user_path(@user)
    add_breadcrumb 'Learning Records', admin_user_activities_path(@user)
  end

  authorize_resource :user

  def index
    @activities = @user.activities
  end
end
