class Admin::UserImportsController < Admin::BaseController

  before_filter :service

  # Breadcrumbs
  before_filter do
    service = Service.find_using_slug params[:service_id]
    add_breadcrumb 'Services', admin_services_path
    add_breadcrumb service.name, admin_service_path(service)
    add_breadcrumb 'User Import', ''
  end

  def new
    @user_import = UserImport.new(:service_id => @service.id, :contents => "first_name,last_name,email,employee_number,date_of_birth,gender\nPaste your user records over this line, do not remove the line above")
  end

  def create
    @user_import = UserImport.new(params[:user_import])
    @user_import.admin_id = current_admin.id

    if @user_import.valid?
      # Since Psych doesn't like ActionDispatch::Http::UploadedFile
      csv_file = params[:user_import][:csv_file].tempfile.read
      @service.delay.import_users(csv_file, current_admin.id)
      flash[:notice] = "Successfully received csv file, we'll email #{current_admin.email} when it's complete"
      redirect_to admin_service_path(@service)
    else
      render action: "new"
    end
  end


  private

    def service
      @service = Service.find_using_slug!(params[:service_id])
    end
end