class Admin::Programs::Assessments::Sections::MultipleChoiceQuestionsController < Admin::BaseController
  helper_method :child_class, :collection, :parent, :resource, :resource_class

  before_filter do
    add_breadcrumbs_for_program(parent.assessment.program)
    add_breadcrumbs_for_assessment(parent.assessment)
    add_breadcrumbs_for_resource
  end

  def create
    resource.update_attributes params[resource_class.model_name.singular]

    location =
      if params[:commit] == I18n.t(:save_and_add_another_question) && resource.valid?
        new_polymorphic_url([:admin, parent, resource_class])
      end

    respond_with(:admin, parent,  resource, location: location)
  end

  def destroy
    resource.destroy
    respond_with(:admin, parent, resource)
  end

  def update
    resource.update_attributes params[resource_class.model_name.singular]
    respond_with(:admin, parent, resource)
  end

  private

  include Admin::BreadcrumbsHelper

  def child_class
    Program::Assessment::MultipleChoiceAnswer
  end

  def collection
    parent.multiple_choice_questions
  end

  def parent
    @parent ||= Program::Assessment::Section.find(params[:program_assessment_section_id])
  end

  def resource
    return @resource if @resource
    if params[:id]
      @resource = resource_class.find(params[:id])
    else
      @resource = collection.build
    end
  end

  def resource_class
    Program::Assessment::MultipleChoiceQuestion
  end
end
