class Admin::Programs::AssessmentsController < Admin::BaseController
  helper_method :collection, :parent, :resource, :resource_class

  before_filter do
    add_breadcrumbs_for_program(parent)
    add_breadcrumbs_for_resource(collection_path: url_for([:admin, parent]))
  end

  def create
    resource.update_attributes params[:program_assessment]
    respond_with(:admin, parent, resource)
  end

  def new
    resource.add_default_sections
  end

  def update
    if params[:commit] == I18n.t('program.assessment.activate')
      resource.activated_at = Time.now
      resource.activated_by_id = current_admin.try(:id)
    end

    resource.update_attributes params[:program_assessment]
    respond_with(:admin, parent, resource)
  end

  private

  include Admin::BreadcrumbsHelper

  def collection
    parent.assessments
  end

  def parent
    @parent ||= Program.find_using_slug!(params[:program_id])
  end

  def resource
    return @resource if @resource
    if params[:id]
      @resource = collection.find(params[:id])
    else
      @resource = collection.build
    end
  end

  def resource_class
    Program::Assessment
  end
end
