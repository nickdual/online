class Admin::ServicesController < Admin::BaseController
  authorize_resource

  before_filter :groups, :countries, :packages
  before_filter :find_service, :only => [:enable, :disable, :reset_token, :show, :edit, :update]

  # Breadcrumbs
  before_filter do
    add_breadcrumb 'Groups', admin_groups_path if @group.present?
    add_breadcrumb @group, admin_group_path(@group) if @group.present?
    service = Service.find_using_slug params[:id]
    add_breadcrumb 'Services', admin_services_path
    add_breadcrumb service.name, admin_service_path(service) if service
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def index
    if params[:group_id]
      @group = Group.find_using_slug!(params[:group_id])
      @services = @group.services.default_order
    else
      @services = Service.default_order
    end
  end

  def new
    if params[:group_id]
      @group = Group.find_using_slug!(params[:group_id])
      @service = @group.services.new
    else
      @service = Service.new
    end
  end

  def create
    if params[:group_id]
      @group = Group.find_using_slug!(params[:group_id])
      @service = @group.services.new(params[:service])
    else
      @service = Service.new(params[:service])
    end

    @service.created_by, @service.updated_by = current_admin, current_admin

    if @service.save
      redirect_to admin_service_path(@service), notice: "Service was successfully created."
    else
      render action: "new"
    end
  end

  def update
    @service.updated_by = current_admin

    if @service.update_attributes(params[:service])
      redirect_to admin_service_path(@service), notice: "Service was successfully updated."
    else
      render action: "edit"
    end
  end

  def enable
    @service.enable!
    redirect_to admin_service_path(@service), :notice => "Successfully enabled service."
  end

  def disable
    @service.disable!(current_admin)
    redirect_to admin_service_path(@service), :notice => "Successfully disabled service."
  end

  def reset_token
    @service.reset_token
    redirect_to admin_service_path(@service), :notice => "Service's activation token was successfully reset."
  end

  private

    def find_service
      @service = Service.find_using_slug!(params[:id])
    end

end
