class Admin::AdminsController < Admin::BaseController

  inherit_resources
  defaults :route_prefix => 'admin'
  before_filter :roles

  # Breadcrumbs
  before_filter do
    admin = Admin.find_using_slug params[:id]
    add_breadcrumb 'Admins', admin_admins_path
    add_breadcrumb admin.name, admin_admin_path(admin) if admin
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  respond_to :html, :csv
  authorize_resource

  def create
    create!(:notice => "Admin was successfully invited.") { admin_admins_path }
  end

  def update
    update! { admin_admins_path }
  end

  protected

  def collection
    @admins ||= end_of_association_chain.default_order
  end

  def resource
    @admin ||= Admin.find_using_slug!(params[:id])
  end

  def create_resource(object)
    object.password, object.password_confirmation = SecureRandom.hex(6)
    object.created_by, object.updated_by = current_admin, current_admin
    super
  end

  def update_resource(object, attributes)
    object.updated_by = current_admin
    super
  end

  def roles
    @roles = Admin.role_options
  end

end
