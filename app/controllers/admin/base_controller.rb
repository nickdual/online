require "admin_responder"

class Admin::BaseController < ApplicationController

  layout "admin"
  before_filter :authenticate_admin!

  self.responder = AdminResponder
  respond_to :html

  def current_ability
    @current_ability ||= AdminAbility.new(current_admin)
  end

  rescue_from CanCan::AccessDenied do
    flash[:notice] = "Access denied."
    redirect_to admin_path
  end

  # Breadcrumbs - generic edition - need to sort out resource loading before this can be used
  before_filter do
    add_breadcrumb '<i class="icon icon-home"></i>Home'.html_safe, admin_path
    # add_breadcrumb controller_name.titleize, url_for(controller: 'admin/' + controller_name, action: 'index')#admin_admins_path
    #   add_breadcrumb action_name.titleize, '' unless action_name == 'index'
  end

  private

  def countries
    @countries = ["Australia", "New Zealand", "South Africa", "United Kingdom", "United States"]
  end

  def groups
    if params[:group_id]
      @group = Group.find_using_slug(params[:group_id])
    else
      @groups = Group.all
    end
  end

  def packages
    @packages = Package.default_order
  end

  def intros_and_closers
    @introductions = Program.introductions.default_order
    @closers = Program.closers.default_order
  end

  def roles
    @roles = Employment.role_options
  end

end
