# This controller is intended to
# - Hold fixtures for JavaScript widgets that are too difficult to test with
#     Jasmine. The fixtures will be used by corresponding integration tests in
#     spec/features/javascript.
# - Serves as a place to manually test the JavaScript widget.
class Admin::StyleGuidesController < ApplicationController
  layout "admin"

  before_filter do
    add_breadcrumb 'Admin', url_for([:admin])
  end

  def show
    render params[:id]
  end
end