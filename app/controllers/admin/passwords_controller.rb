class Admin::PasswordsController < Admin::BaseController

  before_filter :user

  def reset
    @user.send_reset_password_instructions
    flash[:notice] = "Successfully sent password reset instructions to the user's email."
    redirect_to admin_user_path(@user)
  end
  
  private
  
    def user
      @user = User.find_using_slug!(params[:user_id])
    end
    
end
