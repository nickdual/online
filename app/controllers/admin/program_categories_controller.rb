class Admin::ProgramCategoriesController < Admin::BaseController

  inherit_resources
  defaults :route_prefix => 'admin'

  before_filter :parents
  authorize_resource

  # Breadcrumbs
  before_filter do
    program_category = ProgramCategory.find_using_slug params[:id]
    add_breadcrumb 'Program Categories', admin_program_categories_path
    add_breadcrumb program_category.name, admin_program_category_path(program_category) if program_category
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def index
    if params[:search]
      @program_categories = ProgramCategory.search(params[:search])
      flash[:notice] = "#{@program_categories.total_entries} record found"
    else
      @program_categories = ProgramCategory.default_order
    end
  end

  def create
    create! { admin_program_categories_path }
  end

  def update
    update! { admin_program_categories_path }
  end

  protected

  def parents
    @parents = ProgramCategory.roots
  end

  def resource
    @program_category ||= ProgramCategory.find_using_slug!(params[:id])
  end

  def create_resource(object)
    object.created_by, object.updated_by = current_admin, current_admin
    super
  end

  def update_resource(object, attributes)
    object.updated_by = current_admin
    super
  end

end
