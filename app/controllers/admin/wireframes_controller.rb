class Admin::WireframesController < ApplicationController
  layout "admin_wireframes"

  def index
    render(action: params[:id])
  end
end