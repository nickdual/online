class Admin::DashboardController < Admin::BaseController

  # Breadcrumbs
  before_filter do
    add_breadcrumb 'Dashboard', admin_path
  end

  def index
    @users_count = User.all.count
    @programs_count = Program.all.count
    @employment_learning_plans_upcoming_count = EmploymentLearningPlan.upcoming.count
    @program_views_count = LearningRecord.count
  end
end