class Admin::EmploymentsController < Admin::BaseController

  before_filter :employment, only: [:edit, :update, :end]
  before_filter :roles

  authorize_resource

  # Breadcrumbs
  before_filter do
    if params[:user_id]
      @user = User.find_using_slug!(params[:user_id])
      add_breadcrumb 'Users', admin_users_path
      add_breadcrumb @user.name, admin_user_path(@user)
      add_breadcrumb 'Employments', admin_user_path(@user)
    else
      @service = Service.find_using_slug!(params[:service_id])
      add_breadcrumb 'Services', admin_services_path
      add_breadcrumb @service.name, admin_service_path(@service)
      add_breadcrumb 'Employments', admin_service_users_path(@service)
    end

    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def update
    @employment.update_attributes(params[:employment])
    respond_with(:admin, @user)
  end

  def end
    @employment.finish!
    flash[:notice] = 'Employment was successfully terminated.'
    respond_with(:admin, @user)
  end

  protected

    def employment
      @employment = Employment.find(params[:id])
      @user = @employment.user
    end
end
