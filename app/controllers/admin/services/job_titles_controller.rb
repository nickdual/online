class Admin::Services::JobTitlesController < Admin::BaseController
  
  before_filter do
    find_service
    redirect_to_group
    find_job_title if params[:id].present?
    add_breadcrumb 'Services', admin_services_path
    add_breadcrumb @service, admin_service_path(@service)
    add_breadcrumb 'Job Titles', admin_service_job_titles_path(@service)
    add_breadcrumb @job_title, admin_service_job_title_path(@service, @job_title) if @job_title.present?
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  authorize_resource :service
  authorize_resource :job_title, through: :service, class: 'Service::JobTitle'

  def index
    find_job_titles
  end

  def show
    @employments = @job_title.employments
  end

  def new
    @job_title = @service.job_titles.build
  end

  def create
    @job_title = @service.job_titles.build params[:service_job_title]
    if @job_title.save
      redirect_to redirect_path, notice: "Job Title was successfully created"
    else
      render :new
    end
  end

  def destroy
    @job_title.destroy
    redirect_to redirect_path, notice: "Job Title was successfully deleted"
  end

  def edit
  end

  def update
    if @job_title.update_attributes params[:service_job_title]
      redirect_to redirect_path, notice: "Job Title was successfully updated"
    else
      render :edit
    end
  end

  private

  def find_service
    @service ||= Service.find_using_slug! params[:service_id]
  end

  def find_job_title
    find_service
    @job_title ||= @service.job_titles.find params[:id] if params[:id].present?
  end

  def find_job_titles
    find_service
    @job_titles ||= @service.job_titles.default_order
  end

  def redirect_to_group
    redirect_to admin_group_job_titles_path(@service.group), notice: "Job Titles are administered through this Service's Group" if @service.group.present?
  end

  def redirect_path
    admin_service_job_titles_path(@service)
  end
end
