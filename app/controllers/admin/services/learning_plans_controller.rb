class Admin::Services::LearningPlansController < Admin::BaseController

  # Breadcrumbs
  before_filter do
    add_breadcrumb 'Services', admin_services_path
    add_breadcrumb service.name, admin_service_path(service)
    add_breadcrumb 'Learning Plans', admin_service_learning_plans_path(service)
  end

  def index
    @learning_plans = service.learning_plans.default_order
  end

  def show
    @learning_plan = service.learning_plans.find_using_slug! params[:id]
  end

  private

    def service
      @service = Service.find_using_slug!(params[:service_id])
    end

end
