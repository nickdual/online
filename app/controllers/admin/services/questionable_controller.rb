class Admin::Services::QuestionableController < Admin::BaseController
  before_filter :service, :roles

  # Breadcrumbs
  before_filter do
    @service = Service.find_using_slug!(params[:service_id])
    add_breadcrumb 'Services', admin_services_path
    add_breadcrumb @service.name, admin_service_path(@service)
    add_breadcrumb 'Employments', admin_service_users_path(@service)

    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def new
    @user = User::SelfRegister.new(activation_token: @service.activation_token, role: "staff")
  end

  def create
    @questionable = User::Questionable.new(params[:user_self_register])

    if @questionable.save
      redirect_to admin_user_url(@questionable.user), notice: "User was successfully created."
    else
      @user = User::SelfRegister.new(params[:user_self_register])
      render action: "new"
    end
  end

  private

    def service
      Service.find_using_slug!(params[:service_id])
    end
end
