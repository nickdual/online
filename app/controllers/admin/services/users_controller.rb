class Admin::Services::UsersController < Admin::BaseController

  # Breadcrumbs
  before_filter do
    @service = Service.find_using_slug params[:service_id]
    add_breadcrumb 'Services', admin_services_path
    add_breadcrumb @service.name, admin_service_path(@service)
    add_breadcrumb 'Users', admin_service_users_path
  end

  def index
    @employments = @service.employments.active
  end

end
