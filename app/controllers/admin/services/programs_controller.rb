class Admin::Services::ProgramsController < Admin::BaseController

  before_filter :service, :intros_and_closers
  before_filter :service_program, :only => [:edit, :update]

  # Breadcrumbs
  before_filter do
    service = Service.find_using_slug params[:service_id]
    add_breadcrumb 'Services', admin_services_path
    add_breadcrumb service.name, admin_service_path(service)
    add_breadcrumb 'Programs', admin_service_programs_path(service)
  end

  def index
    @service_programs = @service.service_programs.default_order
  end

  def update
    @service_program.update_attributes(params[:service_program])
    flash[:notice] = "Successfully updated program"
    redirect_to admin_service_programs_path(@service)
  end

  private

    def service
      @service = Service.find_using_slug!(params[:service_id])
    end

    def service_program
      @program = Program.find_using_slug!(params[:id])
      @service_program = @service.service_programs.where("program_id = ?", @program.id).first
      @program = @service_program.program
    end

end
