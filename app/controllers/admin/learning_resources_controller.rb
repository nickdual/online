class Admin::LearningResourcesController < Admin::BaseController

  inherit_resources
  defaults :route_prefix => 'admin', :finder => :find_using_slug!
  belongs_to :program, :finder => :find_using_slug!

  # Breadcrumbs
  before_filter do
    @program = Program.find_using_slug!(params[:program_id])
    @learning_resource = @program.learning_resources.find_using_slug!(params[:id]) if params[:id].present?
    add_breadcrumb 'Programs', admin_programs_path
    add_breadcrumb @program.title, admin_program_path(@program)
    add_breadcrumb 'Learning Resources', admin_program_path(@program)
    add_breadcrumb @learning_resource.name, admin_program_path(@program) if @learning_resource
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def create
    create! { admin_program_path(@program) }
  end

  def update
    update! { admin_program_path(@program) }
  end

  def destroy
    destroy! { admin_program_path(@program) }
  end

  def download
    send_file @learning_resource.file, :filename => @learning_resource.resource_name, :disposition => "attachment"
  end

  protected

  def create_resource(object)
    object.created_by, object.updated_by = current_admin, current_admin
    super
  end

end
