class Admin::Groups::ProgramsController < Admin::BaseController

  before_filter :group, :intros_and_closers
  before_filter :group_program, :only => [:edit, :update]

  # Breadcrumbs
  before_filter do
    group = Group.find_using_slug params[:group_id]
    program = Program.find_using_slug params[:id]
    add_breadcrumb 'Groups', admin_groups_path
    add_breadcrumb group.name, admin_group_path(group)
    add_breadcrumb 'Programs', admin_group_programs_path(group)
    add_breadcrumb program.title, admin_group_program_path(group, program) if program
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def index
    @group_programs = @group.group_programs.default_order
  end

  def update
    @group_program.update_attributes(params[:group_program])
    flash[:notice] = "Successfully updated program"
    redirect_to admin_group_programs_path(@group)
  end

  private

    def group
      @group = Group.find_using_slug!(params[:group_id])
    end

    def group_program
      @program = Program.find_using_slug!(params[:id])
      @group_program = @group.group_programs.where("program_id = ?", @program.id).first
      @program = @group_program.program
    end
end
