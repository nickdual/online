class Admin::Groups::UsersController < Admin::BaseController

  # This is for creating new users via coordinators
  inherit_resources
  defaults :route_prefix => 'admin'
  belongs_to :group, :finder => :find_using_slug!

  authorize_resource

  # Breadcrumbs
  before_filter do
    @group = Group.find_using_slug! params[:group_id]
    add_breadcrumb 'Groups', admin_groups_path
    add_breadcrumb @group.name, admin_group_path(@group)
    add_breadcrumb 'Group Coordinators', admin_group_users_path(@group)
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def create
    @user = @group.users.new(params[:user])
    @user.coordinators.build(group: @group)
    @user.password, @user.password_confirmation = SecureRandom.hex(3)
    @user.activation_code_set_at = Time.zone.now

    if @user.save
      redirect_to admin_group_path(@group), notice: "Coordinator was successfully invited."
      UserNotifier.new_user_has_joined_group(@user.coordinators.first, @user.password).deliver
    else
      render action: "new"
    end
  end

end
