class Admin::Groups::CoordinatorsController < Admin::BaseController

  inherit_resources
  defaults :route_prefix => 'admin'
  belongs_to :group, :finder => :find_using_slug!

  authorize_resource

  def create
    @group = Group.find_using_slug!(params[:group_id])
    @coordinator = @group.coordinators.new(params[:coordinator])

    if @coordinator.save
      redirect_to admin_group_path(@group), notice: "Coordinator was successfully invited."
      UserNotifier.existing_user_has_joined_group(@coordinator).deliver
    else
      render action: "new"
    end
  end

  def destroy
    destroy!(:notice => "Coordinator was successfully removed.") { admin_group_path(@group) }
  end
end