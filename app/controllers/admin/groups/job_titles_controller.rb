class Admin::Groups::JobTitlesController < Admin::BaseController

  before_filter do
    find_group
    find_job_title if params[:id].present?
    add_breadcrumb 'Groups', admin_groups_path
    add_breadcrumb @group, admin_group_path(@group)
    add_breadcrumb 'Job Titles', admin_group_job_titles_path(@group)
    add_breadcrumb @job_title, admin_group_job_title_path(@group, @job_title) if @job_title.present?
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  authorize_resource :group
  authorize_resource :job_title, through: :group, class: 'Group::JobTitle'

  def index
    find_job_titles
  end

  def show
    @employments = @job_title.employments
  end

  def new
    @job_title = @group.job_titles.build
  end

  def create
    @job_title = @group.job_titles.build params[:group_job_title]
    if @job_title.save
      redirect_to redirect_path, notice: "Job Title was successfully created"
    else
      render :new
    end
  end

  def destroy
    @job_title.destroy
    redirect_to redirect_path, notice: "Job Title was successfully deleted"
  end

  def edit
  end

  def update
    if @job_title.update_attributes params[:group_job_title]
      redirect_to redirect_path, notice: "Job Title was successfully updated"
    else
      render :edit
    end
  end

  private

  def find_group
    @group ||= Group.find_using_slug! params[:group_id]
  end

  def find_job_title
    find_group
    @job_title ||= @group.job_titles.find params[:id] if params[:id].present?
  end

  def find_job_titles
    find_group
    @job_titles ||= @group.job_titles.default_order
  end

  def redirect_path
    admin_group_job_titles_path(@group)
  end
end
