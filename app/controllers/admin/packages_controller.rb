class Admin::PackagesController < Admin::BaseController

  inherit_resources
  defaults :route_prefix => 'admin'
  before_filter :set_update_params, only: [:update]
  before_filter :programs, :except => :index

  # Breadcrumbs
  before_filter do
    package = Package.find_using_slug params[:id]
    add_breadcrumb 'Packages', admin_packages_path
    add_breadcrumb package.name, admin_package_path(package) if package
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def index
    if params[:search]
      @packages = Package.search(params[:search])
      flash[:notice] = "#{@packages.total_entries} record found"
    else
      @packages = Package.default_order
    end
  end

  def create
    create! { admin_packages_path }
  end

  def update
    resource.updated_by = current_admin
    if resource.update_attributes(params[:package])
      redirect_to [:admin, resource], notice: "Package was successfully updated."
    else
      render action: "edit"
    end
  end

  protected

    def resource
      @package ||= Package.find_using_slug!(params[:id])
    end

    def create_resource(object)
      object.created_by, object.updated_by = current_admin, current_admin
      super
    end

    def update_resource(object, attributes)
      object.updated_by = current_admin
      super
    end

    def programs
      @programs = Program.programs.default_order
    end

    # TODO: This should probably be at model level
    def set_update_params
      if params[:package][:package_programs_attributes]
        params[:package][:package_programs_attributes].each do |program|
          if program.last["selected"] != '1'
            program.last["_destroy"] = true
          end
        end
      end
    end
end
