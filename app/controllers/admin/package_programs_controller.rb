class Admin::PackageProgramsController < Admin::BaseController

  inherit_resources
  defaults :route_prefix => 'admin'
  belongs_to :package, :finder => :find_using_slug!
  actions :edit, :update
  before_filter :intros_and_closers

  def update
    update! { admin_package_path(@package) }
  end

end