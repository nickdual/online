class Admin::ProfilesController < Admin::BaseController
  before_filter :roles

  # Breadcrumbs
  before_filter do
    profile = current_admin
    add_breadcrumb profile.name, edit_admin_profile_path if profile
  end

  def edit
    @profile = current_admin
    respond_with :admin, @profile
  end

  def update
    @profile = current_admin

    # Check to see if user is trying to edit their password
    if params[:admin][:password].blank? && params[:admin][:password_confirmation].blank?
      # Remove the password params so it doesn't try to update it
      params[:admin].delete :password
      params[:admin].delete :password_confirmation
    else
      # Any other case should be caught by Devise validations
    end

    @profile.update_attributes params[:admin]
    # Sign the user back in and bypass another password check
    sign_in @profile, bypass: true

    respond_with :admin, @profile
  end

  protected

  def roles
    @roles = Admin.role_options
  end
end
