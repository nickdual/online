class Admin::UsersController < Admin::BaseController

  authorize_resource

  # Breadcrumbs
  before_filter do
    find_user
    add_breadcrumb 'Users', admin_users_path
    add_breadcrumb @user.name, admin_user_path(@user) if @user
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end

  def index
    if params[:q].blank?
      @users = User
    else
      @users = User.basic_search(params[:q])
    end
    @users = @users.default_order.paginate(page: params[:page])
  end

  def edit
  end

  def update
    if @user.update_attributes params[:user]
      redirect_to admin_user_path(@user), notice: 'User was successfully updated.'
    else
      render :new
    end
  end

  def destroy
    @user.destroy
    redirect_to admin_users_path, notice: 'User was successfully destroyed.'
  end

  private

  def find_user
    if params[:id]
      @user = User.includes(employments: {service: :group}).find_using_slug!(params[:id])
    end
  end
end
