class WelcomeController < ApplicationController
  before_filter :authenticate_user!

  def index
    @current_user_in_json = UserSerializer.new(current_user).as_json(root: false).collect { |key, value| "#{key}: \"#{value}\""}.join(", ")
    # Just rendering the template page :-)
  end
end