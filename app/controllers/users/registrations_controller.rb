class Users::RegistrationsController < ::Devise::RegistrationsController
  layout "public"

  protected

    def build_resource(hash = {})
      self.resource = User::SelfRegister.new(params[:user])
    end

end