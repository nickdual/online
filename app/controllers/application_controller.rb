class ApplicationController < ActionController::Base
  # protect_from_forgery
  layout :layout_by_resource

  def after_sign_in_path_for(resource)
    if resource.is_a?(User)
      stored_location_for(resource) || root_path
    else
      stored_location_for(resource) || admin_path
    end
  end

  protected

  def layout_by_resource
    devise_controller? ? "admin_public" : "application"
  end

end