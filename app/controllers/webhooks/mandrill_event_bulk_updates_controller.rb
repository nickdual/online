class Webhooks::MandrillEventBulkUpdatesController < ApplicationController
  def create
    if params[:mandrill_events]
      MandrillEventBulkUpdateWorker.perform_async(params[:mandrill_events])
    end

    render nothing: true
  end
end
