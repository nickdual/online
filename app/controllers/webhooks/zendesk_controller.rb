class Webhooks::ZendeskController < ApplicationController
  include Zendesk::RemoteAuthHelper
  before_filter :authenticate_user!

  def authorize
    redirect_to zendesk_remote_auth_url(current_user)
  end
end