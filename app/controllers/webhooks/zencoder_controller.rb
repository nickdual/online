class Webhooks::ZencoderController < ApplicationController
  
  before_filter :program
  
  def update
    if params[:output][:state] == "finished"
      attributes = {
        program: @program,
        description: params[:output][:label],
        zencoder_id: params[:output][:id],
        file_uid: params[:output][:url].match(/(\/\d{4}.*)/)[0],
        file_name: params[:output][:url].match(/([^\/]*)$/)[0]
      }
      
      Video.delay.create!(attributes)
      render(:text => "Thanks Zencoder!")
    else
      render(:text => "We only want finished jobs!")
    end
  end
  
  private

    def program
      @program = Program.find(params[:id])
    end
end
