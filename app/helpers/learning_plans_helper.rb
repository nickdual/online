module LearningPlansHelper
  
  def learning_plan_words_distance(due_at)
    due_at > Time.zone.now.to_date ? "from now" : "ago"
  end
  
  def success_or_fail_class(user_learning_plan, program)
    watched_program?(user_learning_plan, program) ? "success" : "fail"
  end
  
  def success_or_fail_marker(user_learning_plan, program)
    watched_program?(user_learning_plan, program) ? "&#10003;" : "X"
  end
  
  def watched_program?(user_learning_plan, program)
    user_learning_plan.programs_watched.map(&:id).include?(program.id)
  end
  
end