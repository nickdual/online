module Admin::AdminsHelper

  def last_logged_in(user)
    if user.current_sign_in_at
      result = ["#{user.name},"]
      result << distance_of_time_in_words_to_now(user.current_sign_in_at)
      result << 'ago'
      result.join(' ')
    else
      "#{user.name} has not signed in before"
    end
  end

  def display_image_fieldset(question)
    if question.new_record? || question.reference_image_uid.blank?
      "display: none;"
    end
  end

  # Package / Programs
  def available_programs(all_programs, selected_program_ids)
    all_programs.reject { |program| selected_program_ids.include?(program.id) } # This needs to be offloaded to sql
  end

  def available_package_programs(selected_programs)
    selected_programs.reject { |program| program.new_record? }
  end

  def build_or_load_package_program(package, program)
    if package.program_ids.include?(program.id)
      package.package_programs.where("program_id = ?", program.id).first
    else
      package.package_programs.new(:program => program)
    end
  end

  def build_or_load_package_package_programs(package_program, program)
    if package_program.playlist_program_ids.include?(program.id)
      package_program.program_lists.where("program_id = ?", program.id).first
    else
      package_program.program_lists.new(:program => program)
    end
  end

  # Groups / Programs
  def build_or_load_group_program_group_program_lists(group_program, program)
    if group_program.program_ids.include?(program.id)
      group_program.program_lists.where("program_id = ?", program.id).first
    else
      group_program.program_lists.new(:program => program)
    end
  end

  def available_group_programs(all_programs, selected_program_ids)
    all_programs.reject { |program| selected_program_ids.include?(program.id) } # This needs to be offloaded to sql
  end

  def available_playlist_programs(selected_programs)
    selected_programs.reject { |program| program.new_record? }
  end

  # Services / Programs
  def build_or_load_service_program_service_program_lists(service_program, program)
    if service_program.program_ids.include?(program.id)
      service_program.program_lists.where("program_id = ?", program.id).first
    else
      service_program.program_lists.new(:program => program, :service_id => service_program.service_id, :service_program_id => service_program.program_id)
    end
  end

end
