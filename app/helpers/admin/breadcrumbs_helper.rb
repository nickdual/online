module Admin::BreadcrumbsHelper
  def add_breadcrumbs_for_assessment(assessment)
    add_breadcrumb assessment.name, url_for([:admin, assessment.program, assessment])
  end

  def add_breadcrumbs_for_program(program)
    add_breadcrumb Program.model_name.human.pluralize, url_for([:admin, Program.model_name.plural])
    add_breadcrumb program.title, url_for([:admin, program])
  end

  def add_breadcrumbs_for_resource(options = {})
    breadcrumb_title = options[:breadcrumb_title] || :name
    collection_path = options[:collection_path] || {action: 'index'}

    add_breadcrumb resource_class.model_name.human.pluralize, collection_path
    add_breadcrumb resource.send(breadcrumb_title), action: 'show' if params[:id]
    add_breadcrumb action_name.titleize, '' unless ['index', 'show'].include? action_name
  end
end
