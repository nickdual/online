module ApplicationHelper
  def number_to_percentage_or_na(number)
    number.to_f.nan? ? 'N/A' : number_to_percentage(number, precision: 0)
  end

  def render_collection_as_csv(collection)
    require 'csv'
    CSV.generate do |csv|
      collection.as_json.tap do |json|
        csv << json.first.keys
        json.map(&:values).each do |row|
          csv << row
        end
      end
    end
  end

  # Note this is statically defined in app/assets/templates/settings/_profile.jst.hamlc as well
  def timezone_list
    [
      ["(GMT+09:30) Adelaide", "Adelaide"],
      ["(GMT+10:00) Brisbane", "Brisbane"],
      ["(GMT+10:00) Canberra", "Canberra"],
      ["(GMT+09:30) Darwin", "Darwin"],
      ["(GMT+10:00) Melbourne", "Melbourne"],
      ["(GMT+08:00) Perth", "Perth"],
      ["(GMT+10:00) Sydney", "Sydney"],
      ["(GMT+00:00) Casablanca", "Casablanca"],
      ["(GMT+00:00) Dublin", "Dublin"],
      ["(GMT+00:00) Edinburgh", "Edinburgh"],
      ["(GMT+00:00) Lisbon", "Lisbon"],
      ["(GMT+00:00) London", "London"],
      ["(GMT+00:00) Monrovia", "Monrovia"]
    ]
  end

  def gender_list
    ["male", "female"]
  end

  def user_import_summary(user)
    [user.name, user.email].join(", ")
  end

  def services_by_group(group, services)
    services.reject { |f| f.group_id != group.id }
  end

  def services_with_no_group(services)
    services.reject { |f| f.group_id != nil }
  end

  def video_id_helper
    @id ||= 0
    @id += 1
    ["video", @id].join("")
  end

  def partial_counter
    @counter ||= 0
    @counter += 1
    @counter
  end

  def video_cdn_url(video)
    if ENV['STREAMING_URL'].blank?
      video.url
    else
      ENV['STREAMING_URL'] + video.file_uid
    end
  end
end
