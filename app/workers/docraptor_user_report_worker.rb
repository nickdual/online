class DocraptorUserReportWorker
  include Sidekiq::Worker
  sidekiq_options :retry => 5 # Only five retries and then we're outta here!

  def perform(klass, instance_id, template, user_id, coordinator)
    locals = {}
    case klass.to_s
      when 'Service'
        service = Service.where(id: instance_id).first
        locals[:service] = service
      when 'Group'
        group = Group.where(id: instance_id).first
        service_ids = group.services.map(&:id)
        services = Service.where(id: service_ids).active.default_order
        locals[:group] = group
        locals[:services] = services
    end

    report = ActionController::Base.new().render_to_string(:template => template, :locals => locals)

    options = {
      document_type:  :xls,
      document_name: klass.to_s.downcase,
      document_content: report
    }

    response = DocRaptor.create(options)

    if response.code == 200
      attachment_filename = "#{options[:document_name]}.#{options[:document_type]}"
      attachment_content = response.body
      user = User.where(id: user_id).first
      CoordinatorNotifier.delay.service_information_report(user, coordinator, attachment_filename, attachment_content)
    else
      puts "[FAILURE] DocraptorUserReportWorker DocRaptor responce code: #{response.code}"
    end

  end
end