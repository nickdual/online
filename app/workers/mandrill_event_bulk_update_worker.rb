class MandrillEventBulkUpdateWorker
  include Sidekiq::Worker
  sidekiq_options :retry => 5 # Only five retries and then we're outta here!

  def perform(events)
    JSON.parse(events).each do |event_json|
      event_json = HashWithIndifferentAccess.new(event_json)
      event = MandrillEvent.new(
        occurred_at: Time.at(event_json[:msg][:ts]),
        status: event_json[:msg][:state],
        bounce_description: event_json[:msg][:bounce_description],
        email: event_json[:msg][:email])
      event.update_user!
    end
  end
end