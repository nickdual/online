class EmploymentLearningPlanProgramSerializer < ActiveModel::Serializer
  attributes :id, :title, :program_id, :viewed_program, :viewed_at_date, :viewed_at_time,
    :thumb_200_200_url, :require_essentials, :require_extension, :require_evidence,
    :completed_essentials, :completed_extension, :completed_evidence,
    :overdue_essentials, :overdue_extension, :overdue_evidence, :duration, :status,
    :is_essential_assessment_active, :is_essential_assessment_unsuccessful

end
