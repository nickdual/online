class CoordinatorSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :group_name, :name
  cache

  def cache_key
    object.cache_key
  end
end
