class VideoSerializer < ActiveModel::Serializer
  attributes :url, :hd

  cache

  def cache_key
    object.cache_key
  end

end
