class GroupSerializer < ActiveModel::Serializer
  attributes  :id,
              :name,
              :address,
              :services_count,
              :employments_count

  cache

  def cache_key
    object.cache_key
  end
end
