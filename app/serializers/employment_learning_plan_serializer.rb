class EmploymentLearningPlanSerializer < ActiveModel::Serializer
  attributes  :id, :formatted_due_at, :employment_id, :service_id,
              :learning_plan_id, :days_remaining, :days_till_active,
              :status, :name, :employment_name, :first_name,
              :description, :formatted_completed_at, :formatted_active_at, :service_name,
              :programs_count, :programs_completed_count, :programs_pending_count,
              :assessment_percentage, :viewed_percentage, :viewed_count, :assessment_count

  cache

  def cache_key
    object.cache_key
  end

end