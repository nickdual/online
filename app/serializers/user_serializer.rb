class UserSerializer < ActiveModel::Serializer
  attributes  :id,
              :first_name,
              :last_name,
              :email,
              :name,
              :is_coordinator,
              :formatted_born_at,
              :gender,
              :time_zone,
              :current_sign_in_at,
              :activation_code_set_at,
              :assessments_completed_count,
              :plans_overdue_count,
              :programs_viewed_count,
              :programs_awaiting_completion_count,
              :learning_plan_progress,
              :active_employments_count

  cache

  def cache_key
    object.cache_key
  end
end
