class LearningResourceSerializer < ActiveModel::Serializer
  attributes  :id, :name, :url, :program_id

  cache

  def cache_key
    object.cache_key
  end
end