class ServiceSerializer < ActiveModel::Serializer
  attributes  :id,
              :name,
              :group_id,
              :address,
              :activation_token,
              :programs_count,
              :plans_count,
              :employments_count

  cache

  def cache_key
    object.cache_key
  end
end