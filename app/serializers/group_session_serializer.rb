class GroupSessionSerializer < ActiveModel::Serializer
  attributes  :id,
              :medium,
              :viewed_at

  has_many :employments, key: :people
  has_one :program

  cache

  def cache_key
    object.cache_key
  end
end