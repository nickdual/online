class LearningPlanSerializer < ActiveModel::Serializer
  attributes  :id,
              :name,
              :days_till_due,
              :description,
              :service_id,
              :formatted_active_at,
              :active_at,
              :active,
              :days_till_active,
              :programs_count,
              :employments_count,
              :overdue_count,
              :active_count,
              :completed_count,
              :pending_count,
              :percentage_overdue,
              :percentage_active,
              :percentage_completed,
              :percentage_pending

  cache

  def cache_key
    object.cache_key
  end

end