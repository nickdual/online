class EmploymentSerializer < ActiveModel::Serializer
  attributes  :id, :user_id, :role, :employee_number,
              :formatted_started_at, :formatted_ended_at, :label,
              :job_title_name, :job_title_id,
              :name, :first_name, :email,
              :service_id, :service_name,
              :email_verified_at, :last_email_status, :last_email_bounce_description, :welcome_email_resent_at

  cache

  def cache_key
    object.cache_key
  end

end