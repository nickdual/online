class Program::Assessment::MatchingTaskItemSerializer < ActiveModel::Serializer
  attributes :id, :text, :answer
end