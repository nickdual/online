class Program::Assessment::MultipleChoiceAnswerSerializer < ActiveModel::Serializer
  attributes :id, :answer, :correct
end