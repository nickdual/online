class Program::Assessment::TrueOrFalseQuestionSerializer < ActiveModel::Serializer
  attributes :id, :question, :answer, :reference_image_url, :image_caption, :question_id

end