class Program::Assessment::SequentialAnswerItemSerializer < ActiveModel::Serializer
  attributes :id, :text, :reference_image_url, :correct_position
end