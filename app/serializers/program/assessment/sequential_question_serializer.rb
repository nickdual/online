class Program::Assessment::SequentialQuestionSerializer < ActiveModel::Serializer
  attributes :id, :question, :question_id
  has_many :answer_items, :randomised_answer_items
end
