class Program::Assessment::MatchingTaskQuestionSerializer < ActiveModel::Serializer
  attributes :id, :directions, :question_id
  has_many :items, :randomised_items
end
