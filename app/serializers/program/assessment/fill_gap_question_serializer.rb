class Program::Assessment::FillGapQuestionSerializer < ActiveModel::Serializer
  attributes :id, :question, :reference_image_url, :question_id
  has_many :answers, :randomised_answers
end