class Program::Assessment::MultipleChoiceQuestionSerializer < ActiveModel::Serializer
  attributes :id, :question, :reference_image_url, :image_caption, :question_id
  has_many :answers, :randomised_answers
end
