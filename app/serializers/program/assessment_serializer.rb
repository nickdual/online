class Program::AssessmentSerializer < ActiveModel::Serializer
  attributes  :id, :assessment_type, :name, :time_allowed, :pass_mark, :description

  cache

  def cache_key
    object.cache_key
  end
end