class LearningRecordSerializer < ActiveModel::Serializer
  attributes :id, :title, :thumb_200_200_url, :thumb_60_60_url, :viewed_at, :downloaded

  cache

  def cache_key
    object.cache_key
  end
end
