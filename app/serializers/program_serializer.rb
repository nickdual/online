class ProgramSerializer < ActiveModel::Serializer
  attributes  :id,
              :code,
              :title,
              :description,
              :duration,
              :image_url,
              :thumb_200_200_url,
              :thumb_60_60_url,
              :video_width,
              :video_height,
              :accreditation,
              :classification,
              :to_label,
              :program_ids,
              :assessments_count,
              :active_assessments_count

  has_one :category
  has_one :program_type

  cache

  def cache_key
    object.cache_key
  end
end
