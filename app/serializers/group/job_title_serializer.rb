class Group::JobTitleSerializer < ActiveModel::Serializer
  attributes  :id,
              :name,
              :truncated_name

  cache

  def cache_key
    object.cache_key
  end
end
